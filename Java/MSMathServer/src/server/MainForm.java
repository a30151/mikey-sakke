package server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

public class MainForm extends JFrame {

	private static final long serialVersionUID = 5022210823534749075L;
	private static final int HTTPS_PORT = 7072;
	private static final int HTTP_PORT = 7071;

	/** The server that is running  */
	private HttpsServer m_SecureServer = null;
	private HttpServer m_Server = null;
	
	/** List of log messages that are displayed */
	private StringBuilder m_LogBuilder = new StringBuilder();

	
	/** References to the UI elements that get updated */
	private JTextArea m_LogText;
	
	/** Date formatter for the log message viewer */
	private DateFormat m_DateFormat = new SimpleDateFormat("HH:mm:ss"); 

	/**
	 * Constructor, builds the form elements
	 * */
	public MainForm() {
	
		this.setSize(520, 570);		
		this.setLayout(null);
		
		this.setTitle("MS Math Server");
		
		// Add a log window
		JLabel logLabel = new JLabel();
		logLabel.setLocation(10, 10);
		logLabel.setSize(380, 20);
		logLabel.setText("log");
		this.getContentPane().add(logLabel);
		m_LogText = new JTextArea();
		m_LogText.setLocation(0, 0);
		m_LogText.setSize(480, 280);
		m_LogText.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(m_LogText); 
		scrollPane.setLocation(10, 30);
		scrollPane.setSize(480, 450);
		this.getContentPane().add(scrollPane);
		
		// Add a button to start the server
		JButton startButton = new JButton();
		startButton.setLocation(10, 490);
		startButton.setSize(100, 25);
		startButton.setText("Start");
		startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				StartServers();
			}
		});
		this.getContentPane().add(startButton);
		
		// Add a button to stop the server
		JButton stopButton = new JButton();
		stopButton.setLocation(120, 490);
		stopButton.setSize(100, 25);
		stopButton.setText("Stop");
		stopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				StopServer();
			}
		});
		this.getContentPane().add(stopButton);
		
		// Add a button to clear all data from the server
		JButton clearButton = new JButton();
		clearButton.setLocation(230, 490);
		clearButton.setSize(100, 25);
		clearButton.setText("Clear");
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ClearAllData();
			}
		});
		this.getContentPane().add(clearButton);
		
		// Stop the server if the window is closed
		this.addWindowListener(new WindowListener() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				StopServer();
			}
			
			public void windowOpened(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowDeactivated(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			public void windowActivated(WindowEvent e) {}
		});
				 
				 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		StartServers();
	}
	
	
	
	/**
	 * Adds a new log and outputs the log list to the screen
	 * */	
	public void addLog(String message) {
		
		synchronized (m_LogBuilder) {

			m_LogBuilder.append(m_DateFormat.format(new Date()));
			m_LogBuilder.append(" : ");
			m_LogBuilder.append(message);
			m_LogBuilder.append("\r\n");
			
			m_LogText.setText(m_LogBuilder.toString());		
			m_LogText.setCaretPosition(m_LogText.getText().length());
		}
	}
	
	/**
	 * Starts the HTTP and HTTPs servers
	 * */
	private void StartServers(){
		if (m_SecureServer != null && m_Server !=null){
			addLog("Server already running");
		}
		else{
			StartSecureServer();
			StartServer();
		}
	}
	
	private void StartSecureServer() {
		
		if ( m_SecureServer != null ) {
			addLog("Server already running");		
		} else {
			try
		    {
				// setup the socket address
	            InetSocketAddress address = new InetSocketAddress ( InetAddress.getLocalHost (), HTTPS_PORT );

	            // initialise the HTTPS server
	            m_SecureServer = HttpsServer.create ( address, 0 );
	           
	            SSLContext sslContext = SSLContext.getInstance ( "TLS" );

	            // initialise the keystore
	            char[] password = "mathserver".toCharArray ();
	            KeyStore ks = KeyStore.getInstance ( "JKS" );
	            FileInputStream fis = new FileInputStream ( "msmathserver.keystore" );
	            ks.load ( fis, password );

	            // setup the key manager factory
	            KeyManagerFactory kmf = KeyManagerFactory.getInstance ( "SunX509" );
	            kmf.init ( ks, password );

	            // setup the trust manager factory
	            TrustManagerFactory tmf = TrustManagerFactory.getInstance ( "SunX509" );
	            tmf.init ( ks );

	            // setup the HTTPS context and parameters
	            sslContext.init ( kmf.getKeyManagers (), tmf.getTrustManagers (), null );

	            m_SecureServer.setHttpsConfigurator ( new HttpsConfigurator( sslContext )
	            {
	                public void configure ( HttpsParameters params )
	                {
	                    try
	                    {
	                        // initialise the SSL context
	                        SSLContext c = SSLContext.getDefault ();
	                        SSLEngine engine = c.createSSLEngine ();
	                        params.setNeedClientAuth ( false );
	                        params.setCipherSuites ( engine.getEnabledCipherSuites () );
	                        params.setProtocols ( engine.getEnabledProtocols () );

	                        // get the default parameters
	                        SSLParameters defaultSSLParameters = c.getDefaultSSLParameters ();
	                        params.setSSLParameters ( defaultSSLParameters );
	                    }
	                    catch ( Exception ex )
	                    {
	                        addLog("Failed to create HTTPS port" );
	                    }
	                }
	            } );
	          
				 m_SecureServer.createContext("/", new RequestHandler(this) );  
	            
	        }
	        catch ( Exception ex )
	        {
	        	addLog("StartServer Error: " + ex.getMessage());
	        }
			
	        m_SecureServer.start();
	        addLog("Secure server started.");
		}
	}
	
	private void StartServer() {
		if ( m_Server != null ) {
			addLog("Server already running");		
		} else {
			try
		    {				
	             //setup the socket address
	            InetSocketAddress address = new InetSocketAddress ( InetAddress.getLocalHost (), HTTP_PORT );
	            
	             //initialise the HTTP server
	           m_Server = HttpServer.create( address, 0 );
	            
	           m_Server.createContext("/", new RequestHandler(this) );  
	        }
	        catch ( Exception ex )
	        {
	        	addLog("StartServer Error: " + ex.getMessage());
	        }
			
	        m_Server.start();
	        addLog("Server started.");
		}
	}
	
	/**
	 * Stops the HTTP and HTTPS servers
	 * */
	private void StopServer() {
		if ( m_SecureServer != null && m_Server != null) {
			m_SecureServer.stop(0);
			m_Server.stop(0);
			m_SecureServer = null;
			m_Server = null;
			
			addLog("Servers stopped.");		
		}
	}
	
	/**
	 * Clears all data off of the server
	 * */
	private void ClearAllData() {
		synchronized (m_LogBuilder) {
			m_LogBuilder = new StringBuilder();
			addLog("Server data cleared.");
		}
	}
}
