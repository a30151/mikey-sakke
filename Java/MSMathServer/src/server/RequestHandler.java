package server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Random;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.Sakke;
import mikeysakke.utils.OctetString;
import mikeysakke.utils.RandomGenerator;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class RequestHandler implements HttpHandler {

	private MainForm m_MainForm;

	public RequestHandler(MainForm form) {
		m_MainForm = form;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String requestMethod = exchange.getRequestMethod();
		if (requestMethod.equalsIgnoreCase("GET")) {
			// find what method has been called
			URI requestURI = exchange.getRequestURI();
			String file = requestURI.getPath().substring(1);
			if (file.equalsIgnoreCase("VALIDATE")) {
				validateKeys(exchange);
			} else if (file.equalsIgnoreCase("GENERATE")) {
				generateSedSsv(exchange);
			} else if (file.equalsIgnoreCase("SIGN")) {
				signMessage(exchange);
			} else if (file.equalsIgnoreCase("VERIFY")) {
				verifySignature(exchange);
			} else if (file.equalsIgnoreCase("EXTRACT")) {
				extractSsv(exchange);
			} else {
				Unknown(exchange);
			}
		}
	}
	
	/**
	 * Random Generator class used for generating an SSV
	 */
	class SsvRandomGenerator implements RandomGenerator
	{
		@Override
		public OctetString generate(int size) {
			byte[] buffer = new byte[size];
			
			Random rand = new Random();			
			rand.nextBytes(buffer);
			
			OctetString os = new OctetString(buffer);
			return os;
		}
	}
	
	/**
	 * "Random" Generator used for SSV/SED generate testing purposes
	 */
	class TestSsvRandomGenerator implements RandomGenerator
	{
		@Override
		public OctetString generate(int size) {
			return OctetString.fromHex("123456789ABCDEF0123456789ABCDEF0");
		}
	}
	
	/**
	 * "Random" Generator used for Sign testing purposes
	 */
	class TestSignRandomGenerator implements RandomGenerator
    {
        @Override
        public OctetString generate(final int n)
        {
            return OctetString.fromHex("34567");
        }
    }
	
	/**
	 * Call on MSLibrary methods for validating keys received from KMS
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void validateKeys(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("VALIDATE: message received: "
				+ exchange.getRequestURI().getRawQuery());

		JSONObject retVal = new JSONObject();

		HashMap<String, String> params = parseQuery(exchange.getRequestURI()
				.getRawQuery());
		try {
			if (params.containsKey("id") && params.containsKey("pvt")
					&& (params.containsKey("kpak"))
					&& (params.containsKey("ssk")) && (params.containsKey("z"))
					&& (params.containsKey("rsk"))
					&& (params.containsKey("parameterSet"))) {

				int paramSet = Integer.parseInt(params.get("parameterSet"));
				//String id = params.get("id").replace("%00", "\0");
				
				boolean validateReceiverKey;
				boolean validateSigningKey;
				

				OctetString hs = new OctetString();
				validateSigningKey = Eccsi.validateSigningKeys(
						OctetString.fromAscii(params.get("id")),
						OctetString.fromHex(params.get("pvt")),
						OctetString.fromHex(params.get("kpak")),
						OctetString.fromHex(params.get("ssk")), hs);
			
				validateReceiverKey = Sakke.validateReceiverSecretKey(
							OctetString.fromAscii(params.get("id")),
							OctetString.fromHex(params.get("z")),
							OctetString.fromHex(params.get("rsk")), paramSet);

				retVal.put("validated", validateSigningKey && validateReceiverKey);
				retVal.put("hs", hs.toHex());
				
				m_MainForm.addLog("VALIDATE complete");
			} else {
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}

		Headers responseHeaders = exchange.getResponseHeaders();
		responseHeaders.set("Content-Type", "text/plain");
		exchange.sendResponseHeaders(200, 0);

		OutputStream responseBody = exchange.getResponseBody();
		responseBody.write(retVal.toString().getBytes());
		responseBody.close();
		exchange.close();
	}

	/**
	 * Call on MSLibrary method for generating a SED and SSV
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void generateSedSsv(HttpExchange exchange) throws IOException{
		m_MainForm.addLog("GENERATE: message received: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		try {
			if (params.containsKey("targetId") && params.containsKey("parameterSet")
					&&(params.containsKey("z"))){
				
				RandomGenerator r;
				if(params.containsKey("test") && 
						Boolean.parseBoolean(params.get("test"))==true){
					r = new TestSsvRandomGenerator();
				}
				else{
					r = new SsvRandomGenerator();
				}
				
				int paramSet = Integer.parseInt(params.get("parameterSet"));
				OctetString sed = new OctetString();
				
				OctetString ssv = 
					Sakke.generateSharedSecretAndSED(
						sed, 
						OctetString.fromAscii(params.get("targetId")), 
						paramSet, 
						OctetString.fromHex(params.get("z")), 
						r);
				retVal.put("ssv", ssv.toHex());
				retVal.put("sed", sed.toHex());
				
				m_MainForm.addLog("GENERATE complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();	
	}

	/**
	 * Call on MSLibrary for signing a message
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void signMessage(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("SIGN: message received: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		try {
			if (params.containsKey("message") && params.containsKey("pvt")
					&&(params.containsKey("ssk")) && (params.containsKey("hs"))
					&& (!params.get("hs").isEmpty())){

				RandomGenerator r;
				if(params.containsKey("test") && 
						Boolean.parseBoolean(params.get("test"))==true){
					r = new TestSignRandomGenerator();
				}
				else{
					r = new SsvRandomGenerator();
				}
				OctetString message;
				if(params.containsKey("messageFormat") && params.get("messageFormat").toLowerCase().equals("ascii")){
					message = OctetString.fromAscii(params.get("message"));
				}
				else{
					message = OctetString.fromHex(params.get("message"));
				}
				
				OctetString signature = Eccsi.sign(
						message,
						OctetString.fromHex(params.get("pvt")), 
						OctetString.fromHex(params.get("ssk")), 
						OctetString.fromHex(params.get("hs")), 
						r);
				
				retVal.put("signature", signature.toHex());
				
				m_MainForm.addLog("SIGN complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();	
	}

	/**
	 * Call on MSLibrary method for verifying signature against a message
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void verifySignature(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("VERIFY: message received: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		
		try {
			if (params.containsKey("message") && params.containsKey("signature")
					&&(params.containsKey("signerId")) && (params.containsKey("kpak"))){
				
				OctetString message;
				if(params.containsKey("messageFormat") && params.get("messageFormat").toLowerCase().equals("ascii")){
					message = OctetString.fromAscii(params.get("message"));
				}
				else{
					message = OctetString.fromHex(params.get("message"));
				}
				
				boolean verify = Eccsi.verify(
						message, 
						OctetString.fromHex(params.get("signature")), 
						OctetString.fromAscii(params.get("signerId")), 
						OctetString.fromHex(params.get("kpak")));
				
				retVal.put("verified", verify);
				
				m_MainForm.addLog("VERIFY complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();
		
	}

	/**
	 * Call on MSLibrary method for extracting a shared secret value
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void extractSsv(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("EXTRACT: message received: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		try {
			if (params.containsKey("sed") && params.containsKey("id")
					&&(params.containsKey("parameterSet")) && (params.containsKey("rsk"))
					&&(params.containsKey("z"))){
				
				int paramSet = Integer.parseInt(params.get("parameterSet"));

				OctetString ssv = Sakke.extractSharedSecret(
						OctetString.fromHex(params.get("sed")),
						OctetString.fromAscii(params.get("id")),
						paramSet,
						OctetString.fromHex(params.get("rsk")),
						OctetString.fromHex(params.get("z")));
				
				if (ssv != null){
					retVal.put("ssv", ssv.toHex());
				}
				else{
					retVal.put("ssv","null");
				}
				
				m_MainForm.addLog("EXTRACT complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();
	}

	/**
	 * Called when an invalid request is made, return a 404 error
	 * */
	private void Unknown(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("404: " + exchange.getRequestURI().toString());

		Headers responseHeaders = exchange.getResponseHeaders();
		responseHeaders.set("Content-Type", "text/plain");
		exchange.sendResponseHeaders(404, 0);
		exchange.close();
	}

	/**
	 * Parses the URL request parameters
	 * */
	private HashMap<String, String> parseQuery(String query)
			throws UnsupportedEncodingException {

		HashMap<String, String> parameters = new HashMap<String, String>();

		if (query != null) {
			String pairs[] = query.split("[&]");

			for (String pair : pairs) {
				String param[] = pair.split("[=]");

				String key = null;
				String value = null;
				if (param.length > 0) {
					key = URLDecoder.decode(param[0],
							System.getProperty("file.encoding"));
				}

				if (param.length > 1) {
					value = URLDecoder.decode(param[1],
							System.getProperty("file.encoding"));
				}

				// Only allow a single parameter with the same name
				// (the first ones get used, the others are discarded)
				if (!parameters.containsKey(key)) {
					parameters.put(key, value);
				}
			}
		}

		return parameters;
	}
}
