package mikeysakke.kms.client.android;

public class KeyData {

    /** KMS Community Identifier */
    private String m_KmsIdentifier;
	/** KMS Public Authentication key */
    private String m_PublicAuthenticationKey;
    /** KMS Public key */
    private String m_KmsPublicKey;
    /** KMS Sakke parameter set index (should be 1) */
    private Integer m_SakkeParameterSetIndex;
    
    /** The local device ID*/
    private String m_Identifier;    
    /** Public key */
    private String m_PublicValidationToken;
    /** Private key */
    private String m_ReceiverSecretKey;
    /** Private key */
    private String m_SecretSigningKey;
    
    /** Hashed signature */
    private String m_HS;
    
	public String getKmsIdentifier() {
		return m_KmsIdentifier;
	}
	public void setKmsIdentifier(String kmsIdentifier) {
		m_KmsIdentifier = kmsIdentifier;
	}
	public String getPublicAuthenticationKey() {
		return m_PublicAuthenticationKey;
	}
	public void setPublicAuthenticationKey(String publicAuthenticationKey) {
		m_PublicAuthenticationKey = publicAuthenticationKey;
	}
	public String getKmsPublicKey() {
		return m_KmsPublicKey;
	}
	public void setKmsPublicKey(String kmsPublicKey) {
		m_KmsPublicKey = kmsPublicKey;
	}
	public Integer getSakkeParameterSetIndex() {
		return m_SakkeParameterSetIndex;
	}
	public void setSakkeParameterSetIndex(Integer sakkeParameterSetIndex) {
		m_SakkeParameterSetIndex = sakkeParameterSetIndex;
	}
	public String getIdentifier() {
		return m_Identifier;
	}
	public void setIdentifier(String m_Identifier) {
		this.m_Identifier = m_Identifier;
	}
	public String getPublicValidationToken() {
		return m_PublicValidationToken;
	}
	public void setPublicValidationToken(String publicValidationToken) {
		this.m_PublicValidationToken = publicValidationToken;
	}
	public String getReceiverSecretKey() {
		return m_ReceiverSecretKey;
	}
	public void setReceiverSecretKey(String receiverSecretKey) {
		this.m_ReceiverSecretKey = receiverSecretKey;
	}
	public String getSecretSigningKey() {
		return m_SecretSigningKey;
	}
	public void setSecretSigningKey(String secretSigningKey) {
		this.m_SecretSigningKey = secretSigningKey;
	}
	public String getHS() {
		return m_HS;
	}
	public void setHS(String hS) {
		this.m_HS = hS;
	}
}
