package mikeysakke.kms.client.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.Sakke;
import mikeysakke.utils.OctetString;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

@SuppressWarnings("deprecation")
public class Client {

    private KeyData m_Keys;
    private String m_KmsServerIpAddress;
    private Integer m_KmsServerPort;
    private String m_Username;
    private String m_Password;
    private String m_Id;
    private static final String DATE = "2012-09";

    public Client(final String kmsServer, 
    		      final int port,
                  final String username, 
                  final String password,
                  final String id) {
        m_KmsServerIpAddress = kmsServer;
        m_KmsServerPort = port;
        m_Username = username;
        m_Password = password;
        m_Id = id;
        m_Keys = new KeyData();
    }

    public KeyData fetchKeyMaterial() throws IOException, JSONException, URISyntaxException {
        // Request keys from KMS
        HttpClient client = getNewHttpClient();

        Credentials defaultcreds = new UsernamePasswordCredentials(m_Username, m_Password);
        ((DefaultHttpClient)client).getCredentialsProvider().setCredentials(new AuthScope(m_KmsServerIpAddress, AuthScope.ANY_PORT), defaultcreds);
        
        String identifier = DATE + "%00" + m_Id + "%00";
        
        HttpGet request = new HttpGet();
        request.setURI(new URI("https://" + m_KmsServerIpAddress + ":" + m_KmsServerPort.toString() + "/secure/key?id=" + identifier));
        HttpResponse exec_response = client.execute(request);
        InputStream content = exec_response.getEntity().getContent();

        BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
        String s = buffer.readLine();
        String response = "";
        
        while (s != null) {
        	response += s;
        	s = buffer.readLine();
        }

        JSONObject jsonObj = new JSONObject(response);

        // If the server rejected the request there will be an error string
        if ( jsonObj.has("error")) {
            String errorString = jsonObj.getString("error");
        	throw new JSONException(errorString);
        }
        
        JSONArray communityArray = jsonObj.getJSONArray("community");
        JSONObject communityObj = (JSONObject) communityArray.get(0);
        String publicAuthenticationKey = communityObj.getString("kmsPublicAuthenticationKey");
        String kmsPublicKey = communityObj.getString("kmsPublicKey");
        String kmsIdentifier = communityObj.getString("kmsIdentifier");
        int sakkeParameterSetIndex = communityObj.getInt("sakkeParameterSetIndex");

        JSONObject publicObj = jsonObj.getJSONObject("public");
        String publicValidationToken = publicObj.getString("userPublicValidationToken");

        JSONObject privateObj = jsonObj.getJSONObject("private");
        String receiverSecretKey = privateObj.getString("receiverSecretKey");
        String secretSigningKey = privateObj.getString("userSecretSigningKey");

        m_Keys.setSecretSigningKey(secretSigningKey);
        m_Keys.setReceiverSecretKey(receiverSecretKey);
        m_Keys.setPublicValidationToken(publicValidationToken);
        m_Keys.setPublicAuthenticationKey(publicAuthenticationKey);// KPAK
        m_Keys.setKmsIdentifier(kmsIdentifier);
        m_Keys.setKmsPublicKey(kmsPublicKey); // Z
        m_Keys.setSakkeParameterSetIndex(sakkeParameterSetIndex);
        m_Keys.setIdentifier(m_Id);
        
        return m_Keys;
    }

    /**
     * Code required to allow an HTTPS connection without verifying the certificate
     * */    
    public HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
    
    /**
     * Class to allow an SSL connection without performing any checks
     * */    
    class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }
    
    
    public boolean validateSigningKeys() {
        boolean valid;

        OctetString identifier = OctetString.fromAscii(DATE + "\0" + m_Keys.getIdentifier() + "\0");
        
        OctetString hs = new OctetString();
        valid = Eccsi.validateSigningKeys(identifier,
							              OctetString.fromHex(m_Keys.getPublicValidationToken()),
							              OctetString.fromHex(m_Keys.getPublicAuthenticationKey()),
							              OctetString.fromHex(m_Keys.getSecretSigningKey()), 
							              hs);

        m_Keys.setHS(hs.toString());
        
        return valid;
    }

    public boolean validateReceiverKey() {
        boolean valid;

        OctetString identifier = OctetString.fromAscii(DATE + "\0" + m_Keys.getIdentifier() + "\0");
        
        valid = Sakke.validateReceiverSecretKey(identifier,
								        		OctetString.fromHex(m_Keys.getKmsPublicKey()),
								        		OctetString.fromHex(m_Keys.getReceiverSecretKey()),
								                m_Keys.getSakkeParameterSetIndex());
        return valid;
    }
}
