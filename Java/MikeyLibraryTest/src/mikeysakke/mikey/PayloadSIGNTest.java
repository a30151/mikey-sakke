package mikeysakke.mikey;

import junit.framework.TestCase;
import mikeysakke.mikey.tables.SType;
import mikeysakke.mikey.utils.Utils;

import org.bouncycastle.math.ec.Arrays;

import com.google.zxing.common.BitArray;

public class PayloadSIGNTest extends TestCase {

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void test() {
        String test = "my test signature";
        byte[] test_signature = test.getBytes();

        System.out.println("test signature string: " + test);
        System.out.println("test signature: " + bytesToString(test_signature));

        // Create a PayloadSIGN
        PayloadSIGN sign = new PayloadSIGN(SType.ECCSI, (short) test_signature.length, test_signature);

        // get encoded
        BitArray encoded_sign = sign.getEncoded();
        byte[] encoded_bytes = new byte[encoded_sign.getSizeInBytes()];
        encoded_sign.toBytes(0, encoded_bytes, 0, encoded_sign.getSizeInBytes());

        System.out.println("encoded_bytes: " + bytesToString(encoded_bytes));

        PayloadSIGN newSign = PayloadSIGN.decode(encoded_bytes);

        System.out.println("Decoded signature: " + bytesToString(newSign.signature));

        assertTrue(arrayEquals(test_signature, newSign.signature));

        assertEquals(test_signature.length, newSign.signatureLen);

        assertEquals(SType.ECCSI, newSign.sType);

    }

    public boolean arrayEquals(final byte[] one, final byte[] two) {
        if (one.length != two.length) {
            return false;
        }
        boolean isEqual = true;

        for (int i = 0; i < one.length; i++) {
            if (one[i] != two[i]) {
                isEqual = false;
            }

        }

        return isEqual;
    }

    public String bytesToString(final byte[] bytes) {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            s.append(bytes[i] + ", ");
        }
        s.append("end.");
        return s.toString();
    }
    
    public void testEncode()
    {
    	byte s_type = 3;
    	short sig_len = 6;
    	byte[] signature = {12, 13, 14, 14, 16, 15};
    	
    	PayloadSIGN payload_sign = new PayloadSIGN(s_type, sig_len, signature);
    	
    	BitArray encoded_sign = payload_sign.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_sign.getSizeInBytes()];
    	encoded_sign.toBytes(0, encoded_bytes, 0, encoded_sign.getSizeInBytes());
    	
    	// Using multiple "greater than" asserts so that, in a failure case, the test 
    	// will continue as far as possible and potentially reveal what the missing 
    	// data is (instead of failing up-front with only the information that the byte 
    	// array is not long enough)
    	assertTrue(encoded_bytes.length > 1); 
    	
    	// First 4 bits is S type, next 12 is signature length
        byte[] first_two = Arrays.copyOfRange(encoded_bytes, 0, 2);
        short s_type_and_len = Utils.convertByteArrayToShort(first_two);
        // bit shift 12 to get first 4 bits
        short s_type_short = (short) (s_type_and_len >>> 12);
        byte s_type_decoded = (byte) s_type_short;
        
        assertEquals(s_type, s_type_decoded);
        
        // bitmask 0xFFF to get rid of first 4 bits
        short len = (short) (s_type_and_len & 0xfff);
        
        assertEquals(sig_len, len);
        
        byte[] signature_decoded = new byte[len];
        signature_decoded = Arrays.copyOfRange(encoded_bytes, 2, 2 + len);

    	assertEquals(signature.length, signature_decoded.length);
    	
    	for (int i = 0; i < signature_decoded.length; i++) {
			assertEquals(signature[i], signature_decoded[i]);
		}
    	
    }
    
    public void testDecode()
    {
    	byte s_type = 3;
    	short sig_len = 6;
    	byte[] signature = {12, 13, 14, 14, 16, 15};
    	
    	PayloadSIGN payload_sign = new PayloadSIGN(s_type, sig_len, signature);
    	
    	BitArray encoded_sign = payload_sign.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_sign.getSizeInBytes()];
    	encoded_sign.toBytes(0, encoded_bytes, 0, encoded_sign.getSizeInBytes());
    	
    	PayloadSIGN decoded_payload = PayloadSIGN.decode(encoded_bytes);
    	
    	assertEquals(s_type, decoded_payload.getsType());
    	assertEquals(sig_len, decoded_payload.getSignatureLen());
    	
    	byte[] decoded_sig = decoded_payload.getSignature();
    	assertEquals(signature.length, decoded_sig.length);
    	for (int i = 0; i < signature.length; i++) {
			assertEquals(signature[i], decoded_sig[i]);
		}
    }
}
