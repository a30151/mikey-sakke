package mikeysakke.mikey;

import com.google.zxing.common.BitArray;

import junit.framework.TestCase;
import mikeysakke.mikey.utils.Utils;

public class GenericIdMapInfoTest extends TestCase {

    public void testGetEncoded() {

        byte cs_id = 3;
        GenericId map_info = new GenericId(cs_id);

        BitArray encoded_map = map_info.getEncoded();
        byte[] encoded_bytes = new byte[encoded_map.getSizeInBytes()];
        encoded_map.toBytes(0, encoded_bytes, 0, encoded_map.getSizeInBytes());

        // Using multiple "greater than" asserts so that, in a failure case, the
        // test
        // will continue as far as possible and potentially reveal what the
        // missing
        // data is (instead of failing up-front with only the information that
        // the byte
        // array is not long enough)
        assertTrue(encoded_bytes.length > 1);

        // first byte should be the CS ID
        assertEquals(cs_id, encoded_bytes[0]);

        // second byte should be the prot type
        assertEquals(GenericId.DEFAULT_PROT_TYPE, encoded_bytes[1]);

        // third byte contains S and #P, get S first...
        byte s_and_p = encoded_bytes[2];
        int s_bit_int = (s_and_p) >> 7;
        boolean s_bit = s_bit_int == 0 ? false : true;

        assertEquals(GenericId.DEFAULT_S, s_bit);

        // ...now get #P

        int p_bits = (s_and_p & 0x7f); // bitmask 127 to get rid of first
                                       // bit
        assertEquals(GenericId.DEFAULT_P_NUMBER, p_bits);

        // DEFAULT_P_NUMBER should be 1 which indicates 1 policy,
        // meaning the next byte should be Ps
        byte[] ps = {encoded_bytes[3]};
        byte[] session_data_lenth_bytes = { encoded_bytes[4], encoded_bytes[5] };
        short session_data_length = Utils.convertByteArrayToShort(session_data_lenth_bytes);
        assertEquals(GenericId.DEFAULT_SESSION_DATA_LENGTH, session_data_length);

        // Default session data length should be 0 otherwise the next step of
        // this test will be wrong - use an assert to enforce
        assertEquals(0, session_data_length);

        // Next byte should be the SPI Length
        byte spi_length = encoded_bytes[6];
        assertEquals(GenericId.DEFAULT_SPI_LENGTH, spi_length);

        // Default SPI Length should be 0 otherwise the next step of
        // this test will be wrong - use an assert to enforce
        assertEquals(0, spi_length);

        // That should be the total length of the byte array.
        assertEquals(7, encoded_bytes.length);
    }

    public void testGetEncodedNonDefaultData() {

        byte cs_id = 3;
        byte prot_type = 2;
        boolean s_flag = true;
        int p_number = 2;

        byte policy1 = 12;
        byte policy2 = 13;
        byte[] policies = {policy1, policy2 };

        int session_data_length = 10; // if s is set, need a multiple of 10

        byte[] session_data = { (byte) 3, (byte) 2, (byte) 1, (byte) 4, (byte) 5,
        		(byte) 3, (byte) 2, (byte) 1, (byte) 4, (byte) 5}; 

        int spi_length = 1;

        byte[] spi_data = { (byte) 18 };

        GenericId map_info = new GenericId(cs_id,
                prot_type,
                s_flag,
                p_number,
                policies,
                session_data_length,
                session_data,
                spi_length,
                spi_data);

        BitArray encoded_map = map_info.getEncoded();
        byte[] encoded_bytes = new byte[encoded_map.getSizeInBytes()];
        encoded_map.toBytes(0, encoded_bytes, 0, encoded_map.getSizeInBytes());

        // Using multiple "greater than" asserts so that, in a failure case, the
        // test
        // will continue as far as possible and potentially reveal what the
        // missing
        // data is (instead of failing up-front with only the information that
        // the byte
        // array is not long enough)
        assertTrue(encoded_bytes.length > 1);

        // first byte should be the CS ID
        int index = 0;
        assertEquals(cs_id, encoded_bytes[index]);

        // second byte should be the prot type
        index++;
        assertEquals(prot_type, encoded_bytes[index]);

        // third byte contains S and #P, get S first...
        index++;
        byte s_and_p = encoded_bytes[index];
        int s_bit_int = (s_and_p) >> 7;
        boolean s_bit = s_bit_int == 0 ? false : true;

        assertEquals(s_flag, s_bit);

        // ...now get #P

        int p_bits = (s_and_p & 0x7f); // bitmask 127 to get rid of first
                                       // bit
        assertEquals(p_number, p_bits);

        // p_number indicates a number of policies...
        index++;
        byte[] encoded_policies = new byte[p_number];
        for (int i = 0; i < p_number; i++) {
            encoded_policies[i] = encoded_bytes[index + i];
            System.out.println("Encoded: " + encoded_policies[i]);
            assertEquals(policies[i], encoded_policies[i]);
        }

        index += p_number;

        // The next two bytes should be the Session Data Length
        byte[] session_data_lenth_bytes = { encoded_bytes[index], encoded_bytes[++index] };
        int encoded_session_data_length = Utils.convertByteArrayToInt(session_data_lenth_bytes);
        assertEquals(session_data_length, encoded_session_data_length);

        // Get session data using the length
        index++;
        byte[] encoded_session_data = new byte[session_data_length];
        for (int i = 0; i < session_data_length; i++) {
            encoded_session_data[i] = encoded_bytes[index + i];
      //      assertEquals(session_data[i], encoded_session_data[i]);
        }

        index += session_data_length;

        // Next byte should be the SPI Length
        byte encoded_spi_length = encoded_bytes[index];
        assertEquals(spi_length, encoded_spi_length);

        // Get the SPI data
        index++;
        byte[] encoded_spi_data = new byte[encoded_spi_length];
        for (int i = 0; i < encoded_spi_length; i++) {
            encoded_spi_data[i] = encoded_bytes[index + i];
            assertEquals(spi_data[i], encoded_spi_data[i]);
        }

        // Done!
    }

    public void testDecodeCsIdMapInfo() {
        byte cs_id = 3;
        byte prot_type = 2;
        boolean s_flag = true;
        int p_number = 2;

        byte policy1 = 12;
        byte policy2 = 13;
        byte[] policies = { policy1, policy2};

        int session_data_length = 10; // if s is set, need a multiple of 10

        byte[] session_data = { (byte) 3, (byte) 2, (byte) 1, (byte) 4, (byte) 5,
        		(byte) 3, (byte) 2, (byte) 1, (byte) 4, (byte) 5}; 

        int spi_length = 1;

        byte[] spi_data = { (byte) 18 };

        GenericId map_info = new GenericId(cs_id,
                prot_type,
                s_flag,
                p_number,
                policies,
                session_data_length,
                session_data,
                spi_length,
                spi_data);

        BitArray encoded_map = map_info.getEncoded();
        byte[] encoded_bytes = new byte[encoded_map.getSizeInBytes()];
        encoded_map.toBytes(0, encoded_bytes, 0, encoded_map.getSizeInBytes());

        // Now decode the byte array...
        GenericId decoded_map = GenericId.decodeCsIdMapInfo(encoded_bytes);

        assertEquals(cs_id, decoded_map.getCsId());

        assertEquals(prot_type, decoded_map.getProtType());

        assertEquals(s_flag, decoded_map.isS());

        assertEquals(p_number, decoded_map.getPNumber());

        byte[] decoded_policies = decoded_map.getPs();
        assertEquals(policies.length, decoded_policies.length);

        for (int i = 0; i < decoded_policies.length; i++) {
            assertEquals(policies[i],
                    decoded_policies[i]);
        }

        assertEquals(session_data_length, decoded_map.getSessionDataLength());

        SessionData[] decoded_session_data = decoded_map.getSessionData();
        assertEquals(session_data_length, s_flag ? (decoded_session_data.length * 10) : (decoded_session_data.length * 4));
        assertEquals(session_data.length, s_flag ? (decoded_session_data.length * 10) : (decoded_session_data.length * 4));

    //    for (int i = 0; i < decoded_session_data.length; i++) {
    //        assertEquals(session_data[i],
    //                decoded_session_data[i]);
    //    }

        assertEquals(spi_length, decoded_map.getSpiLength());

        byte[] decoded_spi_data = decoded_map.getSpi();
        assertEquals(spi_length, decoded_spi_data.length);
        assertEquals(spi_data.length, decoded_spi_data.length);

        for (int i = 0; i < decoded_spi_data.length; i++) {
            assertEquals(spi_data[i],
                    decoded_spi_data[i]);
        }

        // Done!
    }

    public void testBitStuff()
    {
        int start = -64;
        byte start_byte = (byte) start;
        System.out.println("Start byte: " + start_byte);

        int next = start_byte & 0x7f;
        System.out.println("Next: " + next);
    }
    
}
