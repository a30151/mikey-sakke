package mikeysakke.mikey;

import java.util.Date;
import java.util.Random;

import junit.framework.TestCase;
import mikeysakke.mikey.tables.DataType;
import mikeysakke.mikey.tables.NextPayload;
import mikeysakke.mikey.tables.PRFFunc;
import mikeysakke.mikey.tables.SType;
import mikeysakke.mikey.tables.TSType;

import org.bouncycastle.java.SecureRandom;

import com.google.zxing.common.BitArray;

public class MikeySakkeIMessageTest extends TestCase {

	public void testMikeyMessage() {
		
		try{
			byte hdr_version = 1;
			boolean hdr_v = false;
			Random rn = new Random();
			int n = Integer.MAX_VALUE - 0 + 1;
			int hdr_csb_id = rn.nextInt() % n;
			byte hdr_cs_num = 1;
			byte hdr_cs_id_maptype = 2;
			
			byte cs_id = 1;
			
			GenericId map_info = new GenericId(cs_id);
			
			// Create HDR
			PayloadHDR payload_hdr = new PayloadHDR(hdr_version, 
													DataType.SAKKE, 
													NextPayload.T, 
													hdr_v, 
													PRFFunc.MIKEY_1,
													hdr_csb_id,
													hdr_cs_num,
													hdr_cs_id_maptype,
													map_info);
			
			// Create T
			
			// NOT the best way to get a timestamp, this is just for testing.
			// TODO Find a way to get a NTP UTC timestamp.
			Date test = new Date();
			PayloadT payload_t = new PayloadT(NextPayload.RAND,
											  TSType.NTP_UTC,
											  test.getTime());
			
			// Create RAND
			SecureRandom ranGen = new SecureRandom();
			byte[] rand = new byte[16]; // 16 bytes = 128 bits
			ranGen.nextBytes(rand);
			PayloadRAND payload_rand = new PayloadRAND(NextPayload.SAKKE, rand);
			
			// Create SAKKE
			byte sakke_params = 1;
			byte sakke_id_scheme = 1;
			byte[] sakke_data = new byte[273]; // 273 bytes
			ranGen.nextBytes(sakke_data);
			PayloadSAKKE payload_sakke = new PayloadSAKKE(NextPayload.SIGN, 
														  sakke_params,
														  sakke_id_scheme,
														  sakke_data);
			
			// Create SIGN
			byte[] signature = new byte[129];
			ranGen.nextBytes(signature); // this would be found using the MSLibrary normally
			PayloadSIGN payload_sign = new PayloadSIGN(SType.ECCSI, signature);
			
			Payload[] payloads = {payload_hdr, 
								payload_t,
								payload_rand,
								payload_sakke,
								payload_sign};
			
			MikeySakkeIMessage message = new MikeySakkeIMessage(payloads);
			
			byte[] encoded_mikey = message.getEncoded();
			
			BitArray alt_encoded = payload_hdr.getEncoded();
			alt_encoded.appendBitArray(payload_t.getEncoded());
			alt_encoded.appendBitArray(payload_rand.getEncoded());
			alt_encoded.appendBitArray(payload_sakke.getEncoded());
			alt_encoded.appendBitArray(payload_sign.getEncoded());
			byte[] alt_encoded_bytes = new byte[alt_encoded.getSizeInBytes()];
	    	alt_encoded.toBytes(0, alt_encoded_bytes, 0, alt_encoded.getSizeInBytes());
	    	
	    	assertEquals(alt_encoded_bytes.length, encoded_mikey.length);
			
	    	for (int i = 0; i < alt_encoded_bytes.length; i++) {
				assertEquals(alt_encoded_bytes[i], encoded_mikey[i]);
			}
			
	    	PayloadHDR alt_hdr = PayloadHDR.decodeHDR(alt_encoded_bytes);
	    	byte[] remaining_bytes = alt_hdr.getBytesAfterPayload();
	    	assertTrue(remaining_bytes.length < alt_encoded_bytes.length);
	    	assertEquals(payload_hdr, alt_hdr);
	    	
	    	PayloadT alt_t = PayloadT.decodeT(remaining_bytes);
	    	assertNotNull(alt_t);
	    	int previous_length = remaining_bytes.length;
	    	remaining_bytes = alt_t.getBytesAfterPayload();
	    	assertTrue(remaining_bytes.length < previous_length);
	    	assertEquals(payload_t, alt_t);
	    	
	    	PayloadRAND alt_rand = PayloadRAND.decode(remaining_bytes);
	    	assertNotNull(alt_rand);
	    	previous_length = remaining_bytes.length;
	    	remaining_bytes = alt_rand.getBytesAfterPayload();
	    	assertTrue(remaining_bytes.length < previous_length);
	    	assertEquals(payload_rand, alt_rand);
	    	
	    	PayloadSAKKE alt_sakke = PayloadSAKKE.decode(remaining_bytes);
	    	assertNotNull(alt_sakke);
	    	previous_length = remaining_bytes.length;
	    	remaining_bytes = alt_sakke.getBytesAfterPayload();
	    	assertTrue(remaining_bytes.length < previous_length);
	    	assertEquals(payload_sakke, alt_sakke);
	    	
	    	PayloadSIGN alt_sign = PayloadSIGN.decode(remaining_bytes);
	    	assertNotNull(alt_sign);
	    	previous_length = remaining_bytes.length;
	    	remaining_bytes = alt_sign.getBytesAfterPayload();
	    	assertTrue(remaining_bytes.length < previous_length);
	    	assertEquals(payload_sign, alt_sign);
	    	
	    	MikeySakkeIMessage decoded_msg = MikeySakkeIMessage.decode(encoded_mikey);
	    	assertEquals(message, decoded_msg);
	    	
	    	PayloadHDR decoded_hdr = decoded_msg.getPayloadHDR();
	    	assertEquals(alt_hdr, decoded_hdr);
	    	
	    	PayloadT decoded_t = (PayloadT) decoded_msg.getPayload(NextPayload.T);
	    	assertEquals(alt_t, decoded_t);
	    	
	    	PayloadRAND decoded_rand = (PayloadRAND) decoded_msg.getPayload(NextPayload.RAND);
	    	assertEquals(alt_rand, decoded_rand);
	    	
	    	PayloadSAKKE decoded_sakke = (PayloadSAKKE) decoded_msg.getPayload(NextPayload.SAKKE);
	    	assertEquals(alt_sakke, decoded_sakke);
	    	
	    	PayloadSIGN decoded_sign = (PayloadSIGN) decoded_msg.getPayload(NextPayload.SIGN);
	    	assertEquals(alt_sign, decoded_sign);
		} catch(MikeyException e){
			fail(e.getMessage());
		}
	}
	
	public void testMikeyMessageIncorrectPayloadOrder(){
		byte hdr_version = 1;
		boolean hdr_v = false;
		Random rn = new Random();
		int n = Integer.MAX_VALUE - 0 + 1;
		int hdr_csb_id = rn.nextInt() % n;
		byte hdr_cs_num = 1;
		byte hdr_cs_id_maptype = 2;
		
		byte cs_id = 1;
		
		GenericId map_info = new GenericId(cs_id);
		
		// Create HDR
		PayloadHDR payload_hdr = new PayloadHDR(hdr_version, 
												DataType.SAKKE, 
												NextPayload.T, 
												hdr_v, 
												PRFFunc.MIKEY_1,
												hdr_csb_id,
												hdr_cs_num,
												hdr_cs_id_maptype,
												map_info);
		
		
		MikeySakkeIMessage iMessage = new MikeySakkeIMessage(payload_hdr);
		
		//add an incorrect next Payload
		SecureRandom ranGen = new SecureRandom();
		byte sakke_params = 1;
		byte sakke_id_scheme = 1;
		byte[] sakke_data = new byte[273]; // 273 bytes
		ranGen.nextBytes(sakke_data);
		
		PayloadSAKKE payload_sakke = payload_sakke = new PayloadSAKKE(NextPayload.SIGN, 
													  sakke_params,
													  sakke_id_scheme,
													  sakke_data);

		try {
			iMessage.addPayload(payload_sakke);
			fail("MikeyException expected due to incorrect payload order.");
		} catch (MikeyException e) {
			assertEquals(e.getMessage(), "Next expected payload type was " + NextPayload.T);
		}
		
		try {
			MikeySakkeIMessage anotherIMessage = new MikeySakkeIMessage(new Payload[]{payload_hdr, payload_sakke});
			fail("MikeyException expected due to incorrect payload order.");
		} catch (MikeyException e) {
			assertEquals(e.getMessage(), "Incorrect ordering of payloads.");
		}
			
	}
	
	public void testMikeyMessageIncorrectPayloadDecode(){
		// TODO;
	}
	
	public void testMikeyMessageFirstPayloadNotHeader(){
		Date test = new Date();
		PayloadT payload_t = new PayloadT(NextPayload.RAND,
										  TSType.NTP_UTC,
										  test.getTime());
		
		// Create RAND
		SecureRandom ranGen = new SecureRandom();
		byte[] rand = new byte[16]; // 16 bytes = 128 bits
		ranGen.nextBytes(rand);
		PayloadRAND payload_rand = new PayloadRAND(NextPayload.SAKKE, rand);
		
		// Create SAKKE
		byte sakke_params = 1;
		byte sakke_id_scheme = 1;
		byte[] sakke_data = new byte[273]; // 273 bytes
		ranGen.nextBytes(sakke_data);

		PayloadSAKKE payload_sakke = new PayloadSAKKE(NextPayload.SIGN, 
													  sakke_params,
													  sakke_id_scheme,
													  sakke_data);
		
		// Create SIGN
		byte[] signature = new byte[129];
		ranGen.nextBytes(signature); // this would be found using the MSLibrary normally
		PayloadSIGN payload_sign = new PayloadSIGN(SType.ECCSI, signature);
		
		Payload[] payloads = new Payload[]
				{payload_t, payload_rand, payload_sakke, payload_sign};
		try {
			MikeySakkeIMessage iMessage = new MikeySakkeIMessage(payloads);
			fail("MikeyException expected due to no HDR as first payload.");
		} catch (MikeyException e) {
			assertEquals(e.getMessage(), "First payload must be a PayloadHDR.");
		}
	}
	
	public void testMikeyMessageUnsupportedPayloads() {
		byte fakePayloadType = 112;
		class UnsupportedPayload extends Payload{

			public UnsupportedPayload(){
				payloadType = 112;
			}
			@Override
			public BitArray getEncoded() {
				return new BitArray(234223);
			}
			
			public String toString(){
				return "UnsupportedPayload.";
			}
			
		}
		
		byte hdr_version = 1;
		boolean hdr_v = false;
		Random rn = new Random();
		int n = Integer.MAX_VALUE - 0 + 1;
		int hdr_csb_id = rn.nextInt() % n;
		byte hdr_cs_num = 1;
		byte hdr_cs_id_maptype = 2;
		
		byte cs_id = 1;
		
		GenericId map_info = new GenericId(cs_id);
		
		// Create HDR
		PayloadHDR payload_hdr = new PayloadHDR(hdr_version, 
												DataType.SAKKE, 
												fakePayloadType, 
												hdr_v, 
												PRFFunc.MIKEY_1,
												hdr_csb_id,
												hdr_cs_num,
												hdr_cs_id_maptype,
												map_info);
		
		
		Payload unsupported = new UnsupportedPayload();
		
		try {
			MikeySakkeIMessage iMessage = new MikeySakkeIMessage(new Payload[]{payload_hdr, unsupported});
			byte[] encoded = iMessage.getEncoded();
			
			MikeySakkeIMessage decoded = MikeySakkeIMessage.decode(encoded);
			fail("MikeyException expected due to unsupported payload type.");
			
		} catch (MikeyException e) {
			assertEquals(e.getMessage(), "Unsupported payload type " + fakePayloadType);
		}
	}	
}
