package mikeysakke.mikey;

import junit.framework.TestCase;

import org.bouncycastle.java.SecureRandom;

import com.google.zxing.common.BitArray;

public class PayloadTest extends TestCase {

	class TestPayload extends Payload
	{
		public BitArray getEncoded() {
			return null;
		}
		
		public void setStartByte(int start_byte)
		{
			startByte = start_byte;
		}
		
		public void setEndByte(int end_byte)
		{
			this.endByte = end_byte;
		}
		
		public void setBytes(byte[] encoded_bytes)
		{
			originalBytes = encoded_bytes;
		}
		
		public String toString(){
			return "TestPayload";
		}
	}
	
	public void testGetRemainingBytes()
	{
		SecureRandom ranGen = new SecureRandom();
		byte[] rand = new byte[16]; // 16 bytes = 128 bits
		ranGen.nextBytes(rand);
		
		// Check we have the full byte array back
		TestPayload test_payload = new TestPayload();
		test_payload.setBytes(rand);
		
		// With the end set to 0 by default we should get the whole array back
		byte[] remaining_bytes = test_payload.getBytesAfterPayload();
		assertNotNull(remaining_bytes);
		assertEquals(16, remaining_bytes.length);
		
		test_payload.setEndByte(6);
		remaining_bytes = test_payload.getBytesAfterPayload();
		assertNotNull(remaining_bytes);
		assertEquals(10, remaining_bytes.length);
	}
	
}
