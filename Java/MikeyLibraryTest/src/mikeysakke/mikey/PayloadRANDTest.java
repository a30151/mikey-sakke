package mikeysakke.mikey;

import junit.framework.TestCase;

import org.bouncycastle.java.SecureRandom;

import com.google.zxing.common.BitArray;

public class PayloadRANDTest extends TestCase {

	public void testGetEncoded()
	{
		byte next_payload = 6;
		SecureRandom ranGen = new SecureRandom();
		byte[] rand = new byte[16]; // 16 bytes = 128 bits
		ranGen.nextBytes(rand);
		
		PayloadRAND payload_rand = new PayloadRAND(next_payload, rand);
		
		BitArray encoded_rand = payload_rand.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_rand.getSizeInBytes()];
    	encoded_rand.toBytes(0, encoded_bytes, 0, encoded_rand.getSizeInBytes());
    	
    	// Using multiple "greater than" asserts so that, in a failure case, the test 
    	// will continue as far as possible and potentially reveal what the missing 
    	// data is (instead of failing up-front with only the information that the byte 
    	// array is not long enough)
    	assertTrue(encoded_bytes.length > 1); 
    	
    	// First byte should be the next payload
    	byte next_payload_encoded = encoded_bytes[0];
    	assertEquals(next_payload, next_payload_encoded);
    	
    	// Next byte should be the length of the random number
    	byte rand_length = encoded_bytes[1];
    	assertEquals(rand.length, rand_length);
    	
    	for (int i = 0; i < rand.length; i++) {
    		assertEquals(rand[i], encoded_bytes[i+2]);
		}
	}
	
	public void testDecode()
	{
		byte next_payload = 26;
		SecureRandom ranGen = new SecureRandom();
		byte[] rand = new byte[16]; // 16 bytes = 128 bits
		ranGen.nextBytes(rand);
		
		PayloadRAND payload_rand = new PayloadRAND(next_payload, rand);
		
		BitArray encoded_rand = payload_rand.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_rand.getSizeInBytes()];
    	encoded_rand.toBytes(0, encoded_bytes, 0, encoded_rand.getSizeInBytes());
    	
    	PayloadRAND decoded_payload = PayloadRAND.decode(encoded_bytes);
    	
    	assertEquals(next_payload, decoded_payload.getNextPayload());
    	
    	assertEquals(rand.length, decoded_payload.getRandLen());
    	
    	byte[] decoded_rand = decoded_payload.getRand();
    	for (int i = 0; i < rand.length; i++) {
			assertEquals(rand[i], decoded_rand[i]);
		}
	}
}
