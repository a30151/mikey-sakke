package mikeysakke.mikey;

import mikeysakke.mikey.tables.TSType;

import org.bouncycastle.java.SecureRandom;

import com.google.zxing.common.BitArray;

import junit.framework.TestCase;

public class PayloadTTest extends TestCase {

	public void testGetEncoded() {
		byte next_payload = 14;
		byte ts_type = TSType.NTP_UTC;
		// Fill timestamp with random data for convienience
		SecureRandom ranGen = new SecureRandom();
		byte[] ts_value = new byte[8]; // 8 bytes = 64 bits
		ranGen.nextBytes(ts_value);
		
		PayloadT payload_t = new PayloadT(next_payload, ts_type, ts_value);

		BitArray encoded_t = payload_t.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_t.getSizeInBytes()];
    	encoded_t.toBytes(0, encoded_bytes, 0, encoded_t.getSizeInBytes());
    	
    	// Using multiple "greater than" asserts so that, in a failure case, the test 
    	// will continue as far as possible and potentially reveal what the missing 
    	// data is (instead of failing up-front with only the information that the byte 
    	// array is not long enough)
    	assertTrue(encoded_bytes.length > 1); 
    	
    	// First byte should be the next payload
    	byte next_payload_encoded = encoded_bytes[0];
    	assertEquals(next_payload, next_payload_encoded);
    	
    	// Second byte should be the TS type
    	byte encoded_ts_type = encoded_bytes[1];
    	assertEquals(ts_type, encoded_ts_type);
    	
    	// Remainder should be an 8 byte array which is the timestamp
    	assertEquals(10, encoded_bytes.length);
    	
    	for (int i = 0; i < ts_value.length; i++) {
			assertEquals(ts_value[i], encoded_bytes[i + 2]);
		}
	}

	public void testDecode() {
		
		byte next_payload = 14;
		byte ts_type = TSType.NTP_UTC;
		// Fill timestamp with random data for convienience
		SecureRandom ranGen = new SecureRandom();
		byte[] ts_value = new byte[8]; // 8 bytes = 64 bits
		ranGen.nextBytes(ts_value);
		
		PayloadT payload_t = new PayloadT(next_payload, ts_type, ts_value);

		BitArray encoded_t = payload_t.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_t.getSizeInBytes()];
    	encoded_t.toBytes(0, encoded_bytes, 0, encoded_t.getSizeInBytes());
    	
    	PayloadT decoded_t = PayloadT.decodeT(encoded_bytes);
    	
    	assertEquals(next_payload, decoded_t.getNextPayload());
    	
    	assertEquals(ts_type, decoded_t.getTsType());
    	
    	byte[] decoded_ts_value = decoded_t.getTsValue();
    	assertEquals(ts_value.length, decoded_ts_value.length);
    	for (int i = 0; i < decoded_ts_value.length; i++) {
			assertEquals(ts_value[i], decoded_ts_value[i]);
		}
	}

}
