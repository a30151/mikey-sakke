package mikeysakke.mikey;

import mikeysakke.mikey.utils.Utils;

import org.bouncycastle.java.SecureRandom;

import com.google.zxing.common.BitArray;

import junit.framework.TestCase;

public class PayloadSAKKETest extends TestCase {

	public void testGetEncoded() {
		
		byte next_payload = 4;
		byte sakke_params = 3;
		byte id_scheme =  2;
		SecureRandom ranGen = new SecureRandom();
		byte[] sakke_data = new byte[273]; // 16 bytes = 128 bits
		ranGen.nextBytes(sakke_data);
				
		PayloadSAKKE payload_sakke = 
				new PayloadSAKKE(next_payload,
								 sakke_params,
								 id_scheme,
								 (short)sakke_data.length,
								 sakke_data);
		
	    BitArray encoded_sakke = payload_sakke.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_sakke.getSizeInBytes()];
    	encoded_sakke.toBytes(0, encoded_bytes, 0, encoded_sakke.getSizeInBytes());
    	
    	// Using multiple "greater than" asserts so that, in a failure case, the test 
    	// will continue as far as possible and potentially reveal what the missing 
    	// data is (instead of failing up-front with only the information that the byte 
    	// array is not long enough)
    	assertTrue(encoded_bytes.length > 1); 
    	
    	// First byte should be the next payload
    	byte next_payload_encoded = encoded_bytes[0];
    	assertEquals(next_payload, next_payload_encoded);
    	
    	// Next is the SAKKE params
    	byte sakke_params_encoded = encoded_bytes[1];
    	assertEquals(sakke_params, sakke_params_encoded);
    	
    	// Then the ID scheme
    	byte id_scheme_encoded = encoded_bytes[2];
    	assertEquals(id_scheme, id_scheme_encoded);
    	
    	// Next byte should be the length of the random number
    	byte[] data_len_array = {encoded_bytes[3], encoded_bytes[4]}; 
    	short data_length = Utils.convertByteArrayToShort(data_len_array);
    	assertEquals(sakke_data.length, data_length);
    	
    	for (int i = 0; i < sakke_data.length; i++) {
    		assertEquals(sakke_data[i], encoded_bytes[i+5]);
		}
	}

	public void testDecode() {
		
		byte next_payload = 4;
		byte sakke_params = 3;
		byte id_scheme =  2;
		SecureRandom ranGen = new SecureRandom();
		byte[] sakke_data = new byte[273]; // 16 bytes = 128 bits
		ranGen.nextBytes(sakke_data);
				
		PayloadSAKKE payload_sakke = 
				new PayloadSAKKE(next_payload,
								 sakke_params,
								 id_scheme,
								 (short)sakke_data.length,
								 sakke_data);
		
	    BitArray encoded_sakke = payload_sakke.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_sakke.getSizeInBytes()];
    	encoded_sakke.toBytes(0, encoded_bytes, 0, encoded_sakke.getSizeInBytes());
    	
    	PayloadSAKKE decoded_sakke = PayloadSAKKE.decode(encoded_bytes);
    	
    	assertEquals(next_payload, decoded_sakke.getNextPayload());
    	assertEquals(sakke_params, decoded_sakke.getSakkeParams());
    	assertEquals(id_scheme, decoded_sakke.getIdScheme());
    	assertEquals(sakke_data.length, decoded_sakke.getSakkeDataLen());
    	
    	byte[] decoded_data = decoded_sakke.getSakkeData();
    	assertEquals(sakke_data.length, decoded_data.length);
    	for (int i = 0; i < sakke_data.length; i++) {
			assertEquals(sakke_data[i], decoded_data[i]);
		}
	}
}
