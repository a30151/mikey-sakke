/**
 * 
 */
package mikeysakke.mikey;

import java.util.Random;

import org.bouncycastle.java.BigInteger;
import com.google.zxing.common.BitArray;

import junit.framework.TestCase;
import mikeysakke.mikey.utils.Utils;
import mikeysakke.utils.OctetString;

/**
 * @author daniel.leivers
 * 
 */
public class PayloadHDRTest extends TestCase {

    /*
     * (non-Javadoc)�
     * 
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test method for {@link mikeysakke.mikey.Payload#getBitsFromBytes(byte[])}
     * .
     */
    public void testGetBits() {
        String testString = "This is a test.";
        OctetString testData = OctetString.fromAscii(testString);
        int sizebytes = testData.size();
        BitArray bitarray = Utils.getBitsFromBytes(testData.getOctets());

        byte[] array = new byte[sizebytes];
        bitarray.toBytes(0, array, 0, sizebytes);

        OctetString result = new OctetString(array);
        assertEquals(result.toAscii(), testString);

    }

  
    /**
     * Test method for {@link mikeysakke.mikey.PayloadHDR#getEncoded(byte)}.
     */
    public void testGetEncoded() {
    	
    	// Generate a random 32bit CSB ID
    	BigInteger random_number = new BigInteger(32, new Random());
    	int csbid = random_number.intValue();

    	byte cs_id = 1;
    	GenericId map_info = new GenericId(cs_id);
    	
    	PayloadHDR payload_hdr = new PayloadHDR(csbid, map_info);
    	BitArray encoded_hdr = payload_hdr.getEncoded();
    	
    	byte[] encoded_bytes = new byte[encoded_hdr.getSizeInBytes()];
    	encoded_hdr.toBytes(0, encoded_bytes, 0, encoded_hdr.getSizeInBytes());
    	
    	// Using multiple "greater than" asserts so that, in a failure case, the test 
    	// will continue as far as possible and potentially reveal what the missing 
    	// data is (instead of failing up-front with only the information that the byte 
    	// array is not long enough)
    	assertTrue(encoded_bytes.length > 1); 
    	
    	// First byte should be the version
    	byte version = encoded_bytes[0];
    	assertEquals(PayloadHDR.DEFAULT_VERSION, version);
    	
    	assertTrue(encoded_bytes.length > 2);
    	
    	// Second byte should be the data type
    	byte data_type = encoded_bytes[1];
    	assertEquals(PayloadHDR.DEFAULT_DATA_TYPE, data_type);
    	
    	assertTrue(encoded_bytes.length > 3);
    	
    	// Third byte should be the next payload
    	byte next_payload = encoded_bytes[2];
    	assertEquals(PayloadHDR.HDR_DEFAULT_NEXT_PAYLOAD, next_payload);
    	
    	assertTrue(encoded_bytes.length > 4);
    	
    	// The next byte should be the combination of V and PRF func (which should both be 0)
    	// There should be no bits set in this byte
    	int start_range = 8 * 3; // start bit (inclusive) 
    	int end_range = start_range + 8; // end bit (exclusive)
    	boolean is_range = encoded_hdr.isRange(start_range, end_range, false);
    	assertTrue(is_range);
    	byte v_and_prf = encoded_bytes[3];
    	assertEquals(0, v_and_prf);
    	
    	assertTrue(encoded_bytes.length > 8);
    	
    	// The next 4 bytes should be the CSB ID
    	byte[] rand_num = new byte[4];
    	int bytes_to_copy = 4; // need a 32 bit int - which is 4 bytes

    	// copy the 4 bytes which represent the 32 bit int (the CSB ID)
    	for (int i = 4; i < 8; i++) 
    	{
    		rand_num[i - bytes_to_copy] = encoded_bytes[i];
		}
    	
    	assertEquals(4, rand_num.length);
    	
    	int random_num_int = Utils.convertByteArrayToInt(rand_num);
    	assertEquals(csbid, random_num_int);
    	
    	assertTrue(encoded_bytes.length > 9);
    	
    	// The next byte is the number of crypto sessions
    	byte cs_number = encoded_bytes[8];
    	assertEquals(PayloadHDR.DEFAULT_CS_NUMBER, cs_number);
    	
    	assertTrue(encoded_bytes.length >= 10);
    	
    	// The next byte is the CS-ID Map Type
    	byte cs_id_map_type = encoded_bytes[9];
    	assertEquals(PayloadHDR.DEFAULT_CS_ID_MAP_TYPE, cs_id_map_type);
    	
    	int bytes_found_so_far = 10;
    	BitArray map_info_bits = map_info.getEncoded();
    	int total_encoded_size = bytes_found_so_far + map_info_bits.getSizeInBytes();
    	assertEquals(total_encoded_size, encoded_bytes.length);
    	
    	// Check the CS ID Map Info
    	bytes_to_copy = map_info_bits.getSizeInBytes();
    	byte[] encoded_map = new byte[bytes_to_copy];
    	
    	for (int i = 10; i < encoded_bytes.length; i++) 
    	{
    		encoded_map[i - bytes_found_so_far] = encoded_bytes[i];
		}
    	
    	assertEquals(bytes_to_copy, encoded_map.length);
    	
    	GenericId decrypted_map_info = GenericId.decodeCsIdMapInfo(encoded_map);
    	assertNotNull(decrypted_map_info);
    	
    	assertEquals(cs_id, decrypted_map_info.getCsId());
    }
    
    public void testDecode()
    {
    	try{
	    	// Generate a random 32bit CSB ID
	    	BigInteger random_number = new BigInteger(32, new Random());
	    	int csbid = random_number.intValue();
	
	    	byte cs_id = 1;
	    	GenericId map_info = new GenericId(cs_id);
	
	    	PayloadHDR payload_hdr = new PayloadHDR(csbid, map_info);
	    	BitArray encoded_hdr = payload_hdr.getEncoded();
	    	
	    	byte[] encoded_bytes = new byte[encoded_hdr.getSizeInBytes()];
	    	encoded_hdr.toBytes(0, encoded_bytes, 0, encoded_hdr.getSizeInBytes());
	    	
	    	PayloadHDR decoded_hdr = PayloadHDR.decodeHDR(encoded_bytes);
	    	    	
	    	assertEquals(PayloadHDR.DEFAULT_VERSION, decoded_hdr.getVersion());
	    	
	    	assertEquals(PayloadHDR.DEFAULT_DATA_TYPE, decoded_hdr.getDataType());
	    	
	    	assertEquals(PayloadHDR.HDR_DEFAULT_NEXT_PAYLOAD, decoded_hdr.getNextPayload());
	    	
	    	assertEquals(PayloadHDR.DEFAULT_V, decoded_hdr.isV());
	    	
	    	assertEquals(PayloadHDR.DEFAULT_PRF_FUNC, decoded_hdr.getPrfFunc());
	    	
	    	assertEquals(csbid, decoded_hdr.getCsbId());
	    	
	    	assertEquals(PayloadHDR.DEFAULT_CS_NUMBER, decoded_hdr.getCsNumber());
	    	
	    	assertEquals(PayloadHDR.DEFAULT_CS_ID_MAP_TYPE, decoded_hdr.getCsIdMapType());
	    	
	    	assertTrue(map_info.equals(decoded_hdr.getCsIdMapInfo()));
    	} catch (MikeyException e){
    		fail(e.getMessage());
    	}
    }
    
    public void testDecodeUnsupportedCsIdMapType(){
    	try{
    		class SampleUnsupportedCsIdMapInfo extends CsIdMapInfo{
    			
    			public BitArray getEncoded(){
    				BitArray a = new BitArray();
    				a.appendBits(12345, 32);
    				a.appendBits(98765, 32);
    				return a;
    			}
    			
    			public String toString(){
    				return "SampleUnsupportedCsIdMapInfo";
    			}
    		}
    		
    		
	    	// Generate a random 32bit CSB ID
	    	BigInteger random_number = new BigInteger(32, new Random());
	    	int csbid = random_number.intValue();
	
	
	    	CsIdMapInfo map_info = new SampleUnsupportedCsIdMapInfo();
	    	
	
	    	PayloadHDR payload_hdr = new PayloadHDR((byte)1,
	    			(byte)26, (byte) 5, false, (byte)0, csbid, (byte)0, (byte)6, map_info); //6 just a random number for unsupported CS ID Map type
	    	BitArray encoded_hdr = payload_hdr.getEncoded();
	    	
	    	byte[] encoded_bytes = new byte[encoded_hdr.getSizeInBytes()];
	    	encoded_hdr.toBytes(0, encoded_bytes, 0, encoded_hdr.getSizeInBytes());
	    	
	    	PayloadHDR decoded_hdr = PayloadHDR.decodeHDR(encoded_bytes);
	    	
	    	fail("Expecting MikeyException when decoding HDR due to unsupported CS ID Map type");
	    	    	

    	} catch (MikeyException e){
    		assertEquals(e.getMessage(), "Given CS ID Map Type not yet supported.");
    	}
    }

}
