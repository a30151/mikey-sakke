import junit.framework.Test;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        /*GenericIdMapInfoTest.class,
        MikeySakkeIMessageTest.class,
        PayloadHDRTest.class,
        PayloadRANDTest.class,
        PayloadSAKKETest.class,
        PayloadSIGNTest.class,
        PayloadTest.class,
        PayloadTTest.class*/
 })

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$

		//$JUnit-END$
		return suite;
	}

}
