package mypackage;

public class Message {
    private String fromDeviceId;
    private String toDeviceId;
    private String sakkeEncryptedData;
    private String signature;

    public Message(final String fromDeviceId, final String toDeviceId, final String sakkeEncryptedData, final String signature) {
        this.fromDeviceId = fromDeviceId;
        this.toDeviceId = toDeviceId;
        this.sakkeEncryptedData = sakkeEncryptedData;
        this.signature = signature;
    }

    public String getFromDeviceId() {
        return fromDeviceId;
    }

    public void setFromDeviceId(final String fromDeviceId) {
        this.fromDeviceId = fromDeviceId;
    }

    public String getToDeviceId() {
        return toDeviceId;
    }

    public void setToDeviceId(final String toDeviceId) {
        this.toDeviceId = toDeviceId;
    }

    public String getSakkeEncryptedData() {
        return sakkeEncryptedData;
    }

    public void setSakkeEncryptedData(final String sakkeEncryptedData) {
        this.sakkeEncryptedData = sakkeEncryptedData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(final String signature) {
        this.signature = signature;
    }
}
