package mypackage;

import java.io.IOException;
import java.util.Hashtable;

import mikeysakke.kms.client.KeyData;
import mikeysakke.utils.OctetString;
import net.rim.device.api.database.Cursor;
import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.Database;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.database.DatabaseFactory;
import net.rim.device.api.database.Row;
import net.rim.device.api.database.Statement;
import net.rim.device.api.io.MalformedURIException;
import net.rim.device.api.io.URI;

public class DatabaseManager {

    URI dbURI;
    private static String KEY_DATA_TABLE = "key_data";
    private static String KMS_ID_COLUMN = "kms_id";
    private static String KPAK_COLUMN = "kpak";
    private static String Z_COLUMN = "z";
    private static String PARAM_SET_COLUMN = "param_set";
    private static String DEVICE_ID_COLUMN = "device_id";
    private static String PVT_COLUMN = "pvt";
    private static String RSK_COLUMN = "rsk";
    private static String SSK_COLUMN = "ssk";
    private static String HS_COLUMN = "hs";

    private static String DEVICE_KEYS_TABLE = "device_keys";
    private static String SSV_COLUMN = "ssv";
    Database d;

    public DatabaseManager() {

        try {
            dbURI = URI.create("file:///store/databases/IntegrationTest/" +
                    "MSClient.db");
        } catch (IllegalArgumentException e) {

        } catch (MalformedURIException e) {
        }

    }

    /**
     * creates the database with a table for key data and a table for SSVs
     * 
     * @throws DatabaseException
     * @throws MalformedURIException
     * @throws IllegalArgumentException
     * @throws DataTypeException
     */
    public void create() throws IllegalArgumentException, MalformedURIException,
            DatabaseException, DataTypeException {
        if (!DatabaseFactory.exists(dbURI)) {
            d = DatabaseFactory.openOrCreate(dbURI);

            d.executeStatement("CREATE TABLE " + KEY_DATA_TABLE + " ( " +
                    KMS_ID_COLUMN + "    TEXT," +
                    KPAK_COLUMN + "      TEXT," +
                    Z_COLUMN + "         TEXT," +
                    PARAM_SET_COLUMN + " INTEGER," +
                    DEVICE_ID_COLUMN + " TEXT," +
                    PVT_COLUMN + "       TEXT," +
                    RSK_COLUMN + "       TEXT," +
                    SSK_COLUMN + "       TEXT," +
                    HS_COLUMN + "       TEXT )");

            d.executeStatement("CREATE TABLE " + DEVICE_KEYS_TABLE + " ( " +
                    DEVICE_ID_COLUMN + "    TEXT," +
                    SSV_COLUMN + "          TEXT )");

            d.close();
        }

    }

    /**
     * resets the database tables to new
     * 
     * @throws DataTypeException
     * @throws DatabaseException
     * @throws MalformedURIException
     * @throws IllegalArgumentException
     */
    public void reset() throws IllegalArgumentException, MalformedURIException, DatabaseException, DataTypeException {
        if (DatabaseFactory.exists(dbURI)) {
            DatabaseFactory.delete(dbURI);
            create();
        }

    }

    /**
     * Gets key data from the database (returns empty keydata if the table is
     * empty)
     * */
    public KeyData getKeyData() throws DataTypeException, DatabaseException {
        KeyData data = new KeyData();
        d = DatabaseFactory.open(dbURI);

        // See if there are any rows in the KEY_DATA_TABLE
        Statement s = d.createStatement("SELECT * FROM " + KEY_DATA_TABLE);
        s.prepare();
        Cursor c = s.getCursor();

        if (c.first()) {
            Row r = c.getRow();
            data.setKmsIdentifier(r.getString(c.getColumnIndex(KMS_ID_COLUMN)));
            data.setPublicAuthenticationKey(OctetString.fromHex(r.getString(c.getColumnIndex(KPAK_COLUMN))));
            data.setKmsPublicKey(OctetString.fromHex(r.getString(c.getColumnIndex(Z_COLUMN))));
            data.setSakkeParameterSetIndex(r.getInteger(c.getColumnIndex(PARAM_SET_COLUMN)));
            data.setIdentifier(r.getString(c.getColumnIndex(DEVICE_ID_COLUMN)));
            data.setPublicValidationToken(OctetString.fromHex(r.getString(c.getColumnIndex(PVT_COLUMN))));
            data.setReceiverSecretKey(OctetString.fromHex(r.getString(c.getColumnIndex(RSK_COLUMN))));
            data.setSecretSigningKey(OctetString.fromHex(r.getString(c.getColumnIndex(SSK_COLUMN))));
            data.setHS(OctetString.fromHex(r.getString(c.getColumnIndex(HS_COLUMN))));

        } else {
            data = new KeyData();
        }
        c.close();
        s.close();
        d.close();
        return data;
    }

    /**
     * Inserts the given key data into the database
     * 
     * @throws IOException
     * */
    public void setKeyData(final KeyData data) throws DatabaseException {

        d = DatabaseFactory.open(dbURI);

        // Delete the existing row because there should only ever be 1 row
        d.executeStatement("DELETE FROM " + KEY_DATA_TABLE);

        d.executeStatement("INSERT INTO " + KEY_DATA_TABLE + " VALUES ('"
                + data.getKmsIdentifier()
                + "', '" + data.getPublicAuthenticationKey().toHex()
                + "', '" + data.getKmsPublicKey().toHex()
                + "', " + String.valueOf(data.getSakkeParameterSetIndex())
                + ", '" + data.getIdentifier()
                + "', '" + data.getPublicValidationToken().toHex()
                + "', '" + data.getReceiverSecretKey().toHex()
                + "', '" + data.getSecretSigningKey().toHex()
                + "', '" + data.getHS().toHex()
                + "')");

        d.close();

    }

    /**
     * Inserts or updates an SSV per device
     * 
     * @throws DatabaseException
     **/
    public void setDeviceKey(final String deviceId, final String ssv) throws DatabaseException {
        d = DatabaseFactory.open(dbURI);

        // Check to see if there is already a key stored against this device
        Statement s = d.createStatement("SELECT * FROM " + DEVICE_KEYS_TABLE + " WHERE "
                + DEVICE_ID_COLUMN + " = '" + deviceId + "'");
        s.prepare();
        Cursor c = s.getCursor();

        // See if there are any rows in the KEY_DATA_TABLE
        if (c.first()) {
            // update the row to contain the new SSV
            d.executeStatement("UPDATE " + DEVICE_KEYS_TABLE + " SET " +
                    SSV_COLUMN + " = '" + ssv + "' WHERE " + DEVICE_ID_COLUMN +
                    " = '" + deviceId + "'");

        } else {
            // insert a new row with the device ID and SSV
            d.executeStatement("INSERT INTO " + DEVICE_KEYS_TABLE + " VALUES ( '"
                    + deviceId + "', '" + ssv + "')");

        }
        s.close();
        c.close();
        d.close();
    }

    /**
     * Returns a Hashtable containing all the keys currently stored in the
     * database
     * 
     * @throws DatabaseException
     * @throws DataTypeException
     * */
    public Hashtable getDeviceKeys() throws DatabaseException, DataTypeException {

        Database d = DatabaseFactory.open(dbURI);
        Hashtable deviceKeys = new Hashtable();

        // See if there are any rows in the KEY_DATA_TABLE
        Statement s = d.createStatement("SELECT * FROM " + DEVICE_KEYS_TABLE);
        s.prepare();
        Cursor c = s.getCursor();

        if (c.first()) {
            do {
                Row r = c.getRow();
                deviceKeys.put(r.getString(c.getColumnIndex(DEVICE_ID_COLUMN)),
                        r.getString(c.getColumnIndex(SSV_COLUMN)));
            } while (c.next());
        }
        s.close();
        c.close();
        d.close();

        return deviceKeys;
    }

    /**
     * Deletes all device-SSV pairs from the database
     * 
     * @throws DatabaseException
     * */
    public void clearDeviceKeys() throws DatabaseException {

        Database d = DatabaseFactory.open(dbURI);
        // Delete all device-SSV pairs in the database

        d.executeStatement("DELETE FROM " + DEVICE_KEYS_TABLE);
        d.close();
    }
}
