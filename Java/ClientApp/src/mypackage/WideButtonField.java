package mypackage;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.ButtonField;

public class WideButtonField extends ButtonField {

    private final int width;

    public WideButtonField(final String label, final long style) {
        super(label, style);
        width = Display.getWidth();
    }

    public int getPreferredWidth() {
        return width;
    }

}
