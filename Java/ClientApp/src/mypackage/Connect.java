package mypackage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.HttpsConnection;
import javax.microedition.io.file.FileConnection;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class Connect {

    /**
     * Connects to the Key Exchange Server and retrieves active devices and
     * received messages
     * 
     * @param server
     * @param port
     * @param identifier
     * @param tempDevices
     * @param messages
     * @throws JSONException
     * @throws IOException
     */
    public static void connectToKES(final String server, final String port, final String identifier,
            final Vector tempDevices, final Vector messages) throws JSONException, IOException {

        String url = "http://" + server + ":" + port +
                "/check?id=" + identifier + ";interface=wifi";

        HttpConnection httpConnector = (HttpConnection) Connector.open(url);

        httpConnector.setRequestMethod(HttpConnection.GET);

        InputStream input = httpConnector.openInputStream();

        String testresponse = httpConnector.getResponseMessage();

        if (!testresponse.equals("OK")) {
            throw new IOException("Response from server: " + testresponse);
        }

        byte[] data = new byte[1024];
        int len = 0;
        StringBuffer raw = new StringBuffer();

        while (-1 != (len = input.read(data))) {
            raw.append(new String(data, 0, len));
        }

        String response = raw.toString();
        JSONObject jsonObj = new JSONObject(response);

        JSONArray devices = jsonObj.getJSONArray("devices");

        for (int i = 0; i < devices.length(); i++) {
            // don't add ourselves to the list
            String deviceID = devices.get(i).toString();
            if (!deviceID.equals(identifier)) {
                tempDevices.addElement(deviceID);
            }
        }

        JSONArray jMessages = jsonObj.getJSONArray("messages");
        for (int i = 0; i < jMessages.length(); i++) {
            JSONObject jMessage = jMessages.getJSONObject(i);

            String fromDeviceId = jMessage.getString("from");
            String sed = jMessage.getString("sed");
            String signature = jMessage.getString("signature");
            messages.addElement(new Message(fromDeviceId, identifier, sed, signature));
        }

        input.close();
        httpConnector.close();

    }

    /**
     * Sends a message via the server to another device
     * 
     * @param server
     *            - IP address of server
     * @param port
     * @param fromDeviceId
     *            - identifier of device sending the message
     * @param toDeviceId
     *            - identifier of device to receive the message
     * @param SED
     *            - message to be sent
     * @param signature
     *            - signature of fromDeviceId
     * @throws IOException
     */
    public static void sendMessageToServer(final String server, final String port,
            final String fromDeviceId, final String toDeviceId, final String SED,
            final String signature) throws IOException {
        String url = "http://" + server + ":" + port +
                "/send?id=" + fromDeviceId +
                "&toid=" + toDeviceId +
                "&sed=" + SED +
                "&signature=" + signature
                + ";interface=wifi";

        HttpConnection httpConnector = (HttpConnection) Connector.open(url);

        httpConnector.setRequestMethod(HttpConnection.GET);
        String testresponse = httpConnector.getResponseMessage();

        if (!testresponse.equals("OK")) {
            throw new IOException("Response from server: " + testresponse);
        }

        httpConnector.close();
    }

    public static JSONObject mathServerRequest(final String server, final String port,
            final String requestType, final Hashtable parameters) throws IOException, JSONException {

    	JSONObject obj;
        long start = System.currentTimeMillis();

        if (port.equals(MyApp.mathServerPortSecure)) {
            obj =  mathServerRequestSecure(server, port, requestType, parameters);
        }
        else {
            obj =  mathServerRequestUnsecure(server, port, requestType, parameters);
        }
        
        long end = System.currentTimeMillis();
        try 
        {
          FileConnection fc = (FileConnection)Connector.open("file:///store/home/user/MathServerTimings.txt");
          // If no exception is thrown, then the URI is valid, but the file may or may not exist.
          if (!fc.exists())
          {
              fc.create();  // create the file if it doesn't exist
          }
          OutputStream outStream = fc.openOutputStream(fc.fileSize()); 
          outStream.write(("\r\n" + requestType + ": ").getBytes());
          outStream.write(((end - start) + " milliseconds.").getBytes());
          outStream.close();
          fc.close();
         }
         catch (IOException ioe) {}
        
        return obj;

    }

    // Does not work yet
    public static JSONObject mathServerRequestSecure(final String server, final String port,
            final String requestType, final Hashtable parameters) throws IOException, JSONException {

        Enumeration params = parameters.keys();
        StringBuffer uri = new StringBuffer();
        uri.append("https://" + server + ":" + port + "/" + requestType + "?");
        String first = (String) params.nextElement();
        uri.append(first + "=" + parameters.get(first));

        while (params.hasMoreElements())
        {
            String p = (String) params.nextElement();
            uri.append("&" + p + "=" + parameters.get(p));
        }

        uri.append(";deviceside=true;interface=wifi;trustAll");

        // null
        HttpsConnection httpsConnector = (HttpsConnection) Connector.open(uri.toString());
        // inform("Connection opened");
        httpsConnector.setRequestMethod(HttpsConnection.GET);
        // inform("getResponseMessage: " + httpsConnector.getResponseMessage());
        InputStream input = httpsConnector.openInputStream();

        byte[] data = new byte[1024];
        int len = 0;
        StringBuffer raw = new StringBuffer();

        while (-1 != (len = input.read(data))) {
            raw.append(new String(data, 0, len));
        }

        String response = raw.toString();
        // inform(response);

        JSONObject jsonObj = new JSONObject(response);

        input.close();
        httpsConnector.close();

        return jsonObj;
    }

    public static JSONObject mathServerRequestUnsecure(final String server, final String port,
            final String requestType, final Hashtable parameters) throws IOException, JSONException {

        Enumeration params = parameters.keys();
        StringBuffer uri = new StringBuffer();
        uri.append("http://" + server + ":" + port + "/" + requestType + "?");
        String first = (String) params.nextElement();
        uri.append(first + "=" + parameters.get(first));

        while (params.hasMoreElements())
        {
            String p = (String) params.nextElement();
            uri.append("&" + p + "=" + parameters.get(p));
        }

        uri.append(";interface=wifi");

        // null
        HttpConnection httpConnector = (HttpConnection) Connector.open(uri.toString());

        httpConnector.setRequestMethod(HttpConnection.GET);
        InputStream input = httpConnector.openInputStream();

        byte[] data = new byte[1024];
        int len = 0;
        StringBuffer raw = new StringBuffer();

        while (-1 != (len = input.read(data))) {
            raw.append(new String(data, 0, len));
        }

        String response = raw.toString();

        JSONObject jsonObj = new JSONObject(response);

        input.close();
        httpConnector.close();

        return jsonObj;
    }

    // public static void inform(final String text) {

    // UiApplication.getUiApplication().invokeLater(new Runnable() {
    // public void run() {
    // Dialog.inform(text);
    // }
    // });

    // }

}
