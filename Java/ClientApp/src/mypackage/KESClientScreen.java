package mypackage;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import mikeysakke.kms.client.Client;
import mikeysakke.kms.client.KeyData;
import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.MainScreen;

public class KESClientScreen extends MainScreen {

    private final Vector tempDevices = new Vector();
    private final Vector activeDevices = new Vector();
    private final Vector messages = new Vector();

    /** Displays the devices connected to the KES */
    ObjectChoiceField viewDevicesChoiceField = new ObjectChoiceField();

    MainScreen ssvScreen = new MainScreen();

    WideButtonField connectToKESButton = new WideButtonField("Connect", ButtonField.CONSUME_CLICK);
    LabelField statusLabel = new LabelField();
    WideButtonField backButton = new WideButtonField("Back", ButtonField.CONSUME_CLICK);
    WideButtonField sendMessageButton = new WideButtonField("Send Message", ButtonField.CONSUME_CLICK);
    LabelField messageLabel = new LabelField();
    WideButtonField ssvButton = new WideButtonField("SSVs", ButtonField.CONSUME_CLICK);

    WideButtonField ssv_backButton = new WideButtonField("Back", ButtonField.CONSUME_CLICK);
    LabelField ssv_statusLabel = new LabelField();
    LabelField ssv_ssvLabel = new LabelField();
    LabelField ssv_listLabel = new LabelField();

    public KESClientScreen() {

        add(connectToKESButton);

        add(viewDevicesChoiceField);
        add(sendMessageButton);
        sendMessageButton.setEnabled(false); // only enable when device is
                                             // selected
        add(ssvButton);
        add(backButton);
        add(statusLabel);
        add(messageLabel);

        ssvScreen.add(ssv_backButton);
        ssvScreen.add(ssv_statusLabel);
        ssvScreen.add(ssv_ssvLabel);
        ssvScreen.add(ssv_listLabel);
        setListeners();
    }

    /**
     * Set the change listeners for all the buttons on the screen
     */
    public void setListeners() {

        connectToKESButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field field, final int context) {
                new Thread() {
                    public void run() {
                        try {
                            MyApp.updateLabel(statusLabel, "Connecting to KES...");
                            Connect.connectToKES(MyApp.kmsServer, MyApp.kesPort, MyApp.keys.getIdentifier(),
                                    tempDevices, messages);
                            MyApp.updateLabel(statusLabel, "Connected to KES.");
                            updateActiveDevices();
                            if (messages.size() > 0) {
                                MyApp.updateLabel(statusLabel, "decoding messages...");
                                decodeMessages(messages);
                            }

                        } catch (IOException e) {
                            MyApp.updateLabel(statusLabel, "IOException connecting to KES: " + e.getMessage());
                        } catch (JSONException e) {
                            MyApp.updateLabel(statusLabel, "JSONException connecting to KES: " + e.getMessage());
                        }
                    }
                }.start();

            }

        });

        viewDevicesChoiceField.setChangeListener(new FieldChangeListener() {

            public void fieldChanged(final Field field, final int context) {
                sendMessageButton.setEnabled(true);
            }

        });

        sendMessageButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field field, final int context) {
                String chosenDevice = (String) viewDevicesChoiceField.getChoice(viewDevicesChoiceField.getSelectedIndex());
                generateAndSendMessage(chosenDevice);
            }
        });

        // Field change listener to close current screen
        FieldChangeListener back = new FieldChangeListener() {
            public void fieldChanged(final Field field, final int context) {
                UiApplication.getUiApplication().getActiveScreen().close();
                clearSSVStatusLabels();
            }
        };

        backButton.setChangeListener(back);
        ssv_backButton.setChangeListener(back);

        ssvButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field field, final int context) {

                UiApplication.getUiApplication().invokeLater(new Runnable() {
                    public void run() {
                        UiApplication.getUiApplication().pushScreen(ssvScreen);
                        displaySSVs();

                    }
                });

            }
        });
    }

    /** retrieve devices and SSVs from database and display them */
    public void displaySSVs() {
        try {
            DatabaseManager d = new DatabaseManager();
            Hashtable ssvs = d.getDeviceKeys();
            Enumeration e = ssvs.keys();
            StringBuffer s = new StringBuffer();
            while (e.hasMoreElements()) {
                String device = (String) e.nextElement();
                String ssv = (String) ssvs.get(device);
                s.append("\n" + device + ": " + ssv);
            }

            ssv_listLabel.setText(s.toString());

        } catch (DatabaseException e) {
            MyApp.updateLabel(ssv_statusLabel, "DatabaseException retrieving SSVs: " + e.getMessage());
        } catch (DataTypeException e) {
            MyApp.updateLabel(ssv_statusLabel, "DataTpeException retrieving SSVs: " + e.getMessage());

        }
    }

    /** Alert user of messages and ask whether or not to decrypt the keys */
    public void decodeMessages(final Vector msgs) {

        UiApplication.getUiApplication().invokeLater(new Runnable() {
            public void run() {
                StringBuffer s = new StringBuffer();
                s.append("You have recieved");
                for (int i = 0; i < msgs.size(); i++) {
                    Message msg = (Message) msgs.elementAt(i);
                    s.append(" a key from " + msg.getFromDeviceId());
                }
                s.append(". Ok to decrypt the keys?");
                int viewMessages = Dialog.ask(Dialog.D_YES_NO, s.toString());
                if (viewMessages == Dialog.YES) {
                    UiApplication.getUiApplication().pushScreen(ssvScreen);
                    decryptMessages(msgs);
                }
            }
        });

    }

    /** Verifies signatures of messages and then extracts SSV */
    public void decryptMessages(final Vector msgs) {
        new Thread() {
            public void run() {
                int len = msgs.size();
                String SSV = null;
                for (int i = 0; i < len; i++) {
                    Message message = (Message) msgs.elementAt(i);
                    String fromID = message.getFromDeviceId();

                    MyApp.updateLabel(statusLabel, "Decrypting Messages...");

                    MyApp.updateLabel(ssv_statusLabel, "Verifying signature " + (i + 1) + " of " + len + "...");

                    Hashtable parameters = new Hashtable();

                    KeyData keys = MyApp.keys;

                    try {
                        String signerId = Client.DATE + "%00" + fromID + "%00";
                        parameters.put("signerId", signerId);
                        parameters.put("kpak", keys.getPublicAuthenticationKey().toString());
                        parameters.put("message", message.getSakkeEncryptedData());
                        parameters.put("signature", message.getSignature());

                        boolean verified = false;
                        JSONObject jsonObj = Connect.mathServerRequest(MyApp.kmsServer, MyApp.mathServerPortSecure, "VERIFY", parameters);

                        
                        if (jsonObj.has("error")) {
                            MyApp.updateLabel(ssv_statusLabel, "Error verifying Signature: " + jsonObj.get("error"));
                            return;
                        }
                        if (jsonObj.has("verified")) {
                            verified = ((Boolean) jsonObj.get("verified")).booleanValue();
                        }
                        if (!verified) {
                            MyApp.updateLabel(ssv_statusLabel, "Signature failed to verify.");
                            return;
                        }

                        MyApp.updateLabel(ssv_statusLabel, "Extracting key " + (i + 1) + " of " + len + "...");

                        parameters = new Hashtable();

                        String identifier = Client.DATE + "%00" + keys.getIdentifier() + "%00";

                        parameters.put("sed", message.getSakkeEncryptedData());
                        parameters.put("id", identifier);
                        parameters.put("parameterSet", "1");
                        parameters.put("rsk", keys.getReceiverSecretKey().toString());
                        parameters.put("z", keys.getKmsPublicKey().toString());

                        JSONObject jsonObj2 = Connect.mathServerRequest(MyApp.kmsServer, MyApp.mathServerPortSecure, "EXTRACT", parameters);
                        if (jsonObj2.has("error")) {
                            MyApp.updateLabel(ssv_statusLabel, "Error extracting SSV: " + jsonObj2.get("error"));
                            return;
                        }
                        if (jsonObj2.has("ssv")) {
                            SSV = (String) jsonObj2.get("ssv");
                        }
                        else {
                            MyApp.updateLabel(ssv_statusLabel, "No SSV extracted.");
                        }

                        if (SSV == null || SSV.equals("null")) {
                            MyApp.updateLabel(ssv_ssvLabel, "SSV for " + fromID + " is invalid.");
                        } else {
                            MyApp.updateLabel(ssv_statusLabel, "Key(s) decrypted ok.");
                            MyApp.updateLabel(ssv_ssvLabel, "SSV for " + fromID + ": " + SSV);

                            try {
                                DatabaseManager database = new DatabaseManager();
                                database.setDeviceKey(message.getFromDeviceId(), SSV);
                                MyApp.updateLabel(ssv_statusLabel, "SSV for " + message.getFromDeviceId() + " stored.");

                            } catch (IllegalArgumentException e) {
                                MyApp.updateLabel(ssv_statusLabel,
                                        "IllegalArgumentException while writing to database: " + e.getMessage());

                            } catch (DatabaseException e) {
                                MyApp.updateLabel(ssv_statusLabel,
                                        "DatabaseException while writing to database: " + e.getMessage());

                            }
                        }
                    } catch (IOException e) {
                        MyApp.updateLabel(ssv_statusLabel,
                                "IOException while connecting to math server: " + e.getMessage());
                    } catch (JSONException e) {
                        MyApp.updateLabel(ssv_statusLabel,
                                "JSONException while parsing json: " + e.getMessage());
                    }

                }
                MyApp.updateLabel(statusLabel, "");

            }
        }.start();

    }

    /*
     * public String getDateString() { // get current date Date today = new
     * Date(System.currentTimeMillis()); SimpleDateFormat monthFormat = new
     * SimpleDateFormat("MM"); int currentMonth =
     * Integer.parseInt(monthFormat.format(today));
     * 
     * SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy"); int
     * currentYear = Integer.parseInt(yearFormat.format(today)); String date =
     * String.valueOf(currentYear) + "-" + String.valueOf(currentMonth); return
     * date;
     * 
     * }
     */

    private void generateAndSendMessage(final String toDeviceId) {
        int sendMessage = Dialog.ask(Dialog.D_YES_NO, "Generate and send a new SSV to " + toDeviceId + "?");
        if (sendMessage == Dialog.YES) {
            new Thread() {
                public void run() {
                    String signature = null;
                    String ssv = null;
                    String sed = null;

                    try {
                        KeyData keys = MyApp.keys;
                        String targetId = Client.DATE + "%00" + toDeviceId + "%00";
                        MyApp.updateLabel(messageLabel, "Generating Message...");
                        Hashtable parameters = new Hashtable();
                        parameters.put("targetId", targetId);
                        parameters.put("parameterSet", "1");
                        parameters.put("z", keys.getKmsPublicKey().toString());

                        JSONObject jsonObj = Connect.mathServerRequest(MyApp.kmsServer, MyApp.mathServerPortSecure, "GENERATE", parameters);
                        if (jsonObj.has("error")) {
                            MyApp.updateLabel(ssv_statusLabel, "Error generating message: " + jsonObj.get("error"));
                            return;
                        }

                        if (jsonObj.has("ssv") && jsonObj.has("sed")) {
                            ssv = (String) jsonObj.get("ssv");
                            sed = (String) jsonObj.get("sed");
                        }
                        if (ssv == null || sed == null || ssv.equals("null") || sed.equals("null")) {
                            MyApp.updateLabel(ssv_statusLabel, "No SSV/SED generated.");
                            return;
                        }

                        MyApp.updateLabel(messageLabel, "Signing Message...");
                        parameters = new Hashtable();
                        parameters.put("message", sed);
                        parameters.put("pvt", keys.getPublicValidationToken().toString());
                        parameters.put("ssk", keys.getSecretSigningKey().toString());
                        parameters.put("hs", keys.getHS().toString());

                        JSONObject jsonObj2 = Connect.mathServerRequest(MyApp.kmsServer, MyApp.mathServerPortSecure, "SIGN", parameters);
                        if (jsonObj2.has("error")) {
                            MyApp.updateLabel(messageLabel, "Error signing message: " + jsonObj2.get("error"));
                            return;
                        }

                        if (jsonObj2.has("signature")) {
                            signature = jsonObj2.getString("signature");
                        }
                        if (signature == null || signature.equals("null")) {
                            MyApp.updateLabel(messageLabel, "Unable to sign.");
                            return;
                        }

                    } catch (IOException e) {
                        MyApp.updateLabel(ssv_statusLabel,
                                "IOException while connecting to math server: " + e.getMessage());
                    } catch (JSONException e) {
                        MyApp.updateLabel(ssv_statusLabel,
                                "JSONException while parsing json: " + e.getMessage());
                    }

                    try {

                        // store ssv in database
                        MyApp.updateLabel(messageLabel, "Storing SSV...");
                        DatabaseManager database = new DatabaseManager();
                        database.setDeviceKey(toDeviceId, ssv);
                        MyApp.updateLabel(messageLabel, "SSV stored.");

                        // Connect to server and send message
                        MyApp.updateLabel(messageLabel, "Sending Message...");
                        Connect.sendMessageToServer(MyApp.kmsServer, MyApp.kesPort, MyApp.keys.getIdentifier(),
                                toDeviceId, sed, signature);
                        MyApp.updateLabel(messageLabel, "Message sent.");
                    } catch (IOException e) {
                        MyApp.updateLabel(messageLabel, "IOException sending message: " + e.getMessage());
                    } catch (IllegalArgumentException e) {
                        MyApp.updateLabel(messageLabel,
                                "IllegalArgumentException while writing to database: " + e.getMessage());

                    } catch (DatabaseException e) {
                        MyApp.updateLabel(messageLabel,
                                "DatabaseException while writing to database: " + e.getMessage());
                    }

                }
            }.start();
        }

    }

    public void clearSSVStatusLabels() {
        MyApp.updateLabel(ssv_statusLabel, "");
        MyApp.updateLabel(ssv_ssvLabel, "");
    }

    /**
     * Change the list of devices displayed on the screen
     */
    public void updateActiveDevices() {

        synchronized (activeDevices) {
            activeDevices.removeAllElements();
            for (int i = 0; i < tempDevices.size(); i++) {
                activeDevices.addElement(tempDevices.elementAt(i));
            }

            UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                    viewDevicesChoiceField.setLabel("Connected Devices: ");
                    viewDevicesChoiceField.setChoices(getStringArrayFromVector(activeDevices));
                    add(viewDevicesChoiceField);
                }
            });
        }

    }

    public String[] getStringArrayFromVector(final Vector vec) {
        String[] arr = new String[vec.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (String) vec.elementAt(i);
        }
        return arr;
    }

}
