package mypackage;

import java.io.IOException;
import java.util.Hashtable;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import mikeysakke.kms.client.Client;
import mikeysakke.kms.client.KeyData;
import mikeysakke.utils.OctetString;
import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.io.MalformedURIException;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.component.TextField;
import net.rim.device.api.ui.container.FullScreen;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * This class extends the UiApplication class, providing a graphical user
 * interface.
 */
public class MyApp extends UiApplication
{

    MyScreen main;
    FullScreen editCredentialsScreen;
    MainScreen viewKeysScreen;
    KESClientScreen kesClientScreen;
    DatabaseManager database;

    Client client;
    public static KeyData keys;
    public static Hashtable deviceSSVs;

    public static final String kmsServer = "192.168.1.3";
    public static final String kmsPort = "7070";
    public static final String kesPort = "7171";
    public static final String mathServerPort = "7071";
    public static final String mathServerPortSecure = "7072";

    TextField id = new TextField("ID: ", "dan");
    TextField user = new TextField("Username: ", "jim");
    PasswordEditField pass = new PasswordEditField("Password: ", "jimpass");
    TextField kms = new TextField("KMS Server: ", kmsServer);
    TextField port = new TextField("Port: ", kmsPort);

    WideButtonField credentialsBackButton = new WideButtonField("Back", ButtonField.CONSUME_CLICK);
    WideButtonField submitCredentialsButton = new WideButtonField("Submit", ButtonField.CONSUME_CLICK);
    WideButtonField changeDetailsButton = new WideButtonField("Edit Credentials", ButtonField.CONSUME_CLICK);
    WideButtonField fetchKeysButton = new WideButtonField("Fetch New Keys", ButtonField.CONSUME_CLICK);
    LabelField fetchKeyStatusLabel = new LabelField();
    LabelField viewKeysLabel = new LabelField();
    LabelField validateKeysStatusLabel = new LabelField();
    WideButtonField viewKeysButton = new WideButtonField("View Keys", ButtonField.CONSUME_CLICK);
    WideButtonField exitViewKeysScreenButton = new WideButtonField("Back", ButtonField.CONSUME_CLICK);
    WideButtonField exitAppButton = new WideButtonField("Exit", ButtonField.CONSUME_CLICK);

    WideButtonField kESButton = new WideButtonField("Key Exchange Server", ButtonField.CONSUME_CLICK);

    /**
     * Entry point for application
     * 
     * @param args
     *            Command line arguments (not used)
     */
    public static void main(final String[] args)
    {
        // Create a new instance of the application and make the currently
        // running thread the application's event dispatch thread.
        MyApp theApp = new MyApp();

        theApp.enterEventDispatcher();
    }

    /**
     * Creates a new MyApp object
     */
    public MyApp()
    {
        // Push a screen onto the UI stack for rendering.
        editCredentialsScreen = new FullScreen();
        main = new MyScreen();
        viewKeysScreen = new MainScreen();
        viewKeysScreen.add(exitViewKeysScreenButton);

        main.getMainManager().setBackground(BackgroundFactory.createSolidTransparentBackground(Color.GRAY, 128));

        setListeners();
        setMainScreenFields();
        setEditCredentialsScreenFields();
        pushScreen(main);
        initialiseDB();

    }

    /**
     * retrieve data from database if any is stored.
     */
    public void initialiseDB() {

        try {
            database = new DatabaseManager();
            database.create();
            keys = database.getKeyData();
            deviceSSVs = database.getDeviceKeys();
            if (!keys.isEmpty()) {
                // if previously stored keys exist, allow connection to KES
                kESButton.setEnabled(true);
                setViewKeysScreenFields();
            }

        } catch (IllegalArgumentException e) {
            updateLabel(fetchKeyStatusLabel, "IllegalArgumentException: " + e.getMessage());
        } catch (MalformedURIException e) {
            updateLabel(fetchKeyStatusLabel, "MalformedURIException: " + e.getMessage());
        } catch (DatabaseException e) {
            updateLabel(fetchKeyStatusLabel, "DatabaseException: " + e.getMessage());
        } catch (DataTypeException e) {
            updateLabel(fetchKeyStatusLabel, "DataTypeException: " + e.getMessage());
        }

    }

    public Client createClient() {
        keys = new KeyData();
        client = new Client(keys, kms.getText(), port.getText(),
                user.getText(), pass.getText(), id.getText());
        return client;
    }

    /** add edit credentials screen fields. */
    public void setEditCredentialsScreenFields() {
        editCredentialsScreen.add(credentialsBackButton);
        editCredentialsScreen.add(id);
        editCredentialsScreen.add(user);
        editCredentialsScreen.add(pass);
        editCredentialsScreen.add(kms);
        editCredentialsScreen.add(port);
        editCredentialsScreen.add(submitCredentialsButton);

    }

    /** add main screen fields to main screen. */
    public void setMainScreenFields() {
        main.add(changeDetailsButton);
        main.add(fetchKeysButton);
        main.add(viewKeysButton);
        main.add(kESButton);
        main.add(exitAppButton);
        main.add(fetchKeyStatusLabel);
        main.add(validateKeysStatusLabel);
        // should not be able to connect to KES without valid keys
        kESButton.setEnabled(false);
    }

    /** set all button event listeners. */
    public void setListeners() {
        // Set listener for exit app button
        exitAppButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                System.exit(0);
            }
        });

        // Set Listener for Submit Credentials button
        submitCredentialsButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                try {
                    client = createClient();
                    database.reset();
                    updateLabel(viewKeysLabel, "");
                    kESButton.setEnabled(false);

                } catch (IllegalArgumentException e) {
                    Dialog.inform(e.getMessage());
                } catch (MalformedURIException e) {
                    Dialog.inform(e.getMessage());
                } catch (DatabaseException e) {
                    Dialog.inform(e.getMessage());
                } catch (DataTypeException e) {
                    Dialog.inform(e.getMessage());
                }
                finally {
                    editCredentialsScreen.close();
                }

            }
        });

        // Set Listener for Back button
        credentialsBackButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                editCredentialsScreen.close();

            }
        });

        // Set Listener for Fetch Keys button - fetches keys and validates them
        fetchKeysButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                new Thread() {
                    public void run() {
                        updateLabel(fetchKeyStatusLabel, "Fetching keys from KMS...");
                        try {
                            if (client == null) {
                                client = createClient();
                            }
                            client.fetchKeyMaterial();
                            updateLabel(fetchKeyStatusLabel, "Keys Fetched.");

                            updateLabel(validateKeysStatusLabel, "Validating Keys...");

                            keys = client.getKeys();
                            setViewKeysScreenFields();
                            Hashtable parameters = new Hashtable();

                            String identifier = Client.DATE + "%00" + keys.getIdentifier() + "%00";
                            parameters.put("id", identifier);
                            parameters.put("pvt", keys.getPublicValidationToken().toString());
                            parameters.put("kpak", keys.getPublicAuthenticationKey().toString());
                            parameters.put("z", keys.getKmsPublicKey().toString());
                            parameters.put("rsk", keys.getReceiverSecretKey().toString());
                            parameters.put("parameterSet", "1");
                            parameters.put("ssk", keys.getSecretSigningKey().toString());

                            boolean validated = false;

                            JSONObject jsonObj = Connect.mathServerRequest(kmsServer, mathServerPortSecure, "VALIDATE", parameters);
                            if (jsonObj.has("error")) {
                                updateLabel(validateKeysStatusLabel, "Error validating keys: " + jsonObj.get("error"));
                                return;
                            }
                            if (jsonObj.has("validated")) {
                                validated = ((Boolean) jsonObj.get("validated")).booleanValue();
                            }
                            if (jsonObj.has("hs")) {
                                String hs = jsonObj.getString("hs");
                                keys.setHS(OctetString.fromHex(hs));
                            }
                            else {
                                updateLabel(validateKeysStatusLabel, "Did not receive HS from server.");
                                return;
                            }

                            if (validated) {
                                updateLabel(validateKeysStatusLabel, "Keys validated.");

                                // enable view keys and KES button now that
                                // we have keys
                                UiApplication.getUiApplication().invokeLater(new Runnable() {
                                    public void run() {
                                        kESButton.setEnabled(true);
                                        setViewKeysScreenFields();
                                    }
                                });

                                // if validated, store the keys
                                database.setKeyData(keys);
                                updateLabel(validateKeysStatusLabel, "Keys stored.");

                            } else {
                                updateLabel(validateKeysStatusLabel,
                                        "Keys are invalid.");
                            }

                        } catch (IOException e) {
                            updateLabel(fetchKeyStatusLabel, "IOException: " + e.getMessage());
                        } catch (JSONException e) {
                            updateLabel(fetchKeyStatusLabel, "JSONException: " + e.getMessage());
                        } catch (NullPointerException e) {
                            updateLabel(fetchKeyStatusLabel, "NullPointerException: " + e.getMessage());
                        } catch (DatabaseException e) {
                            updateLabel(fetchKeyStatusLabel, "DatabaseException: " + e.getMessage());
                        }
                    }

                }.start();

            }
        }
                );

        // Set Listener for View Keys button
        viewKeysButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                pushScreen(viewKeysScreen);
            }
        });

        // Set listener for exit view screen button
        exitViewKeysScreenButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                viewKeysScreen.close();
            }
        });

        // Set listener for Edit Credentials button
        changeDetailsButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                pushScreen(editCredentialsScreen);
            }
        });

        // Set listener for KES button
        kESButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                UiApplication.getUiApplication().invokeLater(new Runnable() {
                    public void run() {
                        kesClientScreen = new KESClientScreen();
                        pushScreen(kesClientScreen);
                    }

                });

            }
        });

    }

    /** add key information fields to viewKeysScreen. */
    public void setViewKeysScreenFields() {
        if (viewKeysLabel.getManager() == null) {
            viewKeysScreen.add(viewKeysLabel);
        }
        viewKeysLabel.setText("identifier: " + keys.getIdentifier()
                + "\n\nEncoded identifier: " + keys.getEncodedIdentifier()
                + "\n\nRSK: " + keys.getReceiverSecretKey()
                + "\n\nSSK: " + keys.getSecretSigningKey()
                + "\n\nKAPK: " + keys.getPublicAuthenticationKey()
                + "\n\nPVT: " + keys.getPublicValidationToken()
                + "\n\nZ: " + keys.getKmsPublicKey()
                + "\n\nKMS identifier: " + keys.getKmsIdentifier());
    }

    public static void updateLabel(final LabelField label, final String text) {

        UiApplication.getUiApplication().invokeLater(new Runnable() {
            public void run() {
                label.setText(text);
            }
        });

    }

}
