package mikeysakke.src;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.Sakke;
import mikeysakke.kms.client.android.Client;
import mikeysakke.kms.client.android.KeyData;
import mikeysakke.utils.OctetString;
import mikeysakke.utils.RandomGenerator;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ipl.mikeysakke.R;

public class MainActivity extends Activity {

    /** KMS Public Authentication key */
    private KeyData m_KeyData;
    
    /** The ID textbox */
    EditText m_IdTextbox;
    Button m_CheckButton;
    
    
    /** Devices list that is accessed only in the UI thread */
    private ArrayList<String> m_ActiveDevices = new ArrayList<String>();
    /** Staging devices list used on worker thread */
    private ArrayList<String> m_TempDevices = new ArrayList<String>();
    
    private static final String IP_ADDRESS = "192.168.1.3";
    private static final Integer KMS_PORT = 7070;
    private static final Integer KES_PORT = 7171;    
    private static final String DATE_STRING = "2012-09";
    
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup the button listeners
        Button submitButton = (Button)findViewById(R.id.submitButton);
        submitButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				requestKey();
			}
		});
        
        m_CheckButton = (Button)findViewById(R.id.checkButton);
        m_CheckButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				checkKeyExchangeServer();
			}
		});
        
        Button clearButton = (Button)findViewById(R.id.clearButton);
        clearButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearKeys();
			}
		});
        
        Button viewButton = (Button)findViewById(R.id.viewButton);
        viewButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				viewKeys();
			}
		});
        
        // Check the database for existing key data
        DatabaseAdapter adapter = new DatabaseAdapter(this);
        m_KeyData = adapter.getKeyData(this);
                
        // Populate the Identifier text box
        m_IdTextbox = (EditText)findViewById(R.id.identifierTextbox);
        m_IdTextbox.setText(m_KeyData.getIdentifier());
        
        // Disable the ID textbox if keys are already verified
        if ( m_KeyData.getSecretSigningKey() == null || m_KeyData.getSecretSigningKey().equals("") ) {
            updateStatus("Key data not found");  
            m_IdTextbox.setEnabled(true);
            m_CheckButton.setEnabled(false);
        } else {
            updateStatus("Keys verified");
            m_IdTextbox.setEnabled(false);
            m_CheckButton.setEnabled(true);
        }
        
        // Setup the list view to display known active devices
        ListView lv = (ListView)MainActivity.this.findViewById(R.id.devicesList);
        synchronized (m_ActiveDevices) {
        	lv.setAdapter(new ArrayAdapter<String>(this, R.layout.simplerow, m_ActiveDevices));
        }
        
        lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)  {
				
				// Don't allow clicks if another process is running
				ProgressBar progress = (ProgressBar)MainActivity.this.findViewById(R.id.progressBar1);
				if (progress.getVisibility() != View.VISIBLE) {
					String deviceId = null;
					
					// Get the device ID that was clicked on
					synchronized (m_ActiveDevices) {
						if ( m_ActiveDevices.size() > position ) {
							deviceId = m_ActiveDevices.get(position);						
						}
					}
									
					if ( deviceId == null ) {
						updateStatus("Invalid option selected");
					} else {
						SendMessage(deviceId);
					}
				}
			}
		});
                
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    
    /**
     * Called when the "Request" button is pushed. 
     * Gets new key data from the KMS and verifies it
     * */
    public void requestKey() {   
    	
    	// Erase current key data
    	m_KeyData = new KeyData();
    	m_KeyData.setIdentifier(m_IdTextbox.getText().toString());    	
    	
    	updateStatus("Requesting key data...");
    	toggleProgress(true);
        RetreiveKeyTask task = new RetreiveKeyTask();
        task.execute();
    }
    
    /**
     * Class to request keys from the KMS and verify them using the java library
     * */
    class RetreiveKeyTask extends AsyncTask<String, Void, String> {
    	
        protected String doInBackground(String... urls) {
            String response = "";
           
            try {
        		// Use the KMS client library to communicate with the KMS server
        	    Client client = new Client(IP_ADDRESS, KMS_PORT, "jim", "jimpass", m_KeyData.getIdentifier());
        	    
        	    // get the keys
        	    KeyData testKeys = client.fetchKeyMaterial();
                
                // Check the signature (SSK)
                updateStatus("Verifying key signature...");
                boolean validatedEccsi = client.validateSigningKeys();

                // Check the key (RSK)
                updateStatus("Verifying key data...");
                boolean validatedSakke = client.validateReceiverKey();
            
            	if ( validatedEccsi && validatedSakke ) {
            		updateStatus("Keys verified");     
            		
            		m_KeyData = testKeys;
            		
            		// Save the data now it is verified
                    DatabaseAdapter adapter = new DatabaseAdapter(MainActivity.this);
                    adapter.setKeyData(m_KeyData);                      
            	} else {
            		updateStatus("Key verify failed"); 
            	}
			} catch (Exception e) {
				updateStatus(e.getMessage());   
				e.printStackTrace();
			}

            return response;
        }

        protected void onPostExecute(String result) {
        	toggleProgress(false);
        }
    }   
    
    /**
     * Called when the check KES button is pressed, starts an async task to do the work
     * */
    private void checkKeyExchangeServer() {
    	checkKeyExchangeServerTask task = new checkKeyExchangeServerTask();
        task.execute();
    }
   
    /**
     * Task to check the KES for active devices and keys
     * */
    class checkKeyExchangeServerTask extends AsyncTask<String, Void, String> {
    	
        protected String doInBackground(String... urls) {
        	try {
                HttpClient client = new DefaultHttpClient();
            	
                HttpGet request = new HttpGet();
    			request.setURI(new URI("http://" + IP_ADDRESS + ":" + KES_PORT.toString() + "/check?id=" + m_KeyData.getIdentifier()));
    			
                HttpResponse exec_response = client.execute(request);
                InputStream content = exec_response.getEntity().getContent();

                BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
                String s = buffer.readLine();
                
                String response = "";
                while (s != null) {
                	response += s;
                	s = buffer.readLine();
                }

                // Decode response from KMS
                JSONObject obj = new JSONObject(response);
                if (obj.has("error")) {
                	
                	// If an error occurred then tell the user and exit
                    String errorString = obj.getString("error");
                    updateStatus(errorString);
                } else {

                	// Read the active devices from the message
            		m_TempDevices.clear();
                	
                	JSONArray devices = obj.getJSONArray("devices");
                	for (int i = 0; i < devices.length(); i++) {

                		// don't add ourselves to the list
                		String deviceID = devices.get(i).toString();
                		if ( !deviceID.equals(m_KeyData.getIdentifier())) {
                			m_TempDevices.add(deviceID);
                		}
                	}                    	

                	updateActiveDevices();
                	updateStatus("Updated devices from KES");
                	
                	
                	// Check for any messages
                	ArrayList<Message> messages = new ArrayList<Message>();
                	JSONArray jMessages = obj.getJSONArray("messages");
                	for (int i = 0; i < jMessages.length(); i++) {
                		JSONObject jMessage = jMessages.getJSONObject(i);
                
                		String fromDeviceId = jMessage.getString("from");
                		String sed = jMessage.getString("sed");
                		String signature = jMessage.getString("signature");
                		messages.add(new Message(fromDeviceId, m_KeyData.getIdentifier(), sed, signature));
                	}     

                	if (messages.size() > 0) {
                		decodeMessages(messages);
                	}
                }
    			
                return response;
    			
    		} catch (Exception e) {
    			updateStatus(e.getMessage()); 
    			e.printStackTrace();
    		}
        	
        	return "";
        }

        protected void onPostExecute(String result) {
        	toggleProgress(false);
        }
    }  
        
    /**
     * Displays a dialog box when a message is received checking
     * if it is OK to decode the keys
     * */
    private void decodeMessages(final ArrayList<Message> messages) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {				

				// Show an alert dialog that some messages were received
		    	AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    	
		    	String alertString = "You have received ";
		    	
		    	for (Message mess : messages) {
		    		alertString += " a key from " + mess.getFromDeviceId();
		    	}
		    	
		    	alertString += ". OK to decode the key(s)?";
		    	
		    	builder.setMessage(alertString);
		    	builder.setCancelable(false);
		    	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		    	           public void onClick(DialogInterface dialog, int id) {
		    	        	   toggleProgress(true);
		    	        	   decryptKeysTask task = new decryptKeysTask(messages);
		    	        	   task.execute();
		    	           }
		    	       });
		    	builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
		    	           public void onClick(DialogInterface dialog, int id) {
		    	                dialog.cancel();
		    	           }
		    	       });
		    	AlertDialog alert = builder.create();
		    	alert.show();
				
			}
		});
    }
    
    /**
     * Async task to perform the work of decoding the keys and checking the signature
     * */
    class decryptKeysTask extends AsyncTask<String, Void, String> {
    	
    	private ArrayList<Message> m_Messages;
    	
    	public decryptKeysTask(ArrayList<Message> messages) {
    		m_Messages = messages;
    	}
    	
        protected String doInBackground(String... urls) {
        	try {
       		
                OctetString identifier = OctetString.fromAscii(DATE_STRING + "\0" + m_KeyData.getIdentifier() + "\0");
        		
                OctetString SSV = new OctetString();
                
        		for (int i = 0; i < m_Messages.size(); i++) {
        			Message message = m_Messages.get(i);
        			
        			OctetString fromIdentifier = OctetString.fromAscii(DATE_STRING + "\0" + message.getFromDeviceId() + "\0");  			
        			
        			OctetString SED = OctetString.fromHex(message.getSakkeEncryptedData());
        			OctetString signature =  OctetString.fromHex(message.getSignature());
        			
        			updateStatus("Verifying signature " + (i + 1) + " of " + m_Messages.size());
        			boolean signatureVerified = Eccsi.verify(SED, 
							        					     signature, 
							        					     fromIdentifier, 
							        					     OctetString.fromHex(m_KeyData.getPublicAuthenticationKey()));
        			if ( !signatureVerified) {
        				updateStatus("Signature failed to verify"); 
        				return "";
        			}
        			
        			
        			updateStatus("Extracting key " + (i + 1) + " of " + m_Messages.size());
        			SSV = Sakke.extractSharedSecret(SED, 
			    					                identifier, 
			    					                m_KeyData.getSakkeParameterSetIndex(), 
			    					                OctetString.fromHex(m_KeyData.getReceiverSecretKey()), 
			    					                OctetString.fromHex(m_KeyData.getKmsPublicKey()));
        			
        			if ( SSV.empty() ) {
        				updateStatus("Failed to extract key");
        			} else {
            	        DatabaseAdapter adapter = new DatabaseAdapter(MainActivity.this);
            	        adapter.setDeviceKey(message.getFromDeviceId(), SSV.toString());   
        			}     			        		
        		}
                
                updateStatus("Key(s) decoded OK. " + SSV.toString());
    			
    		} catch (Exception e) {
    			updateStatus(e.getMessage()); 
    			e.printStackTrace();
    		}
        	
        	return "";
        }

        protected void onPostExecute(String result) {
        	toggleProgress(false);
        }
    }  

    /**
     * Erases the key data from the sql database
     * */
    public void clearKeys() {   
    	
		// Show a confirm dialog
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Clear all keys from the database?");
    	builder.setCancelable(false);
    	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	           	
    	           	   // Erase current key data
    	               DatabaseAdapter adapter = new DatabaseAdapter(MainActivity.this);
    	               adapter.clearDeviceKeys();
    	               m_KeyData = adapter.resetkeyData(MainActivity.this);
    	               
    	               m_IdTextbox.setText(m_KeyData.getIdentifier());

    	               m_TempDevices.clear();
    	               updateActiveDevices();
    	               
    	               updateStatus("Key data not found");   
    	               m_IdTextbox.setEnabled(true);        
    	               m_CheckButton.setEnabled(false);
    	           }
    	       });
    	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                dialog.cancel();
    	           }
    	       });
    	AlertDialog alert = builder.create();
    	alert.show();
    }
    
    /**
     * Displays the currently stored keys for devices in the database
     * */
    public void viewKeys() {   
    	
    	DatabaseAdapter adapter = new DatabaseAdapter(this);
    	HashMap<String, String> deviceKeys = adapter.getDeviceKeys();
    	
    	String message = "There are currently " + deviceKeys.keySet().size() + " keys in the database:";
    	for ( String deviceID : deviceKeys.keySet() ) {   		
    		message += "\n" + deviceID + " - " + deviceKeys.get(deviceID);
    	}
    	    	
		// Show a confirm dialog to generate and send a key to the selected device
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(message);
    	builder.setCancelable(false);
    	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   dialog.cancel();
    	           }
    	       });

    	AlertDialog alert = builder.create();
    	alert.show();
    }
    
    /**
     * Called when an item in the devices list is clicked on
     * displays a dialog box confirming the key generation and send
     * */
    private void SendMessage(final String toDeviceId) {
		// Show a confirm dialog to generate and send a key to the selected device
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Generate and send a new SSV to " + toDeviceId + "?");
    	builder.setCancelable(false);
    	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   toggleProgress(true);
    	        	   sendMessageTask task = new sendMessageTask(toDeviceId);
    	        	   task.execute();
    	           }
    	       });
    	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                dialog.cancel();
    	           }
    	       });
    	AlertDialog alert = builder.create();
    	alert.show();
    }
    
    /**
     * Class to generate random bytes to use for SSVs 
     * */
    class SsvRandomGenerator implements RandomGenerator
	{
		@Override
		public OctetString generate(int size) {
			byte[] buffer = new byte[size];
			
			Random rand = new Random();			
			rand.nextBytes(buffer);
			
			OctetString os = new OctetString(buffer);
			return os;
		}
	}
    
    /**
     * Class to generate and send a key to another device
     * */
    class sendMessageTask extends AsyncTask<String, Void, String> {
    	
    	private String m_ToDeviceId;
    	
    	public sendMessageTask(String toDeviceId) {
    		m_ToDeviceId = toDeviceId;
    	}
    	
        protected String doInBackground(String... urls) {
        	try {
        		updateStatus("Generating message...");
        		
        		OctetString SED = new OctetString();
        		
                OctetString toIdentifier = OctetString.fromAscii(DATE_STRING + "\0" + m_ToDeviceId + "\0");
                
        		OctetString SSV = Sakke.generateSharedSecretAndSED(SED, 
					        				                       toIdentifier, 
					        				                       m_KeyData.getSakkeParameterSetIndex(), 
					        				                       OctetString.fromHex(m_KeyData.getKmsPublicKey()), 
					        				                       new SsvRandomGenerator());
        		
        		updateStatus("Signing message...");
        		OctetString signature = Eccsi.sign(SED, 
						        				   OctetString.fromHex(m_KeyData.getPublicValidationToken()), 
						        				   OctetString.fromHex(m_KeyData.getSecretSigningKey()), 
						        				   OctetString.fromHex(m_KeyData.getHS()), 
						        				   new SsvRandomGenerator());
        		        		
        		updateStatus("Sending message...");
        		
        		// Send the message to the server
                HttpClient client = new DefaultHttpClient();            	
                HttpGet request = new HttpGet();
    			request.setURI(new URI("http://" + IP_ADDRESS + ":" + KES_PORT.toString() 
    					                                    + "/send?id=" + m_KeyData.getIdentifier() 
    					                                         + "&toid=" + m_ToDeviceId 
    					                                         + "&sed=" + SED.toString() 
    					                                         + "&signature=" + signature.toString()));
    			
                client.execute(request);
                
    	        DatabaseAdapter adapter = new DatabaseAdapter(MainActivity.this);
    	        adapter.setDeviceKey(m_ToDeviceId, SSV.toString());       
                
                updateStatus("Message sent");
                return SED.toString();
    			
    		} catch (Exception e) {
    			updateStatus(e.getMessage()); 
    			e.printStackTrace();
    		}
        	
        	return "";
        }

        protected void onPostExecute(String result) {
        	toggleProgress(false);
        }
    }    
    
    /**
     * Changes the text displayed on the status label on the main screen
     * */
    private void updateStatus(final String status) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
		    	TextView label = (TextView)MainActivity.this.findViewById(R.id.statusLabel);
		    	label.setText(status);				
			}
		});
    }
    
    /**
     * Changes the list of devices displayed on the screen
     * */
    private void updateActiveDevices() {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {				
				synchronized (m_ActiveDevices) {
					
					m_ActiveDevices.clear();
					for (String deviceId : m_TempDevices) {
						m_ActiveDevices.add(deviceId);
					}
					
			        ListView lv = (ListView)MainActivity.this.findViewById(R.id.devicesList);
			        lv.setAdapter(new ArrayAdapter<String>(MainActivity.this, R.layout.simplerow, m_ActiveDevices));
				}				
			}
		});
    }
    
    /**
     * Turns the progress bar on or off, also disables the controls when the progress bar is moving
     * */
    private void toggleProgress(final boolean started) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {				
		    	ProgressBar progress = (ProgressBar)MainActivity.this.findViewById(R.id.progressBar1);
		    	Button Requestbutton = (Button)MainActivity.this.findViewById(R.id.submitButton);	
		    	Button clearButton = (Button)MainActivity.this.findViewById(R.id.clearButton);		
		    	Button viewButton = (Button)MainActivity.this.findViewById(R.id.viewButton);	  
	    	
		    	if (started) {
		    		progress.setVisibility(View.VISIBLE);
		    		Requestbutton.setEnabled(false);	
		    		clearButton.setEnabled(false);
		    		viewButton.setEnabled(false);
		    		m_CheckButton.setEnabled(false);
		    		m_IdTextbox.setEnabled(false);
		    	} else {
		    		progress.setVisibility(View.GONE);
		    		Requestbutton.setEnabled(true);	
		    		clearButton.setEnabled(true);
		    		viewButton.setEnabled(true);
		    		
		    		// If there are keys then enable the check KES button
		    		// Otherwise enable the ID textbox
		            if ( m_KeyData.getSecretSigningKey() != null 
			            	&& !m_KeyData.getSecretSigningKey().equals("") ) {
			            m_CheckButton.setEnabled(true);
		            } else {
		            	m_IdTextbox.setEnabled(true);
		            }
		    	}
			}
		});
    }
}
