package mikeysakke.src;

import java.util.HashMap;

import mikeysakke.kms.client.android.KeyData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.telephony.TelephonyManager;

public class DatabaseAdapter extends SQLiteOpenHelper {

	private static String KEY_DATA_TABLE = "key_data";
	private static String KMS_ID_COLUMN = "kms_id"; 
	private static String KPAK_COLUMN = "kpak";
	private static String Z_COLUMN =  "z";
	private static String PARAM_SET_COLUMN = "param_set";
	private static String DEVICE_ID_COLUMN =  "device_id";
	private static String PVT_COLUMN =  "pvt";
	private static String RSK_COLUMN =  "rsk";
	private static String SSK_COLUMN =  "ssk";
	private static String HS_COLUMN =  "hs";
	
	private static String DEVICE_KEYS_TABLE = "device_keys";
	private static String SSV_COLUMN =  "ssv";
	
	public DatabaseAdapter(Context context) {
		super(context, "sakke_test.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL("CREATE TABLE " + KEY_DATA_TABLE + " ( " + 
					KMS_ID_COLUMN + "    TEXT,"+ 
					KPAK_COLUMN + "      TEXT," + 
					Z_COLUMN + "         TEXT," + 
					PARAM_SET_COLUMN + " INTEGER," + 
					DEVICE_ID_COLUMN + " TEXT NOT NULL," + 
					PVT_COLUMN + "       TEXT," + 
					RSK_COLUMN + "       TEXT," + 
					SSK_COLUMN + "       TEXT," + 
					HS_COLUMN + "       TEXT )" );
		
		db.execSQL("CREATE TABLE " + DEVICE_KEYS_TABLE + " ( " + 
			    	DEVICE_ID_COLUMN + "    TEXT,"+ 
				    SSV_COLUMN + "          TEXT )" );
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
	/**
	 * Gets key data from the database (returns default data if the table is empty)
	 * */
	public KeyData getKeyData(Context context) {
		KeyData data = new KeyData();
		
		SQLiteDatabase db = this.getReadableDatabase();
		
		// See if there are any rows in the KEY_DATA_TABLE
		Cursor c = db.query(KEY_DATA_TABLE, null, "", null, "", "", "");
		
		if (c.moveToFirst()) {
			data.setKmsIdentifier(c.getString(c.getColumnIndex(KMS_ID_COLUMN)));
			data.setPublicAuthenticationKey(c.getString(c.getColumnIndex(KPAK_COLUMN)));
			data.setKmsPublicKey(c.getString(c.getColumnIndex(Z_COLUMN)));
			data.setSakkeParameterSetIndex(c.getInt(c.getColumnIndex(PARAM_SET_COLUMN)));
			data.setIdentifier(c.getString(c.getColumnIndex(DEVICE_ID_COLUMN)));
			data.setPublicValidationToken(c.getString(c.getColumnIndex(PVT_COLUMN)));
			data.setReceiverSecretKey(c.getString(c.getColumnIndex(RSK_COLUMN)));
			data.setSecretSigningKey(c.getString(c.getColumnIndex(SSK_COLUMN)));	
			data.setHS(c.getString(c.getColumnIndex(HS_COLUMN)));			
		} else {
			// Insert a new row with a default identifier for this device
			data = resetkeyData(context);			
		}
		
		c.close();		
		db.close();
		
		return data;
	}
	
	/**
	 * Erases the existing key data and inserts a default value for identifier
	 * */
	public KeyData resetkeyData(Context context) {
		KeyData data = new KeyData();
		
		// Used the IMEI as the default identifier for the device
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE); 
        data.setIdentifier(telephonyManager.getDeviceId()); 
		
        setKeyData(data);
        
		return data;
	}
	
	/**
	 * Inserts the given key data into the database
	 * */
	public void setKeyData(KeyData pr_KeyData) {
		
		SQLiteDatabase db = this.getReadableDatabase();
		
		// Delete the existing row because there should only ever be 1 row in the table
		db.delete(KEY_DATA_TABLE, "", new String[] {});
		
		ContentValues values = new ContentValues();
		values.put(KMS_ID_COLUMN, pr_KeyData.getKmsIdentifier());
		values.put(KPAK_COLUMN, pr_KeyData.getPublicAuthenticationKey());
		values.put(Z_COLUMN, pr_KeyData.getKmsPublicKey());
		values.put(PARAM_SET_COLUMN, pr_KeyData.getSakkeParameterSetIndex());
		values.put(DEVICE_ID_COLUMN, pr_KeyData.getIdentifier());
		values.put(PVT_COLUMN, pr_KeyData.getPublicValidationToken());
		values.put(RSK_COLUMN, pr_KeyData.getReceiverSecretKey());
		values.put(SSK_COLUMN, pr_KeyData.getSecretSigningKey());
		values.put(HS_COLUMN, pr_KeyData.getHS());
		
		db.insert(KEY_DATA_TABLE, null, values);
		
		db.close();
	}
	
	/**
	 * Inserts or updates an SSV per device
	 **/
	public void setDeviceKey(String deviceId, String ssv) {
		
		// Check to see if there is already a key stored against this device
		SQLiteDatabase db = this.getReadableDatabase();
		
		// See if there are any rows in the KEY_DATA_TABLE
		Cursor c = db.query(DEVICE_KEYS_TABLE, null, DEVICE_ID_COLUMN + "=?", new String[] {deviceId}, "", "", "");
		
		if (c.moveToFirst()) {
			// update the row to contain the new SSV
			ContentValues values = new ContentValues();
			values.put(SSV_COLUMN, ssv);
			
			db.update(DEVICE_KEYS_TABLE, values, DEVICE_ID_COLUMN + "=?", new String[] {deviceId});
			
		} else {
			// insert a new row with the device ID and SSV
			ContentValues values = new ContentValues();
			values.put(DEVICE_ID_COLUMN, deviceId);
			values.put(SSV_COLUMN, ssv);
			
			db.insert(DEVICE_KEYS_TABLE, null, values);
		}
		c.close();
		db.close();
	}
	
	/**
	 * Returns a map containing all the keys currently stored in the database
	 * */
	public HashMap<String,String> getDeviceKeys() {
		
		SQLiteDatabase db = this.getReadableDatabase();
		
		HashMap<String,String> deviceKeys = new HashMap<String,String>();
		
		// See if there are any rows in the KEY_DATA_TABLE
		Cursor c = db.query(DEVICE_KEYS_TABLE, null, "", null, "", "", "");
		if (c.moveToFirst()) {
			do {
				deviceKeys.put(c.getString(c.getColumnIndex(DEVICE_ID_COLUMN)), 
						       c.getString(c.getColumnIndex(SSV_COLUMN)));
			} while (c.moveToNext());
		}		
		c.close();
		db.close();
		
		return deviceKeys;
	}
	
	/**
	 * Deletes all device-SSV pairs from the database
	 * */
	public void clearDeviceKeys() {
		
		// Delete all device-SSV pairs in the database
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete(DEVICE_KEYS_TABLE, "", new String[] {});
		db.close();
	}
}
