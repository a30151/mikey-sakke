package msTest;

import org.bouncycastle.java.BigInteger;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECPoint;

import mikeysakke.crypto.SakkeParameterSet;
import mikeysakke.crypto.SakkeParameterSet1;

public class MikkeySakkeRFCDataTest {

    // public static void main(final String[] args) {
    // System.out.println(runTestSuite());
    // }

    public static String runTestSuite() {
        StringBuffer s = new StringBuffer();
        s.append("1. Verifying KMS Setup, Section 6.1 ( Z = z P )  \n");

        // Store expected KMS Public Key Z for a given KMS Master secret z
        BigInteger z = new BigInteger(
                "AFF429D35F84B110D094803B3595A6E2998BC99F", 16);

        BigInteger Zx = new BigInteger(
                "5958EF1B1679BF099B3A030DF255AA6A" +
                        "23C1D8F143D4D23F753E69BD27A832F3" +
                        "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
                        "10EFFE300367A37B61F701D914AEF097" +
                        "24825FA0707D61A6DFF4FBD7273566CD" +
                        "DE352A0B04B7C16A78309BE640697DE7" +
                        "47613A5FC195E8B9F328852A579DB8F9" +
                        "9B1D0034479EA9C5595F47C4B2F54FF2", 16);

        BigInteger Zy = new BigInteger(
                "1508D37514DCF7A8E143A6058C09A6BF" +
                        "2C9858CA37C258065AE6BF7532BC8B5B" +
                        "63383866E0753C5AC0E72709F8445F2E" +
                        "6178E065857E0EDA10F68206B63505ED" +
                        "87E534FB2831FF957FB7DC619DAE6130" +
                        "1EEACC2FDA3680EA4999258A833CEA8F" +
                        "C67C6D19487FB449059F26CC8AAB655A" +
                        "B58B7CC796E24E9A394095754F5F8BAE", 16);

        // Calculate Z = zP and check if it matches expected
        SakkeParameterSet params = new SakkeParameterSet1();
        ECPoint expectedZ = new ECPoint.Fp(params.curve(),
                new ECFieldElement.Fp(params.p(), Zx),
                new ECFieldElement.Fp(params.p(), Zy));

        ECPoint actualZ = params.pointP().multiply(z);
        if (actualZ.equals(expectedZ)) {
            s.append("TEST PASSED\n\n");
        } else {
            s.append("TEST FAILED\n\n");
        }

        s.append("2. Verify RSK for user\n");
        BigInteger b = new BigInteger("323031312D30320074656C3A2B34343737303039303031323300", 16);

        // Generate RSK
        ECPoint RSK = params.pointP().multiply((b.add(z)).modInverse(params.q()));

        ECPoint bPplusZ = (params.pointP().multiply(b)).add(actualZ);

        // BigInteger rhs = Sakke.ComputePairing(parameters, parameters.P,
        // parameters.P);
        // BigInteger lhs = Sakke.ComputePairing(parameters, bPplusZ, RSK);
        // if (rhs.equals(lhs)) {
        // s.append("TEST PASSED\n\n");
        // } else {
        // s.append("TEST FAILED\n\n");
        // }

        return s.toString();
    }

}
