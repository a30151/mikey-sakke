package msTest;

import java.io.IOException;

import org.json.me.JSONException;

import mikeysakke.kms.client.Client;
import mikeysakke.kms.client.KeyData;

public class ClientTest {

    public static String clientTest() {

        StringBuffer s = new StringBuffer();
        String serveradd = "192.168.1.3";
        String port = "7070";
        String username = "jim";
        String password = "jimpass";
        String id = "dan";

        s.append("kmsServer:" + serveradd);
        s.append("\nport:" + port);
        s.append("\nusername:" + username);
        s.append("\npassword:" + password);
        s.append("\nid:" + id);

        KeyData keys = new KeyData();
        Client client = new Client(keys, serveradd, port, username, password, id);
        try {
            client.fetchKeyMaterial();
            s.append("\n\nConnection with kmsServer successfull!");
        } catch (IOException e) {
            s.append("\nIOException: " + e.getMessage());
            return s.toString();
        } catch (JSONException e) {
            s.append("\nJSONException: " + e.getMessage());
            return s.toString();
        }

        keys = client.getKeys();

        s.append("\n\nRetrieved keys for '" + id + "'\n(identifier: " + keys.getIdentifier() + "):\n");
        s.append("\nRSK:\t" + keys.getReceiverSecretKey() + "\n");
        s.append("\nSSK:\t" + keys.getSecretSigningKey() + "\n");
        s.append("\nKPAK:\t" + keys.getPublicAuthenticationKey() + "\n");
        s.append("\nPVT: \t" + keys.getPublicValidationToken() + "\n");
        s.append("\nKMS Identifier:\t" + keys.getKmsIdentifier() + "\n");
        s.append("\nZ:\t" + keys.getKmsPublicKey() + "\n");
        s.append("\nSakkeIndex:\t" + keys.getSakkeParameterSetIndex());

        s.append("\n\nValidating...");
        boolean validated = false;
        try {
            validated = client.validateSigningKeys();
            s.append("\nValidated SSK.");
        } catch (Exception e) {
            s.append("\nException validating SSK: " + e.getMessage());
        }
        if (validated) {
            try {
                validated = client.validateReceiverKey();
                s.append("\nValidated RSK");
            } catch (Exception e) {
                s.append("\nException validating RSK: " + e.getMessage());
            }

        }
        return s.toString();
    }
}
