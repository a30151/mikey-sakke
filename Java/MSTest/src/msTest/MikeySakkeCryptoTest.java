package msTest;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.Sakke;
import mikeysakke.utils.OctetString;
import mikeysakke.utils.RandomGenerator;

public class MikeySakkeCryptoTest {
    public static String test() {
        StringBuffer s = new StringBuffer();
        s.append("\n----- Built-in parameters:-----\n\n");

        s.append("Okay\n");

        s.append("\n----- Community setup: -----\n\n");

        String community = "aliceandbob.co.uk";

        OctetString kpak = OctetString.fromHex(
                "04" + "50D4670BDE75244F28D2838A0D25558A" +
                        "7A72686D4522D4C8273FB6442AEBFA93" +
                        "DBDD37551AFD263B5DFD617F3960C65A" +
                        "8C298850FF99F20366DCE7D4367217F4");

        OctetString Z = OctetString.fromHex(
                "04" + "5958EF1B1679BF099B3A030DF255AA6A" +
                        "23C1D8F143D4D23F753E69BD27A832F3" +
                        "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
                        "10EFFE300367A37B61F701D914AEF097" +
                        "24825FA0707D61A6DFF4FBD7273566CD" +
                        "DE352A0B04B7C16A78309BE640697DE7" +
                        "47613A5FC195E8B9F328852A579DB8F9" +
                        "9B1D0034479EA9C5595F47C4B2F54FF2" +

                        "1508D37514DCF7A8E143A6058C09A6BF" +
                        "2C9858CA37C258065AE6BF7532BC8B5B" +
                        "63383866E0753C5AC0E72709F8445F2E" +
                        "6178E065857E0EDA10F68206B63505ED" +
                        "87E534FB2831FF957FB7DC619DAE6130" +
                        "1EEACC2FDA3680EA4999258A833CEA8F" +
                        "C67C6D19487FB449059F26CC8AAB655A" +
                        "B58B7CC796E24E9A394095754F5F8BAE");

        s.append("KMS id: " + community + "\n");
        s.append("KPAK: " + kpak + "\n");
        s.append("Z: " + Z + "\n");

        s.append("\n----- Alice setup: -----\n\n");

        OctetString aliceId = OctetString.fromAscii("2011-02\0tel:+447700900123\0");

        OctetString aliceSSK = OctetString.fromHex("23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34489A0D");
        OctetString alicePVT = OctetString.fromHex(
                "04" + "758A142779BE89E829E71984CB40EF75" +
                        "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                        "A79D247692F4EDA3A6BDAB77D6AA6474" +
                        "A464AE4934663C5265BA7018BA091F79");

        // Both alice and bob share the necessary community parameters.

        s.append("\n---- Alice validates KMS signing keys ----\n\n");
        s.append("SSK: " + aliceSSK + "\n");

        OctetString aliceHS = new OctetString();
        boolean validated = Eccsi.validateSigningKeys(aliceId, alicePVT, kpak, aliceSSK, aliceHS);

        s.append("Validated: " + validated + "\n");

        s.append("SSK: " + aliceSSK + "\n");
        s.append("PVT: " + alicePVT + "\n");

        s.append("\n----- Bob setup: -----\n\n");

        // Note that for checking against spec; bob happens to have same
        // phone number as alice.
        OctetString bobId = OctetString.fromAscii("2011-02\0tel:+447700900123\0");

        OctetString bobRSK = OctetString.fromHex(
                "04" + "93AF67E5007BA6E6A80DA793DA300FA4" +
                        "B52D0A74E25E6E7B2B3D6EE9D18A9B5C" +
                        "5023597BD82D8062D34019563BA1D25C" +
                        "0DC56B7B979D74AA50F29FBF11CC2C93" +
                        "F5DFCA615E609279F6175CEADB00B58C" +
                        "6BEE1E7A2A47C4F0C456F05259A6FA94" +
                        "A634A40DAE1DF593D4FECF688D5FC678" +
                        "BE7EFC6DF3D6835325B83B2C6E69036B" +

                        "155F0A27241094B04BFB0BDFAC6C670A" +
                        "65C325D39A069F03659D44CA27D3BE8D" +
                        "F311172B554160181CBE94A2A783320C" +
                        "ED590BC42644702CF371271E496BF20F" +
                        "588B78A1BC01ECBB6559934BDD2FB65D" +
                        "2884318A33D1A42ADF5E33CC5800280B" +
                        "28356497F87135BAB9612A1726042440" +
                        "9AC15FEE996B744C332151235DECB0F5");

        // Both alice and bob share the necessary community parameters.
        //
        s.append("\n---- Bob validates his SAKKE receiver key ----\n\n");

        validated = Sakke.validateReceiverSecretKey(bobId, Z, bobRSK, 1);

        s.append("Validated: " + validated + "\n");

        s.append("RSK: " + bobRSK + "\n");

        s.append("\n---- Alice signs message for Bob: ----\n\n");

        OctetString message = OctetString.fromAscii("message");
        message.appendNullTerminator();

        class EphemeralFrom6507 implements RandomGenerator// define a fixed
                                                          // 'random' function
        {
            public OctetString generate(final int p)
            {
                return OctetString.fromHex("34567");
            }
        }
        ;

        RandomGenerator r = new EphemeralFrom6507();

        // message.getOctets().length
        OctetString signature = Eccsi.sign(message, alicePVT, aliceSSK, aliceHS, r);

        s.append("Message Text: " + message.toAscii() + "\n");
        s.append("Message Octets: " + message + "\n");
        s.append("ECCSI Signature: " + signature + "\n");

        s.append("\n---- Bob verifies message from Alice: ----\n\n");

        boolean verified = Eccsi.verify(message, signature, aliceId, kpak);

        s.append("Verified: " + verified + "\n");

        s.append("\n---- Alice sends shared secret to Bob: ----\n\n");

        class SSVFrom6508 implements RandomGenerator// define a fixed 'random'
                                                    // function
        {
            public OctetString generate(final int p)
            {
                return OctetString.fromHex("123456789ABCDEF0123456789ABCDEF0");
            }
        }
        ;

        r = new SSVFrom6508();

        OctetString encrypted = new OctetString();
        OctetString alice_ssv = Sakke.generateSharedSecretAndSED(encrypted, bobId, 1, Z, r);

        s.append("Alice SSV: " + alice_ssv + "\n");
        s.append("SAKKE Encapsulated Data: " + encrypted + "\n");

        s.append("\n---- Bob extracts shared secret: ----\n\n");

        OctetString bob_ssv = Sakke.extractSharedSecret(encrypted, bobId, 1, bobRSK, Z);

        s.append("Bob SSV:  " + bob_ssv + "\n");

        s.append("\n---- Check equivalence: ----\n\n");

        s.append("Alice SSV == Bob SSV:  " + (alice_ssv.equals(bob_ssv)) + "\n");

        return s.toString();
    }
}
