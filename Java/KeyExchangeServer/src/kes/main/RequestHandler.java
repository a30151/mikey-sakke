package kes.main;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Random;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.Sakke;
import mikeysakke.utils.OctetString;
import mikeysakke.utils.RandomGenerator;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class RequestHandler implements HttpHandler {

	private MainForm m_MainForm;

	private static final String DEVICE_ID = "id";
	private static final String TO_DEVICE_ID = "toid";
	private static final String SED = "sed";
	private static final String SIGNATURE = "signature";

	public RequestHandler(MainForm form) {
		m_MainForm = form;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String requestMethod = exchange.getRequestMethod();
		if (requestMethod.equalsIgnoreCase("GET")) {
			// find what method has been called
			URI requestURI = exchange.getRequestURI();
			String file = requestURI.getPath().substring(1);

			if (file.equalsIgnoreCase("CHECK")) {
				CheckForKeys(exchange);
			} else if (file.equalsIgnoreCase("SEND")) {
				SendKey(exchange);
			} else if (file.equalsIgnoreCase("VALIDATE")) {
				validateKeys(exchange);
			} else if (file.equalsIgnoreCase("GENERATE")) {
				generateSedSsv(exchange);
			} else if (file.equalsIgnoreCase("SIGN")) {
				signMessage(exchange);
			} else if (file.equalsIgnoreCase("VERIFY")) {
				verifySignature(exchange);
			} else if (file.equalsIgnoreCase("EXTRACT")) {
				extractSsv(exchange);
			} else {
				Unknown(exchange);
			}
		}
	}
	
	/**
	 * Random Generator class using in generating an SSV
	 */
	class SsvRandomGenerator implements RandomGenerator
	{
		@Override
		public OctetString generate(int size) {
			byte[] buffer = new byte[size];
			
			Random rand = new Random();			
			rand.nextBytes(buffer);
			
			OctetString os = new OctetString(buffer);
			return os;
		}
	}
	
	/**
	 * "Random" Generator used for SSV/SED generate testing purposes
	 */
	class TestSsvRandomGenerator implements RandomGenerator
	{
		@Override
		public OctetString generate(int size) {
			return OctetString.fromHex("123456789ABCDEF0123456789ABCDEF0");
		}
	}
	
	/**
	 * "Random" Generator used for Sign testing purposes
	 */
	class TestSignRandomGenerator implements RandomGenerator
    {
        @Override
        public OctetString generate(final int n)
        {
            return OctetString.fromHex("34567");
        }
    }
	
	/**
	 * Call on MSLibrary methods for validating keys received from KMS
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void validateKeys(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("VALIDATE: message recieved: "
				+ exchange.getRequestURI().getRawQuery());

		JSONObject retVal = new JSONObject();

		HashMap<String, String> params = parseQuery(exchange.getRequestURI()
				.getRawQuery());
		try {
			if (params.containsKey("id") && params.containsKey("pvt")
					&& (params.containsKey("kpak"))
					&& (params.containsKey("ssk")) && (params.containsKey("z"))
					&& (params.containsKey("rsk"))
					&& (params.containsKey("parameterSet"))) {

				int paramSet = Integer.parseInt(params.get("parameterSet"));
				//String id = params.get("id").replace("%00", "\0");
				
				boolean validateReceiverKey;
				boolean validateSigningKey;
				

				OctetString hs = new OctetString();
				validateSigningKey = Eccsi.validateSigningKeys(
						OctetString.fromAscii(params.get("id")),
						OctetString.fromHex(params.get("pvt")),
						OctetString.fromHex(params.get("kpak")),
						OctetString.fromHex(params.get("ssk")), hs);
			
				validateReceiverKey = Sakke.validateReceiverSecretKey(
							OctetString.fromAscii(params.get("id")),
							OctetString.fromHex(params.get("z")),
							OctetString.fromHex(params.get("rsk")), paramSet);

				retVal.put("validated", validateSigningKey && validateReceiverKey);
				retVal.put("hs", hs.toHex());
				
				m_MainForm.addLog("VALIDATE complete");
			} else {
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}

		Headers responseHeaders = exchange.getResponseHeaders();
		responseHeaders.set("Content-Type", "text/plain");
		exchange.sendResponseHeaders(200, 0);

		OutputStream responseBody = exchange.getResponseBody();
		responseBody.write(retVal.toString().getBytes());
		responseBody.close();
		exchange.close();
	}

	/**
	 * Call on MSLibrary method for generating a SED and SSV
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void generateSedSsv(HttpExchange exchange) throws IOException{
		m_MainForm.addLog("GENERATE: message recieved: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		try {
			if (params.containsKey("targetId") && params.containsKey("parameterSet")
					&&(params.containsKey("z"))){
				
				RandomGenerator r;
				if(params.containsKey("test") && 
						Boolean.parseBoolean(params.get("test"))==true){
					r = new TestSsvRandomGenerator();
				}
				else{
					r = new SsvRandomGenerator();
				}
				
				int paramSet = Integer.parseInt(params.get("parameterSet"));
				OctetString sed = new OctetString();
				
				OctetString ssv = 
					Sakke.generateSharedSecretAndSED(
						sed, 
						OctetString.fromAscii(params.get("targetId")), 
						paramSet, 
						OctetString.fromHex(params.get("z")), 
						r);
				retVal.put("ssv", ssv.toHex());
				retVal.put("sed", sed.toHex());
				
				m_MainForm.addLog("GENERATE complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();	
	}

	/**
	 * Call on MSLibrary for signing a message
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void signMessage(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("SIGN: message recieved: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		try {
			if (params.containsKey("message") && params.containsKey("pvt")
					&&(params.containsKey("ssk")) && (params.containsKey("hs"))
					&& (!params.get("hs").isEmpty())){

				RandomGenerator r;
				if(params.containsKey("test") && 
						Boolean.parseBoolean(params.get("test"))==true){
					r = new TestSignRandomGenerator();
				}
				else{
					r = new SsvRandomGenerator();
				}
				
				OctetString signature = Eccsi.sign(
						OctetString.fromAscii(params.get("message")),
						OctetString.fromHex(params.get("pvt")), 
						OctetString.fromHex(params.get("ssk")), 
						OctetString.fromHex(params.get("hs")), 
						r);
				
				retVal.put("signature", signature.toHex());
				
				m_MainForm.addLog("SIGN complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();	
	}

	/**
	 * Call on MSLibrary method for verifying signature against a message
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void verifySignature(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("VERIFY: message recieved: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		try {
			if (params.containsKey("message") && params.containsKey("signature")
					&&(params.containsKey("signerId")) && (params.containsKey("kpak"))){

				boolean verify = Eccsi.verify(
						OctetString.fromAscii(params.get("message")), 
						OctetString.fromHex(params.get("signature")), 
						OctetString.fromAscii(params.get("signerId")), 
						OctetString.fromHex(params.get("kpak")));
				
				retVal.put("verified", verify);
				
				m_MainForm.addLog("VERIFY complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();
		
	}

	/**
	 * Call on MSLibrary method for extracting a shared secret value
	 * and return result to requesting client as a JSON
	 * @param exchange
	 * @throws IOException
	 */
	private void extractSsv(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("EXTRACT: message recieved: " + exchange.getRequestURI().getRawQuery());
		
		JSONObject retVal = new JSONObject();
		
		HashMap<String,String> params = parseQuery(exchange.getRequestURI().getRawQuery());
		try {
			if (params.containsKey("sed") && params.containsKey("id")
					&&(params.containsKey("parameterSet")) && (params.containsKey("rsk"))
					&&(params.containsKey("z"))){
				
				int paramSet = Integer.parseInt(params.get("parameterSet"));

				OctetString ssv = Sakke.extractSharedSecret(
						OctetString.fromHex(params.get("sed")),
						OctetString.fromAscii(params.get("id")),
						paramSet,
						OctetString.fromHex(params.get("rsk")),
						OctetString.fromHex(params.get("z")));
				
				if (ssv != null){
					retVal.put("ssv", ssv.toHex());
				}
				else{
					retVal.put("ssv","null");
				}
				
				m_MainForm.addLog("EXTRACT complete");	
			}
			else{
				retVal.put("error", "Incorrect request parameters");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}
		
   	    Headers responseHeaders = exchange.getResponseHeaders(); 
        responseHeaders.set("Content-Type", "text/plain"); 
        exchange.sendResponseHeaders(200, 0); 
        
        OutputStream responseBody = exchange.getResponseBody(); 
        responseBody.write(retVal.toString().getBytes());
        responseBody.close();
        exchange.close();
	}

	/**
	 * Standard ping for each device to call, adds the device to the active list
	 * will return a list of active devices and any messages for the device
	 */
	private void CheckForKeys(HttpExchange exchange) throws IOException {

		m_MainForm.addLog("CHECK: message recieved: "
				+ exchange.getRequestURI().getRawQuery());

		JSONObject retVal = new JSONObject();

		// Check to see if a device ID has been included in the request
		HashMap<String, String> params = parseQuery(exchange.getRequestURI()
				.getRawQuery());
		try {
			if (params.containsKey(DEVICE_ID)) {

				m_MainForm.updateDevice(params.get(DEVICE_ID));

				// return a list of online device IDs
				JSONArray onlineDevices = m_MainForm.getOnlineDevices();
				retVal.put("devices", onlineDevices);

				// return a list of messages for the device
				JSONArray messages = m_MainForm.getMessages(params
						.get(DEVICE_ID));
				retVal.put("messages", messages);

				m_MainForm.addLog("CHECK: reply sent to "
						+ params.get(DEVICE_ID) + ", " + onlineDevices.length()
						+ " devices" + ", " + messages.length() + " messages");
			} else {
				// No ID supplied
				retVal.put("error", "No ID supplied");
				m_MainForm.addLog("CHECK: Error, no ID supplied");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}

		Headers responseHeaders = exchange.getResponseHeaders();
		responseHeaders.set("Content-Type", "text/plain");
		exchange.sendResponseHeaders(200, 0);

		OutputStream responseBody = exchange.getResponseBody();
		responseBody.write(retVal.toString().getBytes());
		responseBody.close();
		exchange.close();
	}

	/**
	 * Called when a device wants to send a key to another device Adds the key
	 * to the list of messages on the server
	 * */
	private void SendKey(HttpExchange exchange) throws IOException {

		m_MainForm.addLog("SEND: message recieved: "
				+ exchange.getRequestURI().getRawQuery());

		JSONObject retVal = new JSONObject();

		// Check to see if a device ID has been included in the request
		HashMap<String, String> params = parseQuery(exchange.getRequestURI()
				.getRawQuery());
		try {
			if (params.containsKey(DEVICE_ID)) {
				// This is the "from" device ID, update the last heard from date
				m_MainForm.updateDevice(params.get(DEVICE_ID));

				if (params.containsKey(TO_DEVICE_ID)) {
					if (params.containsKey(SED)) {
						if (params.containsKey(SIGNATURE)) {

							// Check that the device is active
							if (m_MainForm.checkDeviceList(params
									.get(TO_DEVICE_ID))) {

								m_MainForm.addMessage(params.get(DEVICE_ID),
										params.get(TO_DEVICE_ID),
										params.get(SED), params.get(SIGNATURE));

								// return a confirm message
								retVal.put("success", "Message sent to "
										+ params.get(TO_DEVICE_ID));

								m_MainForm.addLog("SEND: message for "
										+ params.get(TO_DEVICE_ID)
										+ " added to message list");

							} else {
								// Device supplied is not active
								retVal.put("error",
										"Device " + params.get(TO_DEVICE_ID)
												+ " is not active");
								m_MainForm.addLog("SEND: Error device "
										+ params.get(TO_DEVICE_ID)
										+ " not active");
							}

						} else {
							// No message supplied
							retVal.put("error", "No signature supplied");
							m_MainForm
									.addLog("SEND: Error, no signature supplied");
						}
					} else {
						// No message supplied
						retVal.put("error", "No SED supplied");
						m_MainForm.addLog("SEND: Error, no SED supplied");
					}
				} else {
					// No ID supplied
					retVal.put("error", "No to ID supplied");
					m_MainForm.addLog("SEND: Error, no to ID supplied");
				}
			} else {
				// No ID supplied
				retVal.put("error", "No ID supplied");
				m_MainForm.addLog("SEND: Error, no ID supplied");
			}
		} catch (JSONException e) {
			m_MainForm.addLog(e.getMessage());
		}

		Headers responseHeaders = exchange.getResponseHeaders();
		responseHeaders.set("Content-Type", "text/plain");
		exchange.sendResponseHeaders(200, 0);

		OutputStream responseBody = exchange.getResponseBody();
		responseBody.write(retVal.toString().getBytes());
		responseBody.close();
		exchange.close();
	}

	/**
	 * Called when an invalid request is made, return a 404 error
	 * */
	private void Unknown(HttpExchange exchange) throws IOException {
		m_MainForm.addLog("404: " + exchange.getRequestURI().toString());

		Headers responseHeaders = exchange.getResponseHeaders();
		responseHeaders.set("Content-Type", "text/plain");
		exchange.sendResponseHeaders(404, 0);
		exchange.close();
	}

	/**
	 * Parses the URL request parameters
	 * */
	private HashMap<String, String> parseQuery(String query)
			throws UnsupportedEncodingException {

		HashMap<String, String> parameters = new HashMap<String, String>();

		if (query != null) {
			String pairs[] = query.split("[&]");

			for (String pair : pairs) {
				String param[] = pair.split("[=]");

				String key = null;
				String value = null;
				if (param.length > 0) {
					key = URLDecoder.decode(param[0],
							System.getProperty("file.encoding"));
				}

				if (param.length > 1) {
					value = URLDecoder.decode(param[1],
							System.getProperty("file.encoding"));
				}

				// Only allow a single parameter with the same name
				// (the first ones get used, the others are discarded)
				if (!parameters.containsKey(key)) {
					parameters.put(key, value);
				}
			}
		}

		return parameters;
	}
}
