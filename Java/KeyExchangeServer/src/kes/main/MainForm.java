package kes.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.sun.net.httpserver.HttpServer;

public class MainForm extends JFrame {

	private static final long serialVersionUID = 5022210823534749075L;

	/** The server that is running  */
	private HttpServer m_Server = null;
	
	/** List of log messages that are displayed */
	private StringBuilder m_LogBuilder = new StringBuilder();
	/** List of devices that are active*/
	private ArrayList<Device> m_ActiveDevices = new ArrayList<Device>();
	/** List of messages to be sent out */
	private HashMap<String, ArrayList<Message>> m_Messages = new HashMap<String, ArrayList<Message>>();
	
	/** References to the UI elements that get updated */
	private JTextArea m_LogText;
	private JTextArea m_DevicesText;
	private JTextArea m_MessagesText;
	
	/** Date formatter for the log message viewer */
	private DateFormat m_DateFormat = new SimpleDateFormat("HH:mm:ss"); 

	/**
	 * Constructor, builds the form elements
	 * */
	public MainForm() {
	
		this.setSize(520, 570);		
		this.setLayout(null);
		
		this.setTitle("Key Exchange Server");
		
		// Add a log window
		JLabel logLabel = new JLabel();
		logLabel.setLocation(10, 10);
		logLabel.setSize(380, 20);
		logLabel.setText("log");
		this.getContentPane().add(logLabel);
		m_LogText = new JTextArea();
		m_LogText.setLocation(0, 0);
		m_LogText.setSize(480, 280);
		m_LogText.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(m_LogText); 
		scrollPane.setLocation(10, 30);
		scrollPane.setSize(480, 280);
		this.getContentPane().add(scrollPane);
		
		// Add a devices list
		JLabel devicesLabel = new JLabel();
		devicesLabel.setLocation(10, 310);
		devicesLabel.setSize(200, 20);
		devicesLabel.setText("devices");
		this.getContentPane().add(devicesLabel);
		m_DevicesText = new JTextArea();
		m_DevicesText.setLocation(0, 0);
		m_DevicesText.setSize(200, 150);
		m_DevicesText.setEditable(false);
		scrollPane = new JScrollPane(m_DevicesText); 
		scrollPane.setLocation(10, 330);
		scrollPane.setSize(200, 150);
		this.getContentPane().add(scrollPane);
		
		// Add a messages list
		JLabel messagesLabel = new JLabel();
		messagesLabel.setLocation(220, 310);
		messagesLabel.setSize(100, 20);
		messagesLabel.setText("messages");
		this.getContentPane().add(messagesLabel);
		m_MessagesText = new JTextArea();
		m_MessagesText.setLocation(0, 0);
		m_MessagesText.setSize(270, 150);
		m_MessagesText.setEditable(false);
		scrollPane = new JScrollPane(m_MessagesText); 
		scrollPane.setLocation(220, 330);
		scrollPane.setSize(270, 150);
		this.getContentPane().add(scrollPane);
		
		// Add a button to start the server
		JButton startButton = new JButton();
		startButton.setLocation(10, 490);
		startButton.setSize(100, 25);
		startButton.setText("Start");
		startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				StartServer();
			}
		});
		this.getContentPane().add(startButton);
		
		// Add a button to stop the server
		JButton stopButton = new JButton();
		stopButton.setLocation(120, 490);
		stopButton.setSize(100, 25);
		stopButton.setText("Stop");
		stopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				StopServer();
			}
		});
		this.getContentPane().add(stopButton);
		
		// Add a button to clear all data from the server
		JButton clearButton = new JButton();
		clearButton.setLocation(230, 490);
		clearButton.setSize(100, 25);
		clearButton.setText("Clear");
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ClearAllData();
			}
		});
		this.getContentPane().add(clearButton);
		
		// Stop the server if the window is closed
		this.addWindowListener(new WindowListener() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				StopServer();
			}
			
			public void windowOpened(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowDeactivated(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			public void windowActivated(WindowEvent e) {}
		});
				 
				 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		StartServer();
	}
	
	/**
	 * Updates the last contact time on the given device
	 * Will add the device if it is not in the list
	 */
	public void updateDevice(String deviceId) {
		synchronized (m_ActiveDevices) {
			for (Device d : m_ActiveDevices) {
				if ( d.getDeviceId().equals(deviceId)) {
					d.setLastContactTime(new Date());
					return;
				}
			}
			
			Device newDevice = new Device();
			newDevice.setDeviceId(deviceId);
			newDevice.setLastContactTime(new Date());
			m_ActiveDevices.add(newDevice);
			
			updateDeviceList();
		}
	}
	
	/**
	 * Checks to see if a particular device ID is active
	 * */
	public boolean checkDeviceList(String deviceId) {
		
		synchronized (m_ActiveDevices) {
			for (Device d : m_ActiveDevices) {
				if ( d.getDeviceId().equals(deviceId)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the entire active device list as a JSON array
	 * */
	public JSONArray getOnlineDevices() {
		JSONArray retVal = new JSONArray();
		
		synchronized (m_ActiveDevices) {
			for (Device d : m_ActiveDevices) {
				retVal.put(d.getDeviceId());
			}
		}
		
		return retVal;
	}
	
	/**
	 * Outputs the active device list to the screen
	 * */
	private void updateDeviceList() {
		synchronized (m_ActiveDevices) {
			// update the devices list to display the new device
			StringBuilder builder = new StringBuilder();
			for (Device d : m_ActiveDevices) {
				builder.append(d.getDeviceId());
				builder.append("\r\n");
			}
			m_DevicesText.setText(builder.toString());		
		}
	}
	
	/**
	 * Adds a message to the messages list
	 * */
	public void addMessage(String fromDeviceId, String toDeviceId, String sakkeEncryptedData, String signature) {
		synchronized (m_Messages) {
			if (!m_Messages.containsKey(toDeviceId)) {
				m_Messages.put(toDeviceId, new ArrayList<Message>());
			}
			
			m_Messages.get(toDeviceId).add(new Message(fromDeviceId, toDeviceId, sakkeEncryptedData, signature));
			
			updateMessageList();
		}
	}
	
	/**
	 * Returns the messages list for the given device ID as a JSON array
	 * */
	public JSONArray getMessages(String toDeviceId) {
		JSONArray retVal = new JSONArray();
		
		synchronized (m_Messages) {
			if (!m_Messages.containsKey(toDeviceId)) {
				m_Messages.put(toDeviceId, new ArrayList<Message>());
			}
			
			try {
				for (Message message : m_Messages.get(toDeviceId)) {
					JSONObject jMessage = new JSONObject();
					jMessage.put("from", message.getFromDeviceId());
					jMessage.put("sed", message.getSakkeEncryptedData());
					jMessage.put("signature", message.getSignature());
					retVal.put(jMessage);
				}
			} catch (JSONException e) {
				addLog(e.getMessage());
			}				
			
			m_Messages.remove(toDeviceId);
			
			updateMessageList();
		}
		
		return retVal;
	}
	
	/**
	 * Outputs the message list to the screen
	 * */
	private void updateMessageList() {
		synchronized (m_Messages) {
			// update the messages list to display the new message counts
			StringBuilder builder = new StringBuilder();
			for ( String device : m_Messages.keySet()) {
				builder.append(device);
				builder.append(" : ");
				builder.append(m_Messages.get(device).size());
				builder.append("\r\n");
			}
			m_MessagesText.setText(builder.toString());		
		}
	}
	
	/**
	 * Adds a new log and outputs the log list to the screen
	 * */	
	public void addLog(String message) {
		
		synchronized (m_LogBuilder) {

			m_LogBuilder.append(m_DateFormat.format(new Date()));
			m_LogBuilder.append(" : ");
			m_LogBuilder.append(message);
			m_LogBuilder.append("\r\n");
			
			m_LogText.setText(m_LogBuilder.toString());		
			m_LogText.setCaretPosition(m_LogText.getText().length());
		}
	}
	
	/**
	 * Starts the HTTP server
	 * */
	private void StartServer() {
		
		if ( m_Server != null ) {
			addLog("Server already running");		
		} else {
			try
		    {
	            // setup the socket address
	            InetSocketAddress address = new InetSocketAddress ( InetAddress.getLocalHost (), 7171 );

	            // initialise the HTTP server
	            m_Server = HttpServer.create( address, 0 );
	            
	            m_Server.createContext("/", new RequestHandler(this) );     
	            
	        }
	        catch ( Exception ex )
	        {
	        	addLog("StartServer Error: " + ex.getMessage());
	        }
			
	        m_Server.start();
	        addLog("Server started.");
		}
	}
	
	/**
	 * Stops the HTTP server
	 * */
	private void StopServer() {
		if ( m_Server != null ) {
			m_Server.stop(0);		
			m_Server = null;
			
			addLog("Server stopped.");		
		}
	}
	
	/**
	 * Clears all data off of the server
	 * */
	private void ClearAllData() {
		synchronized (m_ActiveDevices) {
			m_ActiveDevices.clear();
			updateDeviceList();
		}
		
		synchronized (m_Messages) {
			m_Messages.clear();
			updateMessageList();
		}
		
		synchronized (m_LogBuilder) {
			m_LogBuilder = new StringBuilder();
			addLog("Server data cleared.");
		}
	}
}
