package kes.main;

import java.util.Date;

public class Device {

	private String m_DeviceId;
	private Date m_LastContactTime;
	
	public String getDeviceId() {
		return m_DeviceId;
	}
	public void setDeviceId(String deviceId) {
		m_DeviceId = deviceId;
	}
	public Date getLastContactTime() {
		return m_LastContactTime;
	}
	public void setLastContactTime(Date lastContactTime) {
		m_LastContactTime = lastContactTime;
	}
}
