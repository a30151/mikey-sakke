package mikeysakke.android.src;

import mikeysakke.android.sakketestandroid.R;
import mikeysakke.kms.client.android.Client;
import mikeysakke.kms.client.android.KeyData;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class StartActivity extends Activity {

    /** KMS Public Authentication key */
    private KeyData m_KeyData;
    
    /** The ID textbox */
    EditText m_IdTextbox;
    Button m_ConnectButton;
    Button m_UseExistingButton;
    
    private static final String IP_ADDRESS = "192.168.1.3";
    private static final Integer KMS_PORT = 7070;   
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);

        // Setup the button listeners
        m_ConnectButton = (Button)findViewById(R.id.connectButton);
        m_ConnectButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				requestKey();
			}
		});
        
        m_UseExistingButton = (Button)findViewById(R.id.useExistingButton);
        m_UseExistingButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				openMainActivity();
			}
		});
        
        // Check the database for existing key data
        DatabaseAdapter adapter = new DatabaseAdapter(this);
        m_KeyData = adapter.getKeyData(this);
                
        // Populate the Identifier text box
        m_IdTextbox = (EditText)findViewById(R.id.identifierTextbox);
        m_IdTextbox.setText(m_KeyData.getIdentifier());        
		
        // if keys are already saved then skip to the main activity
        openMainActivity();      

        
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    
    @Override
	protected void onResume() {
		super.onResume();
		
		if (!inProgress()) {
	        // Check the database for existing key data
	        DatabaseAdapter adapter = new DatabaseAdapter(this);
	        m_KeyData = adapter.getKeyData(this);
	                
	        // Populate the Identifier text box
	        m_IdTextbox = (EditText)findViewById(R.id.identifierTextbox);
	        m_IdTextbox.setText(m_KeyData.getIdentifier());    
	        
	        
	        if ( m_KeyData.getSecretSigningKey() != null && !m_KeyData.getSecretSigningKey().equals("") ) {
	        	updateStatus("Keys verified"); 
	        	m_UseExistingButton.setEnabled(true);
	        } else {
	        	updateStatus("No keys found"); 
	        	m_UseExistingButton.setEnabled(false);
	        }
		}
	}

	/**
     * Called when the "Request" button is pushed. 
     * Gets new key data from the KMS and verifies it
     * */
    public void requestKey() {   
    	
    	// Erase current key data
    	m_KeyData = new KeyData();
    	m_KeyData.setIdentifier(m_IdTextbox.getText().toString());    	
    	
    	updateStatus("Requesting key data...");
    	toggleProgress(true);
        RetreiveKeyTask task = new RetreiveKeyTask();
        task.execute();
    }
    
    /**
     * Class to request keys from the KMS and verify them using the java library
     * */
    class RetreiveKeyTask extends AsyncTask<String, Void, String> {
    	
        protected String doInBackground(String... urls) {
            String response = "";
           
            try {
        		// Use the KMS client library to communicate with the KMS server
        	    Client client = new Client(IP_ADDRESS, KMS_PORT, "jim", "jimpass", m_KeyData.getIdentifier());
        	    
        	    // get the keys
        	    KeyData testKeys = client.fetchKeyMaterial();
                
                // Check the signature (SSK)
                updateStatus("Verifying key signature...");
                boolean validatedEccsi = client.validateSigningKeys();

                // Check the key (RSK)
                updateStatus("Verifying key data...");
                boolean validatedSakke = client.validateReceiverKey();
            
            	if ( validatedEccsi && validatedSakke ) {
            		updateStatus("Keys verified");     
            		
            		m_KeyData = testKeys;
            		
            		// Save the data now it is verified
                    DatabaseAdapter adapter = new DatabaseAdapter(StartActivity.this);
                    adapter.setKeyData(m_KeyData);   
            	} else {
            		updateStatus("Key verify failed"); 
            	}
			} catch (Exception e) {
				updateStatus(e.getMessage());   
				e.printStackTrace();
			}

            return response;
        }

        protected void onPostExecute(String result) {
        	toggleProgress(false);
        	
        	openMainActivity();
        }
    }   
    
    private void openMainActivity() {
    	
        if ( m_KeyData.getSecretSigningKey() != null && !m_KeyData.getSecretSigningKey().equals("") ) {
        	this.runOnUiThread(new Runnable() {
    			
    			@Override
    			public void run() {
    				Intent i = new Intent(StartActivity.this, TabbedActivity.class);    
    				startActivity(i);				
    			}
    		});
        }        
    }
    
    /**
     * Changes the text displayed on the status label on the main screen
     * */
    private void updateStatus(final String status) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
		    	TextView label = (TextView)StartActivity.this.findViewById(R.id.statusLabel);
		    	label.setText(status);				
			}
		});
    }

    /**
     * Turns the progress bar on or off, also disables the controls when the progress bar is moving
     * */
    private void toggleProgress(final boolean started) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {				
		    	ProgressBar progress = (ProgressBar)StartActivity.this.findViewById(R.id.progressBar1);
	    	
		    	if (started) {
		    		progress.setVisibility(View.VISIBLE);
		    		m_IdTextbox.setEnabled(false);
		    		m_UseExistingButton.setEnabled(false);
		    		m_ConnectButton.setEnabled(false);
		    		
		    	} else {
		    		progress.setVisibility(View.GONE);
		    		m_ConnectButton.setEnabled(true);
		    		
		            if ( m_KeyData.getSecretSigningKey() == null 
			          || m_KeyData.getSecretSigningKey().equals("") ) {
		            	m_IdTextbox.setEnabled(true);		            	
		            } else {
		            	m_UseExistingButton.setEnabled(true);
		            }
		    	}
			}
		});
    }
    
    /**
     * Returns true if the progress bar is being displayed
     * */
    private boolean inProgress() {
		ProgressBar progress = (ProgressBar)StartActivity.this.findViewById(R.id.progressBar1);
		if (progress != null && progress.getVisibility() == View.VISIBLE) {	
			return true;
		}
    	
		return false;
    }
}
