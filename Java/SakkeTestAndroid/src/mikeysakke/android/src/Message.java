package mikeysakke.android.src;

/**
 * Class to store details of a message sent to, or received from the KES
 * */
public class Message {

	private String m_FromDeviceId;
	private String m_ToDeviceId;
	private String m_SakkeEncryptedData;
	private String m_Signature;
	
	public Message(String fromDeviceId, String toDeviceId, String sakkeEncryptedData, String signature) {
		m_FromDeviceId = fromDeviceId;
		m_ToDeviceId = toDeviceId;
		m_SakkeEncryptedData = sakkeEncryptedData;
		m_Signature = signature;
	}
	
	public String getFromDeviceId() {
		return m_FromDeviceId;
	}
	public void setFromDeviceId(String fromDeviceId) {
		this.m_FromDeviceId = fromDeviceId;
	}
	public String getToDeviceId() {
		return m_ToDeviceId;
	}
	public void setToDeviceId(String toDeviceId) {
		this.m_ToDeviceId = toDeviceId;
	}
	public String getSakkeEncryptedData() {
		return m_SakkeEncryptedData;
	}
	public void setSakkeEncryptedData(String m_SakkeEncryptedData) {
		this.m_SakkeEncryptedData = m_SakkeEncryptedData;
	}
	public String getSignature() {
		return m_Signature;
	}
	public void setSignature(String m_Signature) {
		this.m_Signature = m_Signature;
	}	
}
