package mikeysakke.android.src;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import mikeysakke.android.sakketestandroid.R;
import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.Sakke;
import mikeysakke.kms.client.android.KeyData;
import mikeysakke.utils.OctetString;
import mikeysakke.utils.RandomGenerator;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

/**
 * Main activity of the application that displays 3 tabs: keys, devices and SSVs
 * */
public class TabbedActivity extends TabActivity {
	
	/** Keys read from the database*/
    private KeyData m_KeyData;
    /** Devices list that is accessed only in the UI thread */
    private ArrayList<String> m_ActiveDevices = new ArrayList<String>();
	/** Local list of SSVs */
    private  ArrayList<String> m_SSVs = new  ArrayList<String>();
    /** Time task to periodically poll the KES */
    private Timer m_PollKesTimer;
    
    /** KES server IP Address */
    private static final String KES_IP_ADDRESS = "192.168.1.3";
    /** KES server Port */
    private static final Integer KES_PORT = 7171;  
    /** Hard-coded date string (in the final system the keys will rotate every month)*/
    private static final String DATE_STRING = "2012-09";
    
	/** Date formatter for the log message viewer */
	private DateFormat m_DateFormat = new SimpleDateFormat("HH:mm:ss"); 
	
	@Override
	public void onCreate(Bundle savedInstanceState) { 
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.main); 
  
        TabHost tabHost = getTabHost(); 
  
        // Tab for Keys from the KMS 
        TabSpec spec = tabHost.newTabSpec("Keys"); 
        spec.setIndicator("Keys"); 
        getLayoutInflater().inflate(R.layout.keys_tab, tabHost.getTabContentView(), true); 
        spec.setContent(R.id.keys_tab); 
        tabHost.addTab(spec); 

        // Tab for Keys from the KMS 
        spec = tabHost.newTabSpec("Devices"); 
        spec.setIndicator("Devices"); 
        getLayoutInflater().inflate(R.layout.devices_tab, tabHost.getTabContentView(), true); 
        spec.setContent(R.id.devices_tab); 
        tabHost.addTab(spec); 
        
        // Tab for Keys from the KMS 
        spec = tabHost.newTabSpec("SSVs"); 
        spec.setIndicator("SSVs"); 
        getLayoutInflater().inflate(R.layout.ssvs_tab, tabHost.getTabContentView(), true); 
        spec.setContent(R.id.ssvs_tab); 
        tabHost.addTab(spec); 
                
		// Setup the list view to display known active devices
        ListView lv = (ListView)TabbedActivity.this.findViewById(R.id.devicesList);
        
        lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)  {
				
				// Don't allow clicks if another process is running
				if (!inProgress()) {
					String deviceId = null;
					
					// Get the device ID that was clicked on
					synchronized (m_ActiveDevices) {
						if ( m_ActiveDevices.size() > position ) {
							deviceId = m_ActiveDevices.get(position);						
						}
					}
									
					if ( deviceId != null ) {
						SendMessage(deviceId);
					}
				}
			}
		});
        
        DatabaseAdapter adapter = new DatabaseAdapter(this);
        m_KeyData = adapter.getKeyData(this);
        
        populateKeyDataTab();
        populateDevicesTab();
        populateSSVsTab();

        // Start a timer to poll the KES
        m_PollKesTimer = new Timer();
        m_PollKesTimer.scheduleAtFixedRate(new pollKesTimerTask(), 0, 5000);
        
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    } 

	@Override
	protected void onDestroy() {
		// Stop the poll thread when the activity is destroyed
		m_PollKesTimer.cancel();
		
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.clear_menu_item:
	            clearKeys();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	/**
	 * Puts the keys into the text boxes on the key data tab
	 * */
	private void populateKeyDataTab() {
		
		EditText kmsIDText = (EditText)findViewById(R.id.kms_id_text);
		kmsIDText.setText(m_KeyData.getKmsIdentifier());
		
		EditText deviceIdText = (EditText)findViewById(R.id.device_id_text);
		deviceIdText.setText(m_KeyData.getIdentifier());
		
		EditText kpakText = (EditText)findViewById(R.id.kpak_text);
		kpakText.setText(m_KeyData.getPublicAuthenticationKey());
		
		EditText kpkText = (EditText)findViewById(R.id.kms_public_text);
		kpkText.setText(m_KeyData.getKmsPublicKey());
		
		EditText sakkeText = (EditText)findViewById(R.id.sakke_param_index_text);
		sakkeText.setText(m_KeyData.getSakkeParameterSetIndex().toString());
		
		EditText pvtText = (EditText)findViewById(R.id.pvt_text);
		pvtText.setText(m_KeyData.getPublicValidationToken());

		EditText rskText = (EditText)findViewById(R.id.rsk_text);
		rskText.setText(m_KeyData.getReceiverSecretKey());
		
		EditText sskText = (EditText)findViewById(R.id.ssk_text);
		sskText.setText(m_KeyData.getSecretSigningKey());
	}
	
	/**
	 * Updates the list of devices based upon the contents of the local arraylist
	 * */
	private void populateDevicesTab() {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {				
				synchronized (m_ActiveDevices) {				
			        ListView lv = (ListView)TabbedActivity.this.findViewById(R.id.devicesList);
			        lv.setAdapter(new ArrayAdapter<String>(TabbedActivity.this, R.layout.simplerow, m_ActiveDevices));
				}				
			}
		});
	}
	
	/**
	 * Updates the list of SSVs based on the contents of the local arraylist
	 * */
	public void populateSSVsTab() {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {				
				synchronized (m_SSVs) {
					
			        DatabaseAdapter adapter = new DatabaseAdapter(TabbedActivity.this);
			        HashMap<String, String> newList = adapter.getDeviceKeys();
					
			        m_SSVs.clear();
					for (String deviceId : newList.keySet()) {
						m_SSVs.add(deviceId + "\n" + newList.get(deviceId));
					}
					
			        ListView lv = (ListView)TabbedActivity.this.findViewById(R.id.ssvsList);
			        lv.setAdapter(new ArrayAdapter<String>(TabbedActivity.this, R.layout.simplerow, m_SSVs));
				}				
			}
		});
	}
	
	/**
     * Erases the key data from the sql database
     * */
    public void clearKeys() {   
    	
    	if ( inProgress() ) {
    		// TODO: show error?
    		
    	} else {
    		// Show a confirm dialog
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setMessage("Clear all keys from the database?");
        	builder.setCancelable(false);
        	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	           	
        	           	   // Erase current key data
        	               DatabaseAdapter adapter = new DatabaseAdapter(TabbedActivity.this);
        	               adapter.clearDeviceKeys();
        	               m_KeyData = adapter.resetkeyData(TabbedActivity.this);
        	               
        	               TabbedActivity.this.finish();
        	           }
        	       });
        	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	AlertDialog alert = builder.create();
        	alert.show();
    	}
    }
    
    /**
     * Class to periodically check the Key Exchange server for messages and update the devices list
     * */
	class pollKesTimerTask extends TimerTask {

		@Override
		public void run() {
			try {
				
				// If there is a process already running then wait until next time to try again
				if ( inProgress() ) {
					return;
				}
				
                HttpClient client = new DefaultHttpClient();
            	
                HttpGet request = new HttpGet();
    			request.setURI(new URI("http://" + KES_IP_ADDRESS + ":" + KES_PORT.toString() + "/check?id=" + m_KeyData.getIdentifier()));
    			
                HttpResponse exec_response = client.execute(request);
                InputStream content = exec_response.getEntity().getContent();

                BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
                String s = buffer.readLine();
                
                String response = "";
                while (s != null) {
                	response += s;
                	s = buffer.readLine();
                }

                // Decode response from KMS
                JSONObject obj = new JSONObject(response);
                if (obj.has("error")) {
                	
                	// If an error occurred then tell the user and exit
                    String errorString = obj.getString("error");
                    updateStatus(errorString);
                } else {

                	// Read the active devices from the message
            		m_ActiveDevices.clear();
                	
                	JSONArray devices = obj.getJSONArray("devices");
                	for (int i = 0; i < devices.length(); i++) {

                		// don't add ourselves to the list
                		String deviceID = devices.get(i).toString();
                		if ( !deviceID.equals(m_KeyData.getIdentifier())) {
                			m_ActiveDevices.add(deviceID);
                		}
                	}                    	

                	populateDevicesTab();
                	updateStatus("Updated devices from KES " + m_DateFormat.format(new Date()));
                	
                	
                	// Check for any messages
                	ArrayList<Message> messages = new ArrayList<Message>();
                	JSONArray jMessages = obj.getJSONArray("messages");
                	for (int i = 0; i < jMessages.length(); i++) {
                		JSONObject jMessage = jMessages.getJSONObject(i);
                
                		String fromDeviceId = jMessage.getString("from");
                		String sed = jMessage.getString("sed");
                		String signature = jMessage.getString("signature");
                		messages.add(new Message(fromDeviceId, m_KeyData.getIdentifier(), sed, signature));
                	}     

                	if (messages.size() > 0) {
                		decodeMessages(messages);
                	}
                }
    			
    		} catch (Exception e) {
    			updateStatus(e.getMessage()); 
    			e.printStackTrace();
    		}			
		}		
	}
	
	/**
     * Displays a dialog box when a message is received checking
     * if it is OK to decode the keys
     * */
    private void decodeMessages(final ArrayList<Message> messages) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {			
				try {
					toggleProgress(true);
					
					// Show an alert dialog that some messages were received
			    	AlertDialog.Builder builder = new AlertDialog.Builder(TabbedActivity.this);
			    	
			    	String alertString = "You have received ";
			    	
			    	for (Message mess : messages) {
			    		alertString += " a key from " + mess.getFromDeviceId();
			    	}
			    	
			    	alertString += ". OK to decode the key(s)?";
			    	
			    	builder.setMessage(alertString);
			    	builder.setCancelable(false);
			    	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			    	           public void onClick(DialogInterface dialog, int id) {
			    	        	   decryptKeysTask task = new decryptKeysTask(messages);
			    	        	   task.execute();
			    	           }
			    	       });
			    	builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			    	           public void onClick(DialogInterface dialog, int id) {
	 		    	        	   toggleProgress(false);
			    	               dialog.cancel();
			    	           }
			    	       });
			    	AlertDialog alert = builder.create();
			    	alert.show();
				} catch (Exception ex) {
					updateStatus(ex.getMessage());
					ex.printStackTrace();
				}
			}
		});
    }
    
    /**
     * Async task to perform the work of decoding the keys and checking the signature
     * */
    class decryptKeysTask extends AsyncTask<String, Void, String> {
    	
    	private ArrayList<Message> m_Messages;
    	
    	public decryptKeysTask(ArrayList<Message> messages) {
    		m_Messages = messages;
    	}
    	
        protected String doInBackground(String... urls) {
        	try {
       	
        		OctetString identifier = OctetString.fromAscii(DATE_STRING + "\0" + m_KeyData.getIdentifier() + "\0");   
        		
                OctetString SSV = new OctetString();
                
        		for (int i = 0; i < m_Messages.size(); i++) {
        			Message message = m_Messages.get(i);
        			
                    OctetString fromIdentifier = OctetString.fromAscii(DATE_STRING + "\0" + message.getFromDeviceId() + "\0");       			
        			
        			OctetString SED = OctetString.fromHex(message.getSakkeEncryptedData());
        			OctetString signature = OctetString.fromHex(message.getSignature());
        			
        			updateStatus("Verifying signature " + (i + 1) + " of " + m_Messages.size());
        			boolean signatureVerified = Eccsi.verify(SED, 
							        					     signature, 
							        					     fromIdentifier, 
							        					     OctetString.fromHex(m_KeyData.getPublicAuthenticationKey()));
        			if ( !signatureVerified) {
        				updateStatus("Signature failed to verify"); 
        				return "";
        			}
        			
        			
        			updateStatus("Extracting key " + (i + 1) + " of " + m_Messages.size());
        			SSV = Sakke.extractSharedSecret(SED, 
			    					                identifier, 
			    					                m_KeyData.getSakkeParameterSetIndex(), 
			    					                OctetString.fromHex(m_KeyData.getReceiverSecretKey()), 
			    					                OctetString.fromHex(m_KeyData.getKmsPublicKey()));
        			
        			if ( SSV.empty() ) {
        				updateStatus("Failed to extract key");
        			} else {
            	        DatabaseAdapter adapter = new DatabaseAdapter(TabbedActivity.this);
            	        adapter.setDeviceKey(message.getFromDeviceId(), SSV.toString());   
        			}     			        		
        		}
                
                updateStatus("Key(s) decoded OK. " + SSV.toString());
                populateSSVsTab();
                
    		} catch (Exception e) {
    			updateStatus(e.getMessage()); 
    			e.printStackTrace();
    		}
        	
        	return "";
        }

        protected void onPostExecute(String result) {
        	toggleProgress(false);
        }
    }  
    
	/**
     * Called when an item in the devices list is clicked on
     * displays a dialog box confirming the key generation and send
     * */
    private void SendMessage(final String toDeviceId) {
		// Show a confirm dialog to generate and send a key to the selected device
    	toggleProgress(true);
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Generate and send a new SSV to " + toDeviceId + "?");
    	builder.setCancelable(false);
    	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   sendMessageTask task = new sendMessageTask(toDeviceId);
    	        	   task.execute();
    	           }
    	       });
    	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   	toggleProgress(false);
    	        	    dialog.cancel();
    	           }
    	       });
    	AlertDialog alert = builder.create();
    	alert.show();
    }
    
    /**
     * Class to generate random bytes to use for SSVs 
     * */
    class SsvRandomGenerator implements RandomGenerator
	{
		@Override
		public OctetString generate(int size) {
			byte[] buffer = new byte[size];
			
			Random rand = new Random();			
			rand.nextBytes(buffer);
			
			OctetString os = new OctetString(buffer);
			return os;
		}
	}
    
    /**
     * Class to generate and send a key to another device
     * */
    class sendMessageTask extends AsyncTask<String, Void, String> {
    	
    	private String m_ToDeviceId;
    	
    	public sendMessageTask(String toDeviceId) {
    		m_ToDeviceId = toDeviceId;
    	}
    	
        protected String doInBackground(String... urls) {
        	try {
        		updateStatus("Generating message...");
        		
        		OctetString SED = new OctetString();
                
                OctetString toIdentifier = OctetString.fromAscii(DATE_STRING + "\0" + m_ToDeviceId + "\0");

                
        		OctetString SSV = Sakke.generateSharedSecretAndSED(SED, 
					        				                       toIdentifier, 
					        				                       m_KeyData.getSakkeParameterSetIndex(), 
					        				                       OctetString.fromHex(m_KeyData.getKmsPublicKey()), 
					        				                       new SsvRandomGenerator());
        		
        		updateStatus("Signing message...");
        		OctetString signature = Eccsi.sign(SED, 
							        			   OctetString.fromHex(m_KeyData.getPublicValidationToken()), 
							        			   OctetString.fromHex(m_KeyData.getSecretSigningKey()), 
							        			   OctetString.fromHex(m_KeyData.getHS()), 
						        				   new SsvRandomGenerator());
        		        		
        		updateStatus("Sending message...");
        		
        		// Send the message to the server
                HttpClient client = new DefaultHttpClient();            	
                HttpGet request = new HttpGet();
    			request.setURI(new URI("http://" + KES_IP_ADDRESS + ":" + KES_PORT.toString() 
    					                                    + "/send?id=" + m_KeyData.getIdentifier() 
    					                                         + "&toid=" + m_ToDeviceId 
    					                                         + "&sed=" + SED.toString() 
    					                                         + "&signature=" + signature.toString()));
    			
                client.execute(request);
                
    	        DatabaseAdapter adapter = new DatabaseAdapter(TabbedActivity.this);
    	        adapter.setDeviceKey(m_ToDeviceId, SSV.toString());       
                
    	        populateSSVsTab();
                updateStatus("Message sent");
                return SED.toString();
    			
    		} catch (Exception e) {
    			updateStatus(e.getMessage()); 
    			e.printStackTrace();
    		}
        	
        	return "";
        }

        protected void onPostExecute(String result) {
        	toggleProgress(false);
        }
    }    
    
    /**
     * Changes the text displayed on the status label on the main screen
     * */
    private void updateStatus(final String status) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
		    	TextView label = (TextView)TabbedActivity.this.findViewById(R.id.devices_status_label);
		    	label.setText(status);				
			}
		});
    }
            
    /**
     * Turns the progress bar on or off, also disables the controls when the progress bar is moving
     * */
    private void toggleProgress(final boolean started) {
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {				
		    	ProgressBar progress = (ProgressBar)TabbedActivity.this.findViewById(R.id.devices_progress_bar);
	    	
		    	if (started) {
		    		progress.setVisibility(View.VISIBLE);

		    	} else {
		    		progress.setVisibility(View.GONE);
		    	}
			}
		});
    }
    
    /**
     * Returns true if the progress bar is being dispayed
     * */
    private boolean inProgress() {
		ProgressBar progress = (ProgressBar)TabbedActivity.this.findViewById(R.id.devices_progress_bar);
		if (progress != null && progress.getVisibility() == View.VISIBLE) {	
			return true;
		}
    	
		return false;
    }
}
