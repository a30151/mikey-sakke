package mikeysakke.kms.client;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.HttpsConnection;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.Sakke;
import mikeysakke.utils.OctetString;
import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.io.transport.ConnectionFactory;

/**
 * Client class that retrieves keys from the KMS Server and stores them in a
 * KeyData objects.
 */
public class Client {

    public static final String DATE = "2012-09";

    /** The KeyData object that holds the client's keys. */
    private final KeyData keys;

    /** KMS Server address. */
    private final String kmsServer;

    /** Port to connect to the KMS Server. */
    private final String port;

    /** Client's username to be authenticated by KMS Server. */
    private final String username;

    /** Client's password to be authenticated by KMS Server. */
    private final String password;

    /** Client's name or ID. */
    private final String id;

    /**
     * Client constructor that uses default values (mainly for testing).
     * 
     * @param ks
     *            - the KeyData object to use for this clients key storage
     */
    public Client(final KeyData ks) {
        keys = ks;

        // default values
        kmsServer = "192.168.1.3";
        port = "7070";
        username = "jim";
        password = "jimpass";
        id = "dan";

    }

    /**
     * Client constructor, sets the client's field variables.
     * 
     * @param ks
     *            - the KeyData object to use for this client's key storage
     * @param kmsServer
     *            - the KMS server address
     * @param port
     *            - the port to connect to the KMS server
     * @param username
     *            - client's username
     * @param password
     *            - client's password
     * @param id
     *            - client's ID
     */
    public Client(final KeyData ks, final String kmsServer, final String port,
            final String username, final String password, final String id) {
        keys = ks;
        this.kmsServer = kmsServer;
        this.port = port;
        this.username = username;
        this.password = password;
        this.id = id;

    }

    /**
     * @return the KeyData object that stores this client's keys
     */
    public KeyData getKeys() {
        return keys;
    }

    /**
     * Connect to the KMS server and retrieve the keys for this client. If keys
     * are successfully retrieved, they are stored in the client's KeyData
     * object.
     * 
     * @return the KeyData object that has this client's keys stored
     * @throws IOException
     * @throws JSONException
     */
    public KeyData fetchKeyMaterial() throws IOException, JSONException {

        String credentials = username + ":" + password;
        byte[] encodedCredentials = Base64OutputStream.encode(credentials.getBytes(), 0, credentials.length(), false, false);

        String identifier = DATE + "%00" + id + "%00";
        String url = "https://" + kmsServer + ":" + port + "/secure/key?id=" + identifier + ";interface=wifi";

        ConnectionFactory cf = new ConnectionFactory();
        ConnectionDescriptor cd = cf.getConnection(url);
        HttpsConnection httpsConnector = (HttpsConnection) cd.getConnection();
        httpsConnector.setRequestMethod(HttpsConnection.GET);
        httpsConnector.setRequestProperty("Authorization", "Basic " + new String(encodedCredentials));

        InputStream input = httpsConnector.openInputStream();
        String testresponse = httpsConnector.getResponseMessage();

        if (!testresponse.equals("OK")) {
            throw new IOException("Response from server: " + testresponse);
        }

        byte[] data = new byte[1024];
        int len = 0;
        StringBuffer raw = new StringBuffer();

        while (-1 != (len = input.read(data))) {
            raw.append(new String(data, 0, len));
        }

        String response = raw.toString();

        input.close();
        httpsConnector.close();

        JSONObject jsonObj = new JSONObject(response);

        JSONArray communityArray = jsonObj.getJSONArray("community");
        JSONObject communityObj = (JSONObject) communityArray.get(0);
        String publicAuthenticationKey = communityObj.getString("kmsPublicAuthenticationKey");
        String kmsPublicKey = communityObj.getString("kmsPublicKey");
        String kmsIdentifier = communityObj.getString("kmsIdentifier");
        int sakkeParameterSetIndex = communityObj.getInt("sakkeParameterSetIndex");

        JSONObject publicObj = jsonObj.getJSONObject("public");
        String publicValidationToken = publicObj.getString("userPublicValidationToken");

        JSONObject privateObj = jsonObj.getJSONObject("private");
        String receiverSecretKey = privateObj.getString("receiverSecretKey");
        String secretSigningKey = privateObj.getString("userSecretSigningKey");

        keys.setIdentifier(id);
        keys.setSecretSigningKey(OctetString.fromHex(secretSigningKey));
        keys.setReceiverSecretKey(OctetString.fromHex(receiverSecretKey));
        keys.setPublicValidationToken(OctetString.fromHex(publicValidationToken));
        keys.setPublicAuthenticationKey(OctetString.fromHex(publicAuthenticationKey));// KPAK
        keys.setKmsIdentifier(kmsIdentifier);
        keys.setKmsPublicKey(OctetString.fromHex(kmsPublicKey)); // Z
        keys.setSakkeParameterSetIndex(sakkeParameterSetIndex);

        return keys;
    }

    /**
     * Validate the SSK received from the KMS.
     * 
     * @return whether the validation was successful
     */
    public boolean validateSigningKeys() {

        OctetString hs = new OctetString();
        boolean valid = Eccsi.validateSigningKeys(keys.getEncodedIdentifier(),
                keys.getPublicValidationToken(),
                keys.getPublicAuthenticationKey(),
                keys.getSecretSigningKey(), hs);
        keys.setHS(hs);

        return valid;
    }

    /**
     * Validate the RSK received from the KMS.
     * 
     * @return whether the validation was successful
     */
    public boolean validateReceiverKey() {
        boolean valid = Sakke.validateReceiverSecretKey(/* OctetString.fromAscii */(keys.getEncodedIdentifier()),
                keys.getKmsPublicKey(),
                keys.getReceiverSecretKey(),
                keys.getSakkeParameterSetIndex());
        return valid;

    }

}
