package mikeysakke.kms.client;

import mikeysakke.utils.OctetString;

/**
 * KeyData class used to store keys for a client.
 */
public class KeyData {

    /** KMS Community Identifier. */
    private String kmsIdentifier;
    /** KMS Public Authentication key. */
    private OctetString publicAuthenticationKey;
    /** KMS Public key. */
    private OctetString kmsPublicKey;
    /** KMS Sakke parameter set index (should be 1). */
    private int sakkeParameterSetIndex;

    /** The local device ID. */
    private String identifier;

    /** Public key. */
    private OctetString publicValidationToken;
    /** Private key. */
    private OctetString receiverSecretKey;
    /** Private key. */
    private OctetString secretSigningKey;

    /** Hashed signature. */
    private OctetString HS;

    public KeyData() {
        kmsIdentifier = "";
        publicAuthenticationKey = new OctetString();
        kmsPublicKey = new OctetString();
        publicValidationToken = new OctetString();
        receiverSecretKey = new OctetString();
        secretSigningKey = new OctetString();
        HS = new OctetString();
    }

    /** returns the kmsIdentifier. */
    public String getKmsIdentifier() {
        return kmsIdentifier;
    }

    /** sets the kmsIdentifier. */
    public void setKmsIdentifier(final String kmsIdentifier) {
        this.kmsIdentifier = kmsIdentifier;
    }

    /** returns the kms public authentication key. */
    public OctetString getPublicAuthenticationKey() {
        return publicAuthenticationKey;
    }

    /** sets the kms public authentication key. */
    public void setPublicAuthenticationKey(final OctetString publicAuthenticationKey) {
        this.publicAuthenticationKey = publicAuthenticationKey;
    }

    /** returns the kms public key. */
    public OctetString getKmsPublicKey() {
        return kmsPublicKey;
    }

    /** sets the kms public key. */
    public void setKmsPublicKey(final OctetString kmsPublicKey) {
        this.kmsPublicKey = kmsPublicKey;
    }

    /** returns the sakke parameter set index. */
    public int getSakkeParameterSetIndex() {
        return sakkeParameterSetIndex;
    }

    /** sets the sakke parameter set index. */
    public void setSakkeParameterSetIndex(final int sakkeParameterSetIndex) {
        this.sakkeParameterSetIndex = sakkeParameterSetIndex;
    }

    /** returns the identifier. */
    public String getIdentifier() {
        return identifier;
    }

    /** sets the identifier. */
    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    /** returns the public validation token. */
    public OctetString getPublicValidationToken() {
        return publicValidationToken;
    }

    /** sets the public validation token. */
    public void setPublicValidationToken(final OctetString publicValidationToken) {
        this.publicValidationToken = publicValidationToken;
    }

    /** returns the receiver secret key. */
    public OctetString getReceiverSecretKey() {
        return receiverSecretKey;
    }

    /** sets the receiver secret key. */
    public void setReceiverSecretKey(final OctetString receiverSecretKey) {
        this.receiverSecretKey = receiverSecretKey;
    }

    /** returns the secret signing key. */
    public OctetString getSecretSigningKey() {
        return secretSigningKey;
    }

    /** sets the secret signing key. */
    public void setSecretSigningKey(final OctetString secretSigningKey) {
        this.secretSigningKey = secretSigningKey;
    }

    /** returns HS. */
    public OctetString getHS() {
        return HS;
    }

    /** sets HS. */
    public void setHS(final OctetString hs) {
        HS = hs;
    }

    /** returns an encoded Identifier octetString. */
    public OctetString getEncodedIdentifier() {
        return generateEncodedIdentifier(identifier);
    }

    /** generates an encoded Identifier octetString in the format YYYY-MM\0id\0. */
    public static OctetString generateEncodedIdentifier(final String id) {
        return OctetString.fromAscii(Client.DATE + "\0" + id + "\0");
    }

    /** returns whether or not the key data has been fetched or set yet. */
    public boolean isEmpty() {
        return (kmsIdentifier.equals("") && publicAuthenticationKey.empty() &&
                secretSigningKey.empty() && receiverSecretKey.empty()
                && publicValidationToken.empty() && kmsPublicKey.empty());

    }
}
