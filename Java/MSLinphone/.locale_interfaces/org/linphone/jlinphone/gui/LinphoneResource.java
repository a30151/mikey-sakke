package org.linphone.jlinphone.gui;

public interface LinphoneResource {
    // Hash of: "org.linphone.jlinphone.gui.Linphone".
    long BUNDLE_ID = 0x835c3de28daf4b3aL;
    String BUNDLE_NAME = "org.linphone.jlinphone.gui.Linphone";

    int SETTING_ERROR_NO_DOMAIN = 18;
    int SAVE = 21;
    int SETTING_ESCAPE_PLUS = 15;
    int SETTING_ERROR_BAD_CONFIG = 19;
    int SETTING_SIP_ACCOUNT = 7;
    int SETTING_TRANSPORT = 13;
    int CONSOLE = 2;
    int SETTING_USERNAME = 8;
    int SETTINGS_PTIME = 24;
    int SETTING_ADVANCED = 12;
    int ERROR_AUDIO_PERMISSION_DENY = 22;
    int ABOUT_STRING = 20;
    int MSG_OPEN_THREAD = 27;
    int SETTING_ERROR_NO_USER = 16;
    int MSG_DELETE = 28;
    int MUTE = 5;
    int GOODBYE = 0;
    int SETTING_DEBUG = 14;
    int MSG_DELETE_ALL = 29;
    int CLEAR_LOGS = 23;
    int SETTING_PASSWD = 9;
    int MSG_COMPOSE = 26;
    int ABOUT = 3;
    int SETTING_ERROR_NO_PASSWD = 17;
    int SPEAKER = 6;
    int SETTING_PROXY = 11;
    int MESSENGER = 25;
    int SETTINGS = 1;
    int FIND = 4;
    int SETTING_DOMAIN = 10;
}
