package org.linphone.bb;


import net.rim.device.api.system.DeviceInfo;

import org.linphone.jortp.JOrtpFactory;
import org.linphone.jortp.Logger;

public final class Version {
	private static Logger sLogger=JOrtpFactory.instance().createLogger(Version.class.getName());
	private static int version;
	private Version(){throw new IllegalStateException();};
	
	private static void parseSoftwareVersion() {
		if (version != 0) return;
		sLogger.info("Software version string is " + DeviceInfo.getSoftwareVersion());
		char firstDigit=DeviceInfo.getSoftwareVersion().charAt(0);
		String asString=new Character(firstDigit).toString();
		version=Integer.parseInt(asString);
		sLogger.info("Software version is " + version);
	}

	public static final boolean isAtLeast(int min) {
		parseSoftwareVersion();
		return version >= min;
	}
}
