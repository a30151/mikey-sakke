package org.linphone.jortp;

import net.rim.device.api.util.Arrays;

public class SrtpKey {
	/* srtp keys for default mode: AES key and Salt key */
	public byte[] encryptionKey;
	public byte[] saltKey;
	
	public SrtpKey(byte[] pEncryptionKey, byte[] pSaltKey) {
		this.encryptionKey = Arrays.copy(pEncryptionKey);
		this.saltKey = Arrays.copy(pSaltKey);
	}
}
