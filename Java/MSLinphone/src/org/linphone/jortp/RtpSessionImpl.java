package org.linphone.jortp;

import java.util.Random;

public class RtpSessionImpl implements RtpSession {
    private SocketAddress mLocalAddr;
    private SocketAddress mRemoteAddr;
    private RtpProfile mProfile;
    private RtpTransport mRtpTransport;
    private final JitterBuffer mJitterBuffer;
    private final JitterBufferController mJitterBufferController;
    private final RtpStatisticsImpl mRecvStats;
    private final RtpStatisticsImpl mSendStats;
    private PayloadType mRecvPtObj;
    private int mRecvPt;
    private int mSendPt;
    private int mSendSeq;
    private int mRecvSSRC;
    private int mSentSSRC;
    private int mClockrate;
    private final JitterBufferStatistics mJitterBufferStats;
    private TimestampClock mTsc;
    private final static Logger mLog = JOrtpFactory.instance().createLogger("jortp");
    private SrtpCryptographicContext mSrtpContextForSending = null;
    private SrtpCryptographicContext mSrtpContextForReceiver = null;

    RtpSessionImpl() {
        mProfile = RtpProfileImpl.createAVProfile();
        mJitterBuffer = new JitterBuffer() {
            public void onDrop(final RtpPacket p) {
                mRecvStats.mLate++;
            }
        };
        mJitterBufferStats = new JitterBufferStatistics();
        mJitterBufferController = new JitterBufferController() {
            public void onTimestampJump() {
                if (mLog.isLevelEnabled(Logger.Warn)) {
                    mLog.warn("Timestamp jump detected !");
                }
                mJitterBuffer.purge();
                mRecvStats.mCount = 0;
            }
        };
        mRecvStats = new RtpStatisticsImpl();
        mSendStats = new RtpStatisticsImpl();
        mRecvPt = 0;
        mSendPt = 0;
        mRecvPtObj = null;
        mSendSeq = new Random().nextInt(32768); // initial packet sequence
                                                // number is a random < 2^15
        mSentSSRC = new Random().nextInt();

        mClockrate = 8000;
        mRtpTransport = JOrtpFactory.instance().createDefaultTransport();
        mSrtpContextForSending = null;
        mSrtpContextForReceiver = null;

    }

    public JitterBufferParams getJitterBufferParams() {
        return mJitterBufferController.getParams();
    }

    public void setJitterBufferParams(final JitterBufferParams params) {
        mJitterBufferController.setParams(params);
    }

    public JitterBufferStatistics getJitterBufferStatistics() {
        mJitterBufferStats.mDelay = ((float) mJitterBuffer.getDelayTs()) / (float) mClockrate;
        mJitterBufferStats.mSize = ((float) mJitterBuffer.getTimestampDiff()) / (float) mClockrate;
        return mJitterBufferStats;
    }

    private void updateRecvPt(final int pt) {
        PayloadType ptobj;
        if (pt != mRecvPt) {
            ptobj = mProfile.getPayloadType(pt);
            if (ptobj != null) {
                if (mRecvPtObj != null) {
                    mLog.warn("Incoming payload type changed to " + pt);
                }
                mRecvPtObj = ptobj;
                mClockrate = ptobj.getClockRate();
            }
            mRecvPt = pt;
        }
    }

    private void processIncomingPacket(final RtpPacket p, final int user_recvts) {
        if (p.getSocketAddress() != null) {
            mRemoteAddr = p.getSocketAddress();
        }
        mRecvStats.mCount++;
        try {
            ((RtpPacketImpl) p).readHeader();
        } catch (RtpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
        updateRecvPt(p.getPayloadType());
        mJitterBufferController.newIncomingPacket(p, user_recvts, mClockrate);
        mJitterBuffer.put(p);
    }

    private void readFromTransport(final int user_recvts) throws RtpException {
        RtpPacket p;
        while ((p = mRtpTransport.recvfrom()) != null) {
            processIncomingPacket(p, user_recvts);
        }
    }

    public RtpPacket recvPacket(final int user_ts) throws RtpException {
        RtpPacket p;

        // TODO: extract SSRC here, check if already stored. If we don't
        // already have it, store it
        // , initialise mRecvSSRC here, and create the srtpContextForReciever.
        // (separate enableSrtp into two separates?)
        
        //TODO note, we didn't end up doing the above, but it may be a
        // good idea
        int stream_ts;
        if (mTsc == null) {
            readFromTransport(user_ts);
        }
        synchronized (mJitterBuffer) {
            stream_ts = mJitterBufferController.convertTimestamp(user_ts);
            p = mJitterBuffer.get(stream_ts);

            if (p != null && mSrtpContextForReceiver != null) { // is srtp is
                                                                // enable,
                                                                // authenticate
                                                                // and decrypt
                if (mSrtpContextForReceiver.unsecurePacket(p) == false) {
                	p = null; // authentication failed, discard packet
                }
            }

            if (p != null) {
                mRecvSSRC = p.getSSRC();
                mRecvPt = p.getPayloadType();
                mRecvStats.mPlayed++;
            }
        }

        return p;
    }

    public void sendPacket(final RtpPacket p, final int timeStamp) throws RtpException {
        RtpPacketImpl pi = (RtpPacketImpl) p;
        pi.setTimestamp(timeStamp);
        pi.setSeqNumber(mSendSeq);
        pi.setPayloadType(mSendPt);
        pi.setSSRC(mSentSSRC);
        pi.writeHeader();
        pi.setSocketAddress(mRemoteAddr);

        if (mSrtpContextForSending != null) { // srtp is enabled: secure packet
            mSrtpContextForSending.securePacket(p);
        }

        mRtpTransport.sendto(p);
        mSendSeq++;
        mSendStats.mCount++;
    }

    public void setLocalAddr(final SocketAddress addr) throws RtpException {
        mLocalAddr = addr;
        mRtpTransport.init(mLocalAddr);
    }

    public void setRemoteAddr(final SocketAddress addr) throws RtpException {
        mRemoteAddr = addr;
    }

    public void setProfile(final RtpProfile prof) {
        mProfile = prof;
    }

    public void setRecvPayloadTypeNumber(final int pt) {
        updateRecvPt(pt);
    }

    public void setSendPayloadTypeNumber(final int pt) {
        mSendPt = pt;
    }

    public void setTransport(final RtpTransport rtpt) {
        mRtpTransport = rtpt;
        setup();
    }

    public void setSentSSRC(final int ssrc) {
        mSentSSRC = ssrc;
    }

    public int getSentSSRC() {
        return mSentSSRC;
    }

    public int getRecvSSRC() {
        return mRecvSSRC;
    }

    public RtpStatistics getRecvStats() {
        int lost = (int) (mJitterBufferController.getRelativeSeqNumber() + 1 - mRecvStats.mCount);
        mRecvStats.mLost = lost;
        return mRecvStats;
    }

    public RtpStatistics getSendStats() {
        return mSendStats;
    }

    public int getRecvPayloadTypeNumber() {
        return mRecvPt;
    }

    public void close() {
        disableSrtp();
        mRtpTransport.close();
    }

    public RtpProfile getProfile() {
        return mProfile;
    }

    public int getSendPayloadTypeNumber() {
        return mSendPt;
    }

    public void setTimestampClock(final TimestampClock tsc) {
        mTsc = tsc;
        setup();
    }

    private void setup() {
        if (mTsc != null) {
            mRtpTransport.setListener(new RtpTransportListener() {
                public void onPacketReceived(final RtpPacket p) {
                    synchronized (mJitterBuffer) {
                        processIncomingPacket(p, mTsc.getCurrentTimestamp());
                    }

                }
            });
        }
    }

    /* enable/disable the Srtp encryption for this session */
    public void enableSrtp(final short encryptionKeyLength, final short tagKeyLength, final SrtpKey encryptionKey, final SrtpKey decryptionKey, final long recvSSRC) {

        mSrtpContextForSending = new SrtpCryptographicContext(mSentSSRC, encryptionKeyLength, tagKeyLength,
                encryptionKey.encryptionKey, encryptionKey.saltKey);

        mSrtpContextForReceiver = new SrtpCryptographicContext(recvSSRC, encryptionKeyLength, tagKeyLength,
                decryptionKey.encryptionKey, decryptionKey.saltKey);

    }

    private void disableSrtp() {
        mSrtpContextForSending = null;
        mSrtpContextForReceiver = null;
    }
}
