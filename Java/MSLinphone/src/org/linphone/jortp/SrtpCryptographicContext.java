package org.linphone.jortp;


import net.rim.device.api.crypto.AESEncryptorEngine;
import net.rim.device.api.crypto.AESKey;
import net.rim.device.api.crypto.CryptoTokenException;
import net.rim.device.api.crypto.CryptoUnsupportedOperationException;
import net.rim.device.api.crypto.HMAC;
import net.rim.device.api.crypto.HMACKey;
import net.rim.device.api.crypto.SHA1Digest;
import net.rim.device.api.util.Arrays;

/* cryptographic context as defined in rfc3711 3.2.1 */
public class SrtpCryptographicContext {
    /* log errors and warnings */
    private final static Logger mLog = JOrtpFactory.instance().createLogger("srtp context");

    /* Context SSRC */
    private final long SSRC;

    /* rollover Counter (32 bits unsigned integer) */
    private long ROC;

    /* sequence number (16 bits unsigned integer) */
    private int sequenceNumber;

    /***
     * Identifier for the encryption algorithm. Cipher mode definition as
     * described in rfc3711 4.1: - Block cipher mode - n_b is the bit-size of
     * the block for the block cipher : not used, always 128 bits - k_e is the
     * session encryption key - n_e is the bit-length of k_e - k_s is the
     * session salting key - n_s is the bit-length of k_s - SRTP_PREFIX_LENGTH
     * is the octet length of the keystream prefix, a non-negative integer,
     * specified by the message authentication code in use. Not used, always
     * HMAC-SHA1 so prefix is 0.
     ***/

    /*
     * Block cipher mode definition available mode: - NULL encryption -
     * AESinCounterMode
     */
    public final static byte NULL_ENCRYPTION = 0;
    public final static byte AES_IN_COUNTER_MODE = 1;
    private final byte blockCipherMode;
    private final byte[] encryptionKey;
    private final short encryptionKeyLength;
    private final byte[] saltKey;
    private final short saltKeyLength;

    /***
     * Identifier for the authentication algorithm as in rfc3711 4.2: - AUTH_ALG
     * is the authentication algorithm - k_a is the session message
     * authentication key - n_a is the bit-length of the authentication key -
     * n_tag is the bit-length of the output authentication tag -
     * SRTP_PREFIX_LENGTH is the octet length of the keystream prefix as defined
     * above, a parameter of AUTH_ALG
     * 
     ***/
    /* only one authentication available HMAC SHA1 */
    public final static byte HMAC_SHA1 = 0;
    private final byte authenticationAlgorithm;
    private final byte[] authenticationKey;
    private final short authenticationKeyLength;
    private final short authenticationTagLength;

    /*
     * replay index : a 64 bitmask. Each bit set to one represent a packet index
     * already processed LSB(bit 0) is always 1 and is the highest received
     * index. mapping index 63 .. 0 Highest index of a processed packet being n,
     * if a packet of index m has already been processed bit (n-m) of the replay
     * index is set to 1, and 0 otherwise. When an higher index (n') packet
     * arrives, the replay buffer is shifted left by (n'-n). ReplayIndex window
     * is then 64 packets. Older packets are discarded too but with a warning
     * and not an error
     */
    private long replayIndex = 0;

    /*
     * Master Encryption Key (with a counter of RTP packet encrypted using this
     * key
     */
    private final byte[] masterEncryptionKey;

    /* Master salt */
    private final byte[] masterSalt;

    /* key derivation rate */
    private final int keyDerivationRate;

    /*** variable defined for internal purpose, not mentionned in the rfc ***/
    /* key derivation */
    private AESEncryptorEngine masterKeyCryptoEngine;

    /* encryption engine */
    private AESEncryptorEngine AESinCounterModeSessionKeyEngine;

    /* authentication engine */
    private SHA1Digest authenticationHashEngine;
    private HMAC authenticationEngine;

    /*
     * build the cryptoContext: simplified interface, just give an SSRC and keys
     * and tag length
     */
    public SrtpCryptographicContext(final long SSRC, final short keyLength, final short tagLength,
            final byte[] masterEncryptionKey, final byte[] masterSalt)
    {
        this(SSRC,
                AES_IN_COUNTER_MODE, keyLength, (short) 14,
                HMAC_SHA1, (short) 20, tagLength,
                masterEncryptionKey, masterSalt, 0);
    }

    /* build the cryptoContext */
    public SrtpCryptographicContext(final long SSRC,
            final byte blockCipherMode, final short encryptionKeyLength, final short saltKeyLength,
            final byte authenticationAlgorithm, final short authenticationKeyLength, final short authenticationTagLength,
            final byte[] masterEncryptionKey, final byte[] masterSalt, final int keyDerivationRate)
    {
        /* copy parameters */
        this.SSRC = SSRC;

        this.blockCipherMode = blockCipherMode;
        this.encryptionKeyLength = encryptionKeyLength;
        this.saltKeyLength = saltKeyLength;

        this.authenticationAlgorithm = authenticationAlgorithm;
        this.authenticationKeyLength = authenticationKeyLength;
        this.authenticationTagLength = authenticationTagLength;

        this.masterEncryptionKey = Arrays.copy(masterEncryptionKey);
        this.masterSalt = Arrays.copy(masterSalt);
        this.keyDerivationRate = keyDerivationRate;

        /* initialise values */
        ROC = 0;
        sequenceNumber = -1; // Sequence number is initialized to -1, it will be
                             // initialized from the first packet

        /* create encryption engine for key derivation: key is the master key */
        AESKey masterKeyAES = new AESKey(this.masterEncryptionKey, 0, this.encryptionKeyLength * 8);
        try {
            masterKeyCryptoEngine = new AESEncryptorEngine(masterKeyAES);
        } catch (CryptoTokenException e) {
            mLog.error("Crypto error on AES engine creation: " + e.getMessage());
        } catch (CryptoUnsupportedOperationException e) {
            mLog.error("Crypto error on AES engine creation: " + e.getMessage());
        }

        /* initiate the session and salt keys */
        encryptionKey = Arrays.copy(keyDerivation((byte) 0x00, encryptionKeyLength, 0));
        saltKey = Arrays.copy(keyDerivation((byte) 0x02, saltKeyLength, 0));
        authenticationKey = Arrays.copy(keyDerivation((byte) 0x01, authenticationKeyLength, 0));

        /* if needed create the encryption engine and object with sessionKey */
        if (this.blockCipherMode == AES_IN_COUNTER_MODE) {
            AESKey sessionKey = new AESKey(encryptionKey, 0, this.encryptionKeyLength * 8);
            try {
                AESinCounterModeSessionKeyEngine = new AESEncryptorEngine(sessionKey);
            } catch (CryptoTokenException e) {
                mLog.error("Crypto error on AES engine creation: " + e.getMessage());
            } catch (CryptoUnsupportedOperationException e) {
                mLog.error("Crypto error on AES engine creation: " + e.getMessage());
            }
        }

        /* create authentication engine if needed */
        if (this.authenticationTagLength > 0) {
            HMACKey authenticationKey = new HMACKey(this.authenticationKey, 0, this.authenticationKeyLength);
            authenticationHashEngine = new SHA1Digest();
            try {
                authenticationEngine = new HMAC(authenticationKey, authenticationHashEngine);
            } catch (CryptoTokenException e) {
                mLog.error("Crypto error on AES engine creation: " + e.getMessage());
            } catch (CryptoUnsupportedOperationException e) {
                mLog.error("Crypto error on AES engine creation: " + e.getMessage());
            }
        }

    }

    /*
     * derivate key using the AES in counter mode algorithm as in rfc3711 4.3.1
     * and 4.3.3
     */
    private byte[] keyDerivation(final byte label, final short keyLength, final long index) {
        /* create the new key, all bytes to 0 by default at creation */
        byte[] derivatedKey = new byte[keyLength];
        byte[] InitialValue = new byte[16];

        /* compute the initial value as in spec 4.3.1 */
        long keyId = ((long) label) << 48; /*
                                            * keyId on 56 bits: 8 bits of
                                            * label|48 bits of index
                                            */
        if (keyDerivationRate != 0) {
            keyId |= index / keyDerivationRate;
        }
        int offset = 14 - saltKeyLength;
        for (int i = 0; i < saltKeyLength; i++) {
            InitialValue[offset + i] = masterSalt[i];
        }
        for (int i = 7; i < 14; i++) {
            InitialValue[i] ^= (byte) ((keyId >> ((13 - i) * 8)) & 0xFF);
        }

        /* use AES in CM to get the key value */
        AESinCMencrypt(masterKeyCryptoEngine, InitialValue, derivatedKey, 0, keyLength);
        return derivatedKey;
    }

    // authentication using HMAC-SHA1
    private byte[] authenticatePacket(final byte[] data, final int dataLength, final long ROC) {

        /* compute the byte array to authenticate: packet header + payload + ROC */
        byte[] dataToAuthenticate = new byte[dataLength + 4];
        for (int i = 0; i < dataLength; i++) {
            dataToAuthenticate[i] = data[i];
        }
        dataToAuthenticate[dataLength] = (byte) ((ROC >> 24) & 0XFF);
        dataToAuthenticate[dataLength + 1] = (byte) ((ROC >> 16) & 0XFF);
        dataToAuthenticate[dataLength + 2] = (byte) ((ROC >> 8) & 0XFF);
        dataToAuthenticate[dataLength + 3] = (byte) (ROC & 0XFF);
        byte[] authenticationMAC = null;

        /* HMAC-SHA1 with authentication key */
        try {
            authenticationEngine.reset();
            authenticationEngine.update(dataToAuthenticate);
            authenticationMAC = authenticationEngine.getMAC();
        } catch (CryptoTokenException e) {
            mLog.error("Crypto error on HMAC-SHA1 engine: " + e.getMessage());
        }
        return authenticationMAC;
    }

    // packet encryption
    public void securePacket(final RtpPacket packet) {
        /* according to spec 3.3 */
        /*
         * step 1 is done at higher level, as accurate crypto context has
         * already been selected
         */

        /* step 2 : Determine index: ROC*2^16 + SeqNumber */
        long packetIndex = ((ROC) << 16) | (packet.getSeqNumber());

        /* step 3 and 4 : TODO, key management */

        /* step 5 : encryption */
        if (blockCipherMode == AES_IN_COUNTER_MODE) {
            /* determine initial value */
            byte[] InitialValue = buildInitialValue(saltKey, SSRC, packetIndex);

            /* encrypt */
            AESinCMencrypt(AESinCounterModeSessionKeyEngine, InitialValue, packet.getBytes(), packet.getDataOffset(), packet.getRealLength());
        }

        /* step 6 : mki not implemented */
        /* step 7 : authentication */
        /*
         * on the RTP packet Header + payload + padding + ROC, see figure 1 of
         * rfc
         */

        byte[] packetAuthentication = null;
        if (authenticationTagLength > 0) {
            /* compute authentication */
            packetAuthentication = authenticatePacket(packet.getBytes(), packet.getRealLength(), ROC);
            /* append authentication */
            packet.appendData(packetAuthentication, (authenticationTagLength));
        }

        /* step 8 : update ROC */
        if (packet.getSeqNumber() == 0xFFFF) {
            ROC = (ROC + 1) & 0xFFFFFFFF;
        }
       
    }

    // packet decryption : return false in case of authentication failure:
    // packet shall be discarded
    public boolean unsecurePacket(final RtpPacket packet) {
        /* according to spec 3.3 */
        /* step 1 : crypto context determined at higher level */
        /* step 2 : determine packet index as in spec 3.3.1 and appendix A */
        if (sequenceNumber == -1) { /*
                                     * this is our first packet, initialize the
                                     * receiver maintaied 16 bits sequence
                                     * Number using the one find in this packet
                                     */
            sequenceNumber = packet.getSeqNumber();
        }
        long v = ROC;
        int nextSequenceNumber = sequenceNumber;
        // compute it here, update only if authentication and decryption are ok
        if (sequenceNumber < packet.getSeqNumber()) {
            nextSequenceNumber = packet.getSeqNumber();
        }
        boolean updateROCtoV = false; // same comment as previous line
        if (sequenceNumber < 32768) {
            if (packet.getSeqNumber() - sequenceNumber > 32768) {
                v = (ROC - 1) & 0XFFFFFFFF;
                nextSequenceNumber = sequenceNumber;
            } else {
                v = ROC;
            }
        } else {
            if (sequenceNumber - 32768 > packet.getSeqNumber()) {
                v = (ROC + 1) & 0XFFFFFFFF;
                nextSequenceNumber = packet.getSeqNumber();
                updateROCtoV = true;
            } else {
                v = ROC;
            }
        }

        long packetIndex = ((v) << 16) | (packet.getSeqNumber());

        /* step 3 and 4 : TODO, key management */

        /* step 5 : authentication and replay protection */
        /* replay protection */
        long delta = packetIndex - (((ROC) << 16) | (sequenceNumber)); /*
                                                                        * difference
                                                                        * between
                                                                        * current
                                                                        * and
                                                                        * highest
                                                                        * packetIndex
                                                                        */
        if (delta <= 0) { // current packet have an index inferior to highest
                          // one already processed
            if (delta + 63 < 0) { // packet index is out of replay index window
                mLog.warn("Replay protection: packet is out of replay window scope. Packet discarded. SSRC " + packet.getSSRC() + " SeqNum: " + packet.getSeqNumber());
                return false;
            } else {
                if (((replayIndex >> (-delta)) & 0x1) == 1) { /*
                                                               * packet already
                                                               * processed
                                                               */
                    mLog.warn("Replay protection: packet already processed. Packet discarded. SSRC " + packet.getSSRC() + " SeqNum: " + packet.getSeqNumber());
                    return false;
                }
            }
        }

        if (authenticationTagLength > 0) {
            /* authenticated data : header + payload, see figure 1 in rfc */
            /*
             * dataLength is also the offset of authentication tag in the rtp
             * packet buffer
             */
            int dataLength = packet.getRealLength() - authenticationTagLength;

            byte[] authenticationTag = authenticatePacket(packet.getBytes(), dataLength, ROC);
            byte[] packetData = packet.getBytes();

            //TODO TEMP for debugging
            /*try 
            {
              FileConnection fc = (FileConnection)Connector.open("file:///store/home/user/PacketAuthenticate.txt");
              // If no exception is thrown, then the URI is valid, but the file may or may not exist.
              if (!fc.exists())
              {
                  fc.create();  // create the file if it doesn't exist
              }
              OutputStream outStream = fc.openOutputStream(fc.fileSize()); 

              StringBuffer s = new StringBuffer();
              s.append("Packet:");
              for (int i = 0; i < packetData.length; i++){
            	  s.append(" " + packetData[i]);
              }
              s.append("\r\nAuthentication tag:");
              for (int i = 0; i < authenticationTag.length; i++){
            	  s.append(" " + authenticationTag[i]);
              }
              
              s.append("\r\nSSRC: " + packet.getSSRC());
              s.append("\r\nSequence no: " + packet.getSeqNumber() + "\r\n");

             */
            boolean authenticationOk = true;
            for (int i = 0; i < authenticationTagLength; i++) {
                if (authenticationTag[i] != packetData[dataLength + i]) {
                	/*s.append("authentication fail: ");
                	s.append("authenticationTag[" + i + "] = " + 
                			authenticationTag[i] + ", " + 
                			"packetData[" + dataLength + "+" + i + "] = " + 
                			packetData[dataLength + i] + "\r\n");*/
                    authenticationOk = false;
                }
            }
            /*s.append("\r\n\r\n");
            
            outStream.write(s.toString().getBytes());*/
       

            if (authenticationOk == false) {
                mLog.warn("Authentication failed. Packet Discarded. SSRC " + packet.getSSRC() + " SeqNum: " + packet.getSeqNumber());
                return false;
            } 
            /*
            outStream.close();
            fc.close();    
            }
             catch (IOException ioe) 
             {
                System.out.println(ioe.getMessage() );
             }
            /* remove authentication from packet */
            try {
                packet.setRealLength(dataLength);
            } catch (RtpException e) {
                mLog.error("Rtp Packet Error, can't remove the authentication tag from the end of the packet(realLength resize): " + e.getMessage());
            } /*
               * note : data is not physically erased but no more considered to
               * be part of the payload
               */
        }

        /* step 6 : decryption */
        if (blockCipherMode == AES_IN_COUNTER_MODE) {
            /* determine initial value */
            byte[] InitialValue = buildInitialValue(saltKey, packet.getSSRC(), packetIndex);

            /* encrypt */
            AESinCMencrypt(AESinCounterModeSessionKeyEngine, InitialValue, packet.getBytes(), packet.getDataOffset(), packet.getRealLength());
        }

        /* step 7 : update replay index, sequence number and ROC */
        if (delta > 0) { // current index is higher, shift left the replayIndex
                         // and set bit0 to 1
            replayIndex = replayIndex << delta;
            replayIndex |= 0x1;
        } else {
            replayIndex |= (0x1 << -delta);
        }

        sequenceNumber = nextSequenceNumber;

        if (updateROCtoV == true) {
            ROC = v;
        }

        return true;
    }

    /* AES in CM encryption function */
    private void AESinCMencrypt(final AESEncryptorEngine sessionKeyCryptoEngine, final byte[] InitialValue, final byte[] message, final int offset, final int limit) {
        byte[] keyStream = new byte[16];
        byte[] IV = Arrays.copy(InitialValue);

        try {
            // loop on 128 bits blocks of plaintext to produce keyStream
            for (int i = offset; i < limit; i += 16) {
                // InitialValue last two bytes are a counter, set it to current
                // i/16 value
                IV[14] = (byte) ((i >> 12) & 0xFF);
                IV[15] = (byte) ((i >> 4) & 0xFF);

                sessionKeyCryptoEngine.encrypt(IV, 0, keyStream, 0); // compute
                                                                     // keystream

                // keystream XOR message -> message
                // condition on message length, because we might not have a
                // message length being a multiple of 16 bytes
                // in that case, use the left bytes of the keyStream to XOR the
                // needed amount
                for (int j = 0; (j < 16) && (i + j < limit); j++) {
                    message[i + j] ^= keyStream[j];
                }
            }
        } catch (CryptoTokenException e) {
            mLog.error("Crypto error on AES engine: " + e.getMessage());
        }

    }

    /*
     * build the initial value from salt(112 bits), SSRC(32 bits) and index(48
     * bits) as in spec 4.1.1
     */
    private byte[] buildInitialValue(final byte[] saltKey, final long SSRC, final long index) {
        byte[] InitialValue = new byte[16];
        for (int i = 0; i < 4; i++) {
            InitialValue[i] = saltKey[i];
        }
        for (int i = 4; i < 8; i++) {
            InitialValue[i] = (byte) (saltKey[i] ^ ((byte) ((SSRC >> ((7 - i) * 8)) & 0XFF)));
        }
        for (int i = 8; i < 14; i++) {
            InitialValue[i] = (byte) (saltKey[i] ^ ((byte) ((index >> ((13 - i) * 8)) & 0XFF)));
        }
        return InitialValue;
    }
}
