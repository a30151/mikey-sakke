/*
LinphoneCallImpl.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.linphone.jlinphone.core;

import java.io.IOException;
import java.util.Hashtable;

import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.linphone.core.CallDirection;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallLog;
import org.linphone.core.LinphoneCallLog.CallStatus;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.jlinphone.core.LinphoneCoreImpl.LinphoneChatRoomImpl;
import org.linphone.jlinphone.media.AudioStream;
import org.linphone.jlinphone.media.AudioStreamParameters;
import org.linphone.jlinphone.media.jsr135.AudioStreamImpl;
import org.linphone.jlinphone.sal.jsr180.SdpUtils;
import org.linphone.jortp.JOrtpFactory;
import org.linphone.jortp.Logger;
import org.linphone.jortp.PayloadType;
import org.linphone.jortp.RtpException;
import org.linphone.jortp.RtpProfile;
import org.linphone.jortp.SocketAddress;
import org.linphone.sal.Sal;
import org.linphone.sal.SalException;
import org.linphone.sal.SalFactory;
import org.linphone.sal.SalMediaDescription;
import org.linphone.sal.SalOp;
import org.linphone.sal.SalStreamDescription;

import mikeysakke.client.ClientData;
import mikeysakke.client.Connect;
import mikeysakke.client.KeyData;
import mikeysakke.client.MSUtils;
import mikeysakke.client.MikeySakkeSrtpParams;
import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.ui.component.Dialog;
import sip4me.gov.nist.javax.sdp.SdpException;
import sip4me.gov.nist.javax.sdp.SessionDescription;

class LinphoneCallImpl implements LinphoneCall {

    private LinphoneChatRoomImpl mChatRoom;
    private final CallDirection mDir;
    private LinphoneCall.State mState;
    private final SalOp mOp;
    private final SalMediaDescription mLocalDesc;
    private SalMediaDescription mFinal;
    private AudioStreamImpl mAudioStream;
    private LinphoneCallLogImpl mCallLog;
    final private LinphoneCoreImpl mCore;
    private final static Logger mLog = JOrtpFactory.instance().createLogger("LinphoneCore");

    protected LinphoneCallImpl(final LinphoneCoreImpl aCore, final SalOp op, final CallDirection dir) throws SalException {
    	mState = LinphoneCall.State.Idle;
        mOp = op;
        mDir = dir;
        mOp.setUserContext(this);
        mCore = aCore;
        mLocalDesc = makeLocalDesc();
        initMediaStreams();
        mCallLog = new LinphoneCallLogImpl(dir, op.getFrom(), op.getTo());
        op.callSetLocalMediaDescription(getLocalMediaDescription());

    }

    public LinphoneAddress getRemoteAddress() {
        if (mOp != null) {
            if (mDir == CallDirection.Incoming) {
                return LinphoneCoreFactory.instance().createLinphoneAddress(mOp.getFrom());
            } else
                return LinphoneCoreFactory.instance().createLinphoneAddress(mOp.getTo());
        }
        return null;
    }

    public SalMediaDescription getLocalMediaDescription() {
        return mLocalDesc;
    }

    public SalMediaDescription getFinalMediaDescription() {
        return mFinal;
    }

    public void setFinalMediaDescription(final SalMediaDescription aDescription) {
        mFinal = aDescription;
    }

    public void setState(final LinphoneCall.State aState) {
        setState(aState, null);
    }

    public void setState(final LinphoneCall.State aState, final String message) {
        mState = aState;
        String displayStatus = null;
        if (aState == LinphoneCall.State.Connected) {
            displayStatus = "Connected to " + getRemoteAddress().getUserName() != null
                    ? getRemoteAddress().getUserName()
                    : getRemoteAddress().toString();
            mCallLog.setCallStatus(CallStatus.Sucess);
        } else if (aState == LinphoneCall.State.IncomingReceived) {
            displayStatus = getRemoteAddress().getUserName() != null
                    ? getRemoteAddress().getUserName()
                    : getRemoteAddress().toString() + " is calling you";
            mCallLog.setCallStatus(CallStatus.Missed);
        } else if (aState == LinphoneCall.State.OutgoingRinging) {
            displayStatus = "Remote ringing...";
            mCallLog.setCallStatus(CallStatus.Declined);
        } else if (aState == LinphoneCall.State.OutgoingEarlyMedia) {
            displayStatus = "Early media...";
            mCallLog.setCallStatus(CallStatus.Declined);
        } else if (aState == LinphoneCall.State.CallEnd) {
            displayStatus = "Call terminated";
        } else if (aState == LinphoneCall.State.Error) {
            displayStatus = "Call failure [" + message + "]";
        } else if (aState == LinphoneCall.State.OutgoingInit) {
            displayStatus = "Calling  " + (getRemoteAddress().getUserName() != null
                    ? getRemoteAddress().getUserName()
                    : getRemoteAddress().toString());

        }

        if (displayStatus != null)
            mCore.getListener().displayStatus(mCore, displayStatus);
        mCore.getListener().callState(mCore, this, mState, message);

    }

    public LinphoneCallLog getCallLog() {
        return mCallLog;
    }

    public CallDirection getDirection() {
        return mDir;
    }

    public State getState() {
        return mState;
    }

    private SalMediaDescription makeLocalDesc() throws SalException {
        SalMediaDescription md = SalFactory.instance().createSalMediaDescription();

        SalStreamDescription sd = new SalStreamDescription();
        PayloadType pts[] = new PayloadType[1];
        PayloadType amr;
        sd.setAddress(getSal().getLocalAddr());
        sd.setPort(7078);
        sd.setProto(SalStreamDescription.Proto.RtpAvp);
        sd.setType(SalStreamDescription.Type.Audio);
        amr = JOrtpFactory.instance().createPayloadType();
        amr.setClockRate(8000);
        amr.setMimeType("AMR");
        amr.appendRecvFmtp("octet-align=1");
        amr.setNumChannels(1);
        amr.setType(PayloadType.MediaType.Audio);
        amr.setNumber(114);
        pts[0] = amr;
        sd.setPayloadTypes(pts);
        md.addStreamDescription(sd);
        md.setAddress(getSal().getLocalAddr());

        SessionDescription sdp = SdpUtils.toSessionDescription(md);

        // Set MIKEY-SAKKE attributes if call is Outgoing
        if (mDir == CallDirection.Outgoing) {
            try {
                ClientData clientData = ClientData.instance();
                MikeySakkeSrtpParams params = MikeySakkeSrtpParams.instance();
                String to = SalFactory.instance().createSalAddress(mOp.getTo()).getUserName();
                String targetId = KeyData.generateDatedIdentifier(to);
                params.setOtherDeviceId(targetId);

                Hashtable parameters = new Hashtable();
                KeyData keys = clientData.getKeys();

                // Request to Math Server
                parameters.put("targetId", targetId);
                parameters.put("parameterSet", "1");
                parameters.put("z",
                        keys.getKmsPublicKey().toString());

                JSONObject jsonObj = Connect.mathServerRequest("GENERATE",
                        parameters);

                if (jsonObj.has("error")) {
                    Dialog.inform("error while generating SED: " + jsonObj.get("error"));

                }

                String ssv = "";
                String sed = "";

                if (jsonObj.has("ssv") && jsonObj.has("sed")) {
                    ssv = (String) jsonObj.get("ssv");
                    sed = (String) jsonObj.get("sed");
                }
                if (ssv == null || sed == null || ssv.equals("null") || sed.equals("null")) {
                    Dialog.inform("Null SSV or SED received");
                }

                // Generate MIKEY message
                
                byte[] mikeyIMsg = MSUtils.generateMikeySakkeIMessage(sed, keys);

                // encode MIKEY I to base64
                String base64MikeyIMsg = Base64OutputStream.encodeAsString(mikeyIMsg, 0, mikeyIMsg.length, false, false);
                String mikeyIMsgAttribute = "mikey " + base64MikeyIMsg;

                sdp.setAttribute("key-mgmt", mikeyIMsgAttribute);
                params.setCurrentSessionSSV(MSUtils.hexStringToByteArray(ssv));

                md = SdpUtils.toSalMediaDescription(sdp);

            } catch (SdpException e) {
                e.printStackTrace();
                mLog.fatal("SDPException while setting SDP attributes: ", e);
                Dialog.inform("SDPException while setting SDP attributes: " + e.toString());
            } catch (IOException e) {
                mLog.fatal("IOException while generating SSV/SED: ", e);
                Dialog.inform("IOException while generating SSV/SED: " + e.toString());
            } catch (JSONException e) {
                Dialog.inform("JSONException while getting SSV/SED: " + e.toString());
                mLog.fatal("JSONException while getting SSV/SED: ", e);
            } catch (IllegalArgumentException e) {
                mLog.fatal("IllegalArgumentException while generating SSV/SED: ", e);
                Dialog.inform("IllegalArgumentException while generating SSV/SED: " + e.toString());
            } catch (Exception e) {
                mLog.fatal("Problem while generating I Message. \n", e);
                Dialog.inform("Problem while generating I Message: " + e.toString());
            }
        }
        return md;
    }

    public void initMediaStreams() throws SalException {
        mAudioStream = new AudioStreamImpl();
        try {
            String host = "0.0.0.0";
            int port = getLocalMediaDescription().getStream(0).getPort();

            SocketAddress addr = JOrtpFactory.instance().createSocketAddress(host, port);
            if (mCore.mRtpTransport != null)
                mAudioStream.setRtpTransport(mCore.mRtpTransport);
            mAudioStream.init(addr);
        } catch (RtpException e) {
            mAudioStream.stop();
            throw new SalException("Cannot init media stream", e);
        }
    }

    public void startMediaStreams() {
        if (mAudioStream != null) {
            try {
                mAudioStream.start(makeAudioStreamParams());
            } catch (RtpException e) {
                mLog.error("Cannot start stream", e);
            }
        }
    }

    private RtpProfile makeProfile(final SalStreamDescription sd) {
        RtpProfile prof = JOrtpFactory.instance().createRtpProfile();
        PayloadType pts[] = sd.getPayloadTypes();
        int i;
        for (i = 0; i < pts.length; i++) {
            prof.setPayloadType(pts[i], pts[i].getNumber());
        }
        return prof;
    }

    private AudioStreamParameters makeAudioStreamParams() {
        AudioStreamParameters p = null;
        SalStreamDescription sd = getFinalMediaDescription().getStream(0);
        if (sd != null) {
            SocketAddress dest = JOrtpFactory.instance().createSocketAddress(
                    sd.getAddress(), sd.getPort());
            p = new AudioStreamParameters();
            p.setRtpProfile(makeProfile(sd));
            p.setRemoteDest(dest);
            p.setActivePayloadTypeNumber(sd.getPayloadTypes()[0].getNumber());
            p.setUploadPtime(mCore.getUploadPtime());
        }
        return p;
    }

    public void terminateMediaStreams() {
        if (mAudioStream != null) {
            mAudioStream.stop();
            mAudioStream = null;
        }
    }

    public AudioStream getAudioStream() {
        return mAudioStream;
    }

    public SalOp getOp() {
        return mOp;
    }

    private Sal getSal() {
        return mOp.getSal();
    }

    private void setCallLog(final LinphoneCallLogImpl aCallLog) {
        mCallLog = aCallLog;
    }

    public void sendDtmf(final char number) {
        mOp.sendDtmf(number);
    }

    public LinphoneCallParams getCurrentParamsReadOnly() {
        throw new RuntimeException("Not implemented yet");
    }

    public void enableCamera(final boolean enabled) {
        throw new RuntimeException("Not implemented yet");
    }

    public void enableEchoCancellation(final boolean enable) {
        throw new RuntimeException("Not implemented yet");
    }

    public void enableEchoLimiter(final boolean enable) {
        throw new RuntimeException("Not implemented yet");
    }

    public LinphoneCallParams getCurrentParamsCopy() {
        throw new RuntimeException("Not implemented yet");
    }

    public LinphoneCall getReplacedCall() {
        throw new RuntimeException("Not implemented yet");
    }

    public boolean isEchoCancellationEnabled() {
        throw new RuntimeException("Not implemented yet");
    }

    public boolean isEchoLimiterEnabled() {
        throw new RuntimeException("Not implemented yet");
    }

    public LinphoneCallParams getRemoteParams() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean cameraEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    public int getDuration() {
        // TODO Auto-generated method stub
        return 0;
    }

    public float getCurrentQuality() {
        // TODO Auto-generated method stub
        return 0;
    }

    public float getAverageQuality() {
        // TODO Auto-generated method stub
        return 0;
    }

    public String getAuthenticationToken() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isAuthenticationTokenVerified() {
        // TODO Auto-generated method stub
        return false;
    }

    public void setAuthenticationTokenVerified(final boolean verified) {
        // TODO Auto-generated method stub

    }

    public boolean isInConference() {
        // TODO Auto-generated method stub
        return false;
    }

    public float getPlayVolume() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void takeSnapshot(final String path) {
        // TODO Auto-generated method stub

    }

    public void zoomVideo(final float factor, final float cx, final float cy) {
        // TODO Auto-generated method stub

    }

    public void setChatRoom(final LinphoneChatRoomImpl room) {
        mChatRoom = room;
    }

    public LinphoneChatRoomImpl getChatRoom() {
        return mChatRoom;
    }

}
