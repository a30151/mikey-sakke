/*
LinphoneCoreImpl.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.linphone.jlinphone.core;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Vector;

import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.microedition.io.SocketConnection;
import javax.microedition.io.file.FileConnection;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.VolumeControl;

import org.linphone.core.CallDirection;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneAuthInfo;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallLog;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.core.LinphoneCoreListener;
import org.linphone.core.LinphoneFriend;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.OnlineStatus;
import org.linphone.core.PayloadType;
import org.linphone.core.VideoSize;
import org.linphone.jlinphone.gui.msg.MessageStorage;
import org.linphone.jortp.JOrtpFactory;
import org.linphone.jortp.Logger;
import org.linphone.jortp.RtpSessionImpl;
import org.linphone.jortp.RtpTransport;
import org.linphone.jortp.SocketAddress;
import org.linphone.jortp.SrtpKey;
import org.linphone.sal.Sal;
import org.linphone.sal.Sal.Reason;
import org.linphone.sal.SalAuthInfo;
import org.linphone.sal.SalError;
import org.linphone.sal.SalException;
import org.linphone.sal.SalFactory;
import org.linphone.sal.SalListener;
import org.linphone.sal.SalMediaDescription;
import org.linphone.sal.SalOp;
import org.linphone.sal.SalReason;

import mikeysakke.client.KeyData;
import mikeysakke.client.MSUtils;
import mikeysakke.client.MikeySakkeSrtpParams;
import net.rim.blackberry.api.phone.Phone;
import net.rim.device.api.media.control.AudioPathControl;
import net.rim.device.api.notification.NotificationsManager;
import net.rim.device.api.system.Alert;
import net.rim.device.api.system.ControlledAccessException;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.WLANInfo;

public class LinphoneCoreImpl implements LinphoneCore {
    private OnTextMessageListener mTextMessageListener;

    public void setTextMessageListener(final OnTextMessageListener l) {
        mTextMessageListener = l;
    }

    public interface OnTextMessageListener {
        void onTextReceived(LinphoneAddress peer, String message, Date when);

        void onTextSentEvent(Object opaque, LinphoneAddress to, int event, String phrase);
    }

    class TracabableLinphoneCoreListener implements LinphoneCoreListener {
        final LinphoneCoreListener mLinphoneCoreListener;

        TracabableLinphoneCoreListener(final LinphoneCoreListener listener) {
            mLinphoneCoreListener = listener;
        }

        public void show(final LinphoneCore lc) {
            mLog.info(this + " show");
            mLinphoneCoreListener.show(lc);
        }

        public void authInfoRequested(final LinphoneCore lc, final String realm, final String username) {
            mLog.info(this + " authInfoRequested for [" + username + "@" + realm + "]");
            mLinphoneCoreListener.authInfoRequested(lc, realm, username);
        }

        public void displayStatus(final LinphoneCore lc, final String message) {
            mLog.info(this + " displayStatus  [" + message + "]");
            mLinphoneCoreListener.displayStatus(lc, message);
        }

        public void displayMessage(final LinphoneCore lc, final String message) {
            mLog.info(this + " displayMessage  [" + message + "]");
            mLinphoneCoreListener.displayMessage(lc, message);
        }

        public void displayWarning(final LinphoneCore lc, final String message) {
            mLog.info(this + " displayWarning  [" + message + "]");
            mLinphoneCoreListener.displayWarning(lc, message);
        }

        public void globalState(final LinphoneCore lc, final GlobalState state, final String message) {
            mLog.info(this + " globalState  [" + state + "] with message [" + message + "]");
            mLinphoneCoreListener.globalState(lc, state, message);
        }

        public void callState(final LinphoneCore lc, final LinphoneCall call, final State cstate,
                final String message) {
            mLog.info(this + " callState  [" + cstate + "] for [" + call + "] with message [" + message + "]");
            mLinphoneCoreListener.callState(lc, call, cstate, message);
        }

        public void callEncryptionChanged(final LinphoneCore lc, final LinphoneCall call,
                final boolean encrypted, final String authenticationToken) {
            mLog.info(this + " callEncryptionChanged  is encrypted ?[" + encrypted + "] for [" + call + "] with token [" + authenticationToken + "]");
            mLinphoneCoreListener.callEncryptionChanged(lc, call, encrypted, authenticationToken);
        }

        public void registrationState(final LinphoneCore lc, final LinphoneProxyConfig cfg,
                final RegistrationState cstate, final String smessage) {
            mLog.info(this + " registrationState  [" + cstate + "] for [" + cfg + "] with message [" + smessage + "]");
            mLinphoneCoreListener.registrationState(lc, cfg, cstate, smessage);
        }

        public void newSubscriptionRequest(final LinphoneCore lc, final LinphoneFriend lf,
                final String url) {
            mLog.info(this + " newSubscriptionRequest  for [" + lf + "]  [" + url + "]");
            mLinphoneCoreListener.newSubscriptionRequest(lc, lf, url);
        }

        public void notifyPresenceReceived(final LinphoneCore lc, final LinphoneFriend lf) {
            mLog.info(this + " notifyPresenceReceived  for [" + lf + "] ");
            mLinphoneCoreListener.notifyPresenceReceived(lc, lf);
        }

        public void textReceived(final LinphoneCore lc, final LinphoneChatRoom cr,
                final LinphoneAddress from, final String message) {
            mLog.info(this + " textReceived  for [" + cr + "] from [" + from + "] message is [" + message + "]");
            // mLinphoneCoreListener.textReceived(lc, cr,from,message);
            final String uri = from.asStringUriOnly();
            final Date now = new Date();
            MessageStorage.getInstance().receivedMsg(uri, message, now);
            mTextMessageListener.onTextReceived(from, message, now);
        }

        public void ecCalibrationStatus(final LinphoneCore lc, final EcCalibratorStatus status,
                final int delay_ms, final Object data) {
            mLog.info(this + " ecCalibrationStatus   [" + status + "] delay [" + delay_ms + "] ");
            mLinphoneCoreListener.ecCalibrationStatus(lc, status, delay_ms, data);
        }

        public String toString() {
            return " lc [" + hashCode() + "]";
        }

        public void notifyReceived(final LinphoneCore lc, final LinphoneCall call,
                final LinphoneAddress from, final byte[] event) {
            StringBuffer str = null;
            mLog.info(this + " notifyReceived for call [" + call + "] from  [" + from + "]");
            if (event != null) {
                str = new StringBuffer(Math.max(event.length, 50));
                for (int i = 0; i < Math.max(event.length, 50); i++) {
                    str.append((char) event[i]);
                }

                mLog.info(this + " notifyReceived body begening with [" + str != null ? str.toString() : "no body" + "] ");
            }
            mLinphoneCoreListener.notifyReceived(lc, call, from, event);
        }

    };

    Sal mSal;
    LinphoneAuthInfo mAuthInfo;
    LinphoneProxyConfigImpl mProxyCfg;
    LinphoneCoreListener mListener;
    LinphoneCallImpl mCurrentCall;

    Logger mLog = JOrtpFactory.instance().createLogger("LinphoneCore");
    Player mRingPlayer;
    Player mRingBackPlayer;
    Vector mCallLogs;
    private PersistentObject mPersistentObject;
    boolean mNetworkIsUp = false;
    Sal.Transport mTransport = Sal.Transport.Datagram;
    int mSipPort = 5060; // random
    RtpTransport mRtpTransport;
    int mUploadPtime = 20;
    boolean mSpeakerMode = false;
    static boolean isInstanciated = false; // so far only one LinphoneCore is
                                           // allowed
    boolean isValid = true;

    private void startStreamIfNeeded(final LinphoneCallImpl call) {
        SalMediaDescription oldmd = call.getFinalMediaDescription();
        ;
        boolean keepStreams = false;
        if (oldmd != null) {
            if (oldmd.equals(call.getOp().getFinalMediaDescription())) {
                mLog.info("Current and new media descriptions are equal, keeping same streams");
                keepStreams = true;
            } else {
                mLog.info("Current and new media descriptions are different, need to restart streams");
            }
        }
        call.setFinalMediaDescription(call.getOp().getFinalMediaDescription());
        if (call.getAudioStream().isStarted() && !keepStreams) {
            call.terminateMediaStreams(); // comes from; early media state
            try {
                call.initMediaStreams();
            } catch (SalException e) {
                mLog.error("Cannot create media stream", e);
                call.getOp().callTerminate();
                return;
            }
        }
        if (!keepStreams) {
            call.startMediaStreams();
        }

    }

    SalListener mSalListener = new SalListener() {
        private final LinphoneCoreImpl core() {
            return LinphoneCoreImpl.this;
        }

        public void onCallAccepted(final SalOp op) {
            LinphoneCallImpl c = (LinphoneCallImpl) op.getUserContext();

            if (mCurrentCall == null || mCurrentCall != c) {
                op.callTerminate();
            }
            deallocateRingBackPlayerIfNeeded();
            startStreamIfNeeded(c);
            c.setState(LinphoneCall.State.Connected);
            enableSpeaker(mSpeakerMode);
        }

        public void onCallReceived(final SalOp op) {
            try {

                if (mCurrentCall != null || Phone.getActiveCall() != null) {
                    op.callDecline(Sal.Reason.Busy, null);
                } else {
                    mCurrentCall = createIncomingCall(op);
                    mCurrentCall.setState(LinphoneCall.State.IncomingReceived);
                    mCallLogs.addElement(mCurrentCall.getCallLog());

                    op.callRinging();
                    mRingPlayer = Manager.createPlayer(getClass().getResourceAsStream("/oldphone.wav"), "audio/wav");
                    mRingPlayer.realize();
                    ((VolumeControl) mRingPlayer.getControl("VolumeControl")).setLevel(NotificationsManager.getMasterNotificationVolume());
                    AudioPathControl lPathCtr = (AudioPathControl) mRingPlayer.getControl("net.rim.device.api.media.control.AudioPathControl");
                    lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HANDSFREE);
                    mRingPlayer.prefetch();
                    mRingPlayer.setLoopCount(Integer.MAX_VALUE);
                    mRingPlayer.start();
                    if (NotificationsManager.isVibrateOnly()) {
                        Alert.startVibrate(25000);
                    }

                }
            } catch (Throwable e) {
                mLog.error("Cannot create incoming call", e);
                mCurrentCall.terminateMediaStreams();
                op.callDecline(Sal.Reason.Unknown, null);
                return;
            }

        }

        public void onCallRinging(final SalOp op) {
            LinphoneCallImpl c = (LinphoneCallImpl) op.getUserContext();
            if (mCurrentCall == null || mCurrentCall != c) {
                op.callTerminate();
            }
            if (mCurrentCall != null && op.getFinalMediaDescription() == null) {
                mCurrentCall.setState(LinphoneCall.State.OutgoingRinging);
                try {
                    mRingBackPlayer = Manager.createPlayer(getClass().getResourceAsStream("/ringback.wav"), "audio/wav");
                    mRingBackPlayer.realize();
                    mRingBackPlayer.prefetch();
                    mRingBackPlayer.setLoopCount(-1);
                    mRingBackPlayer.start();
                } catch (Throwable e) {
                    mLog.error("cannot play ringback tone", e);
                    deallocateRingBackPlayerIfNeeded();
                    return;
                }
            }
            if (op.getFinalMediaDescription() != null && c.getFinalMediaDescription() == null) {
                mCurrentCall.setState(LinphoneCall.State.OutgoingEarlyMedia);
                deallocateRingBackPlayerIfNeeded(); // just in case
                c.setFinalMediaDescription(op.getFinalMediaDescription());
                c.startMediaStreams();
            }
            enableSpeaker(mSpeakerMode);
        }

        public void onCallTerminated(final SalOp op) {
            if (mCurrentCall != null) {
                deallocateRingBackPlayerIfNeeded();
                deallocateRingPlayerIfNeeded();
                mCurrentCall.terminateMediaStreams();
                mCurrentCall.setState(LinphoneCall.State.CallEnd);
                mCurrentCall = null;
            }
            mSpeakerMode = false;
        }

        public void onAuthRequested(final SalOp op, final String realm, final String userName) {
            try {
                if (mAuthInfo != null && userName.equalsIgnoreCase(mAuthInfo.getUsername())) {
                    op.authenticate(new SalAuthInfo(realm, userName, mAuthInfo.getPassword()));
                } else {
                    mListener.authInfoRequested(LinphoneCoreImpl.this, realm, userName);
                }
            } catch (Exception e) {
                mLog.error("Cannot provide auth info", e);
            }

        }

        public void onAuthSuccess(final SalOp lSalOp, final String realm, final String username) {

        }

        public void onCallFailure(final SalOp op, final String reasonPhrase) {
            deallocateRingBackPlayerIfNeeded();
            if (mCurrentCall != null) {
                mCurrentCall.terminateMediaStreams();
                mCurrentCall.setState(LinphoneCall.State.Error, reasonPhrase);
                mCurrentCall = null;
            }
        }

        public void OnRegisterFailure(final SalOp op, final SalError error,
                final SalReason reason, final String details) {
            mListener.displayStatus(LinphoneCoreImpl.this, "Registration failure [" + details + "]");
            LinphoneProxyConfigImpl proxy = (LinphoneProxyConfigImpl) getDefaultProxyConfig();
            mListener.registrationState(LinphoneCoreImpl.this, proxy, proxy.setState(RegistrationState.RegistrationFailed), details);

        }

        public void OnRegisterSuccess(final SalOp op, final boolean registered) {
            LinphoneProxyConfigImpl proxy = (LinphoneProxyConfigImpl) getDefaultProxyConfig();
            mListener.registrationState(LinphoneCoreImpl.this, proxy
                    , proxy.setState(registered ? RegistrationState.RegistrationOk : RegistrationState.RegistrationCleared), null);
            mProxyCfg.setRegistered(true);
            LinphoneAddress lTo = LinphoneCoreFactory.instance().createLinphoneAddress(op.getTo());
            mListener.displayStatus(LinphoneCoreImpl.this, "Registered to " + lTo.getDomain());

        }

        public void onCallUpdating(final SalOp op) {
            try {
                LinphoneCallImpl c = (LinphoneCallImpl) op.getUserContext();
                op.callAccept();
                startStreamIfNeeded(c);
            } catch (Throwable e) {
                mLog.error("Cannot update media stream", e);
            }
        }

        public void onIoError(final Sal sal) {
            mLog.warn("IO error reporting, closing sal");
            closeSal();
        }

        public void onNotify(final SalOp op, final String from, final byte[] event) {
            LinphoneCallImpl c = null;
            if (op != null && op.getUserContext() != null) {
                c = (LinphoneCallImpl) op.getUserContext();
            }
            mListener.notifyReceived(LinphoneCoreImpl.this, c, LinphoneCoreFactory.instance().createLinphoneAddress(from), event);

        }

        public void onTextMessageSentEvent(final SalOp op, final int event, final String phrase) {
            LinphoneAddress to = LinphoneFactoryImpl.instance().createLinphoneAddress(op.getTo());
            Object opaque = op.getUserContext();
            MessageStorage.getInstance().updateSentMsg(opaque, to, event, phrase);
            mTextMessageListener.onTextSentEvent(opaque, to, event, phrase);
        }

        public void onTextMessageReceived(final SalOp op, final String from, final String msg) {
            LinphoneAddress fromAddr = LinphoneCoreFactory.instance().createLinphoneAddress(from);
            mListener.textReceived(core(), null, fromAddr, msg);
        }
    };

    private LinphoneCallImpl createIncomingCall(final SalOp op) throws SalException {
        return new LinphoneCallImpl(this, op, CallDirection.Incoming);
    }

    private LinphoneCallImpl createOutgoingCall(final LinphoneAddress addr) throws SalException {
        SalOp op = mSal.createSalOp();
        // is route header required ?
        if (getDefaultProxyConfig() != null
                && getDefaultProxyConfig().getDomain().equalsIgnoreCase(addr.getDomain())) {
            op.setRoute(getDefaultProxyConfig().getProxy());
        }
        op.setFrom(LinphoneCoreImpl.this.getIdentity());
        op.setTo(addr.asString());
        LinphoneCallImpl c = new LinphoneCallImpl(this, op, CallDirection.Outgoing);
        return c;
    }

    private String getIdentity() throws SalException {
        if (mProxyCfg != null) {
            return mProxyCfg.getIdentity();
        }
        return "sip:anonymous@" + mSal.getLocalAddr() + ":" + Integer.toString(mSal.getLocalPort());
    }

    public LinphoneCoreImpl(final LinphoneCoreListener listener, final String userConfig,
            final String factoryConfig, final Object userdata) throws LinphoneCoreException {
        if (isInstanciated) {
            throw new LinphoneCoreException("Already instanciated, use LinphoneCore.destroy to release previous instance");
        }
        isInstanciated = true;
        try {
            mPersistentObject = PersistentStore.getPersistentObject("org.jlinphone.logs".hashCode());
            if (mPersistentObject.getContents() != null) {
                mCallLogs = (Vector) mPersistentObject.getContents();
                // limit Call logs number
                while (mCallLogs.size() > 30) {
                    mCallLogs.removeElementAt(0);
                }
            } else {
                mCallLogs = new Vector();
            }

            mListener = new TracabableLinphoneCoreListener(listener);
            mListener.globalState(this, GlobalState.GlobalOn, null);
            // createSal();
        } catch (ControlledAccessException e1) {
            throw new LinphoneCoreException("Persistent store permission denied");
        } catch (Throwable e) {
            destroy();
            mLog.error("Cannot create Linphone core for user conf ["
                    + userConfig
                    + "] factory conf [" + factoryConfig + "]", e);
            throw new LinphoneCoreException("Initialization failure caused by: " + e.getMessage());

        }
    }

    public synchronized void acceptCall(final LinphoneCall aCall) throws LinphoneCoreException {
        checkValidity();
        mLog.info(this + " acceptCall for [" + aCall + "]");
        if (mCurrentCall != null) {
            try {                
            	
                MikeySakkeSrtpParams params = MikeySakkeSrtpParams.instance();

                // set otherDeviceId, as there is no access to getFrom() when
                // needed
                String from = SalFactory.instance().createSalAddress(mCurrentCall.getOp().getFrom()).getUserName();
                params.setOtherDeviceId(KeyData.generateDatedIdentifier(from));
                
                mCurrentCall.getOp().callAccept();

                byte[] encKey = MSUtils.generateKeyMaterialFromTGK(MSUtils.TEK, params.getCurrentSessionSSV(), 
                		params.getCsbId(), params.getSendCsId(), params.getRand());
                //clientData.deleteSSV(); // SSV no longer needs to be stored.

                byte[] salt = MSUtils.generateKeyMaterialFromTGK(MSUtils.SALTING_KEY, params.getCurrentSessionSSV(), 
                		params.getCsbId(), params.getSendCsId(), params.getRand());
                
                byte[] decKey = MSUtils.generateKeyMaterialFromTGK(MSUtils.TEK, params.getCurrentSessionSSV(), 
                		params.getCsbId(), params.getRecCsId(), params.getRand());

                byte[] decSalt = MSUtils.generateKeyMaterialFromTGK(MSUtils.SALTING_KEY, params.getCurrentSessionSSV(), 
                		params.getCsbId(), params.getRecCsId(), params.getRand());
                
                RtpSessionImpl rtp = (RtpSessionImpl) mCurrentCall.getAudioStream().getRtpSession();

                rtp.setSentSSRC(params.getSendSSRC());
                rtp.enableSrtp(
                        (short) encKey.length,
                        (short) 10, //authkey length
                        new SrtpKey(encKey, salt),
                        new SrtpKey(decKey, decSalt), params.getRecSSRC());
                
                try 
                {
                  FileConnection fc = (FileConnection)Connector.open("file:///store/home/user/CallAcceptMikeyParams.txt");
                  // If no exception is thrown, then the URI is valid, but the file may or may not exist.
                  if (!fc.exists())
                  {
                      fc.create();  // create the file if it doesn't exist
                  }
                  OutputStream outStream = fc.openOutputStream(); 
                  outStream.write(params.toString().getBytes());
                  outStream.write(("\r\nTEK: " + MSUtils.toHex(encKey)).getBytes());
                  outStream.write(("\r\nTEK dec: " + MSUtils.toHex(decKey)).getBytes());
                  outStream.write(("\r\nEnc Salt: " + MSUtils.toHex(salt)).getBytes());
                  outStream.write(("\r\nDec Salt: " + MSUtils.toHex(decSalt)).getBytes()); 
                  outStream.write(("\r\n\r\n").getBytes()); 
                  outStream.close();
                  fc.close();
                 }
                 catch (IOException ioe) 
                 {
                    System.out.println(ioe.getMessage() );
                 }



                mCurrentCall.setFinalMediaDescription(mCurrentCall.getOp().getFinalMediaDescription());
                mCurrentCall.setState(LinphoneCall.State.Connected, null);
                deallocateRingPlayerIfNeeded();
                mCurrentCall.startMediaStreams();
                enableSpeaker(mSpeakerMode);
            } catch (Throwable e) {
                throw new LinphoneCoreException("cannot accept call from [" + getRemoteAddress() + "]", e);
            }
        }
    }

    public synchronized void addAuthInfo(final LinphoneAuthInfo info) {
        checkValidity();
        mAuthInfo = info;
    }

    public synchronized void addProxyConfig(final LinphoneProxyConfig proxyCfg)
            throws LinphoneCoreException {
        checkValidity();
        mProxyCfg = (LinphoneProxyConfigImpl) proxyCfg;
        proxyCfg.done();
    }

    public synchronized void clearAuthInfos() {
        checkValidity();
        mAuthInfo = null;
    }

    public synchronized void clearCallLogs() {
        checkValidity();
        mCallLogs.removeAllElements();
    }

    public synchronized void clearProxyConfigs() {
        checkValidity();
        mProxyCfg = null;
    }

    public synchronized void destroy() {
        checkValidity();
        mLog.info(this + " destroy");
        try {
            if (isIncall()) {
                terminateCall(mCurrentCall);
            }
            if (mSal != null) {
                mSal.close();
                mSal = null;
            }
            if (mPersistentObject != null) {
                mPersistentObject.setContents(mCallLogs);
                mPersistentObject.commit();
            }
            if (mListener != null) {
                mListener.globalState(this, GlobalState.GlobalOff, null);
            }
        } finally {
            isInstanciated = false;
            isValid = false;
        }
    }

    public synchronized LinphoneCallLog[] getCallLogs() {
        checkValidity();
        LinphoneCallLog[] array = new LinphoneCallLog[mCallLogs.size()];
        mCallLogs.copyInto(array);
        return array;
    }

    public synchronized LinphoneProxyConfig getDefaultProxyConfig() {
        checkValidity();
        return mProxyCfg;
    }

    public synchronized LinphoneAddress getRemoteAddress() {
        checkValidity();
        if (mCurrentCall != null) {
            return mCurrentCall.getRemoteAddress();
        }
        return null;
    }

    public float getSoftPlayLevel() {
        // TODO Auto-generated method stub
        return 0;
    }

    public synchronized LinphoneAddress interpretUrl(final String rawDestination)
            throws LinphoneCoreException {
        checkValidity();
        mLog.info(this + " interpretUrl for [" + rawDestination + "]");
        String lDest = rawDestination.trim();

        LinphoneAddress addr = LinphoneCoreFactory.instance().createLinphoneAddress(lDest);
        if (addr == null) {
            if (lDest.indexOf("@") == -1) {
                LinphoneProxyConfigImpl cfg = (LinphoneProxyConfigImpl) getDefaultProxyConfig();
                if (cfg != null) {
                    // remove space and -
                    StringBuffer lFilteredDestination = new StringBuffer();
                    boolean isPhoneNumer = isPhoneNumber(lDest);
                    for (int i = 0; i < lDest.length(); i++) {
                        char lCurrentChar = lDest.charAt(i);
                        if (lCurrentChar != ' ' && !(lCurrentChar == '-' && isPhoneNumer)) {
                            lFilteredDestination.append(lCurrentChar);
                        }
                    }
                    lDest = lFilteredDestination.toString();
                    LinphoneAddress tmp = LinphoneCoreFactory.instance()
                            .createLinphoneAddress(cfg.getIdentity());
                    tmp.setDisplayName(null);
                    if (lDest.startsWith("+") && cfg.isDialPlusEscaped()) {
                        tmp.setUserName("00" + lDest.substring(1));
                    } else {
                        tmp.setUserName(lDest);
                    }
                    return tmp;
                } else {
                    throw new LinphoneCoreException("Bad destination [" + lDest + "]");
                }
            } else {
                addr = LinphoneCoreFactory.instance().createLinphoneAddress(
                        "sip:" + lDest);
                if (addr == null) {
                    throw new LinphoneCoreException("Bad destination [" + lDest + "]");
                }
            }
        }
        return addr;
    }

    public synchronized LinphoneCall invite(final String address) throws LinphoneCoreException {
        checkValidity();
        LinphoneAddress addr = interpretUrl(address);
        return invite(addr);
    }

    public synchronized LinphoneCall invite(final LinphoneAddress to) throws LinphoneCoreException {
        checkValidity();
        mLog.info(this + " invite  [" + to + "]");
        if (mCurrentCall != null) {
            return null;
        }
        try {
            LinphoneCallImpl c = createOutgoingCall(to);
            MikeySakkeSrtpParams params = MikeySakkeSrtpParams.instance();
            byte[] encKey = MSUtils.generateKeyMaterialFromTGK(MSUtils.TEK, params.getCurrentSessionSSV(), 
            		params.getCsbId(), params.getSendCsId(), params.getRand());

            byte[] salt = MSUtils.generateKeyMaterialFromTGK(MSUtils.SALTING_KEY, params.getCurrentSessionSSV(), 
            		params.getCsbId(), params.getSendCsId(), params.getRand());
            
            byte[] decKey = MSUtils.generateKeyMaterialFromTGK(MSUtils.TEK, params.getCurrentSessionSSV(), 
            		params.getCsbId(), params.getRecCsId(), params.getRand());

            byte[] decSalt = MSUtils.generateKeyMaterialFromTGK(MSUtils.SALTING_KEY, params.getCurrentSessionSSV(), 
            		params.getCsbId(), params.getRecCsId(), params.getRand());
            try 
            {
              FileConnection fc = (FileConnection)Connector.open("file:///store/home/user/InviteMikeyParams.txt");
              // If no exception is thrown, then the URI is valid, but the file may or may not exist.
              if (!fc.exists())
              {
                  fc.create();  // create the file if it doesn't exist
              }
              OutputStream outStream = fc.openOutputStream(); 
              outStream.write(params.toString().getBytes());
              outStream.write(("\r\nTEK: " + MSUtils.toHex(encKey)).getBytes());
              outStream.write(("\r\nTEK dec: " + MSUtils.toHex(decKey)).getBytes());
              outStream.write(("\r\nEnc Salt: " + MSUtils.toHex(salt)).getBytes());
              outStream.write(("\r\nDec Salt: " + MSUtils.toHex(decSalt)).getBytes()); 
              outStream.write(("\r\n\r\n").getBytes()); 
              outStream.close();
              fc.close();
             }
             catch (IOException ioe) 
             {
                System.out.println(ioe.getMessage() );
             }


            
            
            RtpSessionImpl rtp = (RtpSessionImpl) c.getAudioStream().getRtpSession();
            rtp.setSentSSRC(params.getSendSSRC());
            rtp.enableSrtp(
                    (short) encKey.length,
                    (short) 10, //auth tag length
                    new SrtpKey(encKey, salt),
                    new SrtpKey(decKey, decSalt), params.getRecSSRC());

            c.getOp().call();
            mCurrentCall = c;
            c.setState(LinphoneCall.State.OutgoingInit, null);
            mCallLogs.addElement(c.getCallLog());

            return c;

        } catch (Throwable e) {
            terminateCall(mCurrentCall);
            throw new LinphoneCoreException("Cannot place call to [" + to + "]", e);

        }
    }

    public synchronized boolean isInComingInvitePending() {
        checkValidity();
        return mCurrentCall != null
                && mCurrentCall.getState() == LinphoneCall.State.IncomingReceived;
    }

    public synchronized boolean isIncall() {
        checkValidity();
        return mCurrentCall != null;
    }

    public synchronized boolean isMicMuted() {
        checkValidity();
        if (mCurrentCall != null && mCurrentCall.getAudioStream() != null) {
            return mCurrentCall.getAudioStream().isMicMuted();
        } else {
            return false;
        }
    }

    public synchronized void iterate() {
        checkValidity();
        if (mSal != null && mSal.getTransport() != mTransport) {
            // transport has changed, reconfiguring
            closeSal();
        }
        if (mSal == null && mNetworkIsUp == true) {
            try {
                // create new sal
                createSal();
                if (mProxyCfg != null && mProxyCfg.registerEnabled()) {
                    // mProxyCfg.edit();
                    // mProxyCfg.enableRegister(false);
                    // mProxyCfg.done();
                    // //send unregister
                    // ((LinphoneProxyConfigImpl)mProxyCfg).check(this);
                    // mProxyCfg.edit();
                    // mProxyCfg.enableRegister(true);
                    mProxyCfg.done();
                }

            } catch (Throwable e) {

                mLog.error("Cannot create listening point", e);
            }
        } else if (mSal != null && mNetworkIsUp == false) {
            closeSal();
        }
        if (mSal != null && mProxyCfg != null) {
            mProxyCfg.check(this);
        }
    }

    public synchronized void muteMic(final boolean isMuted) {
        checkValidity();
        if (mCurrentCall != null && mCurrentCall.getAudioStream() != null) {
            mCurrentCall.getAudioStream().muteMic(isMuted);
        }

    }

    public synchronized void sendDtmf(final char number) {
        checkValidity();
        if (mCurrentCall != null) {
            mCurrentCall.sendDtmf(number);
        }
    }

    public synchronized void setDefaultProxyConfig(final LinphoneProxyConfig proxyCfg) {
        checkValidity();
        mProxyCfg = (LinphoneProxyConfigImpl) proxyCfg;
    }

    public synchronized void setNetworkReachable(final boolean isReachable) {
        checkValidity();
        if (mNetworkIsUp != isReachable) {
            mLog.warn("New network state is [" + isReachable + "]");
        }
        mNetworkIsUp = isReachable;

    }

    public void setSoftPlayLevel(final float gain) {
        // TODO Auto-generated method stub

    }

    public synchronized void terminateCall(final LinphoneCall aCall) {
        checkValidity();
        mLog.info(this + " terminateCall  [" + aCall + "]");
        if (mCurrentCall != null) {
            if (isInComingInvitePending()) {
                try {
                    mRingPlayer.stop();
                    mRingPlayer.close();
                    mRingPlayer = null;
                } catch (MediaException e) {
                    mLog.error("cannot stop ringer", e);
                }
                mCurrentCall.getOp().callDecline(Reason.Declined, null);
            } else {
                deallocateRingBackPlayerIfNeeded();
                mCurrentCall.terminateMediaStreams();
                mCurrentCall.getOp().callTerminate();
            }
            if (mCurrentCall != null && mCurrentCall.getState() != LinphoneCall.State.CallEnd) {
                mCurrentCall.setState(LinphoneCall.State.CallEnd, null);
            }
            mCurrentCall = null;
        }
    }

    public Sal getSal() {
        return mSal;
    }

    public synchronized void setSignalingTransportPorts(final Transports aTransport) {
        checkValidity();
        mLog.info(this + " setSignalingTransportPorts  [" + aTransport + "]");
        if (aTransport.udp != 0) {
            mTransport = Sal.Transport.Datagram;
        } else if (aTransport.tcp != 0) {
            mTransport = Sal.Transport.Stream;
        } else {
            mLog.error("Unexpected transport [" + aTransport + "]");
        }
    }

    private synchronized void closeSal() {
        if (isIncall()) {
            terminateCall(mCurrentCall);
        }
        if (mProxyCfg != null
                && (mProxyCfg.getState() == RegistrationState.RegistrationOk
                || mProxyCfg.getState() == RegistrationState.RegistrationProgress)) {
            if (mNetworkIsUp == true) {
                mProxyCfg.edit(); // just to trigger unregister
                mProxyCfg.done();
                mListener.displayStatus(LinphoneCoreImpl.this, "Registration to [" + mProxyCfg.getProxy() + "] cleared");
                LinphoneProxyConfigImpl proxy = (LinphoneProxyConfigImpl) getDefaultProxyConfig();
                mListener.registrationState(LinphoneCoreImpl.this
                        , proxy
                        , proxy.setState(RegistrationState.RegistrationCleared), "connection closed");
            } else {
                // just notify registration is gone
                mListener.displayStatus(LinphoneCoreImpl.this, "Registration to [" + mProxyCfg.getProxy() + "] lost");
                LinphoneProxyConfigImpl proxy = (LinphoneProxyConfigImpl) getDefaultProxyConfig();
                mListener.registrationState(LinphoneCoreImpl.this
                        , proxy
                        , proxy.setState(RegistrationState.RegistrationFailed), "connection lost");

            }
        }
        mSal.close();
        mSal = null;
    }

    public void enableEchoCancellation(final boolean enable) {
        mLog.error("ec not implemenetd");
    }

    public void enablePayloadType(final org.linphone.core.PayloadType pt,
            final boolean enable) throws LinphoneCoreException {
        mLog.error("enablePayloadType not implemenetd");

    }

    public org.linphone.core.PayloadType findPayloadType(final String mime,
            final int clockRate) {
        mLog.error("findPayloadType not implemenetd");
        return null;
    }

    public boolean isEchoCancellationEnabled() {

        return false;
    }

    public synchronized int getPlayLevel() {
        checkValidity();
        if (mCurrentCall != null && mCurrentCall.getAudioStream() != null) {
            return mCurrentCall.getAudioStream().getPlayLevel();
        } else {
            return -1;
        }
    }

    public synchronized void setPlayLevel(final int level) {
        checkValidity();
        if (mCurrentCall != null && mCurrentCall.getAudioStream() != null) {
            mCurrentCall.getAudioStream().setPlayLevel(level);
        } else {
            mLog.error("Cannot ajust playback level to [" + level + "]");
        }
    }

    public float getPlaybackGain() {
        return 0;
    }

    public void setPlaybackGain(final float gain) {
        // TODO Auto-generated method stub

    }

    private void createSal() throws SalException {
        // create Sal
        String localAdd = null;

        String dummyConnString = "socket://192.168.1.3:80;deviceside=true";
        if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {
            dummyConnString += ";interface=wifi";
        }

        try {
            mLog.info("Opening dummy socket connection to : " + dummyConnString);
            SocketConnection dummyCon = (SocketConnection) Connector.open(dummyConnString);
            localAdd = dummyCon.getLocalAddress();
            dummyCon.close();
        } catch (Throwable e) {
            String lErrorStatus = "Network unreachable, please enable wifi/or 3G";
            mLog.error(lErrorStatus, e);
            mListener.displayStatus(LinphoneCoreImpl.this, lErrorStatus);
            setNetworkReachable(false);
            return;
        }

        // check if local port is available
        try {
            String checkPortString = "datagram://:" + mSipPort + ";deviceside=true";
            if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {
                checkPortString += ";interface=wifi";
            }
            Connection test = Connector.open(checkPortString);
            test.close();
        } catch (IOException e) {
            String lErrorStatus = "Local port unavailable";
            mLog.error(lErrorStatus, e);
            mListener.displayStatus(LinphoneCoreImpl.this, lErrorStatus);
            setNetworkReachable(false);
            return;
        }

        SocketAddress addr = JOrtpFactory.instance().createSocketAddress(localAdd, mSipPort);
        mSal = SalFactory.instance().createSal();
        mSal.setUserAgent("jLinphone/0.0.1");
        try {
            mSal.listenPort(addr, mTransport, false);
        } catch (SalException e) {
            mLog.error("Cannot listen of on [" + addr + "]", e);
            throw e;
        }
        mSal.setListener(mSalListener);

        mListener.displayStatus(LinphoneCoreImpl.this, "Ready");
    }

    public synchronized void enableSpeaker(final boolean value) {
        checkValidity();
        if (mCurrentCall != null && mCurrentCall.getAudioStream() != null) {
            mCurrentCall.getAudioStream().enableSpeaker(value);
        }

        mSpeakerMode = value;
        if (mRingBackPlayer != null && mRingBackPlayer.getState() > Player.UNREALIZED) {
            AudioPathControl lPathCtr = (AudioPathControl) mRingBackPlayer.getControl("net.rim.device.api.media.control.AudioPathControl");
            try {
                if (value) {
                    lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HANDSFREE);
                } else {
                    try {
                        lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HEADSET);
                    } catch (MediaException e) {
                        mLog.info("no headset plugged, using earpiece instead");
                        lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HANDSET);
                    }
                }
                mLog.info("Speaker is " + (lPathCtr.getAudioPath() == AudioPathControl.AUDIO_PATH_HANDSFREE ? "enabled" : "disabled"));
            } catch (Throwable e) {
                mLog.error("Cannot " + (value ? "enable" : "disable") + " speaker", e);
            }
        }

    }

    public synchronized boolean isSpeakerEnabled() {
        checkValidity();
        if (mCurrentCall != null && mCurrentCall.getAudioStream() != null) {
            return mCurrentCall.getAudioStream().isSpeakerEnabled();
        } else {
            return false;
        }
    }

    public synchronized LinphoneCall getCurrentCall() {
        return mCurrentCall;
    }

    public void playDtmf(final char number, final int duration) {

    }

    public void stopDtmf() {
        // TODO Auto-generated method stub

    }

    private void deallocateRingBackPlayerIfNeeded() {
        deallocatePlayerIfNeeded(mRingBackPlayer);
        mRingBackPlayer = null;
    }

    private void deallocateRingPlayerIfNeeded() {
        deallocatePlayerIfNeeded(mRingPlayer);
        Alert.stopVibrate();
        mRingPlayer = null;

    }

    private void deallocatePlayerIfNeeded(final Player aPlayer) {
        if (aPlayer != null) {
            try {
                aPlayer.stop();
                aPlayer.close();
            } catch (MediaException e) {
                mLog.error("Cannot stop  player", e);
            }

        }

    }

    protected LinphoneCoreListener getListener() {
        return mListener;
    }

    public void addFriend(final LinphoneFriend lf) throws LinphoneCoreException {
        throw new RuntimeException("Not implemenetd yet");
    }

    public LinphoneChatRoom createChatRoom(final String to) {
        try {
            return new LinphoneChatRoomImpl(to);
        } catch (Exception e) {
            return null;
        }
    }

    public void enableVideo(final boolean vcapEnabled, final boolean displayEnabled) {
        throw new RuntimeException("Not implemenetd yet");

    }

    public FirewallPolicy getFirewallPolicy() {
        throw new RuntimeException("Not implemenetd yet");

    }

    public String getStunServer() {
        throw new RuntimeException("Not implemenetd yet");

    }

    public boolean isVideoEnabled() {
        throw new RuntimeException("Not implemenetd yet");

    }

    public void setFirewallPolicy(final FirewallPolicy pol) {
        throw new RuntimeException("Not implemenetd yet");

    }

    public void setPresenceInfo(final int minuteAway, final String alternativeContact,
            final OnlineStatus status) {
        throw new RuntimeException("Not implemenetd yet");

    }

    public void setPreviewWindow(final Object w) {
        throw new RuntimeException("Not implemenetd yet");

    }

    public void setStunServer(final String stunServer) {
        throw new RuntimeException("Not implemenetd yet");

    }

    public void setVideoWindow(final Object w) {
        throw new RuntimeException("Not implemenetd yet");

    }

    public synchronized boolean getNetworkStateReachable() {
        checkValidity();
        return mNetworkIsUp;
    }

    public LinphoneCallParams createDefaultCallParameters() {
        throw new RuntimeException("Not implemenetd yet");
    }

    public VideoSize getPreferredVideoSize() {
        throw new RuntimeException("Not implemenetd yet");
    }

    public String getRing() {
        throw new RuntimeException("Not implemenetd yet");
    }

    public LinphoneCall inviteAddressWithParams(final LinphoneAddress destination,
            final LinphoneCallParams params) throws LinphoneCoreException {
        // TODO Auto-generated method stub
        throw new RuntimeException("Not implemenetd yet");
    }

    public void setDownloadBandwidth(final int bw) {
        // TODO Auto-generated method stub
        throw new RuntimeException("Not implemenetd yet");
    }

    public void setPreferredVideoSize(final VideoSize vSize) {
        // TODO Auto-generated method stub
        throw new RuntimeException("Not implemenetd yet");
    }

    public void setRing(final String path) {
        // TODO Auto-generated method stub
        throw new RuntimeException("Not implemenetd yet");
    }

    public void setUploadBandwidth(final int bw) {
        // TODO Auto-generated method stub
        throw new RuntimeException("Not implemenetd yet");
    }

    public int updateCall(final LinphoneCall call, final LinphoneCallParams params) {
        // TODO Auto-generated method stub
        throw new RuntimeException("Not implemenetd yet");
    }

    public void setRtpTransport(final RtpTransport t) {
        mRtpTransport = t;
    }

    public synchronized Transports getSignalingTransportPorts() {
        checkValidity();
        Transports transport = new Transports();
        transport.udp = 0;
        transport.tcp = 0;
        transport.tls = 0;
        if (mTransport == Sal.Transport.Datagram) {
            transport.udp = 5060;
        } else if (mTransport == Sal.Transport.Stream) {
            transport.tcp = 5060;
        } else {
            throw new RuntimeException("Unexpected transport [" + mTransport + "]");
        }
        return transport;
    }

    public void enableKeepAlive(final boolean enable) {
        // TODO Auto-generated method stub

    }

    public boolean isKeepAliveEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isNetworkReachable() {
        return getNetworkStateReachable();
    }

    public PayloadType[] listVideoCodecs() {
        // TODO Auto-generated method stub
        return null;
    }

    public void startEchoCalibration(final Object data) throws LinphoneCoreException {
        throw new RuntimeException("Not implemented yet");

    }

    private boolean isPhoneNumber(final String value) {
        for (int i = 0; i < value.length(); i++) {
            char lCurrentChar = value.charAt(i);
            if (lCurrentChar != '+'
                    && lCurrentChar != '-'
                    && lCurrentChar != ' '
                    && (lCurrentChar < '0' || lCurrentChar > '9')) {
                return false;
            }
        }
        return true;
    }

    public void acceptCallWithParams(final LinphoneCall aCall,
            final LinphoneCallParams params) throws LinphoneCoreException {
        // TODO Auto-generated method stub

    }

    public void acceptCallUpdate(final LinphoneCall aCall, final LinphoneCallParams params)
            throws LinphoneCoreException {
        // TODO Auto-generated method stub

    }

    public void deferCallUpdate(final LinphoneCall aCall)
            throws LinphoneCoreException {
        // TODO Auto-generated method stub

    }

    public boolean isEchoLimiterEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    public void setDeviceRotation(final int rotation) {
        // TODO Auto-generated method stub

    }

    public void setVideoDevice(final int id) {
        // TODO Auto-generated method stub

    }

    public int getVideoDevice() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void setRootCA(final String path) {
        // TODO Auto-generated method stub

    }

    public void setDownloadPtime(final int ptime) {
        // TODO Auto-generated method stub

    }

    public void setUploadPtime(final int ptime) {
        checkValidity();
        mUploadPtime = ptime;
    }

    public int getUploadPtime() {
        checkValidity();
        return mUploadPtime;
    }

    public PayloadType[] getAudioCodecs() {
        // TODO Auto-generated method stub
        return null;
    }

    public PayloadType[] getVideoCodecs() {
        // TODO Auto-generated method stub
        return null;
    }

    public void enableIpv6(final boolean enable) {
        // TODO Auto-generated method stub

    }

    public void adjustSoftwareVolume(final int i) {
        // TODO Auto-generated method stub

    }

    public boolean pauseCall(final LinphoneCall call) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean resumeCall(final LinphoneCall call) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean pauseAllCalls() {
        // TODO Auto-generated method stub
        return false;
    }

    public void setZrtpSecretsCache(final String file) {
        // TODO Auto-generated method stub

    }

    public void enableEchoLimiter(final boolean val) {
        // TODO Auto-generated method stub

    }

    public boolean isInConference() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean enterConference() {
        // TODO Auto-generated method stub
        return false;
    }

    public void leaveConference() {
        // TODO Auto-generated method stub

    }

    public void addToConference(final LinphoneCall call) {
        // TODO Auto-generated method stub

    }

    public void addAllToConference() {
        // TODO Auto-generated method stub

    }

    public void removeFromConference(final LinphoneCall call) {
        // TODO Auto-generated method stub

    }

    public void terminateConference() {
        // TODO Auto-generated method stub

    }

    public int getConferenceSize() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void terminateAllCalls() {
        // TODO Auto-generated method stub

    }

    public LinphoneCall[] getCalls() {
        // TODO Auto-generated method stub
        return null;
    }

    public int getCallsNb() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void transferCall(final LinphoneCall call, final String referTo) {
        // TODO Auto-generated method stub

    }

    public void transferCallToAnother(final LinphoneCall callToTransfer,
            final LinphoneCall destination) {
        // TODO Auto-generated method stub

    }

    public LinphoneCall findCallFromUri(final String uri) {
        // TODO Auto-generated method stub
        return null;
    }

    public int getMaxCalls() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void setMaxCalls(final int max) {
        // TODO Auto-generated method stub

    }

    public boolean isMyself(final String uri) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean soundResourcesLocked() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean mediaEncryptionSupported(final MediaEncryption menc) {
        // TODO Auto-generated method stub
        return false;
    }

    public void setMediaEncryption(final MediaEncryption menc) {
        // TODO Auto-generated method stub

    }

    public MediaEncryption getMediaEncryption() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setMediaEncryptionMandatory(final boolean yesno) {
        // TODO Auto-generated method stub

    }

    public boolean isMediaEncryptionMandatory() {
        // TODO Auto-generated method stub
        return false;
    }

    public void setPlayFile(final String path) {
        // TODO Auto-generated method stub

    }

    public void tunnelEnable(final boolean enable) {
        // TODO Auto-generated method stub

    }

    public void tunnelAutoDetect() {
        // TODO Auto-generated method stub

    }

    public void tunnelEnableLogs(final boolean enable) {
        // TODO Auto-generated method stub

    }

    public void tunnelCleanServers() {
        // TODO Auto-generated method stub

    }

    public void tunnelAddServerAndMirror(final String host, final int port,
            final int udpMirrorPort, final int roundTripDelay) {
        // TODO Auto-generated method stub

    }

    public boolean isTunnelAvailable() {
        // TODO Auto-generated method stub
        return false;
    }

    public LinphoneProxyConfig[] getProxyConfigList() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setVideoPolicy(final boolean autoInitiate, final boolean autoAccept) {
        // TODO Auto-generated method stub

    }

    public void setUserAgent(final String name, final String version) {
        // TODO Auto-generated method stub

    }

    public void setCpuCount(final int count) {
        // TODO Auto-generated method stub

    }

    private void checkValidity() {
        if (!isValid) {
            throw new RuntimeException("Linphone core instance [" + this + "] has been destroyed");
        }
    }

    public String toString() {
        return " lc [" + hashCode() + "]";
    }

    public void tunnelSetHttpProxy(final String proxy_host, final int port,
            final String username, final String password) {
        // TODO Auto-generated method stub

    }

    public void removeCallLog(final LinphoneCallLog log) {
        // TODO Auto-generated method stub

    }

    public int getMissedCallsCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    public void resetMissedCallsCount() {
        // TODO Auto-generated method stub

    }

    public void refreshRegisters() {
        if (isNetworkReachable() && mProxyCfg != null) {
            mProxyCfg.edit();
            mProxyCfg.done();
        }
    }

    /**
     * Context for sending messages.
     * 
     * @author guillaume
     */
    class LinphoneChatRoomImpl implements LinphoneChatRoom {

        private final LinphoneAddress peer;
        private final String mIdentity;

        private SalOp createChatRoomSalop(final LinphoneAddress peer) {
            SalOp op = mSal.createSalOp();
            // is route header required ?
            if (getDefaultProxyConfig() != null
                    && getDefaultProxyConfig().getDomain().equalsIgnoreCase(peer.getDomain())) {
                op.setRoute(getDefaultProxyConfig().getProxy());
            }
            op.setFrom(mIdentity);
            op.setTo(peer.asString());
            return op;
        }

        public LinphoneChatRoomImpl(final String to) throws SalException, LinphoneCoreException {
            peer = interpretUrl(to);
            mIdentity = getIdentity();
        }

        public LinphoneAddress getPeerAddress() {
            return peer;
        }

        public synchronized void sendMessage(final Object opaque, final String message) {
            SalOp op;
            // if (mCurrentCall!=null) {
            // op=mCurrentCall.getOp();
            // } else {
            op = createChatRoomSalop(peer);
            op.setUserContext(opaque);
            // }

            try {
                op.sendTextMessage(message);
            } catch (SalException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    public PayloadType findPayloadType(final String mime, final int clockRate, final int channels) {
        throw new RuntimeException("not implemtented");
    }

    public String getVersion() {
        throw new RuntimeException("not implemtented");
    }

}
