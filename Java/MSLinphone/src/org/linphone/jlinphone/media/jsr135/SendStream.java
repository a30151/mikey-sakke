/*
SendStream.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package org.linphone.jlinphone.media.jsr135;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.RecordControl;

import net.rim.device.api.media.control.AudioPathControl;

import org.linphone.jortp.JOrtpFactory;
import org.linphone.jortp.Logger;
import org.linphone.jortp.RtpException;
import org.linphone.jortp.RtpPacket;
import org.linphone.jortp.RtpSession;

public class SendStream extends GenericStream {
	private static Logger sLogger=JOrtpFactory.instance().createLogger("SendStream");
	
	private RtpSession mSession;

	private int mTs=0;
	private static final int mFrameSize=32;
	private final int mPTime;
	private final Buffer[] mBuffers;
	private int mBufferOffset=0;
	private int mCurrentBuffer = -1;
    class Buffer {
    	private byte[] mContent = new byte[mFrameSize];
    	private int mSize = -1;
    	
    	public void setContent(byte content[], int offset, int bufferOffset, int size) {
    		System.arraycopy(content, offset, mContent,bufferOffset, size);
    		mSize = bufferOffset + size;
    	}

    	public byte[] content() {
    		return mContent;
    	}
    	
    	public int size() {
    		return mSize;
    	}
    }
	
	boolean mSkipHeader = true;
	private OutputStream mOutput= new OutputStream(){
		Vector msendQueue = new Vector();
		boolean mClosed =false;
		Thread mSendThread = new Thread(new Runnable() {
			public void run() {
				// TODO Auto-generated method stub
				while (!mClosed) {
					while (!msendQueue.isEmpty()) {
						try {
							RtpPacket lPrtacket = (RtpPacket) msendQueue.elementAt(0);
							msendQueue.removeElementAt(0);
							mSession.sendPacket(lPrtacket, mTs);
						} catch (RtpException e) {
							sLogger.error("Fail to send RTP packet",e);
						}
						mTs+=8*mPTime;
					}
					synchronized (msendQueue) {
						if (msendQueue.isEmpty()) {
							try {
								msendQueue.wait();
							} catch (InterruptedException e) {
								sLogger.error("Send queue interrupted",e);
							}
						}
					}
				}
				sLogger.info("Sender thread terminated");

			}

		});
		public void close() throws IOException {
			sLogger.info("close()");
			mClosed=true;
			synchronized (msendQueue) {
				msendQueue.notifyAll();
			}
		}

		public void flush() throws IOException {

		}

		public void write(byte[] buffer, int offset, int count) {
			try {
				if (sLogger.isLevelEnabled(Logger.Debug))sLogger.debug("Called in write date ["+System.currentTimeMillis()+"] offset ["+offset+"] length ["+count+"] ts ["+mTs+"]");
				int lOffset=offset;
				if (mSkipHeader) {
					sLogger.info("AMR header skipped");
					lOffset += "#!AMR\n".length();
					mSkipHeader=false;
					mSendThread.start();
				}
				while (lOffset < offset+count) {
					int writeSize=0;
					if (count % mFrameSize != 0) {
						if (sLogger.isLevelEnabled(Logger.Debug))sLogger.debug/*sLogger.error*/("Data to read ["+count+"] not multiple of ["+mFrameSize+"] current buffer ["+mCurrentBuffer+ "]");
					}
					if (offset+count-lOffset>=mFrameSize-mBufferOffset) {
						/*nominal case, we can get a full frame*/
						writeSize=mFrameSize-mBufferOffset;
					} else {
						writeSize=offset+count-lOffset;
					}
					mBuffers[mCurrentBuffer].setContent(buffer, lOffset, mBufferOffset,writeSize);
					lOffset+=writeSize;
					if (writeSize!=mFrameSize) 
						if (sLogger.isLevelEnabled(Logger.Debug))sLogger.debug/*sLogger.error*/("writing only ["+writeSize+"] on buff ["+mCurrentBuffer+"] at offset ["+mBufferOffset+"]");

					if (writeSize+mBufferOffset==mFrameSize) {
						/*frame is complete*/
						mBufferOffset=0;
						mCurrentBuffer++;
					} else {
						mBufferOffset+=writeSize;
						
					}
					
					if(mCurrentBuffer >= mBuffers.length) {
						mCurrentBuffer = 0;
						int size = 1;
						for(int i = 0; i < mBuffers.length; ++i) size += mBuffers[i].size();
						RtpPacket packet=JOrtpFactory.instance().createRtpPacket(size);
						
						//set cmr byte
						packet.getBytes()[packet.getDataOffset()]=(byte)0xf0;
						
						int pOffset = 1 + mBuffers.length;
						for(int i = 0; i < mBuffers.length; ++i) {
							packet.getBytes()[packet.getDataOffset() + 1 + i] = mBuffers[i].content()[0];
							if(i < mBuffers.length - 1)
								packet.getBytes()[packet.getDataOffset() + 1 + i] |= (byte)0x80; // Not last
							
							System.arraycopy(mBuffers[i].content(), 1, packet.getBytes(), packet.getDataOffset() + pOffset, mBuffers[i].size() - 1);
							pOffset += mBuffers[i].size() - 1;
						}
						
						synchronized(msendQueue) {
							msendQueue.addElement(packet);
							msendQueue.notify();
						}
//						try {
//						mSession.sendPacket(packet, mTs);
//					} catch (RtpException e) {
//						sLogger.error("Fail to send RTP packet",e);
//					}
//					mTs+=160;
					}
				}

			} catch (Throwable e) {
				sLogger.error("Cannot write data",e);
			}
		}

		public void write(byte[] buffer) throws IOException {
			write( buffer, 0, buffer.length);
			}

		public void write(int arg0) throws IOException {
			sLogger.error("write(int arg0) not implemented");
		}
		
	};

	public SendStream(RtpSession session, int upload_ptime) {
		mSession=session;
		mTs=0;
		mPTime=upload_ptime;
		mBuffers = new Buffer[mPTime/20];
		mCurrentBuffer = 0;
		for(int i= 0; i <mBuffers.length; ++i) {
			mBuffers[i] = new Buffer();
		}
	}

	public void stop() {
		try {
			RecordControl recordControl = (RecordControl) mPlayer.getControl("RecordControl");
			recordControl.commit();
			mOutput.close();
			super.stop();
		}  catch (Throwable e) {
			sLogger.error("InterruptedException in SendStream !",e);
		} 

	}
	
	public void start() {
		try {
			int updateThreshold=mPTime>100?mPTime:100;
			String lPlayerUrl="capture://audio?encoding=audio/amr&updateMethod=time&updateThreshold="+updateThreshold+"&rate=12200&voipMode=true";
			sLogger.info(lPlayerUrl);
			mPlayer = Manager.createPlayer(lPlayerUrl);
			mPlayer.addPlayerListener(this);
			mPlayer.realize();
			RecordControl recordControl = (RecordControl) mPlayer.getControl("RecordControl");
			recordControl.setRecordStream(mOutput);
			recordControl.startRecord();
			AudioPathControl  lPathCtr = (AudioPathControl) mPlayer.getControl("net.rim.device.api.media.control.AudioPathControl");
			//first try headset
			try {
				lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HEADSET);
				sLogger.info("Using headset for recv stream");
			} catch (MediaException e) {
				sLogger.info("no headset plugged, using earpiece");
				lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HANDSET);
			}
			mPlayer.prefetch();
			mPlayer.start();
		} catch (Throwable e) {
			sLogger.error("Cannot start  send stream !",e);
		} 

	}

	public boolean isMicMuted() {
		if (mPlayer!= null) 
			return mPlayer.getState()!=Player.STARTED;
		else
			return false;
	}	

	public void muteMic(boolean value) {
		try {
		if (mPlayer!= null) 
			if (value) {
				mPlayer.stop();
			} else {
				mPlayer.start();
			}
		sLogger.info("Mic "+(value?"muted":"unmuted"));
		} catch (Throwable e) {
			sLogger.error("Cannot "+(value?"mute":"unmute")+" mic", e);
		}
		
	}
	public String toString() {
		return "Sender Stream";
	}

	protected Logger getLogger() {
		return sLogger;
	}

	

}
