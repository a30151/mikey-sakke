/*
RecvStream.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package org.linphone.jlinphone.media.jsr135;

import java.io.IOException;

import javax.microedition.media.Control;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.VolumeControl;
import javax.microedition.media.protocol.ContentDescriptor;
import javax.microedition.media.protocol.DataSource;
import javax.microedition.media.protocol.SourceStream;

import org.linphone.bb.Version;
import org.linphone.jortp.JOrtpFactory;
import org.linphone.jortp.Logger;
import org.linphone.jortp.RtpPacket;
import org.linphone.jortp.RtpSession;
import org.linphone.jortp.TimestampClock;

public class RecvStream extends GenericStream {
	
	private SendStream mSendStream;
	
	private RtpSession mSession;
	private long mStartTime=0;
	private boolean mFirstRead=true;

	private boolean mBuffering=true;
	private static Logger sLogger=JOrtpFactory.instance().createLogger("RecvStream");
	private long mPlayerTs=-1;
	private final int mMaxPTime=1000;
	private final int mFrameSize=32;
	private byte[] mBuffer = new byte[mFrameSize * mMaxPTime / 20];
	private int mBufferPacket = 0;
	private int mBufferStart = 0;
	private int mBufferEnd = 0;
	static final int amrFrameSizes[] = {12, 13, 15, 17, 19, 20, 26, 31, 5, 0};
	
	private void reset() {
		mPlayer=null;
		mStartTime=0;
		mFirstRead=true;
		mBuffering=true;
		mPlayerTs=-1;
		mBufferStart = 0;
		mBufferEnd = 0;
		mBufferPacket = 0;
	}
	private SourceStream mInput= new SourceStream(){
		private boolean priority_set=false;
		
		ContentDescriptor mContentDescriptor=new ContentDescriptor("audio/amr");

		public int read(byte[] b, int offset, int length) throws IOException {
			if (sLogger.isLevelEnabled(Logger.Debug)) sLogger.debug("Called in write date ["+System.currentTimeMillis()+"] offset ["+offset+"] length ["+length+"] ts ["+mPlayerTs+"]");
			int bytesToReturn=mFrameSize;
			
			if (mBuffering){
				bytesToReturn=mFrameSize*8;
			}
			try {
				
				if (!priority_set && Thread.currentThread().getPriority() != Thread.MAX_PRIORITY) {
					Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
					priority_set=true;
				}
				//if (sLogger.isLevelEnabled(Logger.Info) && ((mPlayer.getMediaTime()<<3) % 1000 == 0))sLogger.info("Player media time ["+mPlayer.getMediaTime()+"]");
				int lWrittenLenth=0;
				
				while(lWrittenLenth<bytesToReturn && mRunning){
					long ts=getCurTs();
					if (mPlayerTs==-1) {
						mPlayerTs=ts;
						sLogger.info("Initializing timestamp to ["+mPlayerTs+"]");
					}
					RtpPacket packet=null;

					long diff;
					
					while((diff=(ts-mPlayerTs))>=0){
						if (diff> 800){
							mPlayerTs=ts-800;
							sLogger.warn("Too late, skipping "+ ((diff-800)/8) +" ms...");
						}
						
						// First header
						if (mFirstRead) {
							String lAmrHeader="#!AMR\n";
							lWrittenLenth=lAmrHeader.length();
							System.arraycopy(lAmrHeader.getBytes("US-ASCII"),0, b, offset, lWrittenLenth);
							length = length - lWrittenLenth;
							offset+=lWrittenLenth;
							mFirstRead=false;
						}
						
						// Empty buffer?
						if(mBufferStart == mBufferEnd) {
							// Reset buffer
							mBufferStart = 0;
							mBufferEnd = 0;
							mBufferPacket = 1;
							
							try {
								// Get a packet
								packet=mSession.recvPacket((int)mPlayerTs);
								if (packet!=null){	
									// Serialize
									while((packet.getBytes()[packet.getDataOffset()+mBufferPacket] & (byte)0x80) != 0) ++mBufferPacket; // Toc end?
									
									int packetOffset = 1 + mBufferPacket;
									for(int i = 0; i < mBufferPacket; ++i) {
										mBuffer[mBufferEnd] = packet.getBytes()[packet.getDataOffset()+1+i];
										int amrSize = amrFrameSizes[((mBuffer[mBufferEnd] & 0xff) /8) & 0xf];
										mBufferEnd++;
										System.arraycopy(packet.getBytes(), packet.getDataOffset() + packetOffset, mBuffer, mBufferEnd, amrSize);
										packetOffset += amrSize;
										mBufferEnd += amrSize;
									}
								}
							} catch (Exception e) {
								sLogger.error("Bad RTP packet", e);
								
								// Reset
								mBufferStart = 0;
								mBufferEnd = 0;
								mBufferPacket = 0;
							} 
						}
						if(mBufferEnd - mBufferStart > 0) {
							int readLength = Math.min(mBufferEnd - mBufferStart, length);
							System.arraycopy(mBuffer, mBufferStart, b, offset, readLength);
							offset+=readLength;
							lWrittenLenth+=readLength;
							mBufferStart+=readLength;
							length-=readLength;
						}
						if(length == 0) {
							if (sLogger.isLevelEnabled(Logger.Warn)) sLogger.warn("End of buffer, ["+lWrittenLenth+"] bytes returned");
							return lWrittenLenth;
						}
						mPlayerTs+=8*mBufferPacket*20;
					}
					if (packet==null){
						try {
							Thread.sleep(20);
						} catch (InterruptedException e) {
							//sleep can be interrupted leading media player to stop if not catched 
							if (sLogger.isLevelEnabled(Logger.Info)) sLogger.info("Interrupted exception");
						}
					}
				}
				if (!mRunning){
					//to notify end of stream.
					return -1;
				}
				if (sLogger.isLevelEnabled(Logger.Debug)) sLogger.debug("["+lWrittenLenth+"] bytes returned");
				return lWrittenLenth;
			} catch (Throwable e) {
				sLogger.error("Exiting player input stream",e);
				e.printStackTrace();
				return -1;
			}
			finally {
				if (bytesToReturn > mFrameSize) {
					// we were buffering, so now it's ok resetting value
					mBuffering=false;
				}
			}
		}



		public ContentDescriptor getContentDescriptor() {
			return mContentDescriptor;
		}

		public long getContentLength() {
			return -1;
		}

		public int getSeekType() {
			return SourceStream.SEEKABLE_TO_START; 
		}

		public int getTransferSize() {
			return mFrameSize;
		}

		public long seek(long where) throws IOException {
			sLogger.info("seeking to ["+where+"] just ignored");
			return where;
			//throw new IOException("not seekable");
		}

		public long tell() {
			if (mStartTime==0) return 0;
			return (System.currentTimeMillis() - mStartTime);
		}

		public Control getControl(String controlType) {
			return null;
		}

		public Control[] getControls() {
			return null;
		}
	};

	public RecvStream(RtpSession session,SendStream sendStream) {
		//mThread=new Thread(this,"RecvStream thread");
		mSession=session;
		mSendStream = sendStream;
		
	}




	public void start() {
		mRunning=true;
		mSession.setTimestampClock(new TimestampClock(){
			public int getCurrentTimestamp() {
				return getCurTs();
			}
		});
		try{
			mPlayer = Manager.createPlayer(new DataSource (null) {
				SourceStream[] mStream = {mInput};
				public void connect() throws IOException {
					sLogger.info("connect data source");
					
				}

				public void disconnect() {
					sLogger.info("disconnect data source");
					
				}

				public String getContentType() {
					return "audio/amr";
				}

				public SourceStream[] getStreams() {
					return mStream;
				}

				public void start() throws IOException {
					sLogger.info("start data source");
					
				}

				public void stop() throws IOException {
					sLogger.info("start data source");
					
				}

				public Control getControl(String controlType) {
					return null;
				}

				public Control[] getControls() {
					return null;
				}
				
			});
	
			mPlayer.addPlayerListener(this);
			mPlayer.realize();
			enableSpeaker(mSpeakerMode);

			if (Version.isAtLeast(7)) {
				sLogger.info("Setting receive buffer size=0");
				API7.dontBufferizeStream(mPlayer);
			}

			try {
				mPlayer.prefetch();
				mPlayer.start();
				if (sLogger.isLevelEnabled(Logger.Info)) sLogger.info("Player is started .");
			} catch (MediaException e) {
				sLogger.error("Couldn't start player : " +e.getMessage(), e);
			}
			//if ( DeviceInfo.isSimulator() == false) { //only start player on real device
			//}
	
		}catch (Throwable e){
			sLogger.error("player error:",e);
		}

	}







	public void playerUpdate(Player player, String event, Object eventData) {
		super.playerUpdate(player, event, eventData);
		if (event==PlayerListener.BUFFERING_STARTED){
			mBuffering=true;
		} else if (event == PlayerListener.DEVICE_UNAVAILABLE) {
			//pausing both player and recorder
			try {
				//already stooped mPlayer.stop(); 
				 mSendStream.getPlayer().stop();
			} catch (Throwable e) {
				sLogger.error("Enable to pause media players", e);
			}
		} else if (event == PlayerListener.DEVICE_AVAILABLE
					|| event == "com.rim.externalStop") { /*in case player is stooped due to a corrupted packet*/
			//starting both player and recorder
			try {
				if (mSendStream.getPlayer().getState()==Player.PREFETCHED)
					mSendStream.getPlayer().start();
				stop();
				reset();
				start();
			} catch (Throwable e) {
				sLogger.error("Enable to restart media players", e);
			}			
		}
	}
	
	public int getPlayLevel() {
		if (mPlayer !=null) {
			return ((VolumeControl)mPlayer.getControl("VolumeControl")).getLevel();
		} else {
			return 0;
		}
	}
	
	public void setPlayLevel(int level) {
		if (mPlayer !=null) {
			((VolumeControl)mPlayer.getControl("VolumeControl")).setLevel(level);
		}
	}
	
	private int getCurTs(){
		if (mStartTime==0) {
			mStartTime=System.currentTimeMillis();
		}
		return (int)((System.currentTimeMillis() - mStartTime)*8);
	}

	public long getPlayerTs() {
		return mPlayerTs;
	}
	public String toString() {
		return "Receiver Stream";
	}




	protected Logger getLogger() {
		return sLogger;
	}
}
