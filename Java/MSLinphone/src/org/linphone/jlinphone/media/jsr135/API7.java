package org.linphone.jlinphone.media.jsr135;

import javax.microedition.media.Player;

import net.rim.device.api.media.control.StreamingBufferControl;

public final class API7 {

	public static void dontBufferizeStream(Player mPlayer) {
		String cType="net.rim.device.api.media.control.StreamingBufferControl";
		StreamingBufferControl  bc = (StreamingBufferControl) mPlayer.getControl(cType);
		bc.setBufferTime(0);
	}
}
