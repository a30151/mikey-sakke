/*
GenericStream.java
Copyright (C) 2012  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package org.linphone.jlinphone.media.jsr135;

import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;

import net.rim.device.api.media.control.AudioPathControl;

import org.linphone.jortp.Logger;

public abstract class GenericStream implements PlayerListener {
	protected Player mPlayer;
	boolean mSpeakerMode=false;
	protected boolean mRunning;
	protected abstract Logger getLogger();
	
	public void playerUpdate(Player player, String event, Object eventData) {
		if (!getLogger().isLevelEnabled(Logger.Info)) return;
		String dTxt="";
		if (eventData != null) {
			dTxt = eventData.getClass().getName() +":";
			dTxt+= eventData.toString();
		}
		getLogger().info("Got event " + event + "[" + dTxt + "] on ["+this+"]");
	}
	public void stop() {
		if (mPlayer == null) return;//nothing to stop
		mRunning=false;
		try {
			if (mPlayer.getState() == Player.STARTED) {
				mPlayer.stop();
			} 
			if (mPlayer.getState() != Player.CLOSED) {
				mPlayer.close();
			}
		} catch (MediaException e) {
			getLogger().error("Error stopping stream ["+this+"]",e);
		}
	}	
	
	public void enableSpeaker(boolean value) {
		mSpeakerMode=value;
		if (mPlayer == null) return;//just ignore
		if (mPlayer.getState() < Player.REALIZED) return; //just ignore
		AudioPathControl  lPathCtr = (AudioPathControl) mPlayer.getControl("net.rim.device.api.media.control.AudioPathControl");
		try {
			if (value) {
				lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HANDSFREE);
			} else {
				try {
					lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HEADSET);
				} catch (MediaException e) {
					getLogger().info("no headset plugged, using earpiece instead for generic");
					lPathCtr.setAudioPath(AudioPathControl.AUDIO_PATH_HANDSET);
				}
			}
			getLogger().info("Speaker is "+(lPathCtr.getAudioPath()==AudioPathControl.AUDIO_PATH_HANDSFREE?"enabled":"disabled"));
		} catch (Throwable e) {
			getLogger().error("Cannot "+(value?"enable":"disable")+" speaker", e);
		}		
	}
	public boolean isSpeakerEnabled() {
		return mSpeakerMode;
	}
	
	protected Player getPlayer() {
		return mPlayer;
	}
}
