/*
SalImpl.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.linphone.jlinphone.sal.jsr180;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.Reader;

import org.linphone.jortp.JOrtpFactory;
import org.linphone.jortp.Logger;
import org.linphone.jortp.SocketAddress;
import org.linphone.jortp.SocketAddressImpl;
import org.linphone.sal.Sal;
import org.linphone.sal.SalAddress;
import org.linphone.sal.SalException;
import org.linphone.sal.SalFactory;
import org.linphone.sal.SalListener;
import org.linphone.sal.SalOp;

import sip4me.gov.nist.core.Debug;
import sip4me.gov.nist.core.LogWriter;
import sip4me.gov.nist.microedition.sip.SipConnector;
import sip4me.gov.nist.microedition.sip.StackConnector;
import sip4me.gov.nist.siplite.header.Header;
import sip4me.gov.nist.siplite.stack.ServerLog;
import sip4me.nist.javax.microedition.sip.SipClientConnection;
import sip4me.nist.javax.microedition.sip.SipConnectionNotifier;
import sip4me.nist.javax.microedition.sip.SipDialog;
import sip4me.nist.javax.microedition.sip.SipException;
import sip4me.nist.javax.microedition.sip.SipServerConnection;
import sip4me.nist.javax.microedition.sip.SipServerConnectionListener;

class SalImpl implements Sal, SipServerConnectionListener {
	private SipConnectionNotifier mConnectionNotifier;
	Logger mLog = JOrtpFactory.instance().createLogger("Sal");
	SipClientConnection mRegisterCnx;
	int mRegisterRefreshID;
	private SalListener mSalListener;
	private SalOp mIncallOp;
	private Transport mTransport;
	private String mPublicLocalAddress;
	private int mPublicLocalPort = -1;

	SalImpl() {

	}

	public void setIncallOp(SalOp anIncallOp) {
		mIncallOp = anIncallOp;
	}

	public void close() {
		if (mConnectionNotifier != null) {
			try {
				mConnectionNotifier.close();
			} catch (IOException e) {
				mLog.error("cannot close Sal connection", e);
			}
			mConnectionNotifier = null;
		}

	}

	public String getLocalAddr() throws SalException {
		try {
			if (mPublicLocalAddress != null) {
				return mPublicLocalAddress;
			} else if (mConnectionNotifier != null) {
				return mConnectionNotifier.getLocalAddress();
			} else {
				throw new Exception("no notification listener");
			}

		} catch (Throwable e) {
			throw new SalException(
					"Cannot get Local address from notification listener", e);
		}
	}

	public int getLocalPort() throws SalException {
		try {
			if (mPublicLocalPort != -1) {
				return mPublicLocalPort;
			} else if (mConnectionNotifier != null) {
				return mConnectionNotifier.getLocalPort();
			} else {
				throw new Exception("no notification listener");
			}

		} catch (Throwable e) {
			throw new SalException(
					"Cannot get Local port from notification listener", e);
		}
	}

	public void listenPort(SocketAddress addr, Transport t, boolean isSecure)
			throws SalException {
		// Configure logging of the stack
		try {

			Debug.enableDebug(false);
			LogWriter.needsLogging = true;

			ServerLog.setTraceLevel(ServerLog.TRACE_NONE);

			StackConnector.properties.setProperty(
					"javax.sip.RETRANSMISSION_FILTER", "on");
			if (!StackConnector.properties
					.containsKey("sip4me.gov.nist.javax.sip.NETWORK_LAYER")) {
				StackConnector.properties.setProperty(
						"sip4me.gov.nist.javax.sip.NETWORK_LAYER",
						"sip4me.gov.nist.core.net.BBNetworkLayer");
			}
			StackConnector.properties.setProperty("javax.sip.IP_ADDRESS",
					addr.getHost());
			mLog.info("Stack initialized with IP: " + addr.getHost());
			String SipConnectorUri = "sip:";
			// if (addr.getHost().equalsIgnoreCase("0.0.0.0")) {
			if (addr.getPort() != 0)
				SipConnectorUri += addr.getPort();
			// } else {
			// SipConnectorUri+="anonymous@"+addr.getHost()+":"+addr.getPort();
			// }
			mTransport = t;
			if (t == Transport.Stream) {
				SipConnectorUri += ";transport=tcp";
			}
			mConnectionNotifier = (SipConnectionNotifier) SipConnector
					.open(SipConnectorUri);
			if (addr.getPort() == 0) {
				int attributedPort = mConnectionNotifier.getLocalPort();
				((SocketAddressImpl) addr)
						.updatePortIKnowWhatIDo(attributedPort);
				mLog.info("using random port " + attributedPort);
			}

			mConnectionNotifier.setListener(this);
			System.out.println("SipConnectionNotifier opened at: "
					+ mConnectionNotifier.getLocalAddress() + ":"
					+ mConnectionNotifier.getLocalPort());

		} catch (Exception e) {
			throw new SalException("Cannot listen port for [" + addr
					+ "] reason [" + e.getMessage() + "]", e);
		}
	}

	public void setListener(SalListener listener) {
		mSalListener = listener;
	}

	public void setUserAgent(String ua) {
		// TODO Auto-generated method stub

	}

	public void setPublicLocalAddress(String aPublicLocalAddress) {
		mPublicLocalAddress = aPublicLocalAddress;
	}

	public void setPublicLocalPort(int aPublicLocalPort) {
		mPublicLocalPort = aPublicLocalPort;
	}

	private String readString(SipServerConnection ssc) {
		InputStream is = null;
		Reader in = null;
		final char[] buffer = new char[0x10000];
		StringBuffer out = new StringBuffer();
		try {
			int read;
			is = ssc.openContentInputStream();
			in = new InputStreamReader(is, "UTF-8");
			do {
				read = in.read(buffer, 0, buffer.length);
				if (read > 0) {
					out.append(buffer, 0, read);
				}
			} while (read >= 0);
		} catch (SipException e) {
			mLog.info("no body found , nothing to do");
		} catch (IOException e) {
			mLog.error("error retrieving body ", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}

		return out.toString();
	}

	public void notifyRequest(SipConnectionNotifier ssc) {
		SipServerConnection lCnx = null;
		try {
			SipDialog lDialog = mIncallOp != null ? ((SalOpImpl) mIncallOp)
					.getDialog() : null;
			lCnx = ssc.acceptAndOpen();
			mLog.info("receiving request: " + lCnx.getMethod() + " "
					+ lCnx.getRequestURI());
			if ("NOTIFY".equals(lCnx.getMethod())) {
				SalAddress lFrom = SalFactory.instance().createSalAddress(
						lCnx.getHeader(Header.FROM));
				InputStream lEventInputStream = null;
				byte[] lRawEvent = null;
				try {
					lEventInputStream = lCnx.openContentInputStream();
					lRawEvent = new byte[lEventInputStream.available()];
					lEventInputStream.read(lRawEvent);
				} catch (SipException e) {
					mLog.info("no body found , nothing to do");
				}
				mSalListener.onNotify(null, lFrom.asStringUriOnly(), lRawEvent);
				lCnx.initResponse(200);
				lCnx.send();
			} else if ("MESSAGE".equals(lCnx.getMethod())) {
				String fromAsText = lCnx.getHeader(Header.FROM);
				String content = readString(lCnx);
				mSalListener.onTextMessageReceived(mIncallOp, fromAsText,
						content);
				lCnx.initResponse(200);
				lCnx.send();
			} else if ("INVITE".equals(lCnx.getMethod()) && mIncallOp == null) {
				SalOp lOp = new SalOpImpl(this, mConnectionNotifier,
						mSalListener, lCnx);
				SalAddress lFrom = SalFactory.instance().createSalAddress(
						lCnx.getHeader(Header.FROM));
				SalAddress lTo = SalFactory.instance().createSalAddress(
						lCnx.getHeader(Header.TO));
				lOp.setFrom(lFrom.asStringUriOnly());
				lOp.setTo(lTo.asStringUriOnly());
				lCnx.initResponse(100);
				lCnx.send();
				mIncallOp = lOp;
				mSalListener.onCallReceived(lOp);

			} else if ("INVITE".equals(lCnx.getMethod())
					&& mIncallOp != null
					&& (lDialog == null || lDialog != null
							&& !lDialog.isSameDialog(lCnx))) {
				mLog.info("a call is already in progress, returning busy");
				lCnx.initResponse(486);
				lCnx.send();
			} else if (mIncallOp != null) {
				((SalOpImpl) mIncallOp).notifyRequestReceived(lCnx);
			} else {
				lCnx.initResponse(405);
				lCnx.send();
			}
		} catch (InterruptedIOException ex) {
			mLog.error("sip stack no longuer available", ex);
			mSalListener.onIoError(this);

		} catch (Throwable e) {
			if (lCnx != null) {
				mLog.error("Cannot answer to : " + lCnx.getMethod() + " "
						+ lCnx.getRequestURI(), e);
			} else {
				mLog.error("Unknown error while processing Request", e);
			}
		}

	}

	public SalOp createSalOp() {
		return new SalOpImpl(this, mConnectionNotifier, mSalListener);
	}

	public Transport getTransport() {
		return mTransport;
	}

}
