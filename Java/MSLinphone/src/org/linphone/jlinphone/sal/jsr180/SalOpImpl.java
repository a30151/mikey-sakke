/*
SalOpImpl.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.linphone.jlinphone.sal.jsr180;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;

import org.bouncycastle.math.ec.Arrays;
import org.json.me.JSONObject;
import org.linphone.jortp.JOrtpFactory;
import org.linphone.jortp.Logger;
import org.linphone.sal.OfferAnswerHelper;
import org.linphone.sal.OfferAnswerHelper.AnswerResult;
import org.linphone.sal.Sal;
import org.linphone.sal.Sal.Reason;
import org.linphone.sal.Sal.Transport;
import org.linphone.sal.SalAddress;
import org.linphone.sal.SalAuthInfo;
import org.linphone.sal.SalError;
import org.linphone.sal.SalException;
import org.linphone.sal.SalFactory;
import org.linphone.sal.SalListener;
import org.linphone.sal.SalListener.MessageEvent;
import org.linphone.sal.SalMediaDescription;
import org.linphone.sal.SalOpBase;
import org.linphone.sal.SalReason;

import mikeysakke.client.ClientData;
import mikeysakke.client.Connect;
import mikeysakke.client.KeyData;
import mikeysakke.client.MSUtils;
import mikeysakke.client.MikeySakkeSrtpParams;
import mikeysakke.mikey.CsIdMapInfo;
import mikeysakke.mikey.GenericId;
import mikeysakke.mikey.MikeySakkeIMessage;
import mikeysakke.mikey.PayloadHDR;
import mikeysakke.mikey.PayloadRAND;
import mikeysakke.mikey.PayloadSAKKE;
import mikeysakke.mikey.PayloadSIGN;
import mikeysakke.mikey.SessionData;
import mikeysakke.mikey.SrtpId;
import mikeysakke.mikey.tables.NextPayload;
import net.rim.device.api.io.Base64InputStream;
import sip4me.gov.nist.javax.sdp.SdpException;
import sip4me.gov.nist.javax.sdp.SdpFactory;
import sip4me.gov.nist.javax.sdp.SdpParseException;
import sip4me.gov.nist.javax.sdp.SessionDescription;
import sip4me.gov.nist.microedition.sip.SipConnector;
import sip4me.gov.nist.siplite.header.Header;
import sip4me.gov.nist.siplite.message.Request;
import sip4me.gov.nist.siplite.stack.Dialog;
import sip4me.nist.javax.microedition.sip.SipClientConnection;
import sip4me.nist.javax.microedition.sip.SipClientConnectionListener;
import sip4me.nist.javax.microedition.sip.SipConnectionNotifier;
import sip4me.nist.javax.microedition.sip.SipDialog;
import sip4me.nist.javax.microedition.sip.SipException;
import sip4me.nist.javax.microedition.sip.SipHeader;
import sip4me.nist.javax.microedition.sip.SipRefreshHelper;
import sip4me.nist.javax.microedition.sip.SipRefreshListener;
import sip4me.nist.javax.microedition.sip.SipServerConnection;

class SalOpImpl extends SalOpBase implements SipRefreshListener {
    static Logger mLog = JOrtpFactory.instance().createLogger("Sal");
    SalAuthInfo mAuthInfo;
    SipClientConnection mClientCnx;
    SipServerConnection mInviteServerTransaction;
    final SipConnectionNotifier mConnectionNotifier;
    SalMediaDescription mLocalSalMediaDescription;
    SalMediaDescription mFinalSalMediaDescription;
    final SalListener mSalListener;
    SipDialog mDialog;
    final static int REGISTER_REFRES_HID_NOT_SET = -1;
    int mRegisterRefreshId = REGISTER_REFRES_HID_NOT_SET;

    public SalOpImpl(final Sal sal, final SipConnectionNotifier aConnectionNotifier, final SalListener aSalListener) {
        super(sal);
        mConnectionNotifier = aConnectionNotifier;
        mSalListener = aSalListener;

    }

    public SalOpImpl(final Sal sal, final SipConnectionNotifier aConnectionNotifier, final SalListener aSalListener, final SipServerConnection aServerInviteTransaction) {
        this(sal, aConnectionNotifier, aSalListener);
        mInviteServerTransaction = aServerInviteTransaction;
    }

    public void authenticate(final SalAuthInfo info) throws SalException {
        mAuthInfo = info;
        try {
            if (mClientCnx != null) {
                if (info != null) {
                    mClientCnx.setHeader(Header.CONTACT, getContact()); // fix
                                                                        // contact
                    mClientCnx.setCredentials(info.getUserid(), info.getPassword(), info.getRealm());
                } else {
                    throw new Exception("Bad auth info [" + info + "]");
                }
            } else {
                mLog.warn("no registrar connection ready yet");
            }
        } catch (Exception e) {
            throw new SalException("Cannot authenticate", e);
        }
    }

    public void register(final String proxy, final String from, final int expires) throws SalException {
        // save from
        setFrom(from);
        setTo(from);
        setRoute(proxy);

        try {

            final SalAddress lAddress = SalFactory.instance().createSalAddress(from);
            mClientCnx = (SipClientConnection) SipConnector.open(lAddress.asStringUriOnly());

            mClientCnx.initRequest(Request.REGISTER, mConnectionNotifier);
            mClientCnx.setHeader(Header.FROM, from);
            mClientCnx.setHeader(Header.EXPIRES, String.valueOf(expires));
            if (proxy != null && proxy.length() > 0) {
                mClientCnx.setHeader(Header.ROUTE, getRouteHeaderValue(proxy));
            }

            String contactHdr = "sip:" + lAddress.getUserName() + "@"
                    + getSal().getLocalAddr() + ":"
                    + getSal().getLocalPort();
            mClientCnx.setHeader(Header.CONTACT, contactHdr);
            // mClientCnx.setHeader(Header.ALLOW,
            // "INVITE, ACK, CANCEL, BYE, NOTIFY");

            mClientCnx.setRequestURI("sip:" + lAddress.getDomain());
            mClientCnx.setListener(new SipClientConnectionListener() {
                boolean mAuthSucceded = false;

                public void notifyResponse(final SipClientConnection scc) {
                    // positione credential

                    try {
                        if (mAuthSucceded && scc.getStatusCode() == 401) {
                            mLog.info("Authentication in progress for [" + scc.getRequestURI() + "]");
                            scc.receive(0);
                            return;
                        } else if (scc.receive(0) == false) {
                            mSalListener.OnRegisterFailure(SalOpImpl.this, SalError.NoResponse, SalReason.Unknown, "Timout");
                            scc.close();
                            return;
                        } else {
                            mLog.info("Processing [" + scc.getStatusCode() + "] response for [" + scc.getRequestURI() + "]");
                        }
                        SipHeader lViaHeader = new SipHeader(Header.VIA, scc.getHeader(Header.VIA));
                        if (lViaHeader.getParameter("received") != null) {
                            ((SalImpl) getSal()).setPublicLocalAddress(lViaHeader.getParameter("received"));
                        }
                        if (lViaHeader.getParameter("rport") != null) {
                            ((SalImpl) getSal()).setPublicLocalPort(Integer.parseInt(lViaHeader.getParameter("rport")));
                        }
                        setDefaultContact(lAddress.getUserName());
                        switch (scc.getStatusCode()) {
                            case 401:
                            case 407:
                                if (mAuthSucceded == false) {
                                    SipHeader lAuthHeader = getAuthHeader(scc);
                                    if (lAuthHeader != null) {
                                        mSalListener.onAuthRequested(SalOpImpl.this, lAuthHeader.getParameter("realm"), lAddress.getUserName());
                                    } else {
                                        mLog.error("Cannot get authentication header");
                                    }
                                } // else already authenticated
                                break;

                            case 200:
                                if (mAuthSucceded == false && getAuthInfo() != null) {
                                    mSalListener.onAuthSuccess(SalOpImpl.this, getAuthInfo().getRealm(), getAuthInfo().getUsername());
                                    mAuthSucceded = true;
                                }
                                mSalListener.OnRegisterSuccess(SalOpImpl.this, true);
                                break;
                            default:
                                if (scc.getStatusCode() >= 500) {
                                    mSalListener.OnRegisterFailure(SalOpImpl.this, SalError.Failure, SalReason.Unknown, scc.getReasonPhrase());
                                    scc.close();
                                } else {
                                    mLog.error("Unexpected answer [" + scc + "]");

                                }
                        }
                    } catch (Throwable e) {
                        mLog.error("Cannot process REGISTER answer", e);
                        mSalListener.OnRegisterFailure(SalOpImpl.this, SalError.Failure, SalReason.Unknown, "Cannot process REGISTER answer [" + e.getMessage() + "");
                    }

                }

            });

            if (expires > 0) {
                mRegisterRefreshId = mClientCnx.enableRefresh(this);
            }

            // Finally, send register
            mClientCnx.send();
            mLog.info("REGISTER sent from [" + lAddress + "] to [" + proxy + "]");
        } catch (Throwable e) {
            throw new SalException("cannot send register  from [" + from + "] to [" + proxy + "]", e);
        }

    }

    public void call() throws SalException {

        try {
            SalAddress lToAddress = SalFactory.instance().createSalAddress(getTo());
            final SalAddress lFromAddress = SalFactory.instance().createSalAddress(getFrom());

            /*
             * if (lToAddress.getPortInt() < 0) { lToAddress.setPortInt(5060); }
             */
            mClientCnx = (SipClientConnection) SipConnector.open(lToAddress.asStringUriOnly());
            mClientCnx.initRequest(Request.INVITE, mConnectionNotifier);
            mClientCnx.setHeader(Header.FROM, getFrom());
            mClientCnx.setRequestURI(lToAddress.asStringUriOnly());
            mClientCnx.setHeader(Header.CONTENT_TYPE, "application/sdp");
            if (getRoute() != null && getRoute().length() > 0) {
                mClientCnx.setHeader(Header.ROUTE, getRouteHeaderValue(getRoute()));
            }

            setDefaultContact(lFromAddress.getUserName());
            mClientCnx.setHeader(Header.CONTACT, getContact());

            if (mAuthInfo != null) {
                mClientCnx.setCredentials(mAuthInfo.getUserid(), mAuthInfo.getPassword(), mAuthInfo.getRealm());
            }

            String lSdp = mLocalSalMediaDescription.toString();

            mClientCnx.setHeader(Header.CONTENT_LENGTH, String.valueOf(lSdp.length()));
            mClientCnx.openContentOutputStream().write(lSdp.getBytes("US-ASCII"));

            mClientCnx.setListener(new SipClientConnectionListener() {
                private void computeFinalSalMediaDesc() throws SipException, IOException, SdpParseException, SdpException, SalException {
                    InputStream lSdpInputStream = null;
                    try {
                        lSdpInputStream = mClientCnx.openContentInputStream();
                    } catch (SipException e) {
                        mLog.info("no sdp found , nothing to do");
                        return;
                    }
                    byte[] lRawSdp = new byte[lSdpInputStream.available()];
                    lSdpInputStream.read(lRawSdp);

                    SessionDescription lSessionDescription = SdpFactory.getInstance().createSessionDescription(new String(lRawSdp));

                    SalMediaDescription lRemote = SdpUtils.toSalMediaDescription(lSessionDescription);
                    mFinalSalMediaDescription = OfferAnswerHelper.computeOutgoing(mLocalSalMediaDescription, lRemote);
                }

                public void notifyResponse(final SipClientConnection scc) {
                    try {
                        if (scc.receive(0) == false) {
                            mSalListener.onCallFailure(SalOpImpl.this, "Timout");
                            return;
                        }
                        switch (scc.getStatusCode()) {

                            case 200:
                                computeFinalSalMediaDesc();
                                mDialog = mClientCnx.getDialog();
                                mClientCnx.initAck();
                                mClientCnx.send();

                                mSalListener.onCallAccepted(SalOpImpl.this);
                                break;
                            case 183:
                            case 180:
                                if (mFinalSalMediaDescription != null) {
                                    // already in early media
                                    break;
                                }
                                computeFinalSalMediaDesc();
                                mDialog = mClientCnx.getDialog();
                                mSalListener.onCallRinging(SalOpImpl.this);
                                break;
                            case 401:
                            case 407:
                                if (mAuthInfo == null) {
                                    SipHeader lAuthHeader = getAuthHeader(scc);
                                    if (lAuthHeader != null) {
                                        mSalListener.onAuthRequested(SalOpImpl.this, lAuthHeader.getParameter("realm"), lFromAddress.getUserName());
                                    } else {
                                        mSalListener.onCallFailure(SalOpImpl.this, "Cannot find Auth info from sip message");
                                    }
                                }
                                break;
                            case 487:
                                // nop request terminated
                                break;
                            default:
                                if (scc.getStatusCode() > 300) {
                                    mSalListener.onCallFailure(SalOpImpl.this, scc.getReasonPhrase());
                                } else {
                                    mLog.warn("Unexpected answer [" + scc.getStatusCode() + " " + scc.getRequestURI() + "]");
                                }
                        }
                    } catch (Throwable e) {
                        mLog.error("cannot handle invite answer", e);
                        mSalListener.onCallFailure(SalOpImpl.this, e.getMessage());
                    }

                }
            });
            mClientCnx.send();
            ((SalImpl) getSal()).setIncallOp(this);
        } catch (Throwable e) {
            throw new SalException(e);
        }

    }

    public void callAccept() throws SalException {
        callAccept(mInviteServerTransaction);
    }

    public void callAccept(final SipServerConnection cnx) throws SalException {
        try {
            InputStream lSdpInputStream = cnx.openContentInputStream();
            byte[] lRawSdp = new byte[lSdpInputStream.available()];
            lSdpInputStream.read(lRawSdp);
            SessionDescription lSessionDescription;
            try {
                lSessionDescription = SdpFactory.getInstance().createSessionDescription(new String(lRawSdp));

                // retrieving MIKEY I message
                String iMessageAttribute = lSessionDescription.getAttribute("key-mgmt");

                if (iMessageAttribute != null) {
                    ClientData clientData = ClientData.instance();
                    MikeySakkeSrtpParams params = MikeySakkeSrtpParams.instance();

                    // Decode Mikey Message
                    String base64iMessage = iMessageAttribute.substring(6); // "mikey "
                    
                    byte[] iMessageBytes = Base64InputStream.decode(base64iMessage); 
                    MikeySakkeIMessage iMessage = MikeySakkeIMessage.decode(iMessageBytes);
                   
                    PayloadSIGN signPayload = (PayloadSIGN) iMessage.getPayload(NextPayload.SIGN);
                    if (signPayload == null){
                    	mLog.fatal("PayloadSIGN is null, could not get from iMessage", new Throwable(MSUtils.toHex(iMessageBytes)));
                    }
                    String signature = MSUtils.toHex(signPayload.getSignature());

                    // get the message without the signature to verify
                    byte[] unsignedIMessage = Arrays.copyOfRange(iMessageBytes, 0, (iMessageBytes.length - signPayload.getSignatureLen()));
                    String messageToVerify = MSUtils.toHex(unsignedIMessage);

                   // get ID of sender
                    String fromId = params.getOtherDeviceId();
                   

                    // Send request to Math server
                    KeyData keys = clientData.getKeys();
                    Hashtable parameters = new Hashtable();

                    parameters.put("signerId", fromId);
                    parameters.put("kpak", keys.getPublicAuthenticationKey().toString());
                    parameters.put("message", messageToVerify);
                    parameters.put("signature", signature);

                    boolean verified = false;
                    JSONObject jsonObj =
                            Connect.mathServerRequest("VERIFY", parameters);
                    if (jsonObj.has("error")) {
                        net.rim.device.api.ui.component.Dialog.inform("Error while verifying signature.");
                    }
                    if (jsonObj.has("verified")) {
                        verified = ((Boolean) jsonObj.get("verified")).booleanValue();
                    }
                    if (!verified) {
                        net.rim.device.api.ui.component.Dialog.inform("Signature not verified");
                    }
                    else {
                        // Decode SED and extract SSV if signature is verified
                        PayloadSAKKE sakke = (PayloadSAKKE) iMessage.getPayload(NextPayload.SAKKE);
                        String sed = MSUtils.toHex(sakke.getSakkeData());
                        int paramSet = sakke.getSakkeParams();

                        String SSV = "";
                        parameters = new Hashtable();

                        parameters.put("sed", sed);
                        parameters.put("id", keys.getIdentifier());
                        parameters.put("parameterSet", String.valueOf(paramSet));
                        parameters.put("rsk", keys.getReceiverSecretKey());
                        parameters.put("z", keys.getKmsPublicKey());

                        JSONObject jsonObj2 = Connect.mathServerRequest("EXTRACT", parameters);

                        if (jsonObj2.has("error")) {
                            net.rim.device.api.ui.component.Dialog.inform("Error while extracting secret");
                        }

                        if (jsonObj2.has("ssv")) {
                            SSV = (String) jsonObj2.get("ssv");
                        }

                        if (SSV == null || SSV.equals("null")) {
                            net.rim.device.api.ui.component.Dialog.inform("SSV null");

                        } else {
                            // Store the SSV for use when enabling SRTP
                            params.setCurrentSessionSSV(MSUtils.hexStringToByteArray(SSV));
                            
                            PayloadHDR hdr = iMessage.getPayloadHDR();
                            params.setCsbId(hdr.getCsbId());
                            CsIdMapInfo map = hdr.getCsIdMapInfo();
                            if (map instanceof SrtpId){
                            	params.setRecCsId((byte)1);
                            	params.setSendCsId((byte)2);
                            	SrtpId info = (SrtpId) map;
                            	if (hdr.getCsNumber() == 2){ 
                            		mLog.debug("2 crypto sessions recognized in iMikey");
                            		params.setRecSSRC(info.getSSRCs()[0]);
                            		params.setSendSSRC(info.getSSRCs()[1]);
                            	}
                            	else if (hdr.getCsNumber() == 1){
                            		mLog.debug("1 crypto session recognized in iMikey");
                            		params.setRecSSRC(info.getSSRCs()[0]);
                            	}
                            }
                            else if (map instanceof GenericId){
                            	GenericId genericMap = (GenericId)map;
                            	SessionData[] data = genericMap.getSessionData();
               
                        		if (data.length == 1){
                        			params.setRecSSRC(data[0].getSsrc());
                        		}
                        		else if (data.length > 1){
                        			params.setRecSSRC(data[0].getSsrc());
                        			params.setSendSSRC(data[1].getSsrc());
                        			// nothing to do with more SSRCs, only two streams
                        		}
                            	
                            	params.setRecCsId(((GenericId) map).getCsId());
                            	params.setSendCsId((byte)(((GenericId) map).getCsId() + 1)); 
                            }
                            else throw new Exception("Unexpected CS ID Map info");
                            PayloadRAND rand = (PayloadRAND) iMessage.getPayload(NextPayload.RAND);
                            if (rand == null){
                            	throw new Exception("Rand payload not found.");
                            }
                            
                            else{
                            	params.setRand(rand.getRand());
                            }                            
                        }
                    }
                }
                else {
                    net.rim.device.api.ui.component.Dialog.inform("Did not receive MIKEY I message in SDP");
                }
            } catch (SdpParseException e) {
                throw new SalException("Parser error, cannot parse incoming sdp", e);
            } 

            SalMediaDescription lRemote = SdpUtils.toSalMediaDescription(lSessionDescription);

            AnswerResult lAnswerResult = OfferAnswerHelper.computeIncoming(mLocalSalMediaDescription, lRemote);
            SalAddress lmyAddress = SalFactory.instance().createSalAddress(getTo());
            setDefaultContact(lmyAddress.getUserName());

            if (lAnswerResult.getResult().getNumStreams() == 0) {
                mLog.warn("No codec matching");
                cnx.initResponse(404);
                cnx.send();
                mSalListener.onCallFailure(this, "no matching codecs");
            } else {
                mFinalSalMediaDescription = lAnswerResult.getResult();
                cnx.initResponse(200);
                cnx.setHeader(Header.CONTACT, getContact());
                cnx.setHeader(Header.CONTENT_TYPE, "application/sdp");

                String lSdp = lAnswerResult.getAnswer().toString();
                cnx.setHeader(Header.CONTENT_LENGTH, String.valueOf(lSdp.length()));
                cnx.openContentOutputStream().write(lSdp.getBytes("US-ASCII"));
                cnx.send();
                mDialog = cnx.getDialog();

            }

        } catch (Throwable e) {
            throw new SalException(e);
        }

    }

    public void callDecline(final Reason r, final String redirectUri) {
        try {
            if (mInviteServerTransaction != null) {
                try {
                    int lReason;
                    if (r == Reason.Busy) {
                        lReason = 486;
                    } else if (r == Reason.Declined) {
                        lReason = 603;
                    } else {
                        lReason = 603;
                    }
                    mInviteServerTransaction.initResponse(lReason);
                    mInviteServerTransaction.send();
                    mSalListener.onCallTerminated(this);
                    mInviteServerTransaction = null;

                } catch (Throwable e) {
                    mLog.error("cannot cancel call", e);
                }
            }
        } finally {
            ((SalImpl) getSal()).setIncallOp(null);
        }

    }

    public void callSetLocalMediaDescription(final SalMediaDescription md) {
        mLocalSalMediaDescription = md;

    }

    public void callTerminate() {
        try {
            if (mDialog != null && mDialog.getState() == Dialog.CONFIRMED_STATE) {
                SipClientConnection lByeConnection = mDialog.getNewClientConnection(Request.BYE);
                lByeConnection.send();
            } else if (mClientCnx != null) {
                SipClientConnection lSipCancelCnx = mClientCnx.initCancel();
                lSipCancelCnx.send();
            }

        } catch (Throwable e) {
            mLog.error("cannot terminate call", e);
        } finally {
            mSalListener.onCallTerminated(SalOpImpl.this);
            ((SalImpl) getSal()).setIncallOp(null);
        }
    }

    public SalAuthInfo getAuthInfo() {
        return mAuthInfo;
    }

    public SalMediaDescription getFinalMediaDescription() {
        return mFinalSalMediaDescription;
    }

    public void callRinging() throws SalException {
        try {
            if (mInviteServerTransaction != null) {
                mInviteServerTransaction.initResponse(180);
                mInviteServerTransaction.send();
            } else {
                throw new SalException("no in a proper state");
            }
        } catch (Throwable e) {
            throw new SalException(e);
        }

    }

    public void refreshEvent(final int refreshID, final int statusCode, final String reasonPhrase) {
        mLog.info("refresh event code [" + statusCode + "], reason [" + reasonPhrase + "]");
        if (statusCode == 0) {
            try {
                mClientCnx.close();
            } catch (Throwable e) {
                mLog.error("Cannot close connection", e);
            }
        }
    }

    private SipHeader getAuthHeader(final SipClientConnection scc) {
        SipHeader lAuthHeader = null;
        if (scc.getHeader(Header.PROXY_AUTHENTICATE) != null) {
            lAuthHeader = new SipHeader(Header.AUTHORIZATION, scc.getHeader(Header.PROXY_AUTHENTICATE));
        } else if (scc.getHeader(Header.WWW_AUTHENTICATE) != null) {
            lAuthHeader = new SipHeader(Header.WWW_AUTHENTICATE, scc.getHeader(Header.WWW_AUTHENTICATE));
        }
        return lAuthHeader;
    }

    public void unregister() {
        if (mClientCnx != null) {
            try {
                SipRefreshHelper.getInstance().stop(mRegisterRefreshId);
                // register(getRoute(),getFrom(),0);
            } catch (Throwable e) {
                mLog.error("Cannot unregister", e);
            }
        } else {
            mLog.error("not register");
        }

    }

    void notifyRequestReceived(final SipServerConnection cnx) {
        try {
            if ("BYE".equals(cnx.getMethod()) || "CANCEL".equals(cnx.getMethod())) {
                mSalListener.onCallTerminated(this);
                cnx.initResponse(200);
                cnx.send();
                if ("CANCEL".equals(cnx.getMethod()) && mInviteServerTransaction != null) {
                    mInviteServerTransaction.initResponse(487);
                    mInviteServerTransaction.send();
                }
                ((SalImpl) getSal()).setIncallOp(null);
            } else if ("ACK".equals(cnx.getMethod())) {
                mLog.info("Call to [" + getTo() + "] confirmed");
            } else if ("INVITE".equals(cnx.getMethod())) {
                // reinvite
                mInviteServerTransaction = cnx; // update invite server
                                                // transaction
                mSalListener.onCallUpdating(this);
            } else {
                cnx.initResponse(405);
                cnx.send();
            }
        } catch (Throwable e) {
            mLog.error("Cannot answer to : " + cnx.getMethod() + " " + cnx.getRequestURI(), e);
        }
    }

    private String getRouteHeaderValue(final String proxy) {
        String lRouteHeaderValue = proxy;

        if (getSal().getTransport() != null && getSal().getTransport() == Sal.Transport.Stream) {
            final SalAddress lProxy = SalFactory.instance().createSalAddress(proxy);
            if (lProxy.getTransport() == null) {
                lProxy.setTransport(Sal.Transport.Stream);
            }
            lRouteHeaderValue = lProxy.asStringUriOnly();
        }

        return lRouteHeaderValue + ";lr";
    }

    public void sendDtmf(final char number) {
        mLog.info("sending dtmf [" + number + "]");
        try {
            if (mDialog != null) {
                SipClientConnection lInfoConnection = mDialog.getNewClientConnection(Request.INFO);
                lInfoConnection.setHeader(Header.CONTENT_TYPE, "application/dtmf-relay");
                String ldtmf = new String("Signal=" + number + "\r\nDuration=250\r\n");
                lInfoConnection.setHeader(Header.CONTENT_LENGTH, String.valueOf(ldtmf.length()));
                OutputStream lDtmfOutputStream = lInfoConnection.openContentOutputStream();
                lDtmfOutputStream.write(ldtmf.getBytes("US-ASCII"));

                lInfoConnection.send();
            }
        } catch (Throwable e) {
            mLog.error("cannot send dtmf [" + number + "]", e);
        }

    }

    SipDialog getDialog() {
        return mDialog;
    }

    void setDefaultContact(final String userInfo) throws SalException {
        StringBuffer lcontact = new StringBuffer("sip:");
        if (userInfo != null)
            lcontact.append(userInfo + "@");
        lcontact.append(getSal().getLocalAddr());
        lcontact.append(":" + getSal().getLocalPort());
        if (getSal().getTransport() == Transport.Stream) {
            lcontact.append(";transport=tcp");
        }
        setContact(lcontact.toString());

    }

    /**
     * Sip client connection listener reacting to events of a sent message.
     * 
     * @author guillaume
     */
    private class SCCSentMessageListener implements SipClientConnectionListener {
        private final SalAddress from;

        public SCCSentMessageListener(final SalAddress from) {
            this.from = from;
        }

        private void notifySal(final int event, final String phrase) {
            mSalListener.onTextMessageSentEvent(SalOpImpl.this, event, phrase);
        }

        public void notifyResponse(final SipClientConnection scc) {
            try {
                if (!scc.receive(0)) {
                    notifySal(MessageEvent.FAILURE, "Timout");
                    return;
                }
                switch (scc.getStatusCode()) {
                    case 200:
                        // mDialog=mClientCnx.getDialog();
                        notifySal(MessageEvent.SUCCESS, null);
                        break;
                    case 183:
                    case 180:
                        notifySal(MessageEvent.PROGRESS, null);
                        break;
                    case 401:
                    case 407:
                        if (mAuthInfo == null) {
                            SipHeader lAuthHeader = getAuthHeader(scc);
                            if (lAuthHeader != null) {
                                mSalListener.onAuthRequested(SalOpImpl.this, lAuthHeader.getParameter("realm"), from.getUserName());
                            } else {
                                notifySal(MessageEvent.FAILURE, "Cannot find Auth info from sip message");
                            }
                        }
                        break;
                    case 487:
                        // nop request terminated
                        break;
                    default:
                        if (scc.getStatusCode() > 300) {
                            notifySal(MessageEvent.FAILURE, scc.getReasonPhrase());
                        } else {
                            mLog.warn("Unexpected MESSAGE answer [" + scc.getStatusCode() + " " + scc.getRequestURI() + "]");
                        }
                }
            } catch (Throwable e) {
                mLog.error("cannot handle MESSAGE answer", e);
                notifySal(MessageEvent.FAILURE, e.getMessage());
            }
        }
    }

    public void sendTextMessage(final String msg) throws SalException {
        try {
            SalAddress lToAddress = SalFactory.instance().createSalAddress(getTo());
            final SalAddress lFromAddress = SalFactory.instance().createSalAddress(getFrom());

            mClientCnx = (SipClientConnection) SipConnector.open(lToAddress.asStringUriOnly());
            mClientCnx.initRequest(Request.MESSAGE, mConnectionNotifier);
            mClientCnx.setHeader(Header.FROM, getFrom());
            mClientCnx.setRequestURI(lToAddress.asStringUriOnly());
            mClientCnx.setHeader(Header.CONTENT_TYPE, "text/plain");
            if (getRoute() != null && getRoute().length() > 0) {
                mClientCnx.setHeader(Header.ROUTE, getRouteHeaderValue(getRoute()));
            }

            setDefaultContact(lFromAddress.getUserName());
            mClientCnx.setHeader(Header.CONTACT, getContact());

            if (mAuthInfo != null) {
                mClientCnx.setCredentials(mAuthInfo.getUserid(), mAuthInfo.getPassword(), mAuthInfo.getRealm());
            }

            mClientCnx.setHeader(Header.CONTENT_LENGTH, String.valueOf(msg.length()));
            mClientCnx.openContentOutputStream().write(msg.getBytes("US-ASCII"));
            mClientCnx.setListener(new SCCSentMessageListener(lFromAddress));
            mClientCnx.send();
        } catch (Throwable e) {
            throw new SalException(e);
        }
    }
}
