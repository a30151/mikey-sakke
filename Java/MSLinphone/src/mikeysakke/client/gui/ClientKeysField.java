package mikeysakke.client.gui;

import org.linphone.jlinphone.gui.TabFieldItem;

import mikeysakke.client.ClientData;
import mikeysakke.client.KeyData;
import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.io.MalformedURIException;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ClientKeysField extends VerticalFieldManager implements TabFieldItem {

    private final LabelField idLabel = new LabelField("ID: ");
    private final LabelField kmsIdLabel = new LabelField("KMS ID:");
    private final LabelField kpakLabel = new LabelField("KPAK:");
    private final LabelField zLabel = new LabelField("Z:");
    private final LabelField sskLabel = new LabelField("SSK:");
    private final LabelField rskLabel = new LabelField("RSK:");
    private final LabelField pvtLabel = new LabelField("PVT:");

    private final LabelField idText = new LabelField();
    private final LabelField kmsIdText = new LabelField();
    private final LabelField kpakText = new LabelField();
    private final LabelField zText = new LabelField();
    private final LabelField sskText = new LabelField();
    private final LabelField rskText = new LabelField();
    private final LabelField pvtText = new LabelField();
    private final LabelField statusLabel = new LabelField();

    public ClientKeysField() {
        super(USE_ALL_WIDTH);

        add(statusLabel);
        statusLabel.setFont(Font.getDefault().derive(Font.ITALIC));

        Font bold = Font.getDefault().derive(Font.BOLD);

        add(idLabel);
        idLabel.setFont(bold);
        add(idText);

        add(kmsIdLabel);
        kmsIdLabel.setFont(bold);
        add(kmsIdText);

        add(kpakLabel);
        kpakLabel.setFont(bold);
        add(kpakText);

        add(zLabel);
        zLabel.setFont(bold);
        add(zText);

        add(sskLabel);
        sskLabel.setFont(bold);
        add(sskText);

        add(rskLabel);
        rskLabel.setFont(bold);
        add(rskText);

        add(pvtLabel);
        pvtLabel.setFont(bold);
        add(pvtText);
        // refresh();

    }

    public void refresh() {
        try {
            ClientData client = ClientData.instance();
	        KeyData keys = client.getKeys();
	        idText.setText(keys.getIdentifier());
	        rskText.setText(keys.getReceiverSecretKey());
	        sskText.setText(keys.getSecretSigningKey());
	        kpakText.setText(keys.getPublicAuthenticationKey());
	        pvtText.setText(keys.getPublicValidationToken());
	        zText.setText(keys.getKmsPublicKey());
	        kmsIdText.setText(keys.getKmsIdentifier());
        } catch (IllegalArgumentException e) {
            KmsConnectField.updateLabel(statusLabel, e.toString());
        } catch (MalformedURIException e) {
            KmsConnectField.updateLabel(statusLabel, e.toString());
        } catch (DatabaseException e) {
            KmsConnectField.updateLabel(statusLabel, e.toString());
        } catch (DataTypeException e) {
            KmsConnectField.updateLabel(statusLabel, e.toString());
        } catch (Exception e) {
            KmsConnectField.updateLabel(statusLabel, e.toString());
        }
        
    }

    public void onSelected() {
        refresh();

        // TODO Auto-generated method stub

    }

    public void onUnSelected() {
        // TODO Auto-generated method stub

    }

    public boolean navigateBack() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean keyChar(final char ch, final int status, final int time) {
        return super.keyChar(ch, status, time);
    }
}
