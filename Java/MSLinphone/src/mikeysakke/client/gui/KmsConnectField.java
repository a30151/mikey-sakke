package mikeysakke.client.gui;

import java.io.IOException;

import org.json.me.JSONException;
import org.linphone.jlinphone.gui.LinphonePersistance;
import org.linphone.jlinphone.gui.TabFieldItem;

import mikeysakke.client.Client;
import mikeysakke.client.ClientData;
import mikeysakke.client.Connect;
import mikeysakke.client.KeyData;
import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.io.MalformedURIException;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class KmsConnectField extends VerticalFieldManager implements TabFieldItem {
    private final LinphonePersistance lp = LinphonePersistance.instance();

    private final static String KMS_USERNAME = "mikeysakke.client.username";
    private final static String KMS_PASSWORD = "mikeysakke.client.password";
    private final static String KMS_ID = "mikeysakke.client.id";
    private final static String KMS_SERVER = "mikeysakke.client.server";
    private final static String KMS_PORT = "mikeysakke.client.p";
    public final static String MATH_SERVER = "mikeysakke.client.math.server";
    public final static String MATH_PORT = "mikeysakke.client.math.p";

    private final BasicEditField userNameField;
    private final PasswordEditField userPasswdField;
    private final BasicEditField userIdField;
    private final BasicEditField kmsServerField;
    private final BasicEditField mathServerField;
    private final BasicEditField kmsPortField;
    private final BasicEditField mathServerPortField;

    private final LabelField fetchKeysStatusLabel = new LabelField();
    private final ButtonField fetchKeysButton = new ButtonField("Fetch new keys from KMS", Field.FOCUSABLE | ButtonField.CONSUME_CLICK);

    private ClientData clientData;

    public KmsConnectField() {
        add(fetchKeysButton);
        add(fetchKeysStatusLabel);
        fetchKeysStatusLabel.setFont(Font.getDefault().derive(Font.ITALIC));
        try {
            clientData = ClientData.instance();

        } catch (IllegalArgumentException e) {
            updateLabel(fetchKeysStatusLabel, e.toString());
        } catch (MalformedURIException e) {
            updateLabel(fetchKeysStatusLabel, e.toString());
        } catch (DatabaseException e) {
            updateLabel(fetchKeysStatusLabel, e.toString());
        } catch (DataTypeException e) {
            updateLabel(fetchKeysStatusLabel, e.toString());
        }

        userNameField = new BasicEditField("Username: ", "", 128, 0);
        userNameField.setText(lp.getString(KMS_USERNAME, ""));
        add(userNameField);

        userPasswdField = new PasswordEditField("Password: ", "", 128, 0);
        userPasswdField.setText(lp.getString(KMS_PASSWORD, ""));
        add(userPasswdField);

        userIdField = new BasicEditField("ID: ", "", 128, 0);
        userIdField.setText(lp.getString(KMS_ID, ""));
        add(userIdField);

        kmsServerField = new BasicEditField("KMS: ", "", 128, 0);
        kmsServerField.setText(lp.getString(KMS_SERVER, ""));
        add(kmsServerField);

        kmsPortField = new BasicEditField("KMS Port: ", "", 128, 0);
        kmsPortField.setText(lp.getString(KMS_PORT, ""));
        add(kmsPortField);

        mathServerField = new BasicEditField("Math Server: ", "", 128, 0);
        mathServerField.setText(lp.getString(MATH_SERVER, ""));
        add(mathServerField);

        mathServerPortField = new BasicEditField("Math Server Port: ", "", 128, 0);
        mathServerPortField.setText(lp.getString(MATH_PORT, ""));
        add(mathServerPortField);

        /* Retrieve, validate, and store keys when fetchKeysButton is clicked */
        fetchKeysButton.setChangeListener(new FieldChangeListener() {
            public void fieldChanged(final Field arg0, final int arg1) {
                if (Dialog.ask(Dialog.D_YES_NO, "Fetch new keys?") == Dialog.YES) {

                    new Thread() {
                        public void run() {
                            KeyData keys;
                            try {

                                // Put user details into persistence store for
                                // next
                                // time.
                                lp.put(KMS_ID, userIdField.getText());
                                lp.put(KMS_USERNAME, userNameField.getText());
                                lp.put(KMS_PASSWORD, userPasswdField.getText());
                                lp.put(KMS_SERVER, kmsServerField.getText());
                                lp.put(KMS_PORT, kmsPortField.getText());
                                lp.put(MATH_PORT, mathServerPortField.getText());
                                lp.put(MATH_SERVER, mathServerField.getText());
                                Connect.PORT = mathServerPortField.getText();
                                Connect.SERVER = mathServerField.getText();

                                keys = new KeyData();
                                Client client = new Client(keys,
                                        kmsServerField.getText(),
                                        kmsPortField.getText(),
                                        userNameField.getText(),
                                        userPasswdField.getText(),
                                        userIdField.getText());

                                updateLabel(fetchKeysStatusLabel, "Fetching keys...");
                                keys = client.fetchKeyMaterial();
                                updateLabel(fetchKeysStatusLabel, fetchKeysStatusLabel.getText() +
                                        "Keys fetched.\nValidating keys...");

                                boolean keysValid = client.validateKeys();

                                if (keysValid) {
                                    updateLabel(fetchKeysStatusLabel, fetchKeysStatusLabel.getText() +
                                            "Keys validated.");
                                }
                                else {
                                    updateLabel(fetchKeysStatusLabel, fetchKeysStatusLabel.getText() +
                                            "Keys Invalid.");
                                    return;
                                }

                                clientData.setKeyData(keys);

                            } catch (IOException e) {
                                updateLabel(fetchKeysStatusLabel, e.toString());

                            } catch (JSONException e) {
                                updateLabel(fetchKeysStatusLabel, e.toString());

                            } catch (DatabaseException e) {
                                updateLabel(fetchKeysStatusLabel, e.toString());

                            } catch (Exception e) {
                                updateLabel(fetchKeysStatusLabel, e.toString());
                            }

                        }
                    }.start();
                }
            }
        });

    }

    public static void updateLabel(final LabelField label, final String text) {

        UiApplication.getUiApplication().invokeLater(new Runnable() {
            public void run() {
                label.setText(text);
            }
        });

    }

    public void onSelected() {
        // TODO Auto-generated method stub

    }

    public void onUnSelected() {
        // TODO Auto-generated method stub

    }

    public boolean navigateBack() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean keyChar(final char ch, final int status, final int time) {
        return super.keyChar(ch, status, time);
    }

}
