package mikeysakke.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import mikeysakke.client.gui.KmsConnectField;

import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.linphone.jlinphone.gui.LinphonePersistance;

/**
 * Utility class with method for connecting to the Math Server
 * @author Iris.Esener
 *
 */
public class Connect {
	
	/**
	 * Private constructor should not be called.
	 * @throws Exception - constructor should not be called
	 */
	private Connect() throws Exception {
		throw new Exception();
	}

    public static String SERVER = LinphonePersistance.instance().getString(KmsConnectField.MATH_SERVER, "192.168.1.3");
    public static String PORT = LinphonePersistance.instance().getString(KmsConnectField.MATH_PORT, "7072");;

    public static JSONObject mathServerRequest(final String requestType, final Hashtable parameters) throws IOException, JSONException {
    	// if port is not 7071, assume it requires a secure connection
        return mathServerRequest(!PORT.equals("7071"), SERVER, PORT, requestType, parameters);
    }

    public static JSONObject mathServerRequest(
    		boolean secure,
    		final String server, final String port,
            final String requestType, final Hashtable parameters) 
            		throws IOException, JSONException {

    	String protocol;
    	String protocolParameters;
    	if (secure) {
    		protocol = "https";
    		protocolParameters = ";deviceside=true;interface=wifi;trustAll";
    	} else {
    		protocol = "http";
    		protocolParameters = ";interface=wifi";
    	}
    	
        Enumeration params = parameters.keys();
        StringBuffer uri = new StringBuffer();
        uri.append(protocol + "://" + server + ":" + port + "/" + requestType + "?");
        String first = (String) params.nextElement();
        uri.append(first + "=" + parameters.get(first));

        while (params.hasMoreElements()) {
            String p = (String) params.nextElement();
            uri.append("&" + p + "=" + parameters.get(p));
        }

        uri.append(protocolParameters);

        HttpConnection httpConnector = 
        		(HttpConnection) Connector.open(uri.toString());

        httpConnector.setRequestMethod(HttpConnection.GET);
        InputStream input = httpConnector.openInputStream();

        byte[] data = new byte[1024];
        int len = 0;
        StringBuffer raw = new StringBuffer();

        while (-1 != (len = input.read(data))) {
            raw.append(new String(data, 0, len));
        }

        String response = raw.toString();

        JSONObject jsonObj = new JSONObject(response);

        input.close();
        httpConnector.close();

        return jsonObj;
    }

}
