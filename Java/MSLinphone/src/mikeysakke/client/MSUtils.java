package mikeysakke.client;

import java.nio.ByteBuffer;
import java.util.Hashtable;

import net.rim.device.api.crypto.CryptoTokenException;
import net.rim.device.api.crypto.CryptoUnsupportedOperationException;
import net.rim.device.api.crypto.HMAC;
import net.rim.device.api.crypto.HMACKey;
import net.rim.device.api.crypto.SHA1Digest;
import net.rim.device.api.util.Arrays;

import org.bouncycastle.java.SecureRandom;
import org.json.me.JSONObject;
import org.linphone.jlinphone.gui.LinphonePersistance;

import mikeysakke.mikey.CsIdMapInfo;
import mikeysakke.mikey.GenericId;
import mikeysakke.mikey.MikeySakkeIMessage;
import mikeysakke.mikey.PayloadHDR;
import mikeysakke.mikey.PayloadRAND;
import mikeysakke.mikey.PayloadSAKKE;
import mikeysakke.mikey.PayloadSIGN;
import mikeysakke.mikey.PayloadSP;
import mikeysakke.mikey.PayloadT;
import mikeysakke.mikey.PolicyParam;
import mikeysakke.mikey.SessionData;
import mikeysakke.mikey.SrtpId;
import mikeysakke.mikey.tables.CsIdMapType;
import mikeysakke.mikey.tables.NextPayload;
import mikeysakke.mikey.tables.ProtType;
import mikeysakke.mikey.utils.Utils;

/**
 * Utility class with various useful methods relating to MIKEY-SAKKE.
 */
public class MSUtils {
	
	private static boolean minisipCompatibilityMode = LinphonePersistance.instance().getBoolean("mikeysakke.client.minisipCompatibilityMode", true); // set to true for calling minisip
	
	/** TEK key type. Use with {@link #generateKeyMaterialFromTGK}. */
	public static final int TEK = 0;
	
	/** Salting key type. Use with {@link #generateKeyMaterialFromTGK}. */
	public static final int SALTING_KEY = 1;

	/**
	 * Private constructor should not be called.
	 * @throws Exception - this constructor should not be called
	 */
	private MSUtils() throws Exception {
		throw new Exception(); // should not happen
	}
	
	/**
	 * Generate keying material from the given TGK.
	 * @param keyType - the type of keying material to generate (one of {@link #TEK}, or {@link #SALTING_KEY})
	 * @param tgk - the TGK
	 * @param csbId - The CSB ID
	 * @param csId - the CS ID
	 * @param rand - random number as byte array
	 * @return the keying material
	 * @throws CryptoTokenException 
	 * @throws CryptoUnsupportedOperationException 
	 */
	public static byte[] generateKeyMaterialFromTGK(
			final int keyType,
			final byte[] tgk, final int csbId, 
			final byte csId, final byte[] rand) 
					throws CryptoTokenException, 
					CryptoUnsupportedOperationException{
		
		int constant;
		int keyLength;
		
		switch (keyType) {
		case TEK:
			constant = 0x2AD01C64;
			keyLength = 16;
			break;
		case SALTING_KEY:
			constant = 0x39A2C14B;
			keyLength = 14;
			break;
		default:
			constant = 0;
			keyLength = 0;
		}
		
		byte[] label = new byte[9 + rand.length]; // 9 = 2 ints + 1 byte
		// constant || csId || csbId || rand
		ByteBuffer labelBf = ByteBuffer.wrap(label);
		labelBf.putInt(constant);
		labelBf.put(csId);
		labelBf.putInt(csbId);
		labelBf.put(rand);
		
		return prf(tgk, labelBf.array(), keyLength);
	}
	
	/**
	 * RFC 3830 4.1.1
	 * @param inkey 
	 * @param inkeyLen 
	 * @param label 
	 * @param outkeyLen 
	 * @return byte array
	 * @throws CryptoTokenException 
	 * @throws CryptoUnsupportedOperationException 
	 */
	public static byte[] p(
			final byte[] s, 
			final byte[] label, 
			final int m) 
					throws CryptoTokenException,
					CryptoUnsupportedOperationException{
		HMACKey key = new HMACKey(s);
		HMAC hmac = new HMAC(key, new SHA1Digest());
		byte[] outkey = new byte[m * SHA1Digest.DIGEST_LENGTH];
		ByteBuffer outkeyBf = ByteBuffer.wrap(outkey);
		
		byte[] lbl = Arrays.copy(label); //copy for later
		byte[] Ai = Arrays.copy(label); // start with A0
		
		for(int i = 0; i < m; i++){
			hmac.update(Ai); 
			Ai = hmac.getMAC(); // A_i = HMAC(s, A_(i-1))
			
			// HMAC (s, A_i || label) ...
			hmac.update(Arrays.copy(Ai));
			hmac.update(lbl);		
			byte[] out = hmac.getMAC();
						
			outkeyBf.put(out);
		}
			
		return outkeyBf.array();
	}
	
	/**
	 * RFC 3830 4.1.2
	 * @param inkey
	 * @param label
	 * @param outkeyLengthBytes
	 * @return
	 * @throws CryptoTokenException
	 * @throws CryptoUnsupportedOperationException
	 */
	public static byte[] prf(byte[] inkey, byte[] label, int outkeyLengthBytes) throws CryptoTokenException, CryptoUnsupportedOperationException{
		//let m = outkey_len / 160, rounded up to the nearest integer if not
	    //  already an integer
	      
		int m = (int)Math.ceil((outkeyLengthBytes)/ 20.0); // 8/160 = 1/20
		ByteBuffer outkeyBuffer = ByteBuffer.allocateDirect(m * SHA1Digest.DIGEST_LENGTH);
			
		//  split the inkey into n blocks, inkey = s_1 || ... || s_n, where *
	    //  all s_i, except possibly s_n, are 256 bits each		
		byte[] outkey = new byte[outkeyLengthBytes];
		for (int i = 0; i < inkey.length; i+=32){ //32B  = 256b, inkey.length is also equal to n*32
			int len;
			if (inkey.length - i > 32) len = 32;
			// TODO: The following line is for compatibility with minisip, which is non-compliant with the default PRF description in RFC3830.
			else if (minisipCompatibilityMode && inkey.length - i == 32) len = 0;
			else len = inkey.length - i;
			
			byte[] Si = Arrays.copy(inkey, i, len);
			outkeyBuffer.position(0);
			byte[] out = p(Si, label, m);
			outkeyBuffer.put(out);
			
			for (int j = 0; j < outkeyLengthBytes; j++){
				outkey[j] ^= outkeyBuffer.get(j);
			}
		}
		return outkey;
	}
	
    /**
     * Takes a hexadecimal String and converts it into a byte array. Taken from
     * OctetString class of MSLibrary
     * 
     * @param hexString
     * @return byte array of String in hex representation
     */
    public static byte[] hexStringToByteArray(final String hexString) {
        // Check if the hex string is in multiples of two, and
        // if not, zero pad it
        String hexToConvert = hexString;
        int hexStringLength = hexString.length();
        if (hexStringLength % 2 == 1) {
            hexToConvert = "0" + hexToConvert;
            hexStringLength++;
        }

        // In multiples of two, convert each hex pair to the corresponding byte
        final int hexBase16 = 16;
        final int bitShiftMultiplyBy4 = 4;
        byte[] data = new byte[hexStringLength / 2];
        for (int i = 0; i < hexStringLength; i += 2) {
            // Convert the characters separately and then combine them to create
            // the correct byte.
            int firstChar = Character.digit(hexToConvert.charAt(i), hexBase16);
            int secondChar = Character.digit(hexToConvert.charAt(i + 1), hexBase16);
            data[i / 2] = (byte) ((firstChar << bitShiftMultiplyBy4) + secondChar);
        }
        return data;
    }

    /**
     * Takes a byte array and converts it into a hexadecimal String
     * 
     * @param octets
     * @return hexadecimal String
     */
    public static String toHex(final byte[] octets) {
        StringBuffer hex = new StringBuffer();

        final int bitMaskFirstHexChar = 0xF0;
        final int bitMaskSecondHexChar = 0x0F;
        final int nibbleBitShift = 4;

        // Loop through each byte and append the first and second hex values.
        for (int i = 0; i < octets.length; i++) {
            hex.append(Integer.toHexString((octets[i] & bitMaskFirstHexChar) >> nibbleBitShift));
            hex.append(Integer.toHexString(octets[i] & bitMaskSecondHexChar));
        }
        return hex.toString();
    }

    /**
     * Generate a MikeySakke I Message with default values
     * 
     * @param sed 
     * @return byte array I message
     * @throws Exception 
     */
    public static byte[] generateMikeySakkeIMessage(
    		final String sed, final KeyData keys) throws Exception {
    	MikeySakkeSrtpParams params =  MikeySakkeSrtpParams.instance();
        SecureRandom random = new SecureRandom();
        
        byte csId = 1;
        byte recCsId = 2;

        // Create HDR and CS ID Map Info
        //GenericId csIdMapInfo = new GenericId((byte) random.nextInt(256))
       int ssrc = random.nextInt();
       int recSsrc = random.nextInt();
       int csbId = random.nextInt();
       params.setCsbId(csbId);
       params.setSendSSRC(ssrc);
       params.setRecSSRC(recSsrc);
       params.setSendCsId(csId);
       params.setRecCsId(recCsId);
       
       CsIdMapInfo csIdMapInfo = generateCsIdMapInfo(ssrc, recSsrc, csbId, csId);
        
        PayloadHDR hdr = new PayloadHDR(PayloadHDR.DEFAULT_VERSION,
        		PayloadHDR.DEFAULT_DATA_TYPE, NextPayload.T, 
        		PayloadHDR.DEFAULT_V, PayloadHDR.DEFAULT_PRF_FUNC, 
        		csbId, (byte)2, 
        		(csIdMapInfo instanceof SrtpId) ? 
        				CsIdMapType.SRTP_ID: CsIdMapType.GENERIC_ID,
                csIdMapInfo);

        long ntpDate = toNtpTime(System.currentTimeMillis());
        byte[] timestamp = Utils.longToBytes(ntpDate);

        // T
        PayloadT t = new PayloadT(timestamp);

        byte[] randomBytes = new byte[16]; // 16 bytes = 128 bits
        random.nextBytes(randomBytes);

        params.setRand(randomBytes);
        PayloadRAND rand = new PayloadRAND(NextPayload.SP, 
        		(byte)16, randomBytes);
 
        PayloadSP sp = new PayloadSP(NextPayload.SAKKE, 
        		(byte)0,
        		ProtType.SRTP, 
        		(byte)(39), //13 policyparams * 3 sections each, each 1 byte
        		createPolicyParams());

        PayloadSAKKE sakke = new PayloadSAKKE(hexStringToByteArray(sed));
        
        PayloadSIGN sign = new PayloadSIGN();
        
        MikeySakkeIMessage iMessage = new MikeySakkeIMessage(hdr);
        iMessage.addPayload(t);
        iMessage.addPayload(rand);
        iMessage.addPayload(sp);
        iMessage.addPayload(sakke);
        iMessage.addPayload(sign);        

        byte[] messageToSign = iMessage.getEncoded();

        Hashtable parameters = new Hashtable();

        parameters.put("message", toHex(messageToSign));
        parameters.put("pvt",
                keys.getPublicValidationToken().toString());
        parameters.put("ssk", keys.getSecretSigningKey().toString());
        parameters.put("hs", keys.getHS().toString());

        JSONObject jsonObj = Connect.mathServerRequest("SIGN",
                parameters);

        String signatureStr;
        if (jsonObj.has("signature")) {
            signatureStr = jsonObj.getString("signature");
        }
        else {
            throw new Exception("No signature returned from Math Server");
        }

        byte[] signature = hexStringToByteArray(signatureStr);
      
        return iMessage.getEncoded(signature);
    }
    
    /**
     * Create Policyparam[] with default parameters
     * @return
     */
    private static PolicyParam[] createPolicyParams(){
    	// See I_MESSAGE Interface Document or SRTP ICD
    	PolicyParam zero = new PolicyParam((byte)0, (byte)1, new byte[]{1});
    	PolicyParam one = new PolicyParam((byte)1, (byte)1, new byte[]{16});
    	PolicyParam two = new PolicyParam((byte)2, (byte)1, new byte[]{1});
    	PolicyParam three = new PolicyParam((byte)3, (byte)1, new byte[]{20});
    	PolicyParam four = new PolicyParam((byte)4, (byte)1, new byte[]{14});
    	PolicyParam five = new PolicyParam((byte)5, (byte)1, new byte[]{0});
    	PolicyParam six = new PolicyParam((byte)6, (byte)1, new byte[]{0});
    	PolicyParam seven = new PolicyParam((byte)7, (byte)1, new byte[]{1});
    	PolicyParam eight = new PolicyParam((byte)8, (byte)1, new byte[]{0});
    	PolicyParam nine = new PolicyParam((byte)9, (byte)1, new byte[]{0});
    	PolicyParam ten = new PolicyParam((byte)10, (byte)1, new byte[]{1});
    	PolicyParam eleven = new PolicyParam((byte)11, (byte)1, new byte[]{10});
    	PolicyParam twelve = new PolicyParam((byte)12, (byte)1, new byte[]{0});
    	
    	return new PolicyParam[]{zero, one, two, three, four, five,
    			six, seven, eight, nine, ten, eleven, twelve};
    }
    
    /**
     * Generate a GenericID or SRTP-ID CS ID Map Info depending on useGenericId boolean
     * @param ssrc
     * @param recSsrc
     * @param csbId
     * @param csId
     * @return CsIdMapInfo
     */
    private static CsIdMapInfo generateCsIdMapInfo(
    		int ssrc, int recSsrc, int csbId, byte csId){
    	CsIdMapInfo csIdMapInfo;
    	// Minisip sends Srtp ID instead of CS
    	if (!minisipCompatibilityMode){
    		csIdMapInfo = new GenericId(csId, GenericId.DEFAULT_PROT_TYPE,
    				GenericId.DEFAULT_S, GenericId.DEFAULT_P_NUMBER, new byte[]{1},
    				8 /*2 ssrcs*/, new SessionData[]{new SessionData(ssrc), new SessionData(recSsrc)},
    				GenericId.DEFAULT_SPI_LENGTH, new byte[0]);
    	}
    	else{
    		csIdMapInfo = new SrtpId();
    		((SrtpId)csIdMapInfo).addPolicy((byte)0, ssrc, 0);
    		((SrtpId)csIdMapInfo).addPolicy((byte)0, recSsrc, 0);
    	}    	
    	return csIdMapInfo;
    		
    }

    /**
     * Taken from TimeStamp.java class of org.apache.commons.net.http
     * 
     * Converts Java time to 64-bit NTP time representation.
     * 
     * @param t
     *            Java time
     * @return NTP timestamp representation of Java time value.
     */
    protected static long toNtpTime(final long t)
    {
        /**
         * baseline NTP time if bit-0=0 -> 7-Feb-2036 @ 06:28:16 UTC
         */
        final long msb0baseTime = 2085978496000L;

        /**
         * baseline NTP time if bit-0=1 -> 1-Jan-1900 @ 01:00:00 UTC
         */
        final long msb1baseTime = -2208988800000L;

        boolean useBase1 = t < msb0baseTime; // time < Feb-2036
        long baseTime;
        if (useBase1) {
            baseTime = t - msb1baseTime; // dates <= Feb-2036
        } else {
            // if base0 needed for dates >= Feb-2036
            baseTime = t - msb0baseTime;
        }

        long seconds = baseTime / 1000;
        long fraction = ((baseTime % 1000) * 0x100000000L) / 1000;

        if (useBase1) {
            seconds |= 0x80000000L; // set high-order bit if msb1baseTime 1900
                                    // used
        }

        long time = seconds << 32 | fraction;
        return time;
    }
    
    /** Set whether the application is in Minisip compatibility mode. 
     *  This is used to allow the application to interoperate with minisip,
     *  which behaves in a non RFC compliant way. */
    public static void minisipCompatibilityMode(boolean mode){
    	minisipCompatibilityMode= mode;
    }
   
}
