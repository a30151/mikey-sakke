package mikeysakke.client;

/**
 * KeyData class used to store keys for a client.
 */
public class KeyData {

    /** KMS Community Identifier. */
    private String kmsIdentifier;
    /** KMS Public Authentication key. */
    private String publicAuthenticationKey;
    /** KMS Public key. */
    private String kmsPublicKey;
    /** KMS Sakke parameter set index (should be 1). */
    private int sakkeParameterSetIndex;

    /** The local device ID. */
    private String identifier;

    /** Public key. */
    private String publicValidationToken;
    /** Private key. */
    private String receiverSecretKey;
    /** Private key. */
    private String secretSigningKey;

    /** Hashed signature. */
    private String HS;

    public KeyData() {
        kmsIdentifier = "";
        publicAuthenticationKey = "";
        kmsPublicKey = "";
        publicValidationToken = "";
        receiverSecretKey = "";
        secretSigningKey = "";
        HS = "";
    }

    /** returns the kmsIdentifier. */
    public String getKmsIdentifier() {
        return kmsIdentifier;
    }

    /** sets the kmsIdentifier. */
    public void setKmsIdentifier(final String kmsIdentifier) {
        this.kmsIdentifier = kmsIdentifier;
    }

    /** returns the kms public authentication key. */
    public String getPublicAuthenticationKey() {
        return publicAuthenticationKey;
    }

    /** sets the kms public authentication key. */
    public void setPublicAuthenticationKey(final String publicAuthenticationKey) {
        this.publicAuthenticationKey = publicAuthenticationKey;
    }

    /** returns the kms public key. */
    public String getKmsPublicKey() {
        return kmsPublicKey;
    }

    /** sets the kms public key. */
    public void setKmsPublicKey(final String kmsPublicKey) {
        this.kmsPublicKey = kmsPublicKey;
    }

    /** returns the sakke parameter set index. */
    public int getSakkeParameterSetIndex() {
        return sakkeParameterSetIndex;
    }

    /** sets the sakke parameter set index. */
    public void setSakkeParameterSetIndex(final int sakkeParameterSetIndex) {
        this.sakkeParameterSetIndex = sakkeParameterSetIndex;
    }

    /** returns the identifier. */
    public String getIdentifier() {
        return identifier;
    }

    /** sets the identifier. */
    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    /** returns the public validation token. */
    public String getPublicValidationToken() {
        return publicValidationToken;
    }

    /** sets the public validation token. */
    public void setPublicValidationToken(final String publicValidationToken) {
        this.publicValidationToken = publicValidationToken;
    }

    /** returns the receiver secret key. */
    public String getReceiverSecretKey() {
        return receiverSecretKey;
    }

    /** sets the receiver secret key. */
    public void setReceiverSecretKey(final String receiverSecretKey) {
        this.receiverSecretKey = receiverSecretKey;
    }

    /** returns the secret signing key. */
    public String getSecretSigningKey() {
        return secretSigningKey;
    }

    /** sets the secret signing key. */
    public void setSecretSigningKey(final String secretSigningKey) {
        this.secretSigningKey = secretSigningKey;
    }

    /** returns HS. */
    public String getHS() {
        return HS;
    }

    /** sets HS. */
    public void setHS(final String hs) {
        HS = hs;
    }

    /** generates an encoded Identifier octetString in the format YYYY-MM\0id\0. */
    public static String generateDatedIdentifier(final String id, final String date) {
        return date + "%00tel:" + id + "%00";
    }

    /** generates an encoded Identifier octetString in the format YYYY-MM\0tel:id\0. */
    public static String generateDatedIdentifier(final String id) {
        return Client.DATE + "%00" + "tel:" + id + "%00";
    }

    /** returns whether or not the key data has been fetched or set yet. */
    public boolean isEmpty() {
        return (kmsIdentifier.equals("") && publicAuthenticationKey.equals("") &&
                secretSigningKey.equals("") && receiverSecretKey.equals("")
                && publicValidationToken.equals("") && kmsPublicKey.equals(""));

    }
}
