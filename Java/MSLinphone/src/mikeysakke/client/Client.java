package mikeysakke.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Hashtable;

import javax.microedition.io.HttpsConnection;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.io.transport.ConnectionFactory;

/**
 * Client class that retrieves keys from the KMS Server and stores them in a
 * KeyData objects.
 */
public class Client {

    /**
     * The current date used for encoding identifiers.
     */
    public static final String DATE = getDateString();

    /** The KeyData object that holds the client's keys. */
    private final KeyData keys;

    /** KMS Server address. */
    private final String kmsServer;

    /** Port to connect to the KMS Server. */
    private final String port;

    /** Client's username to be authenticated by KMS Server. */
    private final String username;

    /** Client's password to be authenticated by KMS Server. */
    private final String password;

    /** Client's name or ID. */
    private final String id;

    /**
     * Client constructor that uses default values (mainly for testing).
     * 
     * @param ks
     *            - the KeyData object to use for this clients key storage
     */
    public Client(final KeyData ks) {
        keys = ks;
        // default values
        kmsServer = "192.168.1.3";
        port = "7070";
        username = "jim";
        password = "jimpass";
        id = "dan";
    }

    /**
     * Client constructor, sets the client's field variables.
     * 
     * @param ks
     *            - the KeyData object to use for this client's key storage
     * @param kmsServer
     *            - the KMS server address
     * @param port
     *            - the port to connect to the KMS server
     * @param username
     *            - client's username
     * @param password
     *            - client's password
     * @param id
     *            - client's ID
     */
    public Client(final KeyData ks, final String kmsServer, final String port,
            final String username, final String password, final String id) {
        keys = ks;
        this.kmsServer = kmsServer;
        this.port = port;
        this.username = username;
        this.password = password;
        this.id = id;
    }

    /**
     * @return the KeyData object that stores this client's keys
     */
    public KeyData getKeys() {
        return keys;
    }

    /**
     * Connect to the KMS server and retrieve the keys for this client. If keys
     * are successfully retrieved, they are stored in the client's KeyData
     * object.
     * 
     * @return the KeyData object that has this client's keys stored
     * @throws IOException 
     * @throws JSONException 
     */
    public KeyData fetchKeyMaterial() throws IOException, JSONException {

        String credentials = username + ":" + password;
        byte[] encodedCredentials = 
        		Base64OutputStream.encode(credentials.getBytes(), 
        				0, credentials.length(), false, false);

        String identifier = KeyData.generateDatedIdentifier(id, DATE);
        
        String url = "https://" + kmsServer + ":" 
        			+ port + "/secure/key?id=" + 
        			identifier + ";interface=wifi";

        ConnectionFactory cf = new ConnectionFactory();
        ConnectionDescriptor cd = cf.getConnection(url);
        HttpsConnection httpsConnector = (HttpsConnection) cd.getConnection();
        httpsConnector.setRequestMethod(HttpsConnection.GET);
        httpsConnector.setRequestProperty("Authorization", 
        		"Basic " + new String(encodedCredentials));

        InputStream input = httpsConnector.openInputStream();
        String testresponse = httpsConnector.getResponseMessage();

        if (!testresponse.equals("OK")) {
            throw new IOException("Response from server: " + testresponse);
        }

        byte[] data = new byte[1024];
        int len = 0;
        StringBuffer raw = new StringBuffer();

        while (-1 != (len = input.read(data))) {
            raw.append(new String(data, 0, len));
        }

        String response = raw.toString();

        input.close();
        httpsConnector.close();

        JSONObject jsonObj = new JSONObject(response);

        JSONArray communityArray = jsonObj.getJSONArray("community");
        JSONObject communityObj = (JSONObject) communityArray.get(0);
        String publicAuthenticationKey = 
        		communityObj.getString("kmsPublicAuthenticationKey");
        String kmsPublicKey = communityObj.getString("kmsPublicKey");
        String kmsIdentifier = communityObj.getString("kmsIdentifier");
        int sakkeParameterSetIndex = 
        		communityObj.getInt("sakkeParameterSetIndex");

        JSONObject publicObj = jsonObj.getJSONObject("public");
        String publicValidationToken = 
        		publicObj.getString("userPublicValidationToken");

        JSONObject privateObj = jsonObj.getJSONObject("private");
        String receiverSecretKey = privateObj.getString("receiverSecretKey");
        String secretSigningKey = privateObj.getString("userSecretSigningKey");

        keys.setIdentifier(identifier);
        keys.setSecretSigningKey(secretSigningKey);
        keys.setReceiverSecretKey(receiverSecretKey);
        keys.setPublicValidationToken(publicValidationToken);
        keys.setPublicAuthenticationKey(publicAuthenticationKey);// KPAK
        keys.setKmsIdentifier(kmsIdentifier);
        keys.setKmsPublicKey(kmsPublicKey); // Z
        keys.setSakkeParameterSetIndex(sakkeParameterSetIndex);

        return keys;
    }

    /**
     * Validate the keys received from the KMS.
     * 
     * @return whether the validation was successful
     * @throws JSONException
     * @throws IOException
     */
    public boolean validateKeys() throws IOException, JSONException {

        Hashtable parameters = new Hashtable();

        String identifier = keys.getIdentifier();
        parameters.put("id", identifier);
        parameters.put("pvt", keys.getPublicValidationToken().toString());
        parameters.put("kpak", keys.getPublicAuthenticationKey().toString());
        parameters.put("z", keys.getKmsPublicKey().toString());
        parameters.put("rsk", keys.getReceiverSecretKey().toString());
        parameters.put("parameterSet", 
        		String.valueOf(keys.getSakkeParameterSetIndex()));
        parameters.put("ssk", keys.getSecretSigningKey().toString());

        JSONObject jsonObj = Connect.mathServerRequest("VALIDATE", parameters);

        boolean validated = false;

        if (jsonObj.has("error")) {
            validated = false;
        }
        else if (jsonObj.has("validated")) {
            validated = ((Boolean) jsonObj.get("validated")).booleanValue();

            if (jsonObj.has("hs")) {
                keys.setHS((String) jsonObj.get("hs"));
            }
            else {
                validated = false; // return false even if validated if there is
                                   // no HS
            }
        }

        return validated;
    }
    
	/**
	 * Get the current year and month formatted as a four digit year followed by
	 * a hyphen, then a two digit month.
	 * 
	 * @return the formatted date string.
	 */
	private static String getDateString() {
		// get current date
		Date today = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
		return dateFormat.format(today);
	}
}
