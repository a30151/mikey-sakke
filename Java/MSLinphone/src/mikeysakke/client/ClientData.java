package mikeysakke.client;

import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.io.MalformedURIException;

/**
 * Singleton class containing data about the client during the session.
 */
public final class ClientData {

    /**
     * The singleton instance.
     */
    private static ClientData instance;

    /**
     * The client's KMS and user keys.
     */
    private KeyData keys;

    /**
     * The DatabaseManager that handles storage of keys.
     */
    private final DatabaseManager dm;
    
    /**
     * Initialises the ClientData and DatabaseManager objects, retrieves saved
     * Keys from the Database.
     * 
     * @throws MalformedURIException 
     * @throws DatabaseException 
     * @throws DataTypeException 
     */
    private ClientData() throws MalformedURIException, 
    DatabaseException, DataTypeException {
        dm = new DatabaseManager();
        keys = dm.getKeyData();
    }

    /**
     * Static method for returning an existing ClientData if it exists, or a new
     * one.
     * 
     * @return the singleton ClientData
     * @throws MalformedURIException 
     * @throws DatabaseException 
     * @throws DataTypeException 
     */
    public static final synchronized ClientData instance() throws 
    MalformedURIException, DatabaseException, DataTypeException {
        if (instance == null) {
            instance = new ClientData();
        }
        return instance;
    }

    /**
     * Sets the keys to the given KeyData and stores it in the database.
     * 
     * @param data 
     * @throws DatabaseException 
     */
    public void setKeyData(final KeyData data) throws DatabaseException {
        keys = data;
        dm.setKeyData(data);
    }

    /**
     * Gets the client's KeyData.
     * 
     * @return keys
     */
    public KeyData getKeys() {
        return keys;
    }

    /**
     * Checks if the KeyData is empty or not.
     * 
     * @return true if keys is not empty
     */
    public boolean containsKeys() {
        return !keys.isEmpty();
    }

}
