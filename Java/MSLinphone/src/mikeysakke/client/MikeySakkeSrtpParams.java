package mikeysakke.client;

/**
 * Singleton class containing parameters for MIKEY-SAKKE and SRTP.
 */
public class MikeySakkeSrtpParams {

	/**
	 * The singleton instance.
	 */
	private static MikeySakkeSrtpParams instance;
	
    /**
     * The SSV for the current session, as a hex byte array.
     */
    private byte[] currentSessionSSV;

    /**
     * The ID of the device on the other end of the call.
     */
    private String otherDeviceId;


	/**
     * Values received through MIKEY.
     */
    private int csbId;
    private byte recCsId;
    private byte sendCsId;
    private byte[] rand;
    
    private int recSSRC;
    private int sendSSRC;
    
	private MikeySakkeSrtpParams(){
		currentSessionSSV = new byte[0];
		otherDeviceId = "";
		csbId = 0;
		recCsId = 0;
		sendCsId = 0;
		recSSRC = 0;
		sendSSRC = 0;
		rand = new byte[0];
		
	}
	public static MikeySakkeSrtpParams instance(){
		if (instance == null) {
            instance = new MikeySakkeSrtpParams();
        }
        return instance;
	}  
	
	
    public byte[] getCurrentSessionSSV() {
		return currentSessionSSV;
	}
	public void setCurrentSessionSSV(byte[] currentSessionSSV) {
		this.currentSessionSSV = currentSessionSSV;
	}
	public String getOtherDeviceId() {
		return otherDeviceId;
	}
	public void setOtherDeviceId(String otherDeviceId) {
		this.otherDeviceId = otherDeviceId;
	}
	public int getCsbId() {
		return csbId;
	}
	public void setCsbId(int csbId) {
		this.csbId = csbId;
	}
	public byte getRecCsId() {
		return recCsId;
	}
	public void setRecCsId(byte csId) {
		this.recCsId = csId;
	}
	public byte getSendCsId() {
		return sendCsId;
	}
	public void setSendCsId(byte csId) {
		this.sendCsId = csId;
	}
	public byte[] getRand() {
		return rand;
	}
	public void setRand(byte[] rand) {
		this.rand = rand;
	}
	public int getRecSSRC() {
		return recSSRC;
	}
	public void setRecSSRC(int recSSRC) {
		this.recSSRC = recSSRC;
	}
	public int getSendSSRC() {
		return sendSSRC;
	}
	public void setSendSSRC(int sendSSRC) {
		this.sendSSRC = sendSSRC;
	}
	
	
	// For debugging purposes
	public String toString(){
		StringBuffer s = new StringBuffer();
		s.append("SSV:");
		for (int i = 0; i < currentSessionSSV.length; i++){
			s.append(" " + currentSessionSSV[i]);
		}
		s.append("\r\n Other device: " + otherDeviceId);
		s.append("\r\n CBS ID: " + csbId);
		s.append("\r\n Receiving CS ID: " + recCsId);
		s.append("\r\n Sending CS ID: " + sendCsId);
		s.append("\r\n Receiving SSRC: " + recSSRC);
		s.append("\r\n Sending SSRC: " + sendSSRC);
		s.append("\r\n Rand:");
		for (int i = 0; i < rand.length; i++){
			s.append(" " + rand[i]);
		}

		return s.toString();
		
	}
}
