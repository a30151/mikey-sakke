package mikeysakke.client;

import net.rim.device.api.database.Cursor;
import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.Database;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.database.DatabaseFactory;
import net.rim.device.api.database.Row;
import net.rim.device.api.database.Statement;
import net.rim.device.api.io.MalformedURIException;
import net.rim.device.api.io.URI;

/**
 * Class for managing tables in database.
 *
 */
public class DatabaseManager {

    /**
     * The URI of the database in the BlackBerry device.
     */
    private URI dbURI;

    /**
     * Strings representing column names.
     */
    private static String KEY_DATA_TABLE = "key_data";
    private static String KMS_ID_COLUMN = "kms_id";
    private static String KPAK_COLUMN = "kpak";
    private static String Z_COLUMN = "z";
    private static String PARAM_SET_COLUMN = "param_set";
    private static String DEVICE_ID_COLUMN = "device_id";
    private static String PVT_COLUMN = "pvt";
    private static String RSK_COLUMN = "rsk";
    private static String SSK_COLUMN = "ssk";
    private static String HS_COLUMN = "hs";

    /**
     * The Database object where the data will be stored.
     */
    private Database d;

    public DatabaseManager() throws IllegalArgumentException,
    MalformedURIException, DatabaseException, DataTypeException {

        dbURI = URI.create("file:///store/databases/LinphoneDB/" 
        			+ "LinphoneDB.db");
        create();

    }

    /**
     * creates the database with a table for key data and a table for SSVs.
     * 
     * @throws DatabaseException 
     * @throws MalformedURIException 
     * @throws IllegalArgumentException 
     * @throws DataTypeException 
     */
    private void create() throws IllegalArgumentException, 
    					MalformedURIException,
            DatabaseException, DataTypeException {

        if (!DatabaseFactory.exists(dbURI)) {
            d = DatabaseFactory.openOrCreate(dbURI);

            d.executeStatement("CREATE TABLE " + KEY_DATA_TABLE + " ( "
                    + KMS_ID_COLUMN + "    TEXT,"
                    + KPAK_COLUMN + "      TEXT," 
                    + Z_COLUMN + "         TEXT," 
                    + PARAM_SET_COLUMN + " INTEGER," 
                    + DEVICE_ID_COLUMN + " TEXT," 
                    + PVT_COLUMN + "       TEXT," 
                    + RSK_COLUMN + "       TEXT,"
                    + SSK_COLUMN + "       TEXT,"
                    + HS_COLUMN + "       TEXT )");

            d.close();
        }

    }

    /**
     * resets the database tables to new.
     * 
     * @throws DataTypeException 
     * @throws DatabaseException 
     * @throws MalformedURIException 
     * @throws IllegalArgumentException 
     */
    public void reset() throws IllegalArgumentException,
    MalformedURIException, DatabaseException, DataTypeException {
        if (DatabaseFactory.exists(dbURI)) {
            DatabaseFactory.delete(dbURI);
            create();
        }

    }
    
    /**
     * Gets key data from the database (returns empty keydata if the table is
     * empty).
     * @return KeyData
     * @throws DataTypeException 
     * @throws DatabaseException 
     */
    public KeyData getKeyData() throws DataTypeException, DatabaseException {
        KeyData data = new KeyData();
        d = DatabaseFactory.open(dbURI);

        // See if there are any rows in the KEY_DATA_TABLE
        Statement s = d.createStatement("SELECT * FROM " + KEY_DATA_TABLE);
        s.prepare();
        Cursor c = s.getCursor();

        if (c.first()) {
            Row r = c.getRow();
            data.setKmsIdentifier(r.getString(c.getColumnIndex(KMS_ID_COLUMN)));
            data.setPublicAuthenticationKey(r.getString(c.getColumnIndex(KPAK_COLUMN)));
            data.setKmsPublicKey(r.getString(c.getColumnIndex(Z_COLUMN)));
            data.setSakkeParameterSetIndex(r.getInteger(c.getColumnIndex(PARAM_SET_COLUMN)));
            data.setIdentifier(r.getString(c.getColumnIndex(DEVICE_ID_COLUMN)));
            data.setPublicValidationToken(r.getString(c.getColumnIndex(PVT_COLUMN)));
            data.setReceiverSecretKey(r.getString(c.getColumnIndex(RSK_COLUMN)));
            data.setSecretSigningKey(r.getString(c.getColumnIndex(SSK_COLUMN)));
            data.setHS(r.getString(c.getColumnIndex(HS_COLUMN)));

        } else {
            data = new KeyData();
        }
        c.close();
        s.close();
        d.close();
        return data;
    }

    /**
     * Inserts the given key data into the database.
     * @param data 
     * @throws DatabaseException 
     */
    public void setKeyData(final KeyData data) throws DatabaseException {

        d = DatabaseFactory.open(dbURI);

        // Delete the existing row because there should only ever be 1 row
        d.executeStatement("DELETE FROM " + KEY_DATA_TABLE);

        d.executeStatement("INSERT INTO " + KEY_DATA_TABLE + " VALUES ('"
                + data.getKmsIdentifier()
                + "', '" + data.getPublicAuthenticationKey()
                + "', '" + data.getKmsPublicKey()
                + "', " + String.valueOf(data.getSakkeParameterSetIndex())
                + ", '" + data.getIdentifier()
                + "', '" + data.getPublicValidationToken()
                + "', '" + data.getReceiverSecretKey()
                + "', '" + data.getSecretSigningKey()
                + "', '" + data.getHS()
                + "')");

        d.close();

    }
}
