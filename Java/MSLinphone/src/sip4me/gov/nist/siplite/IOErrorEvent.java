package sip4me.gov.nist.siplite;

public class IOErrorEvent extends SipEvent {
    private String mHost;
    private int mPort;
    private String mTransport;

    public IOErrorEvent(Object source, String host, int port,
            String transport) {
        super(source);
        mHost = host;
        mPort = port;
        mTransport = transport;
    }

    public String getHost() {
        return mHost;
    }

    public int getPort() {
        return mPort;
    }

    public String getTransport() {
        return mTransport;
    }

}
