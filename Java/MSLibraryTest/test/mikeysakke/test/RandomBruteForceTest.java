package mikeysakke.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.bouncycastle.java.BigInteger;
import org.bouncycastle.java.SecureRandom;
import org.junit.Test;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.EccsiParameterSet;
import mikeysakke.crypto.Sakke;
import mikeysakke.crypto.SakkeParameterSet1;
import mikeysakke.utils.OctetString;
import mikeysakke.utils.RandomGenerator;

public class RandomBruteForceTest {

    @Test
    public void test() throws Exception {
        int max = 1000;
        for (int i = 0; i < max; i++) {
            System.out.print("Running " + (i + 1) + " of " + max + "...");
            runMSCryptoTest();
            System.out.println("\tTest passed.");
        }
    }

    /**
     * Generates a random OctetString
     * 
     * @return a random OctetString of random length between 1 and 255
     */
    public static OctetString generateRandomIdentifier() {
        SecureRandom rand = new SecureRandom();
        int randomInt = rand.nextInt(255) + 1;
        OctetString random = new OctetString(randomInt);
        rand.nextBytes(random.getOctets());
        return random;
    }

    /**
     * Generates a random BigInteger based on given parameters
     * 
     * @param greaterThan
     *            - a BigInteger the result must be greater than
     * @param lessThan
     *            - a BigInteger the result must be less than
     * @return random BigInteger where greaterThan < randomInt < lessThan
     */
    public static BigInteger generateRandomNumber(final BigInteger greaterThan, final BigInteger lessThan) {
        BigInteger randomInt;
        do {
            OctetString random = new OctetString(lessThan, (lessThan.bitLength() + 7) / 8);
            SecureRandom rand = new SecureRandom();
            rand.nextBytes(random.getOctets());
            randomInt = new BigInteger(1, random.getOctets());
        } while (randomInt.compareTo(lessThan) != -1 && randomInt.compareTo(greaterThan) != 1);

        return randomInt;
    }

    public static void runMSCryptoTest() throws Exception {
        SakkeParameterSet1 sakkeParameters = new SakkeParameterSet1();

        OctetString Z = new OctetString();
        BigInteger KSAK = BigInteger.ZERO;
        OctetString KPAK = new OctetString();
        OctetString aliceID = new OctetString();
        OctetString bobID = new OctetString();
        OctetString aliceHS = new OctetString();
        OctetString testHS = new OctetString();
        OctetString alicePVT = new OctetString();
        OctetString aliceSSK = new OctetString();
        OctetString bobRSK = new OctetString();
        OctetString message = new OctetString();
        OctetString signature = new OctetString();
        OctetString SED = new OctetString();
        OctetString aliceSSV = new OctetString();
        OctetString bobSSV = new OctetString();
        class Random implements RandomGenerator
        {
            OctetString lastGenerated = new OctetString();

            @Override
            public OctetString generate(final int n)
            {

                byte[] buffer = new byte[n];

                SecureRandom rand = new SecureRandom();
                rand.nextBytes(buffer);
                OctetString os = new OctetString(buffer);
                lastGenerated = os;
                return os;

            }
        }
        Random r = new Random();
        try {
            // KMS setup
            BigInteger z = generateRandomNumber(BigInteger.ONE, sakkeParameters.q());
            Z = new OctetString(sakkeParameters.pointP().multiply(z).getEncoded());

            KSAK = generateRandomNumber(BigInteger.ONE, EccsiParameterSet.q);
            KPAK = new OctetString(EccsiParameterSet.G.multiply(KSAK).getEncoded());

            // generate random identifiers for alice and bob
            aliceID = generateRandomIdentifier();
            bobID = generateRandomIdentifier();

            // SSK and PVT to alice

            BigInteger v = generateRandomNumber(BigInteger.ZERO, EccsiParameterSet.q);
            alicePVT = new OctetString(EccsiParameterSet.G.multiply(v).getEncoded());

            // HS = hash (G || KPAK || ID || PVT)
            OctetString unhashedHS = new OctetString();
            unhashedHS.append(EccsiParameterSet.GString);
            unhashedHS.append(KPAK);
            unhashedHS.append(aliceID);
            unhashedHS.append(alicePVT);
            aliceHS = new OctetString(EccsiParameterSet.hashLength);
            EccsiParameterSet.hash.update(unhashedHS.getOctets(), 0, unhashedHS.size());
            EccsiParameterSet.hash.doFinal(aliceHS.getOctets(), 0);

            // SSK = (v*HS + KPAK) mod q
            BigInteger ssk = (v.multiply(new BigInteger(1, aliceHS.getOctets())).add(KSAK)).mod(EccsiParameterSet.q);
            aliceSSK = new OctetString(ssk, EccsiParameterSet.qLengthInBytes);

            // testHS to check if validateSigningKeys generates and caches the
            // correct value
            testHS = new OctetString();
            boolean aliceOK = Eccsi.validateSigningKeys(aliceID, alicePVT, KPAK, aliceSSK, testHS);

            assertTrue("SSK, PVT not verified.", aliceOK);
            assertTrue("HS generated incorrectly in verification.", testHS.equals(aliceHS));

            // RSK to bob
            BigInteger inv = new BigInteger(1, bobID.getOctets());
            inv = inv.add(z).modInverse(sakkeParameters.q());

            bobRSK = new OctetString(sakkeParameters.pointP().multiply(inv).getEncoded());

            boolean bobOK = Sakke.validateReceiverSecretKey(bobID, Z, bobRSK, 1);

            assertTrue("RSK not verified.", bobOK);

            // alice signs message for bob

            // generate a random message
            message = generateRandomIdentifier();

            // alice signs message
            signature = Eccsi.sign(message, alicePVT, aliceSSK, aliceHS, r);

            // bob verifies signature
            boolean sigVerified = Eccsi.verify(message, signature, aliceID, KPAK);

            assertTrue("Signature is null.", signature != null);
            assertTrue("Signature not verified.", sigVerified);

            // alice generates a shared secret and SED
            SED = new OctetString();
            aliceSSV = Sakke.generateSharedSecretAndSED(SED, bobID, 1, Z, r);

            int SEDexpectedSize = sakkeParameters.encDataLengthBytes();

            assertEquals("SED length " +
                    SED.size() +
                    ", expected: " +
                    SEDexpectedSize, SEDexpectedSize, SED.size());

            // bob extracts shared secret
            bobSSV = Sakke.extractSharedSecret(SED, bobID, 1, bobRSK, Z);

            assertTrue("Bob's extracted SSV not equal to Alice's generated SSV.", bobSSV.equals(aliceSSV));

            System.out.print("\tSSV: " + bobSSV);

        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
            System.out.println("KSAK: " + KSAK
                    + "\nZ: " + Z
                    + "\nKPAK: " + KPAK
                    + "\naliceID: " + aliceID
                    + "\naliceSSK: " + aliceSSK
                    + "\nalicePVT: " + alicePVT
                    + "\naliceHS: " + aliceHS
                    + "\ntestHS: " + testHS
                    + "\nbobID: " + bobID
                    + "\n bobRSK: " + bobRSK
                    + "\nMessage: " + message
                    + "\nSignature: " + signature
                    + "\naliceSSV: " + aliceSSV
                    + "\nSED: " + SED
                    + "\nbobSSV: " + bobSSV
                    + "\nlast generated random: " + r.lastGenerated);
            throw e;
        } catch (Error e) {
            System.out.println("Error: " + e.getMessage());
            System.out.println("KSAK: " + KSAK
                    + "\nZ: " + Z
                    + "\nKPAK: " + KPAK
                    + "\naliceID: " + aliceID
                    + "\naliceSSK: " + aliceSSK
                    + "\nalicePVT: " + alicePVT
                    + "\naliceHS: " + aliceHS
                    + "\ntestHS: " + testHS
                    + "\nbobID: " + bobID
                    + "\n bobRSK: " + bobRSK
                    + "\nMessage: " + message
                    + "\nSignature: " + signature
                    + "\naliceSSV: " + aliceSSV
                    + "\nSED: " + SED
                    + "\nbobSSV: " + bobSSV
                    + "\nlast generated random: " + r.lastGenerated);
            throw e;
        }

    }
}
