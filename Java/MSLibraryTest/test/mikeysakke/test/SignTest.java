package mikeysakke.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.bouncycastle.java.BigInteger;
import org.junit.Test;

import mikeysakke.crypto.Eccsi;
import mikeysakke.crypto.EccsiParameterSet;
import mikeysakke.utils.OctetString;
import mikeysakke.utils.RandomGenerator;

public class SignTest {
    class EphemeralRandomGenerator implements RandomGenerator
    {

        @Override
        public OctetString generate(final int n)
        {

            return OctetString.fromHex("34567");
        }
    }

    private static final OctetString HS = OctetString.fromHex(
            "490F3FEBBC1C902F6289723D7F8CBF79"
                    + "DB88930849D19F38F0295B5C276C14D1");

    private static final OctetString message = OctetString.fromAscii("message\0");

    private static final OctetString pvt = OctetString.fromHex(
            "04" + "758A142779BE89E829E71984CB40EF75" +
                    "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                    "A79D247692F4EDA3A6BDAB77D6AA6474" +
                    "A464AE4934663C5265BA7018BA091F79");

    private static final OctetString ssk = OctetString.fromHex(
            "23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34489A0D");

    private static final OctetString expectedSignature = OctetString.fromHex(
            "269D4C8FDEB66A74E4EF8C0D5DCC597D" +
                    "DFE6029C2AFFC4936008CD2CC1045D81" +
                    "E09B528D0EF8D6DF1AA3ECBF80110CFC" +
                    "EC9FC68252CEBB679F4134846940CCFD" +
                    "04" +

                    "758A142779BE89E829E71984CB40EF75" +
                    "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                    "A79D247692F4EDA3A6BDAB77D6AA6474" +
                    "A464AE4934663C5265BA7018BA091F79");

    @Test
    public void signOKTest() {
        RandomGenerator r = new EphemeralRandomGenerator();
        OctetString signature = Eccsi.sign(message, pvt, ssk, HS, r);

        assertTrue("Signature does not match expected", signature.equals(expectedSignature));
    }

    @Test
    public void signIncorrectSSVTest() {
        RandomGenerator r = new EphemeralRandomGenerator();
        OctetString badSSK = OctetString.fromHex(
                "23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34489A05");
        OctetString signature = Eccsi.sign(message, pvt, badSSK, HS, r);

        assertFalse("Signature matches expected with badSSK", signature.equals(expectedSignature));
    }

    @Test
    public void signIncorrectPVTTest() {
        RandomGenerator r = new EphemeralRandomGenerator();
        OctetString badPVT = OctetString.fromHex(
                "04" + "758A142779BE89E829E71984CB40EF75" +
                        "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                        "A79D247692F4EDA3A6BDAB77D6AA6474" +
                        "A464AE4934663C5265BA7018BA091F7");
        OctetString signature = Eccsi.sign(message, badPVT, ssk, HS, r);

        assertFalse("Signature matches expected with badPVT", signature.equals(expectedSignature));
    }

    @Test
    public void signIncorrectSignatureLengthTest() {
        RandomGenerator r = new EphemeralRandomGenerator();

        OctetString badPVT = OctetString.fromHex(
                "04" + "758A142779BE89E829E71984CB40EF75" +
                        "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                        "A79D247692F4EDA3A6BDAB77474" +
                        "A464AE4934663C5265BA7018F7");
        OctetString signature = Eccsi.sign(message, badPVT, ssk, HS, r);

        assertNull("Signature matches expected incorrect PVT / signature length", signature);

    }

    @Test
    public void signIncorrectHSTest() {
        RandomGenerator r = new EphemeralRandomGenerator();
        OctetString badHS = OctetString.fromHex(
                "490F3FEBBC1C902F6289723D7F8CBF79"
                        + "DB88930849D19F38F0295B5C276C14D8");
        OctetString signature = Eccsi.sign(message, pvt, ssk, badHS, r);

        assertFalse("Signature matches expected with badHS", signature.equals(expectedSignature));
    }

    @Test
    public void signEmptyHSTest() {
        RandomGenerator r = new EphemeralRandomGenerator();
        Error emptyHS = new Error();
        Error nullHS = new Error();
        try {
            Eccsi.sign(message, pvt, ssk, new OctetString(), r);
        } catch (Error e) {
            emptyHS = e;
        }
        try {
            Eccsi.sign(message, pvt, ssk, null, r);
        } catch (Error e) {
            nullHS = e;

        }

        assertTrue("Error message for null HS does not match expected",
                nullHS.getMessage().equals("Implementation currently requires cached HS"));

        assertTrue("Error message for empty HS does not match expected",
                emptyHS.getMessage().equals("Implementation currently requires cached HS"));

    }

    @Test
    public void randomNumberZeroTest() {

        // generate zero on first try to get complete coverate in do-while loop
        class ZeroFirstRandomGenerator implements RandomGenerator
        {
            private boolean firsttry = true;
            private boolean secondtry = false;

            @Override
            public OctetString generate(final int n)
            {
                if (firsttry) {
                    firsttry = false;
                    secondtry = true;
                    return new OctetString(BigInteger.ZERO, 1);
                }
                if (secondtry) {
                    secondtry = false;
                    return new OctetString(EccsiParameterSet.q, EccsiParameterSet.qLengthInBytes);
                }
                return OctetString.fromHex("34567");
            }
        }

        RandomGenerator r = new ZeroFirstRandomGenerator();
        OctetString signature = Eccsi.sign(message, pvt, ssk, HS, r);

        assertTrue("Signature does not match expected", signature.equals(expectedSignature));

    }

}
