package mikeysakke.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mikeysakke.crypto.Eccsi;
import mikeysakke.utils.OctetString;

public class VerifyMessageAgainstSignatureTest {
    private static final OctetString message = OctetString.fromAscii("message\0");
    private static final OctetString signature = OctetString.fromHex(
            "269D4C8FDEB66A74E4EF8C0D5DCC597D" +
                    "DFE6029C2AFFC4936008CD2CC1045D81" +
                    "E09B528D0EF8D6DF1AA3ECBF80110CFC" +
                    "EC9FC68252CEBB679F4134846940CCFD" +
                    "04" +

                    "758A142779BE89E829E71984CB40EF75" +
                    "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                    "A79D247692F4EDA3A6BDAB77D6AA6474" +
                    "A464AE4934663C5265BA7018BA091F79");

    private final static OctetString id =
            OctetString.fromAscii("2011-02\0tel:+447700900123\0");

    private final static OctetString kpak = OctetString.fromHex(
            "04" + "50D4670BDE75244F28D2838A0D25558A" +
                    "7A72686D4522D4C8273FB6442AEBFA93" +
                    "DBDD37551AFD263B5DFD617F3960C65A" +
                    "8C298850FF99F20366DCE7D4367217F4");

    @Test
    public void verifyOKTest() {

        boolean verified = Eccsi.verify(message, signature, id, kpak);
        assertTrue("Signature failed to verify", verified);
    }

    @Test
    public void verifyIncorrectSignatureTest() {

        OctetString badSignature = OctetString.fromHex(
                "269D4C8FDEB66A74E4EF8C0D5DCC597D" +
                        "DFE6029C2AFFC4936008CD2CC1045D81" +
                        "E09B528D0EF8D6DF1AA3ECBF80110CFC" +
                        "EC9FC68252CEBB679F4134846940CCFD" +
                        "04" +

                        "758A142779BE89E829E71984CB40EF75" +
                        "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                        "A79D247692F4EDA3A6BDAB77D6AA6474" +
                        "A464AE4934663C5265BA7018BA091F12");
        boolean verified = Eccsi.verify(message, badSignature, id, kpak);
        assertFalse("Signature verified with badSignature", verified);
    }

    @Test
    public void verifyIncorrectMessageTest() {

        OctetString badMessage = OctetString.fromAscii("badmessage\0");
        boolean verified = Eccsi.verify(badMessage, signature, id, kpak);
        assertFalse("Signature verified with badMessage", verified);
    }

    @Test
    public void verifyIncorrectKPAKTest() {

        OctetString badKPAK = OctetString.fromHex(
                "04" + "50D4670BDE75244F28D2838A0D25558A" +
                        "7A72686D4522D4C8273FB6442AEBFA93" +
                        "DBDD37551AFD263B5DFD617F3960C65A" +
                        "8C298850FF99F20366DCE7D436721701");
        boolean verified = Eccsi.verify(message, signature, id, badKPAK);
        assertFalse("Signature verified with badKPAK", verified);
    }

    @Test
    public void verifyIncorrectIDTest() {

        OctetString badId = OctetString.fromAscii("2011-02\0tel:+447700900125\0");
        boolean verified = Eccsi.verify(message, signature, badId, kpak);
        assertFalse("Signature verified with badId", verified);
    }

}
