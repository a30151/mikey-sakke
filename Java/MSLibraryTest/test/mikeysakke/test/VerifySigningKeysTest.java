package mikeysakke.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mikeysakke.crypto.Eccsi;
import mikeysakke.utils.OctetString;

public class VerifySigningKeysTest {

    private final static OctetString kpak = OctetString.fromHex(
            "04" + "50D4670BDE75244F28D2838A0D25558A" +
                    "7A72686D4522D4C8273FB6442AEBFA93" +
                    "DBDD37551AFD263B5DFD617F3960C65A" +
                    "8C298850FF99F20366DCE7D4367217F4");

    private final static OctetString id =
            OctetString.fromAscii("2011-02\0tel:+447700900123\0");

    private final static OctetString ssk =
            OctetString.fromHex("23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34489A0D");

    private final static OctetString pvt = OctetString.fromHex(
            "04" + "758A142779BE89E829E71984CB40EF75" +
                    "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                    "A79D247692F4EDA3A6BDAB77D6AA6474" +
                    "A464AE4934663C5265BA7018BA091F79");

    @Test
    public void VerifySigningKeysOKTest() {
        boolean validated = Eccsi.validateSigningKeys(id, pvt, kpak, ssk, new OctetString());
        assertTrue("SSK, PVT failed validation", validated);
    }

    @Test
    public void VerifySigningKeysIncorrectKPAKTest() {
        OctetString badKpak = OctetString.fromHex(
                "04" + "50D4670BDE75244F28D2838A0D25558A" +
                        "7A72686D4522D4C8273FB6442AEBFA93" +
                        "DBDD37551AFD263B5DFE617F3960C65A" +
                        "8C298850FF99F20366DCE7D4367217F4");

        boolean validated = Eccsi.validateSigningKeys(id, pvt, badKpak, ssk, new OctetString());
        assertFalse("SSK, PVT passed validation with badKpak", validated);
    }

    @Test
    public void VerifySigningKeysIncorrectIDTest() {
        OctetString badId = OctetString.fromAscii("2011-01\0tel:+447700900123\0");

        boolean validated = Eccsi.validateSigningKeys(badId, pvt, kpak, ssk, new OctetString());
        assertFalse("SSK, PVT passed validation with badId", validated);
    }

    @Test
    public void VerifySigningKeysIncorrectSSKTest() {
        OctetString badSsk = OctetString.fromHex("23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34499A0D");

        boolean validated = Eccsi.validateSigningKeys(id, pvt, kpak, badSsk, new OctetString());
        assertFalse("SSK, PVT passed validation with badSsk", validated);
    }

    @Test
    public void VerifySigningKeysIncorrectPVTTest() {
        OctetString badPvt = OctetString.fromHex(
                "04" + "758A142779BE89E829E71984CB40EF75" +
                        "1CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                        "A79D247692F4EDA3A6BDAB77D6AA6474" +
                        "A464AE4934663C5265BA7018BA091F79");

        boolean validated = Eccsi.validateSigningKeys(id, badPvt, kpak, ssk, new OctetString());
        assertFalse("SSK, PVT passed validation with badPvt", validated);
    }
}
