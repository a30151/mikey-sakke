package mikeysakke.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        VerifySigningKeysTest.class,
        VerifyRSKTest.class,
        ExtractSharedSecretTest.class,
        GenerateSharedSecretAndSEDTest.class,
        SignTest.class,
        VerifyMessageAgainstSignatureTest.class })
public class AllTests {

}
