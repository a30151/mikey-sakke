package mikeysakke.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mikeysakke.crypto.Sakke;
import mikeysakke.utils.OctetString;

public class VerifyRSKTest {
    private final static OctetString Z = OctetString.fromHex
            ("04" + "5958EF1B1679BF099B3A030DF255AA6A" +
                    "23C1D8F143D4D23F753E69BD27A832F3" +
                    "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
                    "10EFFE300367A37B61F701D914AEF097" +
                    "24825FA0707D61A6DFF4FBD7273566CD" +
                    "DE352A0B04B7C16A78309BE640697DE7" +
                    "47613A5FC195E8B9F328852A579DB8F9" +
                    "9B1D0034479EA9C5595F47C4B2F54FF2" +

                    "1508D37514DCF7A8E143A6058C09A6BF" +
                    "2C9858CA37C258065AE6BF7532BC8B5B" +
                    "63383866E0753C5AC0E72709F8445F2E" +
                    "6178E065857E0EDA10F68206B63505ED" +
                    "87E534FB2831FF957FB7DC619DAE6130" +
                    "1EEACC2FDA3680EA4999258A833CEA8F" +
                    "C67C6D19487FB449059F26CC8AAB655A" +
                    "B58B7CC796E24E9A394095754F5F8BAE");

    private final static OctetString id = OctetString.fromAscii("2011-02\0tel:+447700900123\0");

    private final static OctetString rsk = OctetString.fromHex(
            "04" + "93AF67E5007BA6E6A80DA793DA300FA4" +
                    "B52D0A74E25E6E7B2B3D6EE9D18A9B5C" +
                    "5023597BD82D8062D34019563BA1D25C" +
                    "0DC56B7B979D74AA50F29FBF11CC2C93" +
                    "F5DFCA615E609279F6175CEADB00B58C" +
                    "6BEE1E7A2A47C4F0C456F05259A6FA94" +
                    "A634A40DAE1DF593D4FECF688D5FC678" +
                    "BE7EFC6DF3D6835325B83B2C6E69036B" +

                    "155F0A27241094B04BFB0BDFAC6C670A" +
                    "65C325D39A069F03659D44CA27D3BE8D" +
                    "F311172B554160181CBE94A2A783320C" +
                    "ED590BC42644702CF371271E496BF20F" +
                    "588B78A1BC01ECBB6559934BDD2FB65D" +
                    "2884318A33D1A42ADF5E33CC5800280B" +
                    "28356497F87135BAB9612A1726042440" +
                    "9AC15FEE996B744C332151235DECB0F5");

    @Test
    public void VerifyRSKOKTest() {

        boolean validated = Sakke.validateReceiverSecretKey(id, Z, rsk, 1);
        assertTrue("RSK failed validation", validated);
    }

    @Test
    public void verifyRSKIncorrectZTest() {
        OctetString badZ = OctetString.fromHex(
                "04" + "5958EF1B1679BF099B3A030DF255AA6A" +
                        "23C1D8F143D4D23F753E69BD27A832F3" +
                        "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
                        "10EFFE300367A37B61F701D914AEF097" +
                        "24825FA0707D61A6DFF4FBD7273566CD" +
                        "DE352A0B04B7C16A78309BE640697DE7" +
                        "47613A5FC195E8B9F328852A579DB8F9" +
                        "9B1D0034479EA9C5595F47C4B2F54FF2" +

                        "1508D37514DCF7A8E143A6058C09A6BF" +
                        "2C9858CA37C258065AE6BF7532BC8B5B" +
                        "63383866E0753C5AC0E72709F8445F2E" +
                        "6178E065857E0EDA10F68206B63505ED" +
                        "87E534FB2831FF957FB7DC619DAE6130" +
                        "1EEACC2FDA3680EA4999258A833CEA8F" +
                        "C67C6D19487FB449059F26CC8AAB655A" +
                        "B58B7CC796E24E9A394095754F5F8BAF");

        boolean validated = Sakke.validateReceiverSecretKey(id, badZ, rsk, 1);
        assertFalse("RSK passed validation with badZ", validated);
    }

    @Test
    public void verifyRSKIncorrectIDTest() {

        OctetString badId = OctetString.fromAscii("2011-02\0tel:+447700900124\0");

        boolean validated = Sakke.validateReceiverSecretKey(badId, Z, rsk, 1);
        assertFalse("RSK passed validation with badId", validated);
    }

    @Test
    public void verifyRSKIncorrectRSKTest() {
        OctetString badRSK = OctetString.fromHex(
                "04" + "93AF67E5007BA6E6A80DA793DA300FA4" +
                        "B52D0A74E25E6E7B2B3D6EE9D18A9B5C" +
                        "5023597BD82D8062D34019563BA1D25C" +
                        "0DC56B7B979D74AA50F29FBF11CC2C93" +
                        "F5DFCA615E609279F6175CEADB00B58C" +
                        "6BEE1E7A2A47C4F0C456F05259A6FA94" +
                        "A634A40DAE1DF593D4FECF688D5FC678" +
                        "BE7EFC6DF3D6835325B83B2C6E69036B" +

                        "155F0A27241094B04BFB0BDFAC6C670A" +
                        "65C325D39A069F03659D44CA27D3BE8D" +
                        "F311172B554160181CBE94A2A783320C" +
                        "ED590BC42644702CF371271E496BF20F" +
                        "588B78A1BC01ECBB6559934BDD2FB65D" +
                        "2884318A33D1A42ADF5E33CC5800280B" +
                        "28356497F87135BAB9612A1726042440" +
                        "9AC15FEE996B744C332151235DECB0FA");

        boolean validated = Sakke.validateReceiverSecretKey(id, Z, badRSK, 1);
        assertFalse("RSK passed validation with badRSK", validated);
    }

}
