import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import org.apache.http.client.ClientProtocolException;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.junit.Test;

public class ValidateKeysTest {
	 private final static String kpak = 
		 "04" + "50D4670BDE75244F28D2838A0D25558A" +
		         "7A72686D4522D4C8273FB6442AEBFA93" +
		         "DBDD37551AFD263B5DFD617F3960C65A" +
		         "8C298850FF99F20366DCE7D4367217F4";

    private final static String id =
            "2011-02%00tel:%2B447700900123%00";

    private final static String ssk =
            "23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34489A0D";

    private final static String pvt = 
    	 "04" + "758A142779BE89E829E71984CB40EF75" +
		         "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
		         "A79D247692F4EDA3A6BDAB77D6AA6474" +
		         "A464AE4934663C5265BA7018BA091F79";
    
    private final static String Z = 
    	"04" + "5958EF1B1679BF099B3A030DF255AA6A" +
		        "23C1D8F143D4D23F753E69BD27A832F3" +
		        "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
		        "10EFFE300367A37B61F701D914AEF097" +
		        "24825FA0707D61A6DFF4FBD7273566CD" +
		        "DE352A0B04B7C16A78309BE640697DE7" +
		        "47613A5FC195E8B9F328852A579DB8F9" +
		        "9B1D0034479EA9C5595F47C4B2F54FF2" +
		
		        "1508D37514DCF7A8E143A6058C09A6BF" +
		        "2C9858CA37C258065AE6BF7532BC8B5B" +
		        "63383866E0753C5AC0E72709F8445F2E" +
		        "6178E065857E0EDA10F68206B63505ED" +
		        "87E534FB2831FF957FB7DC619DAE6130" +
		        "1EEACC2FDA3680EA4999258A833CEA8F" +
		        "C67C6D19487FB449059F26CC8AAB655A" +
		        "B58B7CC796E24E9A394095754F5F8BAE";

	private final static String rsk =
		"04" + "93AF67E5007BA6E6A80DA793DA300FA4" +
		        "B52D0A74E25E6E7B2B3D6EE9D18A9B5C" +
		        "5023597BD82D8062D34019563BA1D25C" +
		        "0DC56B7B979D74AA50F29FBF11CC2C93" +
		        "F5DFCA615E609279F6175CEADB00B58C" +
		        "6BEE1E7A2A47C4F0C456F05259A6FA94" +
		        "A634A40DAE1DF593D4FECF688D5FC678" +
		        "BE7EFC6DF3D6835325B83B2C6E69036B" +
		
		        "155F0A27241094B04BFB0BDFAC6C670A" +
		        "65C325D39A069F03659D44CA27D3BE8D" +
		        "F311172B554160181CBE94A2A783320C" +
		        "ED590BC42644702CF371271E496BF20F" +
		        "588B78A1BC01ECBB6559934BDD2FB65D" +
		        "2884318A33D1A42ADF5E33CC5800280B" +
		        "28356497F87135BAB9612A1726042440" +
		        "9AC15FEE996B744C332151235DECB0F5";

    @Test
    public void validateKeysOKTest() {
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("id", id);
	    	params.put("pvt", pvt);
	    	params.put("kpak", kpak);
	    	params.put("ssk", ssk);
	    	params.put("z", Z);
	    	params.put("rsk", rsk);
	    	params.put("parameterSet", "1");
	    	
			JSONObject obj = Connect.mathServerRequest("VALIDATE", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean validated = false;
			if (obj.has("validated")){
				validated = (Boolean)obj.get("validated");
			}
			
			assertTrue("Keys not validated.", validated);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }
    
    @Test
    public void VerifyKeysIncorrectKPAKTest() {
        String badKpak =
                "04" + "50D4670BDE75244F28D2838A0D25558A" +
                        "7A72686D4522D4C8273FB6442AEBFA93" +
                        "DBDD37551AFD263B5DFE617F3960C65A" +
                        "8C298850FF99F20366DCE7D4367217F4";

        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("id", id);
	    	params.put("pvt", pvt);
	    	params.put("kpak", badKpak);
	    	params.put("ssk", ssk);
	    	params.put("z", Z);
	    	params.put("rsk", rsk);
	    	params.put("parameterSet", "1");
	    	
			JSONObject obj = Connect.mathServerRequest("VALIDATE", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean validated = false;
			if (obj.has("validated")){
				validated = (Boolean)obj.get("validated");
			}
			
			assertFalse("Keys passed validation with badKpak", validated);
				
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
        
    }
    
    @Test
    public void VerifyKeysIncorrectIDTest() {
        String badId = "2011-01%00tel:%2B447700900123%00";

        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("id", badId);
	    	params.put("pvt", pvt);
	    	params.put("kpak", kpak);
	    	params.put("ssk", ssk);
	    	params.put("z", Z);
	    	params.put("rsk", rsk);
	    	params.put("parameterSet", "1");
	    	
			JSONObject obj = Connect.mathServerRequest("VALIDATE", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean validated = false;
			if (obj.has("validated")){
				validated = (Boolean)obj.get("validated");
			}
			
			assertFalse("Keys passed validation with badId", validated);
				
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}       
    }
    
    @Test
    public void VerifyKeysIncorrectSSKTest() {
        String badSsk = "23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34499A0D";

        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("id", id);
	    	params.put("pvt", pvt);
	    	params.put("kpak", kpak);
	    	params.put("ssk", badSsk);
	    	params.put("z", Z);
	    	params.put("rsk", rsk);
	    	params.put("parameterSet", "1");
	    	
			JSONObject obj = Connect.mathServerRequest("VALIDATE", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean validated = false;
			if (obj.has("validated")){
				validated = (Boolean)obj.get("validated");
			}
			
			assertFalse("Keys passed validation with badSsk", validated);
				
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}      
    }
    
    @Test
    public void VerifyKeysIncorrectPVTTest() {
        String badPvt = "04" + "758A142779BE89E829E71984CB40EF75" +
						        "1CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
						        "A79D247692F4EDA3A6BDAB77D6AA6474" +
						        "A464AE4934663C5265BA7018BA091F79";

        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("id", id);
	    	params.put("pvt", badPvt);
	    	params.put("kpak", kpak);
	    	params.put("ssk", ssk);
	    	params.put("z", Z);
	    	params.put("rsk", rsk);
	    	params.put("parameterSet", "1");
	    	
			JSONObject obj = Connect.mathServerRequest("VALIDATE", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean validated = false;
			if (obj.has("validated")){
				validated = (Boolean)obj.get("validated");
			}
			
			assertFalse("Keys passed validation with badPvt", validated);			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}      
    }
    
    @Test
    public void VerifyKeysIncorrectZTest() {
        String badZ = "04" + "5958EF1B1679BF099B3A030DF255AA6A" +
					        "23C1D8F143D4D23F753E69BD27A832F3" +
					        "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
					        "10EFFE300367A37B61F701D914AEF097" +
					        "24825FA0707D61A6DFF4FBD7273566CD" +
					        "DE352A0B04B7C16A78309BE640697DE7" +
					        "47613A5FC195E8B9F328852A579DB8F9" +
					        "9B1D0034479EA9C5595F47C4B2F54FF2" +
					
					        "1508D37514DCF7A8E143A6058C09A6BF" +
					        "2C9858CA37C258065AE6BF7532BC8B5B" +
					        "63383866E0753C5AC0E72709F8445F2E" +
					        "6178E065857E0EDA10F68206B63505ED" +
					        "87E534FB2831FF957FB7DC619DAE6130" +
					        "1EEACC2FDA3680EA4999258A833CEA8F" +
					        "C67C6D19487FB449059F26CC8AAB655A" +
					        "B58B7CC796E24E9A394095754F5F8BAF";

        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("id", id);
	    	params.put("pvt", pvt);
	    	params.put("kpak", kpak);
	    	params.put("ssk", ssk);
	    	params.put("z", badZ);
	    	params.put("rsk", rsk);
	    	params.put("parameterSet", "1");
	    	
			JSONObject obj = Connect.mathServerRequest("VALIDATE", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean validated = false;
			if (obj.has("validated")){
				validated = (Boolean)obj.get("validated");
			}
			
			assertFalse("Keys passed validation with badZ", validated);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}      
    }
    
    @Test
    public void VerifyKeysIncorrectRSKTest() {
        String badRsk =  "04" + "93AF67E5007BA6E6A80DA793DA300FA4" +
						        "B52D0A74E25E6E7B2B3D6EE9D18A9B5C" +
						        "5023597BD82D8062D34019563BA1D25C" +
						        "0DC56B7B979D74AA50F29FBF11CC2C93" +
						        "F5DFCA615E609279F6175CEADB00B58C" +
						        "6BEE1E7A2A47C4F0C456F05259A6FA94" +
						        "A634A40DAE1DF593D4FECF688D5FC678" +
						        "BE7EFC6DF3D6835325B83B2C6E69036B" +
						
						        "155F0A27241094B04BFB0BDFAC6C670A" +
						        "65C325D39A069F03659D44CA27D3BE8D" +
						        "F311172B554160181CBE94A2A783320C" +
						        "ED590BC42644702CF371271E496BF20F" +
						        "588B78A1BC01ECBB6559934BDD2FB65D" +
						        "2884318A33D1A42ADF5E33CC5800280B" +
						        "28356497F87135BAB9612A1726042440" +
						        "9AC15FEE996B744C332151235DECB0FA";

        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("id", id);
	    	params.put("pvt", pvt);
	    	params.put("kpak", kpak);
	    	params.put("ssk", ssk);
	    	params.put("z", Z);
	    	params.put("rsk", badRsk);
	    	params.put("parameterSet", "1");
	    	
			JSONObject obj = Connect.mathServerRequest("VALIDATE", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			boolean validated = false;
			if (obj.has("validated")){
				validated = (Boolean)obj.get("validated");
			}
			
			assertFalse("Keys passed validation with badRsk", validated);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}      
    }
    
  
    


}
