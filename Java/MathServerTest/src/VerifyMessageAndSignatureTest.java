import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;


import org.apache.http.client.ClientProtocolException;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.junit.Test;



public class VerifyMessageAndSignatureTest {

	private static final String message = "message%00";
    private static final String signature =
		    	"269D4C8FDEB66A74E4EF8C0D5DCC597D" +
		        "DFE6029C2AFFC4936008CD2CC1045D81" +
		        "E09B528D0EF8D6DF1AA3ECBF80110CFC" +
		        "EC9FC68252CEBB679F4134846940CCFD" +
		        "04" +
		
		        "758A142779BE89E829E71984CB40EF75" +
		        "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
		        "A79D247692F4EDA3A6BDAB77D6AA6474" +
		        "A464AE4934663C5265BA7018BA091F79";

    private final static String id = "2011-02%00tel:%2B447700900123%00";

    private final static String kpak =
    	 "04" + "50D4670BDE75244F28D2838A0D25558A" +
		         "7A72686D4522D4C8273FB6442AEBFA93" +
		         "DBDD37551AFD263B5DFD617F3960C65A" +
		         "8C298850FF99F20366DCE7D4367217F4";

    @Test
    public void verifyOKTest() {
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("message", message);
	    	params.put("signerId", id);
	    	params.put("kpak", kpak);
	    	params.put("signature", signature);
	    	params.put("messageFormat", "ascii");
	    	
			JSONObject obj = Connect.mathServerRequest("VERIFY", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean verified = false;
			if (obj.has("verified")){
				verified = (Boolean)obj.get("verified");
			}
			else{
				fail("Missing verification.");
			}
			
			assertTrue("Message/Signature failed to verify.", verified);			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}    
    }

    @Test
    public void verifyIncorrectSignatureTest() {
    	String badSignature =
                "269D4C8FDEB66A74E4EF8C0D5DCC597D" +
                "DFE6029C2AFFC4936008CD2CC1045D81" +
                "E09B528D0EF8D6DF1AA3ECBF80110CFC" +
                "EC9FC68252CEBB679F4134846940CCFD" +
                "04" +

                "758A142779BE89E829E71984CB40EF75" +
                "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                "A79D247692F4EDA3A6BDAB77D6AA6474" +
                "A464AE4934663C5265BA7018BA091F12";
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("message", message);
	    	params.put("signerId", id);
	    	params.put("kpak", kpak);
	    	params.put("signature", badSignature);
	    	params.put("messageFormat", "ascii");
	    	
			JSONObject obj = Connect.mathServerRequest("VERIFY", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean verified = false;
			if (obj.has("verified")){
				verified = (Boolean)obj.get("verified");
			}
			else{
				fail("Missing verification.");
			}
			
			assertFalse("Message/Signature verified with badSignature.", verified);			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}	
    }

    @Test
    public void verifyIncorrectMessageTest() {
        String badMessage ="badmessag%00";
		try {
			Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("message", badMessage);
	    	params.put("signerId", id);
	    	params.put("kpak", kpak);
	    	params.put("signature", signature);
	    	params.put("messageFormat", "ascii");
	    	
			JSONObject obj = Connect.mathServerRequest("VERIFY", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean verified = false;
			if (obj.has("verified")){
				verified = (Boolean)obj.get("verified");
			}
			else{
				fail("Missing verification.");
			}
			
			assertFalse("Message/Signature verified with badMessage.", verified);			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}	
    }

    @Test
    public void verifyIncorrectKPAKTest() {
    	String badKPAK = "04" + 
    			"50D4670BDE75244F28D2838A0D25558A" +
                "7A72686D4522D4C8273FB6442AEBFA93" +
                "DBDD37551AFD263B5DFD617F3960C65A" +
                "8C298850FF99F20366DCE7D436721701";
		try {
			Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("message", message);
	    	params.put("signerId", id);
	    	params.put("kpak", badKPAK);
	    	params.put("signature", signature);
	    	params.put("messageFormat", "ascii");
	    	
			JSONObject obj = Connect.mathServerRequest("VERIFY", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean verified = false;
			if (obj.has("verified")){
				verified = (Boolean)obj.get("verified");
			}
			else{
				fail("Missing verification.");
			}
			
			assertFalse("Message/Signature verified with badKPAK.", verified);			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}	
    }

    @Test
    public void verifyIncorrectIDTest() {
    	String badId = "2011-02%00tel:%2B447700900125%00";
		try {
			Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("message", message);
	    	params.put("signerId", badId);
	    	params.put("kpak", kpak);
	    	params.put("signature", signature);
	    	params.put("messageFormat", "ascii");
	    	
			JSONObject obj = Connect.mathServerRequest("VERIFY", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			boolean verified = false;
			if (obj.has("verified")){
				verified = (Boolean)obj.get("verified");
			}
			else{
				fail("Missing verification.");
			}
			
			assertFalse("Message/Signature verified with badId.", verified);			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

}
