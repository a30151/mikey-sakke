import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import org.apache.http.client.ClientProtocolException;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.junit.Test;

public class GenerateSecretTest {
	
    private final static String Z =
    	"04" + "5958EF1B1679BF099B3A030DF255AA6A" +
		        "23C1D8F143D4D23F753E69BD27A832F3" +
		        "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
		        "10EFFE300367A37B61F701D914AEF097" +
		        "24825FA0707D61A6DFF4FBD7273566CD" +
		        "DE352A0B04B7C16A78309BE640697DE7" +
		        "47613A5FC195E8B9F328852A579DB8F9" +
		        "9B1D0034479EA9C5595F47C4B2F54FF2" +
		
		        "1508D37514DCF7A8E143A6058C09A6BF" +
		        "2C9858CA37C258065AE6BF7532BC8B5B" +
		        "63383866E0753C5AC0E72709F8445F2E" +
		        "6178E065857E0EDA10F68206B63505ED" +
		        "87E534FB2831FF957FB7DC619DAE6130" +
		        "1EEACC2FDA3680EA4999258A833CEA8F" +
		        "C67C6D19487FB449059F26CC8AAB655A" +
		        "B58B7CC796E24E9A394095754F5F8BAE";
    
    private final static String id = "2011-02%00tel:%2B447700900123%00";

    private final static String expectedSED =
    	"04" + "44E8AD44AB8592A6A5A3DDCA5CF896C7" +
		        "18043606A01D650DEF37A01F37C228C3" +
		        "32FC317354E2C274D4DAF8AD001054C7" +
		        "6CE57971C6F4486D5723043261C506EB" +
		        "F5BE438F53DE04F067C776E0DD3B71A6" +
		        "290133283725A532F21AF145126DC1D7" +
		        "77ECC27BE50835BD28098B8A73D9F801" +
		        "D893793A41FF5C49B87E79F2BE4D56CE" +
		
		        "557E134AD85BB1D4B9CE4F8BE4B08A12" +
		        "BABF55B1D6F1D7A638019EA28E15AB1C" +
		        "9F76375FDD1210D4F4351B9A009486B7" +
		        "F3ED46C965DED2D80DADE4F38C6721D5" +
		        "2C3AD103A10EBD2959248B4EF006836B" +
		        "F097448E6107C9EDEE9FB704823DF199" +
		        "F832C905AE45F8A247A072D8EF729EAB" +
		        "C5E27574B07739B34BE74A532F747B86" +
		        
		        "89E0BC661AA1E91638E6ACC84E496507";

    private final static String expectedSSV = "123456789abcdef0123456789abcdef0";

    @Test
    public void GenerateSharedSecretAndSEDOKTest() {
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("targetId", id);
	    	params.put("parameterSet", "1");
	    	params.put("z", Z);
	    	params.put("test", "true");
	    	
			JSONObject obj = Connect.mathServerRequest("Generate", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String ssv = "";
			String sed = "";
			if (obj.has("ssv")){
				ssv = (String)obj.get("ssv");
			}
			else{
				fail("Missing SSV.");
			}
			if (obj.has("sed")){
				sed = (String)obj.getString("sed");
			}
			else {
				fail("Missing SED.");
			}
			
			assertTrue("SSV incorrect: " + ssv, ssv.equals(expectedSSV));
			assertTrue("SED incorrect: " + sed, sed.equals(expectedSED.toLowerCase()));
			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

    @Test
    public void GenerateSharedSecretAndSEDIncorrectIDTest() {
    	String badId = "2011-02%00tel:%2B447700900125%00";
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("targetId", badId);
	    	params.put("parameterSet", "1");
	    	params.put("z", Z);
	    	params.put("test", "true");
	    	
			JSONObject obj = Connect.mathServerRequest("Generate", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			String ssv = "";
			String sed = "";
			if (obj.has("ssv")){
				ssv = (String)obj.get("ssv");
			}
			else{
				fail("Missing SSV.");
			}
			if (obj.has("sed")){
				sed = (String)obj.getString("sed");
			}
			else {
				fail("Missing SED.");
			}
			
			assertTrue("SSV incorrect: " + ssv, ssv.equals(expectedSSV));
			assertFalse("SED correct with badId.", sed.equals(expectedSED.toLowerCase()));
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

    @Test
    public void GenerateSharedSecretAndSEDIncorrectZTest() {
    	String badZ = "04" + "5958EF1B1679BF099B3A030DF255AA6A" +
					        "23C1D8F143D4D23F753E69BD27A832F3" +
					        "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
					        "10EFFE300367A37B61F701D914AEF097" +
					        "24825FA0707D61A6DFF4FBD7273566CD" +
					        "DE352A0B04B7C16A78309BE640697DE7" +
					        "47613A5FC195E8B9F328852A579DB8F9" +
					        "9B1D0034479EA9C5595F47C4B2F54FF2" +
					
					        "1508D37514DCF7A8E143A6058C09A6BF" +
					        "2C9858CA37C258065AE6BF7532BC8B5B" +
					        "63383866E0753C5AC0E72709F8445F2E" +
					        "6178E065857E0EDA10F6AAA6B63505ED" +
					        "87E534FB2831FF957FB7DC619DAE6130" +
					        "1EEACC2FDA3680EA4999258A833CEA8F" +
					        "C67C6D19487FB449059F26CC8AAB655A" +
					        "B58B7CC796E24E9A394095754F5F8BAF";
    	
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("targetId", id);
	    	params.put("parameterSet", "1");
	    	params.put("z", badZ);
	    	params.put("test", "true");
	    	
			JSONObject obj = Connect.mathServerRequest("Generate", params);
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String ssv = "";
			String sed = "";
			if (obj.has("ssv")){
				ssv = (String)obj.get("ssv");
			}
			else{
				fail("Missing SSV.");
			}
			if (obj.has("sed")){
				sed = (String)obj.getString("sed");
			}
			else {
				fail("Missing SED.");
			}
			
			assertTrue("SSV incorrect: " + ssv, ssv.equals(expectedSSV));
			assertFalse("SED correct with badZ.", sed.equals(expectedSED.toLowerCase()));
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }
}
