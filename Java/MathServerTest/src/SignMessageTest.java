import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import org.apache.http.client.ClientProtocolException;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.junit.Test;

public class SignMessageTest {
	private static final String HS =
			"490F3FEBBC1C902F6289723D7F8CBF79" + 
			"DB88930849D19F38F0295B5C276C14D1";

    private static final String message = "message%00";

    private static final String pvt = 
    	"04" + "758A142779BE89E829E71984CB40EF75" +
		        "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
		        "A79D247692F4EDA3A6BDAB77D6AA6474" +
		        "A464AE4934663C5265BA7018BA091F79";

    private static final String ssk =
            "23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34489A0D";

    private static final String expectedSignature =
            "269D4C8FDEB66A74E4EF8C0D5DCC597D" +
            "DFE6029C2AFFC4936008CD2CC1045D81" +
            "E09B528D0EF8D6DF1AA3ECBF80110CFC" +
            "EC9FC68252CEBB679F4134846940CCFD" +
            "04" +

            "758A142779BE89E829E71984CB40EF75" +
            "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
            "A79D247692F4EDA3A6BDAB77D6AA6474" +
            "A464AE4934663C5265BA7018BA091F79";

    @Test
    public void signOKTest() {
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("message", message);
	    	params.put("pvt", pvt);
	    	params.put("ssk", ssk);
	    	params.put("hs", HS);
	    	params.put("test", "true");
	    	params.put("messageFormat", "ascii");
	    	
			JSONObject obj = Connect.mathServerRequest("SIGN", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String signature = "";
			if (obj.has("signature")){
				signature = (String)obj.get("signature");
			}
			else{
				fail("Missing signature.");
			}
			
			assertTrue("Signature incorrect: " + signature, 
					signature.equals(expectedSignature.toLowerCase()));			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

    @Test
    public void signIncorrectSSVTest() {
       String badSSK = "23F374AE1F4033F3E9DBDDAAEF20F4CF0B86BBD5A138A5AE9E7E006B34489A05";
       try {
    	   Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("message", message);
	    	params.put("pvt", pvt);
	    	params.put("ssk", badSSK);
	    	params.put("hs", HS);
	    	params.put("test", "true");
	    	params.put("messageFormat", "ascii");
	    	
			JSONObject obj = Connect.mathServerRequest("SIGN", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String signature = "";
			if (obj.has("signature")){
				signature = (String)obj.get("signature");
			}
			else{
				fail("Missing signature.");
			}
			
			assertFalse("Signature correct with badSSK.", 
					signature.equals(expectedSignature.toLowerCase()));			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

    @Test
    public void signIncorrectPVTTest() {
        String badPVT =
                "04" + "758A142779BE89E829E71984CB40EF75" +
                        "8CC4AD775FC5B9A3E1C8ED52F6FA36D9" +
                        "A79D247692F4EDA3A6BDAB77D6AA6474" +
                        "A464AE4934663C5265BA7018BA091F7";
        try {
     	   Hashtable<String, String> params = new Hashtable<String, String>();
 	    	params.put("message", message);
 	    	params.put("pvt", badPVT);
 	    	params.put("ssk", ssk);
 	    	params.put("hs", HS);
 	    	params.put("test", "true");
 	    	params.put("messageFormat", "ascii");
 	    	
 			JSONObject obj = Connect.mathServerRequest("SIGN", params);
 			
 			if (obj.has("error")){
 				fail("Error returned from Math Server: " + obj.get("error"));
 			}
 			
 			String signature = "";
 			if (obj.has("signature")){
 				signature = (String)obj.get("signature");
 			}
 			else{
 				fail("Missing signature.");
 			}
 			
 			assertFalse("Signature correct with badPVT.", 
 					signature.equals(expectedSignature.toLowerCase()));			
 			
 		} catch (ClientProtocolException e) {
 			e.printStackTrace();
 			fail(e.getMessage());
 		} catch (URISyntaxException e) {
 			e.printStackTrace();
 			fail(e.getMessage());
 		} catch (IOException e) {
 			e.printStackTrace();
 			fail(e.getMessage());
 		} catch (JSONException e) {
 			e.printStackTrace();
 			fail(e.getMessage());
 		}       
    }
    
    @Test
    public void signIncorrectHSTest() {
        String badHS = 
                "490F3FEBBC1C902F6289723D7F8CBF79"
                        + "DB88930849D19F38F0295B5C276C14D8";
        try {
      	   Hashtable<String, String> params = new Hashtable<String, String>();
  	    	params.put("message", message);
  	    	params.put("pvt", pvt);
  	    	params.put("ssk", ssk);
  	    	params.put("hs", badHS);
  	    	params.put("test", "true");
  	    	params.put("messageFormat", "ascii");
  	    	
  			JSONObject obj = Connect.mathServerRequest("SIGN", params);
  			
  			if (obj.has("error")){
  				fail("Error returned from Math Server: " + obj.get("error"));
  			}
  			
  			String signature = "";
  			if (obj.has("signature")){
  				signature = (String)obj.get("signature");
  			}
  			else{
  				fail("Missing signature.");
  			}
  			
  			assertFalse("Signature correct with badHS.", 
  					signature.equals(expectedSignature.toLowerCase()));			
  			
  		} catch (ClientProtocolException e) {
  			e.printStackTrace();
  			fail(e.getMessage());
  		} catch (URISyntaxException e) {
  			e.printStackTrace();
  			fail(e.getMessage());
  		} catch (IOException e) {
  			e.printStackTrace();
  			fail(e.getMessage());
  		} catch (JSONException e) {
  			e.printStackTrace();
  			fail(e.getMessage());
  		}       
    }
}
