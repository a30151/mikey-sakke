import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;


import org.apache.http.client.ClientProtocolException;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.junit.Test;


public class ExtractSecretTest {

   private final static String rsk =
            "04" + "93AF67E5007BA6E6A80DA793DA300FA4" +
                    "B52D0A74E25E6E7B2B3D6EE9D18A9B5C" +
                    "5023597BD82D8062D34019563BA1D25C" +
                    "0DC56B7B979D74AA50F29FBF11CC2C93" +
                    "F5DFCA615E609279F6175CEADB00B58C" +
                    "6BEE1E7A2A47C4F0C456F05259A6FA94" +
                    "A634A40DAE1DF593D4FECF688D5FC678" +
                    "BE7EFC6DF3D6835325B83B2C6E69036B" +

                    "155F0A27241094B04BFB0BDFAC6C670A" +
                    "65C325D39A069F03659D44CA27D3BE8D" +
                    "F311172B554160181CBE94A2A783320C" +
                    "ED590BC42644702CF371271E496BF20F" +
                    "588B78A1BC01ECBB6559934BDD2FB65D" +
                    "2884318A33D1A42ADF5E33CC5800280B" +
                    "28356497F87135BAB9612A1726042440" +
                    "9AC15FEE996B744C332151235DECB0F5";

    private final static String Z =
            "04" + "5958EF1B1679BF099B3A030DF255AA6A" +
                    "23C1D8F143D4D23F753E69BD27A832F3" +
                    "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
                    "10EFFE300367A37B61F701D914AEF097" +
                    "24825FA0707D61A6DFF4FBD7273566CD" +
                    "DE352A0B04B7C16A78309BE640697DE7" +
                    "47613A5FC195E8B9F328852A579DB8F9" +
                    "9B1D0034479EA9C5595F47C4B2F54FF2" +

                    "1508D37514DCF7A8E143A6058C09A6BF" +
                    "2C9858CA37C258065AE6BF7532BC8B5B" +
                    "63383866E0753C5AC0E72709F8445F2E" +
                    "6178E065857E0EDA10F68206B63505ED" +
                    "87E534FB2831FF957FB7DC619DAE6130" +
                    "1EEACC2FDA3680EA4999258A833CEA8F" +
                    "C67C6D19487FB449059F26CC8AAB655A" +
                    "B58B7CC796E24E9A394095754F5F8BAE";
    
    private final static String id = "2011-02%00tel:%2B447700900123%00";

    private final static String SED =
            "04" + "44E8AD44AB8592A6A5A3DDCA5CF896C7" +
                    "18043606A01D650DEF37A01F37C228C3" +
                    "32FC317354E2C274D4DAF8AD001054C7" +
                    "6CE57971C6F4486D5723043261C506EB" +
                    "F5BE438F53DE04F067C776E0DD3B71A6" +
                    "290133283725A532F21AF145126DC1D7" +
                    "77ECC27BE50835BD28098B8A73D9F801" +
                    "D893793A41FF5C49B87E79F2BE4D56CE" +

                    "557E134AD85BB1D4B9CE4F8BE4B08A12" +
                    "BABF55B1D6F1D7A638019EA28E15AB1C" +
                    "9F76375FDD1210D4F4351B9A009486B7" +
                    "F3ED46C965DED2D80DADE4F38C6721D5" +
                    "2C3AD103A10EBD2959248B4EF006836B" +
                    "F097448E6107C9EDEE9FB704823DF199" +
                    "F832C905AE45F8A247A072D8EF729EAB" +
                    "C5E27574B07739B34BE74A532F747B86" +

                    "89E0BC661AA1E91638E6ACC84E496507";

    private final static String expectedSSV = "123456789abcdef0123456789abcdef0";

    @Test
    public void extractSharedSecretOKTest() {
    	try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("sed", SED);
	    	params.put("id", id);
	    	params.put("parameterSet", "1");
	    	params.put("rsk", rsk);
	    	params.put("z", Z);
	    	
			JSONObject obj = Connect.mathServerRequest("EXTRACT", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String ssv = "";
			if (obj.has("ssv")){
				ssv = (String)obj.get("ssv");
			}
			else{
				fail("Missing SSV.");
			}
			
			assertTrue("SSV incorrect: " + ssv, ssv.equals(expectedSSV));			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

    @Test
    public void extractSharedSecretIncorrectSEDTest() {
        String badSED = "04" +
                "310c9f91e3433aed68429a062ad283d86d4" +
                "a0a8579eaab57b68ef9f1ae0c4949b4457c" +
                "ce67eff4e6a36c6e9041a8cb922af84984e" +
                "b100f96c89a9a694efcf34bfd09635e9e3c" +
                "1d27c4a5eafd72c2a60e702563ebf80cba3" +
                "109d18879fd026d34840190109f1df4d95d" +
                "75fbc241ca50fbd2588634178b727f8e621" +
                "a41a643dd354ceef51a8f5937ac7a2395ae" +
                "c0721038bafd59b204f0f226891cafff346" +
                "8ba22d14c4482a2926f4cbb4958a15324c8" +
                "480bdaaa02ed57ca174267e0762376213f2" +
                "a9485985204f05a440dd28445e202a5e8d4" +
                "97499b2d021cae17627f7ecb31a696c0893" +
                "58b6250b128c2abaddd740266d190ee7d43" +
                "a37ee6702a420a729abc953b9ee35632ef0" +
                "92a0931bd9210fe8bAB";
        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("sed", badSED);
	    	params.put("id", id);
	    	params.put("parameterSet", "1");
	    	params.put("rsk", rsk);
	    	params.put("z", Z);
	    	
			JSONObject obj = Connect.mathServerRequest("EXTRACT", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String ssv = "";
			if (obj.has("ssv")){
				ssv = (String)obj.get("ssv");
			}
			else{
				fail("Missing SSV.");
			}
			
			assertEquals("SSV not null with badSED: " + ssv, "null", ssv);			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

     @Test
    public void extractSharedSecretBadIDTest() {
        String badId = "2011-02%00tel:%2B447700900125%00";
        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("sed", SED);
	    	params.put("id", badId);
	    	params.put("parameterSet", "1");
	    	params.put("rsk", rsk);
	    	params.put("z", Z);
	    	
			JSONObject obj = Connect.mathServerRequest("EXTRACT", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String ssv = "";
			if (obj.has("ssv")){
				ssv = (String)obj.get("ssv");
			}
			else{
				fail("Missing SSV.");
			}
			
			assertEquals("SSV not null with badId: " + ssv, "null", ssv);				
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }

    @Test
    public void extractSharedSecretBadZTest() {
        String badZ = "04" + "5958EF1B1679BF099B3A030DF255AA6A" +
                "23C1D8F143D4D23F753E69BD27A832F3" +
                "8CB4AD53DDEF4260B0FE8BB45C4C1FF5" +
                "10EFFE300367A37B61F701D914AEF097" +
                "24825FA0707D61A6DFF4FBD7273566CD" +
                "DE352A0B04B7C16A78309BE640697DE7" +
                "47613A5FC195E8B9F328852A579DB8F9" +
                "9B1D0034479EA9C5595F47C4B2F54FF2" +

                "1508D37514DCF7A8E143A6058C09A6BF" +
                "2C9858CA37C258065AE6BF7532BC8B5B" +
                "63383866E0753C5AC0E72709F8445F2E" +
                "6178E065857E0EDA10F68206B63505ED" +
                "87E534FB2831FF957FB7DC619DAE6130" +
                "1EEACC2FDA3680EA4999258A833CEA8F" +
                "C67C6D19487FB449059F26CC8AAB655A" +
                "B58B7CC796E24E9A394095754F5F8BAF";
        try {
    		Hashtable<String, String> params = new Hashtable<String, String>();
	    	params.put("sed", SED);
	    	params.put("id", id);
	    	params.put("parameterSet", "1");
	    	params.put("rsk", rsk);
	    	params.put("z", badZ);
	    	
			JSONObject obj = Connect.mathServerRequest("EXTRACT", params);
			
			if (obj.has("error")){
				fail("Error returned from Math Server: " + obj.get("error"));
			}
			
			String ssv = "";
			if (obj.has("ssv")){
				ssv = (String)obj.get("ssv");
			}
			else{
				fail("Missing SSV.");
			}
			
			assertEquals("SSV not null with badZ: " + ssv, "null", ssv);			
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
       
    }
}
