import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.me.JSONException;
import org.json.me.JSONObject;


public class Connect {
	
	private static final String IP = "172.17.8.242";
	private static final String HTTPS_PORT = "7072";
	private static final String HTTP_PORT = "7071";
	
	public static JSONObject mathServerRequest(String requestType,
			Hashtable<String, String> parameters) throws ClientProtocolException, URISyntaxException, IOException, JSONException{
		return mathServerRequestSecure(requestType, parameters);
		//return mathServerRequestUnsecure(requestType, parameters);
	}

	public static JSONObject mathServerRequestSecure(String requestType,
			Hashtable<String, String> parameters) 
				throws URISyntaxException, ClientProtocolException, IOException, JSONException{	
		
			
		HttpClient client = getNewHttpClient();
		HttpGet request = new HttpGet();
		
		Enumeration<String> params = parameters.keys();
		StringBuffer uri = new StringBuffer();
		uri.append("https://" + IP + ":" + HTTPS_PORT + "/" + requestType + "?");
		String first = (String) params.nextElement();
		uri.append(first + "=" + parameters.get(first));
		
		while (params.hasMoreElements())
		{
			String p = (String) params.nextElement();
			uri.append("&" + p + "=" + parameters.get(p));	
		}
		
		request.setURI(new URI(uri.toString()));
		
	    HttpResponse exec_response = client.execute(request);
	    InputStream content = exec_response.getEntity().getContent();
	
	    BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
	    String s = buffer.readLine();
	    
	    String response = "";
	    while (s != null) {
	    	response += s;
	    	s = buffer.readLine();
	    }			
		// Decode response from KMS
	    JSONObject obj = new JSONObject(response);
	    
	    return obj;
	}
	
	public static JSONObject mathServerRequestUnsecure(String requestType,
			Hashtable<String, String> parameters) 
				throws URISyntaxException, ClientProtocolException, IOException, JSONException{
			
		
		
	HttpClient client = new DefaultHttpClient();;
	HttpGet request = new HttpGet();
	
	Enumeration<String> params = parameters.keys();
	StringBuffer uri = new StringBuffer();
	uri.append("http://" + IP + ":" + HTTP_PORT + "/" + requestType + "?");
	String first = (String) params.nextElement();
	uri.append(first + "=" + parameters.get(first));
	
	
	while (params.hasMoreElements())
	{
		String p = (String) params.nextElement();
		uri.append("&" + p + "=" + parameters.get(p));	
	}
	
	request.setURI(new URI(uri.toString()));
	
    HttpResponse exec_response = client.execute(request);
    InputStream content = exec_response.getEntity().getContent();

    BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
    String s = buffer.readLine();
    
    String response = "";
    while (s != null) {
    	response += s;
    	s = buffer.readLine();
    }			
	// Decode response from KMS
    JSONObject obj = new JSONObject(response);
    
    return obj;
}
	
    /**
     * Code required to allow an HTTPS connection without verifying the certificate
     * */    
    private static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
    
    
}
