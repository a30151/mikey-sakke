package mikeySakkeApp;

import msTest.ClientTest;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;

/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class MyScreen extends MainScreen {
    /**
     * Creates a new MyScreen object
     */
    public MyScreen() {
        // Set the displayed title of the screen
        setTitle("MyTitle");

        add(new LabelField(ClientTest.clientTest()));
        // add(new LabelField(Tests.arrayTest()));
        // add(new LabelField(MikeySakkeCryptoTest.test()));
        // add(new LabelField(Tests.rfc6508HashToIntegerTest()));
        // add(new LabelField(Tests.computePairingTest()));
        // add(new LabelField(Tests.PF_pArithmeticTest()));
        // add(new LabelField(Tests.clientTest()));

    }
}
