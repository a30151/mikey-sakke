package mikeysakke.mikey;

import mikeysakke.mikey.tables.NextPayload;

import org.bouncycastle.math.ec.Arrays;

import com.google.zxing.common.BitArray;

/**
 * Class representing a MIKEY Payload of any type.
 */
public abstract class Payload {

	private static int hashCode(byte[] array) {
		int prime = 31;
		if (array == null)
			return 0;
		int result = 1;
		for (int index = 0; index < array.length; index++) {
			result = prime * result + array[index];
		}
		return result;
	}

	protected int startByte = 0;
    protected int endByte = 0;
    
    protected byte[] originalBytes;
    
    protected byte payloadType;
    
    /**
     * next payload (8 bits): identifies the payload that is added after this
     * payload.
     */
    protected byte nextPayload = NextPayload.LAST_PAYLOAD;
    
    public byte[] getBytesAfterPayload()
    {
    	return Arrays.copyOfRange(originalBytes, getEndByte(), originalBytes.length);
    }
    
    public void setOriginalBytes(byte[] original_bytes)
    {
    	originalBytes = original_bytes;
    }
    
    public boolean hasNextPayload()
    {
    	return nextPayload != NextPayload.LAST_PAYLOAD;
    }
    
    public byte getNextPayload()
    {
    	return nextPayload;
    }
    
    /**
     * Encode the Payload object into a BitArray
     */
    public abstract BitArray getEncoded();

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getEndByte();
		result = prime * result + nextPayload;
		result = prime * result + Payload.hashCode(originalBytes);
		result = prime * result + startByte;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payload other = (Payload) obj;
		if (nextPayload != other.nextPayload)
			return false;
		return true;
	}
	
	public byte getPayloadType(){
		return payloadType;
	}
	
	public abstract String toString();

	public int getEndByte() {
		return endByte;
	}

	public void setEndByte(int endByte) {
		this.endByte = endByte;
	}
}
