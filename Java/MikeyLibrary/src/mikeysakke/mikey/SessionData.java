package mikeysakke.mikey;

import java.util.Vector;

import mikeysakke.mikey.utils.Utils;

import org.bouncycastle.math.ec.Arrays;

import com.google.zxing.common.BitArray;

/**
 * Represents a Session Data part of the CS ID map info for
 * the GENERIC-ID map type.
 * RFC 6043 section 6.1.1
 *
 */
public class SessionData {

	int ssrc;
	int roc; //optional
	short seq; //optional
	boolean includeOptional;
	
	public SessionData(int ssrc){
		includeOptional = false;
		this.ssrc = ssrc;
	}
	
	public SessionData(int ssrc, int roc, short seq){
		includeOptional = true;
		this.ssrc = ssrc;
		this.roc = seq;		
	}
	
	public BitArray getEncoded(){
		BitArray bits = new BitArray();
		bits.appendBits(ssrc, 32);
		if (includeOptional){
			bits.appendBits(roc, 32);
			bits.appendBits(seq, 16);
		}
		
		return bits;
	}
	

	public static SessionData[] decodeSessionData(boolean s, byte[] encoded, short totalLen){
		int ssrc;
		int roc;
		short seq;
		
		Vector data = new Vector();
		for (int i = 0; i < totalLen; i++){
			SessionData sessionData;
			byte[] ssrcBytes = Arrays.copyOfRange(encoded, i, i+4);
			
			ssrc = Utils.convertByteArrayToInt(ssrcBytes);
			if (s){
				byte[] rocBytes = Arrays.copyOfRange(encoded, i+4, i+8);
				byte[] seqBytes = Arrays.copyOfRange(encoded, i+8, i+10);
				roc = Utils.convertByteArrayToInt(rocBytes);
				seq = Utils.convertByteArrayToShort(seqBytes);
				i+=6;
				sessionData = new SessionData(ssrc, roc, seq);
			}
			else {
				sessionData = new SessionData(ssrc);
			}
			data.addElement(sessionData);	
			i+=3; // i++ after every loop, so only +3 here	
		}
		
		SessionData[] result = new SessionData[data.size()];
		for (int i = 0; i < result.length; i++){
			result[i] = (SessionData)data.elementAt(i);
		}

		return result;
	}
	
	public boolean equals(SessionData other){
		if (ssrc == other.getSsrc()
				&& roc == other.getRoc()
				&& seq == other.getSeq()) return true;
		else return false;
	}

	public int getSsrc() {
		return ssrc;
	}

	public int getRoc() {
		return roc;
	}

	public short getSeq() {
		return seq;
	}

	public boolean includeOptional() {
		return includeOptional;
	}
}
