package mikeysakke.mikey;

import org.bouncycastle.math.ec.Arrays;

import mikeysakke.mikey.tables.NextPayload;
import mikeysakke.mikey.tables.ProtType;
import mikeysakke.mikey.utils.Utils;

import com.google.zxing.common.BitArray;
/**
 * Describes a Security Policy Payload for a MIKEY-SAKKE I_MESSAGE. 
 * RFC 3830 section 6.10.
 * 
 * The Security Policy payload defines a set of policies that apply to a
 * specific security protocol.
 */
public class PayloadSP extends Payload{

	private byte policyNo;
	private byte protType;
	private short policyParamLen;
	private PolicyParam[] policyParams;
	
	public static final byte SP_DEFAULT_NEXT_PAYLOAD = NextPayload.SAKKE;
	
	
	/**
	 * Constructs a PayloadSP object with the given parameters
	 * @param nextPayload
	 * @param policyNo
	 * @param protType
	 * @param policyParamLen
	 * @param policyParam
	 */
	public PayloadSP(final byte nextPayload, 
			final byte policyNo,
			final byte protType,
			final short policyParamLen, 
			final PolicyParam[] policyParam){
		
		if (!checkLengthsOk(policyParamLen, policyParam)){
			throw new IllegalArgumentException("Given length does not match actual length.");
		}
		this.payloadType = NextPayload.SP;
		this.nextPayload = nextPayload;
		this.policyNo = policyNo;
		this.protType = protType;
		this.policyParamLen = policyParamLen;
		this.policyParams = policyParam;
	}

	
	/**
	 * Constructs a PayloadSP with default values
	 */
	public PayloadSP(short policyParamLen, PolicyParam[] policyParam){
		this(SP_DEFAULT_NEXT_PAYLOAD,
				(byte)1,
				ProtType.SRTP,
				policyParamLen,
				policyParam
				);
	}
	
		/**
	 * Checks if the policyParamLen matches the actual length of the
	 * PolicyParam array
	 * @param len
	 * @param params
	 * @return
	 */
	private boolean checkLengthsOk(short len, PolicyParam[] params) {
		// len is the total length of all the policy parameters.
		int lenCount = 0;
		
		//Loop through the params, adding to lenCount for every byte
		for (int i = 0; i < params.length; i++){
			// Add 2 for length and type variables, plus the length of the value
			lenCount += (2 + params[i].getLength());
		}
		
		if (lenCount != len){
			return false;
		}
		else return true;

	}
	
	public BitArray getEncoded() {
		BitArray bits = new BitArray();
		bits.appendBits(nextPayload, 8);
		bits.appendBits(policyNo, 8);
		bits.appendBits(protType, 8);
		bits.appendBits(policyParamLen, 16);
		for (int i = 0; i < policyParams.length; i++){
			bits.appendBitArray(policyParams[i].getEncoded());
		}
		return bits;
	}
	
	/**
	 * Decodes the given byte array into a PayloadSP
	 * @param encoded
	 * @return
	 */
	public static PayloadSP decodeSP(byte[] encoded){
		byte nextPayload = encoded[0];
		byte policyNo = encoded[1];
        byte protType = encoded[2];
        byte[] policyParamLenBytes = Arrays.copyOfRange(encoded, 3, 5);
        short policyParamLen = Utils.convertByteArrayToShort(policyParamLenBytes);
        
        PolicyParam[] policyParams = PolicyParam.decodePolicyParams(
        		Arrays.copyOfRange(encoded, 5, 5 + policyParamLen), policyParamLen);

        PayloadSP result = new PayloadSP(nextPayload, 
        		policyNo, 
        		protType, 
        		policyParamLen, 
        		policyParams);
        
        result.setEndByte(5 + policyParamLen);
        result.originalBytes = encoded;
        return result;
	}

	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("Security Policy Payload.\n");
		str.append("\tNext payload:" + nextPayload + "\n");
		str.append("\tPolicy No: " + policyNo + "\n");
		str.append("\tProt type: " + protType + "\n");
		str.append("\tPolicy param length: " +  policyParamLen + "\n");
		str.append("\tPolicy params:\n");
		for (int i = 0; i < policyParams.length; i++){
			str.append("\t" + policyParams[i].toString());
		}
		return str.toString();
	}
	

}
