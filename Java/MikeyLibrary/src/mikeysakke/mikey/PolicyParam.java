package mikeysakke.mikey;

import java.util.Vector;

import org.bouncycastle.math.ec.Arrays;

import mikeysakke.mikey.utils.Utils;

import com.google.zxing.common.BitArray;

/**
 * Represents a Policy param part of the Security Protocol Payload.
 * RFC 3830 section 6.10
 *
 */
public class PolicyParam {
	
	/**
	 * The type of the policy parameter
	 */
	private byte type;
	
	/**
	 * The length of the policy parameter's value in bytes
	 */
	private byte length;
	
	/**
	 * The value of the policy parameter
	 */
	private byte[] value;
	
	public PolicyParam(byte type, byte length, byte[] value){
		if (value.length != length){
			throw new IllegalArgumentException("Given length does not match actual length.");
		}
		this.type = type;
		this.length = length;
		this.value = Arrays.copyOf(value, length);
	}
	
	public BitArray getEncoded() {
		BitArray bits = new BitArray();
		bits.appendBits(type, 8);
		bits.appendBits(length, 8);
		bits.appendBitArray(Utils.getBitsFromBytes(value));
		return bits;
	}
	
	/**
	 * Decodes the given byte array into a PolicyParam array
	 * @param encoded
	 * @param totalLen - the total length of the PolicyParam's in bytes
	 * @return
	 */
	public static PolicyParam[] decodePolicyParams(byte[] encoded, short totalLen){
		byte type;
		byte length;
		byte[] value;
		Vector params = new Vector();
		for (int i = 0; i < totalLen; i++){
			type = encoded[i++];
			length = encoded[i++];
			value = Arrays.copyOfRange(encoded, i, i+length);
			i = i + length - 1;
			PolicyParam policyParam = new PolicyParam(type, length, value);
			params.addElement(policyParam);
		}
		
		PolicyParam[] result = new PolicyParam[params.size()];
		for (int i = 0; i < result.length; i++){
			result[i] = (PolicyParam)params.elementAt(i);
		}

		return result;
	}

	public byte getLength() {
		return length;
	}
	
	public byte getType(){
		return type;
	}
	
	public byte[] getValue(){
		return value;
	}
	
	public String toString(){
		StringBuffer str = new StringBuffer();
		str.append("\ttype: " + type);
		str.append("  length: " + length);
		str.append("  value:");
		for (int i = 0; i < value.length; i++){
			str.append(" " + value[i]);
		}
		str.append("\n");
		return str.toString();
	}
}
