package mikeysakke.mikey.utils;

import com.google.zxing.common.BitArray;

public final class Utils {

	/**
	 * Convert an array of bytes to an array of bits.
	 * 
	 * @param bytes
	 * @return the resulting BitArray
	 */
    public static BitArray getBitsFromBytes(final byte[] bytes) {
        BitArray data = new BitArray(0);
        int bytelen = bytes.length;
        for (int i = 0; i < bytelen; i++) {
            data.appendBits(bytes[i], 8);
        }
        return data;
    }

	/**
	 * Helper function to convert a byte array consisting of 4 bytes in to an
	 * int, or a byte array consisting of 2 bytes in to an int. This assumes the
	 * highest order byte is first in the byte_number array. In the case of a 2
	 * byte array, the bytes are considered to represent the two low order bytes
	 * of the int.
	 * 
	 * @param byte_number
	 * 
	 * @return byte_number converted to an int, or 0 if byte_number does not
	 *         consist of either two or four bytes.
	 */
    public static int convertByteArrayToInt(final byte[] byte_number)
    {
        int result = 0;
        if (byte_number.length == 4)
        {
            result = ((0xFF & byte_number[0]) << 24) |
                     ((0xFF & byte_number[1]) << 16) |
                     ((0xFF & byte_number[2]) << 8)  |
                     (0xFF & byte_number[3]);
        }
        else if (byte_number.length == 2)
        {
            result = ((0xFF & byte_number[0]) << 8) |
                     (0xFF & byte_number[1]);
        }

        return result;
    }

	/**
	 * Helper function to convert a byte array consisting of 2 bytes in to a 16
	 * bit short. This assumes the highest order byte is first in the
	 * byte_number array.
	 * 
	 * @param byte_number
	 * @return byte_number converted to a short, or 0 if byte_number does not
	 *         consist of two bytes.
	 * 
	 */
    public static short convertByteArrayToShort(final byte[] byte_number)
    {
        short result = 0;

        if (byte_number.length == 2)
        {
            result = (short) (((0xFF & byte_number[0]) << 8) |
                    (0xFF & byte_number[1]));
        }

        return result;
    }
    
	/**
	 * Convert the given long value to an array of 8 bytes (high order byte
	 * first).
	 * 
	 * @param v
	 *            the value to convert.
	 * @return an 8 byte array containing a representation of v.
	 */
   public static final byte[] longToBytes(long v) {
        byte[] writeBuffer = new byte[ 8 ];

        writeBuffer[0] = (byte)(v >>> 56);
        writeBuffer[1] = (byte)(v >>> 48);
        writeBuffer[2] = (byte)(v >>> 40);
        writeBuffer[3] = (byte)(v >>> 32);
        writeBuffer[4] = (byte)(v >>> 24);
        writeBuffer[5] = (byte)(v >>> 16);
        writeBuffer[6] = (byte)(v >>>  8);
        writeBuffer[7] = (byte)(v >>>  0);

        return writeBuffer;
    }
}
