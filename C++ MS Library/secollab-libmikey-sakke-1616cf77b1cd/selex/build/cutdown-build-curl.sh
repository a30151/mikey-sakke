cd $CURL

MACH=$(${CROSS_PREFIX}gcc -dumpmachine)

mkdir -p $MACH
cd $MACH

if [ -z "$NO_CONFIGURE" ]
then
WITHOUT=$(../configure  --help | grep -- '--without' | awk '{print $1}' |
   grep -vE '(ssl|PACKAGE)')
DISABLE=$(../configure --help | grep -- '--disable.*support' | awk '{print $1}' |
   grep -vE '(http|file|ftp|proxy|cookies)')

if [[ $MACH = *android* ]]; then
   if [ "${OPENSSL-undefined}" = "undefined" ]; then
      echo >&2 "Must set OPENSSL to blank or root dir of openssl-android."
   fi

   # OpenSSL-Android flags overriding pkg-config in libmcrypto
   export OPENSSL_CFLAGS="${OPENSSL:+-I${OPENSSL}/include}"
   export OPENSSL_LIBS="${OPENSSL:+-L${OPENSSL}/libs/armeabi} -lssl -lcrypto"
fi

echo -e '#include <openssl/ssl.h>\nint main(){return 0;}' | ${CROSS_PREFIX}gcc -xc - $CPPFLAGS $CFLAGS $LDFLAGS $OPENSSL_CFLAGS $OPENSSL_LIBS -o/dev/null ||
   { echo >&2 "Set OPENSSL to root dir of openssl source."; exit 20; }

CPPFLAGS="$CPPFLAGS $OPENSSL_CFLAGS" \
LDFLAGS="$LDFLAGS $OPENSSL_LIBS" \
../configure --host $MACH ${WITHOUT} ${DISABLE} --disable-manual || exit $?
fi

make "$@"

