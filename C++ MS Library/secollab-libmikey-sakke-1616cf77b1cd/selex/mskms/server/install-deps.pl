#!/usr/bin/env perl

use 5.010;
use strict;
use warnings;

use File::Basename;
use Cwd 'abs_path';
use CPAN;
use File::Find;

system('cpan ' . join ' ', qw/
   Crypt::Blowfish
   Crypt::CBC
   Crypt::DES_EDE3
   Crypt::Rijndael
   Data::Compare
   Encoding::BER
   Digest::SHA
   Digest::SHA1
   UUID::Tiny
   Math::BigInt::GMP
   IO::Socket::SSL
   DBD::SQLite
   Mojolicious/) == 0 or die @_;

CPAN::HandleConfig->load;

my $this_dir = abs_path(dirname($0));
my $build_dir = $CPAN::Config->{build_dir};

# Patch, rebuild and reinstall Math::BigInt::GMP
#
#my $bigint_gmp_dir = <$build_dir/Math-BigInt-GMP*>;
#system(<<CMD);
#cd "$bigint_gmp_dir"
#patch -p1 < "$this_dir/patches/Math-BigInt-GMP-export-mpz-sv-conversions.diff"
#make
#su -c "make install"
#CMD

# Fetch, patch, build and install Crypt::ECDSA
#
#my $ecdsa_dir = "$build_dir/Crypt-ECDSA-ajb1";
#system(<<CMD);
#git clone https://github.com/gitpan/Crypt-ECDSA "$ecdsa_dir"
#cd "$ecdsa_dir"
#patch -p1 < "$this_dir/patches/Crypt-ECDSA-use-Math-BigInt-GMP-mpz-sv-conversion.diff"
#perl Makefile.PL
#make
#su -c 'make install'
#CMD

