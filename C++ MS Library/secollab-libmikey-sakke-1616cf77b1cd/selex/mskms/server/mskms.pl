#!/usr/bin/hypnotoad -f

use Mojolicious::Lite;
use IO::Socket::SSL;

use DBI;
use Digest::SHA1 qw/sha1_base64/;
use Data::Dumper;

# https://github.com/gitpan/Crypt-ECDSA  +  thisdir/patches/*
use Crypt::ECDSA;
use Crypt::ECDSA::Util qw/bint hex_bint/;

use Carp qw/croak/;

sub dbconnect { DBI->connect("dbi:SQLite:mskms.db"); }


say "Initializing signing elliptic curve...";

my $sign_ec = Crypt::ECDSA::Curve::Prime->new(standard => 'ECP-256');

say "sign_ec->Gx:  " . $sign_ec->{'G_x'}->as_hex;
say "sign_ec->Gy:  " . $sign_ec->{'G_y'}->as_hex;
say "sign_ec->b:   " . $sign_ec->{'b'}->as_hex;
say "sign_ec->q:   " . $sign_ec->{'q'}->as_hex;

# Describes a parameter set for MIKEY-SAKKE encryption using the SAKKE
# cryptosystem (RFC 6508). Includes additional calculated information for
# performance. See RFC 6509 section 2.1.1 paragraph 1 and RFC 6508 section 2.1,
# 2.3 for more details.
my %sakke_param_sets;
%sakke_param_sets =
(
   1 => { ## RFC 6509

         n    =>  128,

         p    =>  hex_bint (join '',
                  qw/997ABB1F 0A563FDA 65C61198 DAD0657A
                     416C0CE1 9CB48261 BE9AE358 B3E01A2E
                     F40AAB27 E2FC0F1B 228730D5 31A59CB0
                     E791B39F F7C88A19 356D27F4 A666A6D0
                     E26C6487 326B4CD4 512AC5CD 65681CE1
                     B6AFF4A8 31852A82 A7CF3C52 1C3C09AA
                     9F94D6AF 56971F1F FCE3E823 89857DB0
                     80C5DF10 AC7ACE87 666D807A FEA85FEB/),

         q    =>  hex_bint (join '',
                  qw/265EAEC7 C2958FF6 99718466 36B4195E
                     905B0338 672D2098 6FA6B8D6 2CF8068B
                     BD02AAC9 F8BF03C6 C8A1CC35 4C69672C
                     39E46CE7 FDF22286 4D5B49FD 2999A9B4
                     389B1921 CC9AD335 144AB173 595A0738
                     6DABFD2A 0C614AA0 A9F3CF14 870F026A
                     A7E535AB D5A5C7C7 FF38FA08 E2615F6C
                     203177C4 2B1EB3A1 D99B601E BFAA17FB/),

         Px   =>  hex_bint (join '',
                  qw/53FC09EE 332C29AD 0A799005 3ED9B52A
                     2B1A2FD6 0AEC69C6 98B2F204 B6FF7CBF
                     B5EDB6C0 F6CE2308 AB10DB90 30B09E10
                     43D5F22C DB9DFA55 718BD9E7 406CE890
                     9760AF76 5DD5BCCB 337C8654 8B72F2E1
                     A702C339 7A60DE74 A7C1514D BA66910D
                     D5CFB4CC 80728D87 EE9163A5 B63F73EC
                     80EC46C4 967E0979 880DC8AB EAE63895/),

         Py   =>  hex_bint (join '',
                  qw/0A824906 3F6009F1 F9F1F053 3634A135
                     D3E82016 02990696 3D778D82 1E141178
                     F5EA69F4 654EC2B9 E7F7F5E5 F0DE55F6
                     6B598CCF 9A140B2E 416CFF0C A9E032B9
                     70DAE117 AD547C6C CAD696B5 B7652FE0
                     AC6F1E80 164AA989 492D979F C5A4D5F2
                     13515AD7 E9CB99A9 80BDAD5A D5BB4636
                     ADB9B570 6A67DCDE 75573FD7 1BEF16D7/),

         g    =>  hex_bint (join '',
                  qw/66FC2A43 2B6EA392 148F1586 7D623068
                     C6A87BD1 FB94C41E 27FABE65 8E015A87
                     371E9474 4C96FEDA 449AE956 3F8BC446
                     CBFDA85D 5D00EF57 7072DA8F 541721BE
                     EE0FAED1 828EAB90 B99DFB01 38C78433
                     55DF0460 B4A9FD74 B4F1A32B CAFA1FFA
                     D682C033 A7942BCC E3720F20 B9B7B040
                     3C8CAE87 B7A0042A CDE0FAB3 6461EA46/),

         sha => 256, # sha-id
         hash_len => 32, # octets
   }
);

while (my ($id, $set) = each(%sakke_param_sets))
{
   $set->{E} = Crypt::ECDSA::Curve::Prime->new
   (
      standard => 'generic_prime',
      p => $set->{p},
      a => -3,
      b => 0,
   );
   $set->{P} = Crypt::ECDSA::Point->new
   (
      curve => $set->{E},
      X => $set->{Px},
      Y => $set->{Py},
   );
   die "P is not on curve for parameter set $id"
      if not $set->{P}->is_on_curve();
}

# hardcode to param set 1 (6509)
my $sakke_param_set_id = 1;
my $sakke_param_set = $sakke_param_sets{$sakke_param_set_id};


# cache public data
my $this_server_id;
my $Z;
my $KPAK;

# If the database has not been created then make it now
if (! -r "mskms.db")
{
   say "Creating database...";

   my $dbh = dbconnect();
   my $sql = <<ENDSQL;

   ------------ MIKEY-SAKKE Data ------------

   CREATE TABLE server_private_data
   (
      kmsSecretAuthenticationKey BLOB,     -- 6507 integer KSAK.
      kmsMasterSecret BLOB                 -- 6508 integer z.
   );
   CREATE TABLE server_public_data
   (
      id INTEGER PRIMARY KEY,              -- internal key per community.
      kmsIdentifier VARCHAR(255) NOT NULL, -- 6509 arbitrary string.
      kmsPublicAuthenticationKey BLOB,     -- 6507 ecpoint KPAK.
      kmsPublicKey BLOB,                   -- 6508 ecpoint Z.
      sakkeParameterSetIndex INTEGER       -- SAKKE parameter set being used.
   );
   CREATE TABLE identities                 -- could identify a device,
   (                                       -- access-point/user-agent or user.
      userIdentifier BLOB                  -- user identifier: in 6709 this
         PRIMARY KEY,                      -- includes a datestamp;
      accountId INTEGER                    -- reference to login account
         REFERENCES accounts(id),          -- that created this data.
      userPublicValidationToken BLOB,      -- 6507 ecpoint PVT
      userSecretSigningKey BLOB,           -- 6507 integer SSK
      receiverSecretKey BLOB               -- 6508 ecpoint RSK
   );
   CREATE TABLE account_uris
   (
      id INTEGER REFERENCES accounts(id),  -- the account
      uri VARCHAR(240) PRIMARY KEY         -- a URI registered to the account
   );
   CREATE INDEX AccountURIs ON account_uris(id);
   CREATE INDEX ServerID ON server_public_data(kmsIdentifier);

   ------------ User Authentication ------------

   CREATE TABLE this_server
   (
      whoami INTEGER                       -- ref to public data
         REFERENCES server_public_data(id),
      dbVersion INTEGER DEFAULT 1          -- db version for migration
   );
   CREATE TABLE accounts
   (
      id INTEGER PRIMARY KEY,              -- internal key.
      name VARCHAR(64) UNIQUE NOT NULL,    -- user name for https access.
      sha1 CHAR(20),                       -- SHA-1 of user's password.
      admin BOOLEAN                        -- whether user has access
                                           -- to administrative functions
   );
   CREATE TABLE auth_sessions
   (
      sid VARCHAR(255) PRIMARY KEY,        -- auth session key
      accountId INTEGER                    -- user authenticated
         REFERENCES accounts(id),
      lastActivity TIMESTAMP               -- last auth activity 
         DEFAULT CURRENT_TIMESTAMP,
      clientData VARCHAR(255)              -- user-agent and ip address
   );
   CREATE INDEX AccountIdentities ON identities(accountId);
   CREATE INDEX AccountSessions ON auth_sessions(accountId);
ENDSQL

   for my $stm (split /(?<=\));/, $sql)
   {
      $dbh->do($stm) or die $dbh->errstr . " in " . $stm;
   }

   print "Creating default users...  ";

   for (['bob', 'bobpass'],
        ['jim', 'jimpass'],
        ['admin', 'admin', 1])
   {
      my ($name, $pass, $admin) = @$_;
      $dbh->do("INSERT INTO accounts (name, sha1, admin) VALUES (?, ?, ?)",
                 undef,($name, sha1_base64($pass), !!$admin)) or die $dbh->errstr;
   }

   say "Done.  Login as admin:admin to reconfigure.";

   my $initial_identity = 'unspecified.domain.co.uk';

   $dbh->do("INSERT INTO server_public_data (kmsIdentifier, sakkeParameterSetIndex) ".
            "VALUES ('$initial_identity', $sakke_param_set_id)") or die $dbh->errstr;

   $this_server_id = scalar $dbh->selectrow_array(
            "SELECT id FROM server_public_data ".
            "WHERE kmsIdentifier = '$initial_identity'") or die $dbh->errstr;

   $dbh->do("INSERT INTO this_server (whoami) VALUES ($this_server_id)") or die $dbh->errstr;

   say "Configured initial KMS identity as '$initial_identity'.  Login as an administrator to reconfigure.";

   # Set-up initial MIKEY-SAKKE data
   community_rekey($dbh);

   $dbh->disconnect();
}
else # cache public data
{
   refresh_public_cache();
}

# Loads the key data from the database into the local variables
sub refresh_public_cache
{
   my $dbh = dbconnect();

   $this_server_id = scalar $dbh->selectrow_array('SELECT whoami FROM this_server') or die $dbh->errstr;
   $KPAK = Crypt::ECDSA::Point->new(
            curve => $sign_ec,
            octet => pack('H*',$dbh->selectrow_array(
                        "SELECT kmsPublicAuthenticationKey ".
                          "FROM server_public_data ".
                         "WHERE id = $this_server_id")));
   $Z = hex_bint $dbh->selectrow_array("SELECT kmsPublicKey ".
                                         "FROM server_public_data ".
                                        "WHERE id = $this_server_id");
   $sakke_param_set_id = $dbh->selectrow_array("SELECT sakkeParameterSetIndex ".
                                                 "FROM server_public_data ".
                                                "WHERE id = $this_server_id");
   $sakke_param_set = $sakke_param_sets{$sakke_param_set_id};
   $dbh->disconnect();
}

# Counts the number of bits in the given big integer
sub bitcount
{
   my $x = shift;
   my $rc = 0;
   while ($x != 0)
   {
      ++$rc;
      $x >>= 1;
   }
   return $rc;
}

# Changes the KMS identifier (currently hardcoded to unspecified.domain.co.uk)
sub change_server_name
{
   my $dbh = shift;
   my $newname = shift;

   $dbh->do("UPDATE server_public_data 
                SET kmsIdentifier = ?
              WHERE id = $this_server_id",
            undef,
            ($newname)) or croak $dbh->errstr;
}

# Adds a new URI to the database
sub add_account_uri
{
   my $dbh = shift;
   my $accountId = shift;
   my $uri = shift;

   $dbh->do("INSERT INTO account_uris (id, uri) VALUES (?, ?)",
            undef,
            ($accountId, $uri)) or return {error => $dbh->errstr};

   {success => "URI '$uri' added to account '$accountId'"}
}

# Removes a URI from the database
sub remove_account_uri
{
   my $dbh = shift;
   my $accountId = shift;
   my $uri = shift;

   $dbh->do("DELETE FROM account_uris
              WHERE id = ?
                AND uri = ?",
            undef,
            ($accountId, $uri)) or return {error => $dbh->errstr};

   {success => "URI '$uri' removed from account '$accountId'"}
}

# Adds a new user to the database
# (Must be logged in as admin to do this)
sub add_user
{
   my $dbh = shift;
   my $accountId = shift;
   
   my $is_admin = $dbh->selectrow_array(
      "SELECT admin FROM accounts WHERE id = $accountId");

   return {error => "Need to be administrator to add users."} if not $is_admin;

   my $name = shift;
   my $pass = shift;
   my $admin = shift;
   
   $dbh->do("INSERT INTO accounts (name, sha1, admin) VALUES (?, ?, ?)",
            undef,
            ($name, sha1_base64($pass), !!$admin)) or return {error => $dbh->errstr};

   {success => "User '$name' added."}
}

# Gets the KSAK
sub get_KSAK
{
   my $dbh = shift;
   return hex_bint $dbh->selectrow_array(
      "SELECT kmsSecretAuthenticationKey FROM server_private_data");
}

# Gets the KMS master secret key
sub get_master_secret
{
   my $dbh = shift;
   return hex_bint $dbh->selectrow_array(
      "SELECT kmsMasterSecret FROM server_private_data");
}

# Generates or retrieves the PVT, SSK and RSK for the given identifier
# Will also return the KMS public key information so that the reciever
# can validate the keys
sub get_key_material
{
   my $dbh = shift;
   my $accountId = shift;
   my $userIdentifier = shift;

   say "get_key_material($accountId, '" . $userIdentifier. "')";

   return {error => "Invalid arguments"}
      if not defined $accountId or not defined $userIdentifier;

   # get URI component for matching to user account (commencing after
   # first NUL character)
   # 
   my $uri = substr($userIdentifier, index($userIdentifier, "\0")+1);

   my $requestedAccountHolder = $dbh->selectrow_array("SELECT id FROM account_uris WHERE uri = ?",undef,($uri));

   return {error => "The URI '$uri' is not associated with an account"}
      if not defined $requestedAccountHolder;

   # if the requesting account is the owner of the uri then
   # return private data in addition to the public data
   #
   my $isOwner = $accountId == $requestedAccountHolder;

   say "Requesting user: $accountId; Details requested for: $uri: (ID:$userIdentifier) (account:$requestedAccountHolder)";

   my $private;
   my $error;

   if ($isOwner)
   {
      $private = $dbh->selectrow_hashref("SELECT userSecretSigningKey,
                                                 receiverSecretKey
                                            FROM identities
                                           WHERE userIdentifier = ?",undef,($userIdentifier));


      if (not defined($private))
      {
         # ===== Compute signing keys. =====================

         my $AuthKey = Crypt::ECDSA::Key->new(
               curve => $sign_ec,
               X => $sign_ec->{'G_x'},
               Y => $sign_ec->{'G_y'},
               d => bint 0x23456,  ## XXX: RFC6507 example override
            );

         my $PVT = $AuthKey->G * $AuthKey->secret;

         my $hash = new Digest::SHA($sakke_param_set->{sha});

         # XXX: test vector from 6507
         # $userIdentifier = "2011-02\0tel:+447700900123\0";

         say "G    := " . unpack('H*', $AuthKey->G->to_octet);
         say "KPAK := " . unpack('H*', $KPAK->to_octet);
         say "ID   := " . $userIdentifier;
         say "PVT  := " . unpack('H*', $PVT->to_octet);

         $hash->add($AuthKey->G->to_octet);
         $hash->add($KPAK->to_octet);
         $hash->add($userIdentifier);
         $hash->add($PVT->to_octet);

         my $HS = hex_bint $hash->hexdigest;

         my $KSAK = get_KSAK $dbh;

         my $SSK = ($KSAK + $HS * $AuthKey->secret) % $AuthKey->G->order; 

         my $SSK_hex = substr($SSK->as_hex,2);
         my $PVT_hex = unpack('H*', $PVT->to_octet);

         say "$userIdentifier: PVT = " . $PVT_hex;
         say "$userIdentifier: HS  = " . $HS->as_hex;
         say "$userIdentifier: SSK = " . $SSK_hex;
         
         # ===== Compute receiver secret. =====================

         my $z = get_master_secret $dbh;

         my $inv = hex_bint(unpack('H*', $userIdentifier));
         $inv = ($inv + $z)->bmodpow(-1, $sakke_param_set->{q});
         my $RSK = $sakke_param_set->{P} * $inv;

         my $RSK_hex = pointToHex($RSK);

         say "$userIdentifier: RSK = " . $RSK_hex;


         $dbh->do("INSERT INTO identities (userIdentifier,
                                    accountId,
                                    userPublicValidationToken,
                                    userSecretSigningKey,
                                    receiverSecretKey)
                   VALUES (?, ?, ?, ?, ?)",undef,($userIdentifier, $accountId, $PVT_hex, $SSK_hex, $RSK_hex));
         
         $error = $dbh->errstr;

         $private =
         {
            userSecretSigningKey => $SSK_hex,
            receiverSecretKey => $RSK_hex,
         }
         if not $error;
      }
   }
   else
   {
      return {error => "The URI '$uri' is not associated with the requesting account"}
   }

   my $public = $dbh->selectrow_hashref("SELECT identities.userPublicValidationToken
                                           FROM identities
                                          WHERE userIdentifier = ?",undef,($userIdentifier));

   my $community = $dbh->selectall_arrayref("SELECT kmsIdentifier,
             kmsPublicAuthenticationKey,
             kmsPublicKey,
             sakkeParameterSetIndex 
        FROM server_public_data", {Slice => {}});

   my %result;

   $result{error} = $error if $error;
   $result{public} = $public if $public;
   $result{community} = $community if $community;
   $result{private} = $private if $private;

   return \%result;
}

# Takes an elliptic point and outputs it in the correct hex format
# The old code just did unpack('H*', $point->to_octet)
# but this does not pad the point up to $sakke_param_set->{n} bytes
sub pointToHex
{
   my $point = shift;
   
   # Get the co-ordinates (and remove the 0x from the start of the string)
   my $x = $point->X()->as_hex();
   $x = substr($x, 2, length($x));
   my $y = $point->Y()->as_hex();
   $y = substr($y, 2, length($y));
 
   #Ensure that there are always $sakke_param_set->{n} bytes of hex (* 2 for hex chars)
   my $pointLength = $sakke_param_set->{n} * 2;
   while (length($x) < $pointLength)
   {
      $x = "0" . $x;
   }

   while (length($y) < $pointLength)
   {
      $y = "0" . $y;
   }

   return "04" . $x . $y;
}

# Generates the KMS private key and associated public keys
# Will invalidate any existing key material
sub community_rekey
{
   my $dbh = shift;

   say "Community rekey: Generating new MIKEY-SAKKE secrets and corresponding public data...";

   say "Authentication Keys...";

   my $AuthKey = Crypt::ECDSA::Key->new(
         curve => $sign_ec,
         X => $sign_ec->{'G_x'},
         Y => $sign_ec->{'G_y'},
         d => bint 0x12345,  ## XXX: RFC6507 example override
      );

   my $KSAK = $AuthKey->secret;
   $KPAK = $AuthKey->Q; # cache in global

   say "G: ".unpack('H*', $AuthKey->G->to_octet);
   say "KSAK: ".$KSAK->as_hex;
   say "KPAK: ".unpack('H*', $KPAK->to_octet);

   say "CHECK: ".unpack('H*', ($AuthKey->G * $AuthKey->secret)->to_octet);

   say "Done.";


   say "SAKKE Keys...";
  
   say "n:         ".$sakke_param_set->{n};
   say "p:         ".$sakke_param_set->{p}->as_hex;
   say "q:         ".$sakke_param_set->{q}->as_hex;
   say "Px:        ".$sakke_param_set->{P}->X->as_hex;
   say "Py:        ".$sakke_param_set->{P}->Y->as_hex;
   say "P->order:  ".$sakke_param_set->{P}->order->as_hex;
   say "g:         ".$sakke_param_set->{g}->as_hex;
   say "sha:       ".$sakke_param_set->{sha};
   say "hash_len:  ".$sakke_param_set->{hash_len};

   my $q = $sakke_param_set->{q};

   my $z;
   {
      my $qw = bitcount($q-1);
      say $qw;
      for (;;) # probably unnecessary
      {
         my $zr = Crypt::ECDSA::Util::random_bits($qw);
         $z = $zr % $q;
         last # if $zr != $z;
      }
   } 

   # XXX: overwrite random with RFC6508 example
   $z = hex_bint join '', qw/AFF429D3 5F84B110 D094803B 3595A6E2 998BC99F/;

   # cache in global
   $Z = $sakke_param_set->{P} * $z;

   say "z:         ".$z->as_hex;
   say "Z:         ".unpack('H*', $Z->to_octet);

   $dbh->do('DELETE FROM server_private_data');
   $dbh->do("INSERT INTO server_private_data VALUES (?, ?)",undef,(substr($KSAK->as_hex,2), substr($z->as_hex,2)));

   $dbh->do("UPDATE server_public_data
                SET kmsPublicAuthenticationKey = ?,
                    kmsPublicKey = ?
              WHERE id = $this_server_id",
            undef,(unpack('H*', $KPAK->to_octet), unpack('H*', $Z->to_octet)))
}

# Gets the ID of the account that is logged in on the current session
sub get_account_from_session
{
   my $mojo = shift;
   my $sid = $mojo->session('sid');
   return scalar app->dbh->selectrow_array("SELECT accountId FROM auth_sessions WHERE sid = ?",undef,($sid));
}

# Gets the username of the logged in account for the current session
sub get_username_from_session
{
   my $mojo = shift;
   my $sid = $mojo->session('sid');
   return app->dbh->selectrow_array("SELECT accounts.name 
                                       FROM auth_sessions 
                                      INNER JOIN accounts
                                         ON accounts.id = auth_sessions.accountId
                                      WHERE sid = ?",undef,($sid));
}

app->attr(dbh => \&dbconnect);
app->config(hypnotoad => {listen => ['https://*:7070']});

use lib 'lib';

plugin 'auth_helper';
plugin 'basic_auth';

################################################################################
# This seciton defines the returns for different web requests
group
{
   under '/secure' => sub
   {
      my $mojo = shift;
      return 1 if $mojo->check_auth()->{authorized};

      # support direct access via basic-auth for /key subpath
      if ((my $sub = $mojo->req->url->path->parts->[1]))
      {
         if ($sub eq 'key')
         {
            return 1 if $mojo->basic_auth('mikey-sakke-kms' => sub
               {
                  if (not $mojo->login(@_)->{authorized})
                  {
                     $mojo->res->headers->www_authenticate("Basic realm=mikey-sakke-kms");
                     $mojo->res->code(401);
                     $mojo->rendered;
                     return 0;
                  }
                  return 1;
               });
            return 0;
         }
      }
      $mojo->session('post-login-url' => $mojo->req->url->path->to_abs_string);
      $mojo->redirect_to('/login');
      return 0;
   };

   # Returns the keys for the given ID
   get '/key' => sub
   {
      my $mojo = shift;
      my $accountId = get_account_from_session($mojo);
      my $userIdentifier = $mojo->param('id');
      my $material = get_key_material(app->dbh, $accountId, $userIdentifier);
      return $mojo->render_json($material);
   };

   # Adds the given URI to the database
   any ['post', 'get'] => '/add' => sub
   {
      my $mojo = shift;
      my $accountId = get_account_from_session($mojo);
      my $uri = $mojo->param('uri');
      return $mojo->render_json(add_account_uri(app->dbh, $accountId, $uri));
   };

   # Removes the given ID from the database
   any ['post', 'get'] => '/remove' => sub
   {
      my $mojo = shift;
      my $accountId = get_account_from_session($mojo);
      my $uri = $mojo->param('uri');
      return $mojo->render_json(remove_account_uri(app->dbh, $accountId, $uri));
   };
   
   # Adds the given user to the database (must be logged in as Admin to do this)
   any ['post', 'get'] => '/add-user' => sub
   {
      my $mojo = shift;
      my $accountId = get_account_from_session($mojo);
      my $name = $mojo->param('name');
      my $pass = $mojo->param('pass');
      return $mojo->render_json(add_user(app->dbh, $accountId, $name, $pass, 0));
   };
};

# The index page for the KMS control panel
# Displays a list of URIs that are currently in the system
get '/index' => sub
{
   my $mojo = shift;
   my $row;

   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }

   write_header($mojo);

   my $dbh = dbconnect();
   
   $mojo->write_chunk('<H2>Current URIs</H2>');
   $mojo->write_chunk('<a href=adduri>Add URI</a><br>');
   $mojo->write_chunk('<CENTER><table border=1 width=60%>');
   $mojo->write_chunk('<tr><td><b>URI</b></td><td><b>added by</b></td><td><b>delete</b></td></tr>');
   
   # Get a list of URIs from the database
   my $uris = $dbh->selectall_arrayref("SELECT account_uris.rowid, 
                                               account_uris.uri,
                                               accounts.name 
                                          FROM account_uris 
                                         INNER JOIN accounts
                                            ON account_uris.id == accounts.id
					                            ORDER BY account_uris.uri");
   
   # Loop round all the URIs adding them to the tabkle
   foreach $row (@$uris)
   {
      my $uri = $row->[1];
      $uri =~ s/\0//;
      $mojo->write_chunk('<tr><td><a href="viewuri?uri=' . $uri . '">' . $uri . '</a></td>');
      $mojo->write_chunk('<td>' . $row->[2] . '</td>');
      $mojo->write_chunk('<td><a href="deleteuri?uri=' . $uri . '">delete</a></td></tr>');
   }

   $mojo->write_chunk('</table></CENTER>');

   write_footer($mojo);
};

# Displays the users screen in the KMS control panel
get '/users' => sub
{
   my $mojo = shift;
   my $row;

   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }
   write_header($mojo);

   $mojo->write_chunk('<H2>Current users</H2>');
   $mojo->write_chunk('<a href=adduser>Add user</a><br>');
   $mojo->write_chunk('<CENTER><table border=1 width=60%>');
   $mojo->write_chunk('<tr><td><b>Name</b></td><td><b>Admin</b></td><td><b>delete</b></td></tr>');
  
   # Get a list of user accounts from the database 
   my $dbh = dbconnect();
   my $users = $dbh->selectall_arrayref("SELECT accounts.name,
					                                      accounts.admin,
                                                accounts.rowid
                                           FROM accounts
                                       ORDER BY accounts.name");

   # loop round the users adding them to the table
   foreach $row (@$users)
   {
      my $isAdmin = $row->[1];
      if (!($isAdmin))
      {
         $isAdmin = "0";
      }

      $mojo->write_chunk('<tr><td>' . $row->[0] . '</a></td>');
      $mojo->write_chunk('<td>' . $isAdmin . '</td>');
      $mojo->write_chunk('<td><a href="deleteuser?id=' . $row->[2] . '">delete</a></td></tr>');
   }

   $mojo->write_chunk('</table></CENTER>');

   write_footer($mojo);
};

# Adds the given user information to the database
# (Must be logged on as admin to use this)
get '/adduser' => sub
{
   my $mojo = shift;
   my $error = "";
   my $name = $mojo->param('name');
   my $pass = $mojo->param('pass');
   my $admin = $mojo->param('admin');
   my $adminChecked = ""; 

   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }

   if ($admin == 1)
   {
      $adminChecked = "checked=yes"; 
   }
   else 
   {
      $admin = 0;
      $adminChecked = ""; 
   }

   if ($name && $pass) 
   {
      # If this is being called with parameters then try and add the user
      my $dbh = dbconnect();
      my $accountId = get_account_from_session($mojo);

      my $result = add_user($dbh, $accountId, $name, $pass, $admin);
	
      # If an error occured the display it to the user
      # Otherwise redirect back to the main users screen
      if ($result->{'error'})
      {
         $error = $result->{'error'};
      }
      else
      {
         return $mojo->redirect_to('users');
      }
   }
   else 
   {
      $name = "";
      $pass = "";
   }

   write_header($mojo);
   $mojo->write_chunk('<H2>Add New User</H2>');
   $mojo->write_chunk('<CENTER>');

   if ($error)
   {
      $mojo->write_chunk($error);
      $mojo->write_chunk('<br>');
   }

   # Display an HTML form the enter the user details
   $mojo->write_chunk('<FORM action=adduser>');
   $mojo->write_chunk('<table>');
   $mojo->write_chunk('<tr><td>Username:</td><td><input type="text" name="name" value="' . $name . '"></td></tr>');
   $mojo->write_chunk('<tr><td>Password:</td><td><input type="password" name="pass" value="' . $pass . '"></td></tr>');
   $mojo->write_chunk('<tr><td>Admin:</td><td><input type="Checkbox" name="admin" value="1"' . $adminChecked . '"></td></tr>');
   $mojo->write_chunk('<tr><td colspan=2 align=right><input type="submit" value="Add"></td></tr>');
   $mojo->write_chunk('</table>');
   $mojo->write_chunk('</FORM>');

   write_footer($mojo);
};

# Deletes the given user information from the database
# TODO: currently this is unprotected, but you should have to be admin
# and not allowed to delete yourself etc...
get '/deleteuser' => sub
{
   my $mojo = shift;
   my $userId = $mojo->param('id');
   my $error = "";
   my $rows;
   my $row;

   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }

   if ($userId) 
   {
      my $dbh = dbconnect();
      
      $dbh->do("DELETE FROM accounts WHERE id = ?",
               undef,($userId)) or $error = $dbh->errstr;

      if (!$error)
      {
         return $mojo->redirect_to('users');
      }
   }

   write_header($mojo);
   $mojo->write_chunk('<H2>Delete User</H2>');
   $mojo->write_chunk('<CENTER>');
   $mojo->write_chunk('Failed to delete - ' . $error);
   $mojo->write_chunk('<br>');
   write_footer($mojo);
};

# Shows a login screen 
# This is displayed whenever a page is accessed that the user should be logged in for
get '/login' => sub
{
   my $mojo = shift;
   my $error = $mojo->flash('error');
   write_header($mojo);

   $mojo->write_chunk("<CENTER>");
   $mojo->write_chunk("<br><br>");
   
   if ($error)
   {
      $mojo->write_chunk($error);
   }
   else
   {
      $mojo->write_chunk("Please log in to continue");
   }

   # Create an HTML form to enter login information into
   $mojo->write_chunk('<br>');
   $mojo->write_chunk("<form action='/login' method='POST'>");
   $mojo->write_chunk('<table>');
   $mojo->write_chunk("<tr><td><label for='username'>User name:</label></td><td><input type='text' name='username'></td></tr>");
   $mojo->write_chunk("<tr><td><label for='password'>Password:</label></td><td><input type='password' name='password'></td></tr>");
   $mojo->write_chunk("<tr><td colspan=2 align=right><input type='submit' value='Log in'>");
   $mojo->write_chunk("<input type='reset' value='Clear'></td></tr>");
   $mojo->write_chunk('</table>');
   $mojo->write_chunk("</form>");

   write_footer($mojo);
};

# Shows the details of the given ID including all the keys generated for it
get '/viewuri' => sub
{
   my $mojo = shift;
   my $uri = $mojo->param('uri');
   my $row;
   my $rows;
   my $error = "";
 
   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }
  
   write_header($mojo);
   $mojo->write_chunk('<H2>URI Details</H2><br>');

   # Get the URI details from the database
   my $dbh = dbconnect();
   $rows = $dbh->selectall_arrayref("SELECT account_uris.uri,
                                              accounts.name 
                                         FROM account_uris 
                                        INNER JOIN accounts
                                           ON account_uris.id == accounts.id
                                        WHERE account_uris.uri = ?
                                           OR account_uris.uri = ?",undef,($uri, $uri . "\0")) or $error = $dbh->errstr;


   $mojo->write_chunk('<table style="table-layout:fixed;word-wrap:break-word;" width=80%>');
   $mojo->write_chunk('<tr><td><b>URI:</b><br>' . $rows->[0][0] . '</td></tr>');
   $mojo->write_chunk('<tr><td><b>Added By:</b><br>' . $rows->[0][1] . '</td></tr>');
   
   # Get the key information from the database
   # This join needs to be done by looping through becuase the userIdentifier
   # column contains \0 characters and the LIKE sql statment does not work
   $rows = $dbh->selectall_arrayref("SELECT userIdentifier,
                                            userPublicValidationToken,
                                            userSecretSigningKey,
                                            receiverSecretKey
                                       FROM identities");



   foreach $row (@$rows)
   {      
      # only display identities related to the uri
      my $regex = "\0" . $uri . "\0";
      if ($row->[0] =~ m/$regex$/g)
      {
         $mojo->write_chunk('<tr><td><b>Identity:</b><br>' . $row->[0] . '</td></tr>');
         $mojo->write_chunk('<tr><td><b>PVT:</b><br>' . $row->[1] . '</td></tr>');
         $mojo->write_chunk('<tr><td><b>SSK:</b><br>' . $row->[2] . '</td></tr>');
         $mojo->write_chunk('<tr><td><b>RSK:</b><br>' . $row->[3] . '</td></tr>');
      }
   }
   
   $mojo->write_chunk('</table>');

   write_footer($mojo);
};

# Shows a screen to Add URI information to the database
get '/adduri' => sub
{
   my $mojo = shift;
   my $uri = $mojo->param('uri');
   my $error = "";

   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }

   if ($uri) 
   {
      # if a URI parameter is given then attempt to add it to the datbase
      my $dbh = dbconnect();
      my $accountId = get_account_from_session($mojo);

      $dbh->do("INSERT INTO account_uris (id, uri) VALUES (?, ?)",
               undef,
               ($accountId, $uri . "\0")) or $error = $dbh->errstr;

      # If the insert was OK then redirect to the main page
      # Otherwise display the error
      if (!$error)
      {
         return $mojo->redirect_to('index');
      }
   }
   else 
   {
      $uri = "";
   }


   write_header($mojo);
   $mojo->write_chunk('<H2>Add New URI</H2>');
   $mojo->write_chunk('<CENTER>');

   if ($error)
   {
      $mojo->write_chunk($error);
      $mojo->write_chunk('<br>');
   }

   # Display an HTML table to insert the URI details into
   $mojo->write_chunk('<FORM action=adduri>');
   $mojo->write_chunk('New URI:<input type="text" name="uri" value="' . $uri . '">');
   $mojo->write_chunk('<input type="submit" value="Add">');
   $mojo->write_chunk('</FORM>');

   write_footer($mojo);
};

# Deletes the given URI and returns to the main page
# or displays the error if it fails
get '/deleteuri' => sub
{
   my $mojo = shift;
   my $uri = $mojo->param('uri');
   my $error = "";
   my $rows;
   my $row;

   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }

   if ($uri) 
   {
      my $dbh = dbconnect();
      my $accountId = 2;
      
      # Attempt to delete the given URI from the database
      $dbh->do("DELETE FROM account_uris WHERE uri = ? OR uri = ?",
               undef,($uri, $uri . "\0")) or $error = $dbh->errstr;

      if (!$error)
      {
         # Delete any related keys from the identities table 
         $rows = $dbh->selectall_arrayref("SELECT userIdentifier,
                                                  rowid
                                             FROM identities");
         foreach $row (@$rows)
         {      
            # only delete identities related to the uri
            # This join needs to be done by looping through becuase the userIdentifier
            # column contains \0 characters and the LIKE sql statment does not work
            my $regex = "\0" . $uri . "\0";
            if ($row->[0] =~ m/$regex$/g)
            {
               $dbh->do("DELETE FROM identities WHERE rowid = ?",undef,($row->[1])) or $error = $dbh->errstr; 
            }
         }
      }

      if (!$error)
      {
         return $mojo->redirect_to('index');
      }
   }

   write_header($mojo);
   $mojo->write_chunk('<H2>Delete URI</H2>');
   $mojo->write_chunk('<CENTER>');
   $mojo->write_chunk('Failed to delete - ' . $error);
   $mojo->write_chunk('<br>');
   write_footer($mojo);
};

# Displays a screen with the KMS public key information 
get '/viewkeys' => sub
{
   my $mojo = shift;
   my $rows;

   if (!$mojo->check_auth()->{authorized})
   {      
       return $mojo->redirect_to('login');
   }

   write_header($mojo);
   $mojo->write_chunk('<H2>View Keys</H2>');

   # Get the key information from the server_public_data table
   my $dbh = dbconnect();
   $rows = $dbh->selectall_arrayref("SELECT kmsIdentifier,
                                            sakkeParameterSetIndex,
                                            kmsPublicAuthenticationKey,
                                            kmsPublicKey
                                       FROM server_public_data");

   $mojo->write_chunk('<table style="table-layout:fixed;word-wrap:break-word;" width=80%>');
   $mojo->write_chunk('<tr><td><b>KMS ID:</b><br>' . $rows->[0][0] . '</td></tr>');
   $mojo->write_chunk('<tr><td><b>Parameter Set:</b><br>' . $rows->[0][1] . '</td></tr>');
   $mojo->write_chunk('<tr><td><b>KPAK:</b><br>' . $rows->[0][2] . '</td></tr>');
   $mojo->write_chunk('<tr><td><b>Z:</b><br>' . $rows->[0][3] . '</td></tr>');

   $mojo->write_chunk('</table>');


   write_footer($mojo);
};

# Writes the standard top bar for each of the KMS control panel pages
sub write_header
{
   my $mojo = shift;
   $mojo->write_chunk('<html><title>KMS Control Panel</title><body>');
   $mojo->write_chunk('<table border=0 width=100% height=100%>');
   $mojo->write_chunk('<tr><td colspan=4 bgcolor=gray><H1>KMS Control Panel</H1></td></tr><tr>');

   if ($mojo->check_auth()->{authorized})
   {
      # Get the logged in username
      my $username = get_username_from_session($mojo);

      $mojo->write_chunk('<td align=center bgcolor=gray>');
      $mojo->write_chunk($username);
      $mojo->write_chunk(' <a href=logout>Log out</a></td>');
   }
   else 
   {
      $mojo->write_chunk('<td align=center bgcolor=gray>Not Logged In</a></td>');
   }
   $mojo->write_chunk('<td align=center bgcolor=gray><a href=index>URIs</a></td>');
   $mojo->write_chunk('<td align=center bgcolor=gray><a href=users>Users</a></td>');
   $mojo->write_chunk('<td align=center bgcolor=gray><a href=viewkeys>KMS Public Keys</a></td></tr>');
   $mojo->write_chunk('<tr height=100%><td colspan=4 valign=top bgcolor=lightblue>');
}

# Writes the standard bottom bar for each of the KMS control panel pages
sub write_footer
{
   my $mojo = shift;
   $mojo->finish('</td></tr></table></body></html>');
}

# Handles the login button clicks and redirects to index if login is OK
post '/login' => sub
{
   my $mojo = shift;

   my $user = $mojo->login($mojo->param('username'), $mojo->param('password'));

   if ($user->{authorized})
   {
      # redirect to index if authorised
      my $post_login_url = $mojo->session('post-login-url') || '/';
      delete $mojo->session->{'post-login-url'};
      return $mojo->redirect_to($post_login_url);
   }

   $mojo->flash(error => "401:Failed to authenticate.");
   $mojo->redirect_to('/login');
};

# Deletes the login information from the session and redirects to index
any ['get', 'post'] => '/logout' => sub
{
   my $mojo = shift;
   $mojo->logout($mojo->param('all-sessions'));
   $mojo->redirect_to('/');
};

# Defaults to index if no page is specified
get '/' => sub 
{ 
   my $mojo = shift; 
   return $mojo->redirect_to('/index') 
};

app->start;

# vim: ft=perlsql

