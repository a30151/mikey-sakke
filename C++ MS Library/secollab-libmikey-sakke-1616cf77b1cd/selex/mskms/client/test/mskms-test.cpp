//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Test
// Item Name        : mskms-test.cpp
// Item Description : Tests the mikey-sakke KMS client.
//
//******************************************************************************

#include <mskms/client.h>
#include <mskms/flat-file-key-storage.inl>
#include <iostream>


template <int N> inline std::string from_array(char const (&array) [N])
{
   return std::string(array, N);
}


using namespace MikeySakkeKMS;


void HandleFetchResult(ClientPtr /*kms*/, KeyAccessPtr keys,
                       std::string const& identifier, sys::error_code error)
{
   if (error)
   {
      std::cerr << "Failed to fetch keys for identifier '"
                << identifier << "': " << error.message() << "\n";
      return;
   }

   std::cout
      << "Retrieved keys for '"<<identifier<<"':\n"
      << "\n"
      << "RSK:      " << keys->GetPrivateKey(identifier, "RSK") << "\n"
      << "SSK:      " << keys->GetPrivateKey(identifier, "SSK") << "\n"
      << "\n"
      << "PVT:      " << keys->GetPublicKey(identifier, "PVT") << "\n"
      << "\n"
      ;

   std::vector<std::string> communities = keys->GetCommunityIdentifiers();

   std::cout
      << "Communities known to KMS:\n"
      ;

   for (std::vector<std::string>::const_iterator
        it = communities.begin(), end = communities.end();
        it != end;
        ++it)
   {
      std::cout
         << "\nCommunity: '" << *it << "'\n"
         << "\n"
         << "SakkeSet: " << keys->GetPublicParameter(*it, "SakkeSet") << "\n"
         << "KPAK:     " << keys->GetPublicKey(*it, "KPAK") << "\n"
         << "Z:        " << keys->GetPublicKey(*it, "Z") << "\n"
         ;
   }
}


int main(int argc, char** argv)
{
   if (argc > 1 && strcmp("-h", argv[1]) == 0)
   {
      std::cout << "Usage: mskms-test [-h] [--no-fetch] <server-addr:127.0.0.1:7070> <username:jim> <password:jimpass> <datastamp:2012-02> <uri:tel:+447700900123>\n";
      return 20;
   }
   bool no_fetch = false;
   if (argc > 1 && strcmp("--no-fetch", argv[1]) == 0)
   {
      no_fetch = true;
      --argc;
      ++argv;
   }
   std::string kmsServer = "127.0.0.1:7070";
   std::string username = "jim";
   std::string password = "jimpass";
   std::string identifier = from_array("2011-02\0tel:+447700900123");
   if (argc > 1)
      kmsServer.assign(argv[1]);
   if (argc > 2)
      username.assign(argv[2]);
   if (argc > 3)
      password.assign(argv[3]);
   if (argc > 5) { // give as two args
      identifier.assign(argv[4]); // datestamp
      identifier.append(1, 0);
      identifier.append(argv[5]); // uri
      identifier.append(1, 0);
   }

   KeyStoragePtr ks(new FlatFileKeyStorage("./key-data"));

   if (no_fetch)
   {
      HandleFetchResult(ClientPtr(), ks, identifier, make_error_code(sys::errc::success));
   }
   else
   {
      ClientPtr kms(new Client(ks));

      using namespace std::placeholders;

      kms->FetchKeyMaterial(kmsServer, Client::DontVerifySSLCertificate,
                            username, password, identifier,
                            std::bind(HandleFetchResult, kms, ks, _1, _2));
   }
}

