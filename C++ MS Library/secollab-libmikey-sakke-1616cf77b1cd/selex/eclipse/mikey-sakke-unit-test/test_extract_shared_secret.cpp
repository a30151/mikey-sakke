//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_extract_shared_secret.cpp
// Item Description : Unit Tests for the ExtractSharedSecret method.
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#include <mscrypto/parameter-set.h>
#include <mscrypto/sakke.h>
#include <mskms/runtime-key-storage.inl>
#include <util/octet-string.h>

#include "key_data.h"

using namespace mikey_sakke_unit_test;
using namespace MikeySakkeCrypto;

namespace
{
	static std::string community = "testextractsharedsecret.test";
}

BOOST_AUTO_TEST_CASE( testExtractSharedSecretOK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "RSK", getValidRSK());
	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString ssv = ExtractSharedSecret(getValidSED(),
										  id,
										  community,
										  keys);

	OctetString expectedSSV = OctetString::skipws
	         ("12345678 9ABCDEF0 12345678 9ABCDEF0");

	BOOST_CHECK( ssv == expectedSSV );
}

BOOST_AUTO_TEST_CASE( testExtractSharedSecretIncorrectSED )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "RSK", getValidRSK());
	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

    OctetString ssv = ExtractSharedSecret(getInvalidSED(),
    									  id,
    									  community,
    									  keys);

    BOOST_CHECK( ssv == OctetString("") );
}

BOOST_AUTO_TEST_CASE( testExtractSharedSecretBadID )
{
	OctetString id = constructId("tel:+447700900125");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "RSK", getValidRSK());
	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString ssv = ExtractSharedSecret(getValidSED(),
			                              id,
			                              community,
			                              keys);

	BOOST_CHECK( ssv == OctetString("") );
}

BOOST_AUTO_TEST_CASE( testExtractSharedSecretBadZ )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "RSK", getValidRSK());
	keys->StorePublicKey(community, "Z", getInvalidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString ssv = ExtractSharedSecret(getValidSED(),
										  id,
										  community,
										  keys);

	BOOST_CHECK( ssv == OctetString("") );
}

