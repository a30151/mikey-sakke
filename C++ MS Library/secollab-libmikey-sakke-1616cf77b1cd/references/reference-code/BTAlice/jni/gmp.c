#include <jni.h>
#include <gmp.h>
#include <stdlib.h>

// CONSTANTS
mpz_t p; // 1024-bit prime p as defined in Pairing Based IBE
mpz_t q; // prime q dividing p+1 as defined in Pairing Based IBE
mpz_t qminusone; // q-1

// Fp2mult variables
mpz_t Fp2tempa[2], Fp2tempb[2];
void initFp2()
{
	mpz_init(Fp2tempa[0]); mpz_init(Fp2tempa[1]);
	mpz_init(Fp2tempb[0]); mpz_init(Fp2tempb[1]);
}
void clearFp2()
{
	mpz_clear(Fp2tempa[0]); mpz_clear(Fp2tempa[1]);
	mpz_clear(Fp2tempb[0]); mpz_clear(Fp2tempb[1]);
}
// function to multiply two elements PF_p[i], c = a.b
void Fp2mult(mpz_t c[2], mpz_t a[2], mpz_t b[2])
{
	mpz_set(Fp2tempa[0],a[0]); mpz_set(Fp2tempa[1],a[1]);
	mpz_set(Fp2tempb[0],b[0]); mpz_set(Fp2tempb[1],b[1]);

	mpz_mul(c[0],Fp2tempa[0],Fp2tempb[0]);
	mpz_submul(c[0],Fp2tempa[1],Fp2tempb[1]);
	mpz_mod(c[0],c[0],p);

	mpz_mul(c[1],Fp2tempa[0],Fp2tempb[1]);
	mpz_addmul(c[1],Fp2tempa[1],Fp2tempb[0]);
	mpz_mod(c[1],c[1],p);
}

// function to return number of bits in an mpz_t, eg bits(4=0b100) = 3
unsigned int bits(mpz_t n)
{
	mpz_t m; mpz_init_set(m,n);
	unsigned int i=0;
	while(mpz_cmp_ui(m,0))
	{
		mpz_fdiv_q_2exp(m,m,1);
		i++;
	}
	mpz_clear(m);
	return i;
}

// ELLIPTIC ARITHMETIC FUNCTIONS
mpz_t lambda, lambda2, EAT1, EAT, EAR[2];
void initEAV()
{
	mpz_init(lambda); mpz_init(lambda2); mpz_init(EAT1); mpz_init(EAT); mpz_init(EAR[0]); mpz_init(EAR[1]);
}
void clearEAV()
{
	mpz_clear(lambda); mpz_clear(lambda2); mpz_clear(EAT1); mpz_clear(EAT); mpz_clear(EAR[0]); mpz_clear(EAR[1]);
}

// function to double a point (R=[2]P) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_double(mpz_t R[2], mpz_t P[2])
{
	if(mpz_cmp(P[0],p)==0) // if P = infinity
	{
		mpz_set(EAR[0],p);
		mpz_set(EAR[1],p);
	}
	else // if P != infinity
	{
		mpz_powm_ui(lambda,P[0],2,p);
		mpz_sub_ui(lambda,lambda,1);
		mpz_mul_ui(lambda,lambda,3);
		mpz_mul_2exp(EAT1,P[1],1);
		mpz_invert(EAT1,EAT1,p);
		mpz_mul(lambda,lambda,EAT1);
		mpz_mod(lambda,lambda,p); // lambda = (3.Px^2 - 3)/(2.Py)

		mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

		mpz_mul_2exp(EAT1,P[0],1);
		mpz_sub(EAR[0],lambda2,EAT1);
		mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - 2*Px

		mpz_sub(EAR[1],EAT1,lambda2);
		mpz_add(EAR[1],EAR[1],P[0]);
		mpz_mul(EAR[1],EAR[1],lambda);
		mpz_mod(EAR[1],EAR[1],p);
		mpz_sub(EAR[1],EAR[1],P[1]);
		mpz_mod(EAR[1],EAR[1],p); // Ry = (3*Px - lambda^2)*lambda - Py
	}
	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to add two points (R=P+Q) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_addition(mpz_t R[2], mpz_t P[2], mpz_t Q[2])
{
	// set EAT = -Q[1] mod p
	mpz_set(EAT,Q[1]);
	mpz_neg(EAT,EAT);
	mpz_mod(EAT,EAT,p);

	if(mpz_cmp(P[0],p)==0) // if P = point at infinity
	{
		mpz_set(EAR[0],Q[0]);
		mpz_set(EAR[1],Q[1]);
	}
	else if(mpz_cmp(Q[0],p)==0) // if Q = point at infinity
	{
		mpz_set(EAR[0],P[0]);
		mpz_set(EAR[1],P[1]);
	}
	else if(mpz_cmp(P[0],Q[0])==0 && mpz_cmp(P[1],EAT)==0) // if P = -Q
	{
		mpz_set(EAR[0],p);
		mpz_set(EAR[1],p);
	}
	else if(mpz_cmp(P[0],Q[0])) // if Px != Qx
	{
		mpz_sub(lambda,Q[1],P[1]);
		mpz_sub(EAT1,Q[0],P[0]);
		mpz_invert(EAT1,EAT1,p);
		mpz_mul(lambda,lambda,EAT1);
		mpz_mod(lambda,lambda,p); // lambda = (Qy-Py)/(Qx-Px)

		mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

		mpz_sub(EAR[0],lambda2,P[0]);
		mpz_sub(EAR[0],EAR[0],Q[0]);
		mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - Px - Qx

		mpz_sub(EAR[1],Q[0],lambda2);
		mpz_addmul_ui(EAR[1],P[0],2);
		mpz_mul(EAR[1],EAR[1],lambda);
		mpz_mod(EAR[1],EAR[1],p);
		mpz_sub(EAR[1],EAR[1],P[1]);
		mpz_mod(EAR[1],EAR[1],p); // Ry = ((2*Px + Py) - lambda^2)*lambda - Py
	}
	else // if Px = Qx but P != -Q
	{
		mpz_powm_ui(lambda,P[0],2,p);
		mpz_sub_ui(lambda,lambda,1);
		mpz_mul_ui(lambda,lambda,3);
		mpz_mul_2exp(EAT1,P[1],1);
		mpz_invert(EAT1,EAT1,p);
		mpz_mul(lambda,lambda,EAT1);
		mpz_mod(lambda,lambda,p); // lambda = (3.Px^2 - 3)/(2.Py)

		mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

		mpz_mul_2exp(EAT1,P[0],1);
		mpz_sub(EAR[0],lambda2,EAT1);
		mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - 2*Px

		mpz_sub(EAR[1],EAT1,lambda2);
		mpz_add(EAR[1],EAR[1],P[0]);
		mpz_mul(EAR[1],EAR[1],lambda);
		mpz_mod(EAR[1],EAR[1],p);
		mpz_sub(EAR[1],EAR[1],P[1]);
		mpz_mod(EAR[1],EAR[1],p); // Ry = (3*Px - lambda^2)*lambda - Py
	}
	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to double a point (R=[2]P) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_double_specific(mpz_t R[2], mpz_t P[2])
{
	mpz_powm_ui(lambda,P[0],2,p);
	mpz_sub_ui(lambda,lambda,1);
	mpz_mul_ui(lambda,lambda,3);
	mpz_mul_2exp(EAT1,P[1],1);
	mpz_invert(EAT1,EAT1,p);
	mpz_mul(lambda,lambda,EAT1);
	mpz_mod(lambda,lambda,p); // lambda = (3.Px^2 - 3)/(2.Py)

	mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

	mpz_mul_2exp(EAT1,P[0],1);
	mpz_sub(EAR[0],lambda2,EAT1);
	mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - 2*Px

	mpz_sub(EAR[1],EAT1,lambda2);
	mpz_add(EAR[1],EAR[1],P[0]);
	mpz_mul(EAR[1],EAR[1],lambda);
	mpz_mod(EAR[1],EAR[1],p);
	mpz_sub(EAR[1],EAR[1],P[1]);
	mpz_mod(EAR[1],EAR[1],p); // Ry = (3*Px - lambda^2)*lambda - Py

	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to add two points (R=P+Q) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_addition_specific(mpz_t R[2], mpz_t P[2], mpz_t Q[2])
{
	mpz_sub(lambda,Q[1],P[1]);
	mpz_sub(EAT1,Q[0],P[0]);
	mpz_invert(EAT1,EAT1,p);
	mpz_mul(lambda,lambda,EAT1);
	mpz_mod(lambda,lambda,p); // lambda = (Qy-Py)/(Qx-Px)

	mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

	mpz_sub(EAR[0],lambda2,P[0]);
	mpz_sub(EAR[0],EAR[0],Q[0]);
	mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - Px - Qx

	mpz_sub(EAR[1],Q[0],lambda2);
	mpz_addmul_ui(EAR[1],P[0],2);
	mpz_mul(EAR[1],EAR[1],lambda);
	mpz_mod(EAR[1],EAR[1],p);
	mpz_sub(EAR[1],EAR[1],P[1]);
	mpz_mod(EAR[1],EAR[1],p); // Ry = ((2*Px + Py) - lambda^2)*lambda - Py

	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to multiply a point on the curve by an mpz_t integer, R=[n]Q
void elliptic_mult(mpz_t R[2], mpz_t n, mpz_t Q[2])
{
	unsigned int m=bits(n)-1;
	unsigned int i;

	mpz_set(R[0],Q[0]);
	mpz_set(R[1],Q[1]);

	if(mpz_cmp_ui(n,0))
	{
		for(i=1;i<=m;i++)
		{
			elliptic_double(R,R);
			if(mpz_tstbit(n,m-i)==1) elliptic_addition(R,R,Q);
		}
	}
	else
	{
		mpz_set(R[0],p);
		mpz_set(R[1],p);
	}
}


//// function to raise elt a of FP_p (where a = B/A is F_p rep of A + iB) to power n
void Fp2power(mpz_t c, mpz_t a, mpz_t n)
{
	mpz_t p; mpz_init_set_str(p,"997ABB1F 0A563FDA 65C61198 DAD0657A 416C0CE1 9CB48261 BE9AE358 B3E01A2E F40AAB27 E2FC0F1B 228730D5 31A59CB0 E791B39F F7C88A19 356D27F4 A666A6D0 E26C6487 326B4CD4 512AC5CD 65681CE1 B6AFF4A8 31852A82 A7CF3C52 1C3C09AA 9F94D6AF 56971F1F FCE3E823 89857DB0 80C5DF10 AC7ACE87 666D807A FEA85FEB",16);
	mpz_t temp; mpz_init(temp);
	mpz_t A[2]; mpz_init_set_ui(A[0],1); mpz_init_set(A[1],a);
	mpz_t ans[2]; mpz_init_set_ui(ans[0],1); mpz_init_set(ans[1],a);

	unsigned int m=bits(n)-1;
	unsigned int i;

	if(mpz_cmp_ui(n,0))
	{
		for(i=1;i<=m;i++)
		{
			if(mpz_tstbit(n,m-i)==0) Fp2mult(ans,ans,ans);
			else
			{
				Fp2mult(ans,ans,ans);
				Fp2mult(ans,ans,A);
			}
		}
		mpz_invert(c,ans[0],p);
		mpz_mul(c,c,ans[1]);
		mpz_mod(c,c,p); // x = vy/vx where v = vx + i.vy
	}
	else mpz_set_ui(c,0);
	mpz_clear(temp);
	mpz_clear(ans[0]); mpz_clear(ans[1]);
	mpz_clear(A[0]); mpz_clear(A[1]);
}

jstring Java_dja_alice_Alice_power(JNIEnv * env, jobject thiz, jstring rin){
	const char * rs;
	rs = (*env)->GetStringUTFChars(env,rin,NULL);

	mpz_t g; mpz_init_set_str(g,"66FC2A432B6EA392148F15867D623068C6A87BD1FB94C41E27FABE658E015A87371E94744C96FEDA449AE9563F8BC446CBFDA85D5D00EF577072DA8F541721BEEE0FAED1828EAB90B99DFB0138C7843355DF0460B4A9FD74B4F1A32BCAFA1FFAD682C033A7942BCCE3720F20B9B7B0403C8CAE87B7A0042ACDE0FAB36461EA46",16);
	mpz_t r; mpz_init_set_str(r,rs,16);
	(*env)->ReleaseStringUTFChars(env,rin,rs);
	mpz_t gr; mpz_init(gr);

	Fp2power(gr,g,r);

	char * ret = malloc(mpz_sizeinbase(gr,16));
	gmp_sprintf(ret,"%Zx",gr);

	return (*env)->NewStringUTF(env,ret);
}

jstring Java_dja_alice_Alice_maths(JNIEnv * env, jobject thiz, jstring rin){
	initEAV();
	initFp2();

	// set value of p and q
	mpz_init_set_str(p,"997ABB1F 0A563FDA 65C61198 DAD0657A 416C0CE1 9CB48261 BE9AE358 B3E01A2E F40AAB27 E2FC0F1B 228730D5 31A59CB0 E791B39F F7C88A19 356D27F4 A666A6D0 E26C6487 326B4CD4 512AC5CD 65681CE1 B6AFF4A8 31852A82 A7CF3C52 1C3C09AA 9F94D6AF 56971F1F FCE3E823 89857DB0 80C5DF10 AC7ACE87 666D807A FEA85FEB",16);
	mpz_init_set_str(q,"265EAEC7 C2958FF6 99718466 36B4195E 905B0338 672D2098 6FA6B8D6 2CF8068B BD02AAC9 F8BF03C6 C8A1CC35 4C69672C 39E46CE7 FDF22286 4D5B49FD 2999A9B4 389B1921 CC9AD335 144AB173 595A0738 6DABFD2A 0C614AA0 A9F3CF14 870F026A A7E535AB D5A5C7C7 FF38FA08 E2615F6C 203177C4 2B1EB3A1 D99B601E BFAA17FB",16);
	// set qminusone
	mpz_init(qminusone); mpz_sub_ui(qminusone,q,1);

	mpz_t r; mpz_init_set_str(r,"146812802e82d50f25eea39f75e4e91a3e44619af7ae649fe113cb65d2b32e84530a18fae0fefc62757628f62f8040597840fff4a517a7c7f3f7e696ab38f05377e4851ad8294152aaeb6ffcce2114256eb96269757731db75868cceacf1202cf2263a77e7f4fa59986152b4c7a555065a3290770c86f3bb8ade405c526ed54b",16);
	mpz_t b; mpz_init_set_str(b,"323031302d30370074656c3a2b34343132333435363738393000",16);

	mpz_t P[2];
	mpz_init_set_str(P[0],"53FC09EE332C29AD0A7990053ED9B52A2B1A2FD60AEC69C698B2F204B6FF7CBFB5EDB6C0F6CE2308AB10DB9030B09E1043D5F22CDB9DFA55718BD9E7406CE8909760AF765DD5BCCB337C86548B72F2E1A702C3397A60DE74A7C1514DBA66910DD5CFB4CC80728D87EE9163A5B63F73EC80EC46C4967E0979880DC8ABEAE63895",16);
	mpz_init_set_str(P[1],"0A8249063F6009F1F9F1F0533634A135D3E82016029906963D778D821E141178F5EA69F4654EC2B9E7F7F5E5F0DE55F66B598CCF9A140B2E416CFF0CA9E032B970DAE117AD547C6CCAD696B5B7652FE0AC6F1E80164AA989492D979FC5A4D5F213515AD7E9CB99A980BDAD5AD5BB4636ADB9B5706A67DCDE75573FD71BEF16D7",16);

	mpz_t Z[2];
	mpz_init_set_str(Z[0],"5958EF1B1679BF099B3A030DF255AA6A23C1D8F143D4D23F753E69BD27A832F38CB4AD53DDEF4260B0FE8BB45C4C1FF510EFFE300367A37B61F701D914AEF09724825FA0707D61A6DFF4FBD7273566CDDE352A0B04B7C16A78309BE640697DE747613A5FC195E8B9F328852A579DB8F99B1D0034479EA9C5595F47C4B2F54FF2",16);
	mpz_init_set_str(Z[1],"1508D37514DCF7A8E143A6058C09A6BF2C9858CA37C258065AE6BF7532BC8B5B63383866E0753C5AC0E72709F8445F2E6178E065857E0EDA10F68206B63505ED87E534FB2831FF957FB7DC619DAE61301EEACC2FDA3680EA4999258A833CEA8FC67C6D19487FB449059F26CC8AAB655AB58B7CC796E24E9A394095754F5F8BAE",16);



	mpz_t bP[2];
	mpz_init(bP[0]);mpz_init(bP[1]);

	mpz_t bPZ[2];
	mpz_init(bPZ[0]);mpz_init(bPZ[1]);

	mpz_t R[2];
	mpz_init(R[0]);mpz_init(R[1]);

	elliptic_mult(bP, b, P);
	elliptic_addition(bPZ, bP, Z);
	elliptic_mult(R, r, bPZ);

	clearEAV();
	clearFp2();

	char * ret = malloc(mpz_sizeinbase(R[0],16));
	gmp_sprintf(ret,"%Zx",R[0]);

	return (*env)->NewStringUTF(env,ret);
}
