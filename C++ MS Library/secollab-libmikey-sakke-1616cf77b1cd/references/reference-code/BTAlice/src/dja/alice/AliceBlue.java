package dja.alice;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Random;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class AliceBlue extends Activity {
	TextView tv;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		tv = ((TextView)findViewById(R.id.textview));
		
		long stime = System.nanoTime();
		long btime = System.nanoTime();
		
		//check for support
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		if(adapter==null) {debug("Bluetooth is not available");return;}
		
		//check enabled
		if(!adapter.isEnabled()){
			Intent btenable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(btenable,3);
		}
		
		BluetoothDevice bob = null;
//		for(BluetoothDevice d:  adapter.getBondedDevices()){
//			if(d.getName()=="Bob") {bob = d;break;}
//		}
		
		bob = adapter.getBondedDevices().iterator().next();
		
		if(bob==null) {debug("Can't find Bob");return;}
		

		final UUID myuuid = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
		BluetoothSocket sock = null;
		try {
			sock = bob.createRfcommSocketToServiceRecord(myuuid);
		} catch (IOException e) {debug("Socket setup");}
		
		try {
			sock.connect();
		} catch (IOException e1) {debug("Socket connect");}
		
		
		tv.append("Bluetooth setup: "+(System.nanoTime()-btime)/1000000000.0+"s");
		
		long time = System.nanoTime();
		
		// SAKKE input variables
		String b = "323031312d30320074656c3a2b34343737303039303031323300";
		String Z = "045958EF1B1679BF099B3A030DF255AA6A23C1D8F143D4D23F753E69BD27A832F38CB4AD53DDEF4260B0FE8BB45C4C1FF510EFFE300367A37B61F701D914AEF09724825FA0707D61A6DFF4FBD7273566CDDE352A0B04B7C16A78309BE640697DE747613A5FC195E8B9F328852A579DB8F99B1D0034479EA9C5595F47C4B2F54FF21508D37514DCF7A8E143A6058C09A6BF2C9858CA37C258065AE6BF7532BC8B5B63383866E0753C5AC0E72709F8445F2E6178E065857E0EDA10F68206B63505ED87E534FB2831FF957FB7DC619DAE61301EEACC2FDA3680EA4999258A833CEA8FC67C6D19487FB449059F26CC8AAB655AB58B7CC796E24E9A394095754F5F8BAE";

		// ECCSI input variables
		byte[] HS = {(byte)0x49,(byte)0x0f,(byte)0x3f,(byte)0xeb,(byte)0xbc,(byte)0x1c,(byte)0x90,(byte)0x2f,(byte)0x62,(byte)0x89,(byte)0x72,(byte)0x3d,(byte)0x7f,(byte)0x8c,(byte)0xbf,(byte)0x79,(byte)0xdb,(byte)0x88,(byte)0x93,(byte)0x08,(byte)0x49,(byte)0xd1,(byte)0x9f,(byte)0x38,(byte)0xf0,(byte)0x29,(byte)0x5b,(byte)0x5c,(byte)0x27,(byte)0x6c,(byte)0x14,(byte)0xd1};
		byte[] SSK = {(byte)0x23,(byte)0xf3,(byte)0x74,(byte)0xae,(byte)0x1f,(byte)0x40,(byte)0x33,(byte)0xf3,(byte)0xe9,(byte)0xdb,(byte)0xdd,(byte)0xaa,(byte)0xef,(byte)0x20,(byte)0xf4,(byte)0xcf,(byte)0x0b,(byte)0x86,(byte)0xbb,(byte)0xd5,(byte)0xa1,(byte)0x38,(byte)0xa5,(byte)0xae,(byte)0x9e,(byte)0x7e,(byte)0x00,(byte)0x6b,(byte)0x34,(byte)0x48,(byte)0x9a,(byte)0x0d};
		byte[] PVT = {(byte)0x04,(byte)0x75,(byte)0x8a,(byte)0x14,(byte)0x27,(byte)0x79,(byte)0xbe,(byte)0x89,(byte)0xe8,(byte)0x29,(byte)0xe7,(byte)0x19,(byte)0x84,(byte)0xcb,(byte)0x40,(byte)0xef,(byte)0x75,(byte)0x8c,(byte)0xc4,(byte)0xad,(byte)0x77,(byte)0x5f,(byte)0xc5,(byte)0xb9,(byte)0xa3,(byte)0xe1,(byte)0xc8,(byte)0xed,(byte)0x52,(byte)0xf6,(byte)0xfa,(byte)0x36,(byte)0xd9,(byte)0xa7,(byte)0x9d,(byte)0x24,(byte)0x76,(byte)0x92,(byte)0xf4,(byte)0xed,(byte)0xa3,(byte)0xa6,(byte)0xbd,(byte)0xab,(byte)0x77,(byte)0xd6,(byte)0xaa,(byte)0x64,(byte)0x74,(byte)0xa4,(byte)0x64,(byte)0xae,(byte)0x49,(byte)0x34,(byte)0x66,(byte)0x3c,(byte)0x52,(byte)0x65,(byte)0xba,(byte)0x70,(byte)0x18,(byte)0xba,(byte)0x09,(byte)0x1f,(byte)0x79};
//		byte[] ID = {(byte)0x32,(byte)0x30,(byte)0x31,(byte)0x31,(byte)0x2d,(byte)0x30,(byte)0x32,(byte)0x00,(byte)0x74,(byte)0x65,(byte)0x6c,(byte)0x3a,(byte)0x2b,(byte)0x34,(byte)0x34,(byte)0x37,(byte)0x37,(byte)0x30,(byte)0x30,(byte)0x39,(byte)0x30,(byte)0x30,(byte)0x31,(byte)0x32,(byte)0x33,(byte)0x00};
//		byte[] KPAK = {(byte)0x04,(byte)0x50,(byte)0xd4,(byte)0x67,(byte)0x0b,(byte)0xde,(byte)0x75,(byte)0x24,(byte)0x4f,(byte)0x28,(byte)0xd2,(byte)0x83,(byte)0x8a,(byte)0x0d,(byte)0x25,(byte)0x55,(byte)0x8a,(byte)0x7a,(byte)0x72,(byte)0x68,(byte)0x6d,(byte)0x45,(byte)0x22,(byte)0xd4,(byte)0xc8,(byte)0x27,(byte)0x3f,(byte)0xb6,(byte)0x44,(byte)0x2a,(byte)0xeb,(byte)0xfa,(byte)0x93,(byte)0xdb,(byte)0xdd,(byte)0x37,(byte)0x55,(byte)0x1a,(byte)0xfd,(byte)0x26,(byte)0x3b,(byte)0x5d,(byte)0xfd,(byte)0x61,(byte)0x7f,(byte)0x39,(byte)0x60,(byte)0xc6,(byte)0x5a,(byte)0x8c,(byte)0x29,(byte)0x88,(byte)0x50,(byte)0xff,(byte)0x99,(byte)0xf2,(byte)0x03,(byte)0x66,(byte)0xdc,(byte)0xe7,(byte)0xd4,(byte)0x36,(byte)0x72,(byte)0x17,(byte)0xf4};
//		byte[] sakkepayload = {(byte)0x6d,(byte)0x65,(byte)0x73,(byte)0x73,(byte)0x61,(byte)0x67,(byte)0x65,(byte)0x00};

		// create SAKKE payload
		long ctime = System.nanoTime();
		time=System.nanoTime();
		byte[] payload = sakke(b,Z);
		tv.append("SAKKE: "+(System.nanoTime()-time)/1000000000.0+"s");
		
		// create signature and append to payload
		time=System.nanoTime();
		byte[] signedpayload = sign(SSK,PVT,HS,payload);
		tv.append("\nSigning: "+(System.nanoTime()-time)/1000000000.0+"s");

		tv.append("\nPackage creation: "+(System.nanoTime()-ctime)/1000000000.0+"s");

		send(sock, signedpayload);
		
		tv.append("\nTotal: "+(System.nanoTime()-stime)/1000000000.0+"s");
		
	}
	
	private byte[] sakke(String b, String Z){
		int n = 128;
		BigInteger q = new BigInteger("265EAEC7C2958FF69971846636B4195E905B0338672D20986FA6B8D62CF8068BBD02AAC9F8BF03C6C8A1CC354C69672C39E46CE7FDF222864D5B49FD2999A9B4389B1921CC9AD335144AB173595A07386DABFD2A0C614AA0A9F3CF14870F026AA7E535ABD5A5C7C7FF38FA08E2615F6C203177C42B1EB3A1D99B601EBFAA17FB",16);

		// select random SSV,jbyteArray HSin, jbyteArray sakkepayloadin E [0,2^n]
//		BigInteger SSV = new BigInteger("123456789ABCDEF0123456789ABCDEF0",16);
		BigInteger SSV = new BigInteger(n,new Random());
		tv.append("\nSSV: "+SSV.toString(16)+"\n");

		// r = HTIR(SSV||b,q,hash)
		BigInteger r = HTIR.htir(SSV.toString(16)+b, q);
		
		//R(b,s)
		long time = System.nanoTime();
		byte[] Rbs = muladdmul(r.toString(16),b,Z);
		tv.append("SAKKE EC multiplication: "+(System.nanoTime()-time)/1000000000.0+"s\n");

		// H = SSV xor HTIR(g^r,2^n,hash)n
		BigInteger gr = new BigInteger(power(r.toString(16)),16);
				
		byte[] H = SSV.xor(HTIR.htir(gr.toString(16), BigInteger.ONE.add(BigInteger.ONE).pow(n))).toByteArray();
		if(H.length==17&&H[0]==0){
			byte [] ret = new byte[16];
			for(int i=0;i<16;i++){
				ret[i]=H[i+1];
			}
			H= ret;
		}

		byte[] payload = new byte[Rbs.length+H.length];
		for(int i=0;i<Rbs.length;i++)payload[i]=Rbs[i];
		for(int i=0;i<H.length;i++)payload[Rbs.length+i]=H[i];

		return payload;
	}

	private byte[] sign(byte[] SSK, byte[] PVT, byte[] HS, byte[] payload){
//		byte[] HS = {(byte)0x49,(byte)0x0f,(byte)0x3f,(byte)0xeb,(byte)0xbc,(byte)0x1c,(byte)0x90,(byte)0x2f,(byte)0x62,(byte)0x89,(byte)0x72,(byte)0x3d,(byte)0x7f,(byte)0x8c,(byte)0xbf,(byte)0x79,(byte)0xdb,(byte)0x88,(byte)0x93,(byte)0x08,(byte)0x49,(byte)0xd1,(byte)0x9f,(byte)0x38,(byte)0xf0,(byte)0x29,(byte)0x5b,(byte)0x5c,(byte)0x27,(byte)0x6c,(byte)0x14,(byte)0xd1};
//		byte[] sakkepayload = {(byte)0x6d,(byte)0x65,(byte)0x73,(byte)0x73,(byte)0x61,(byte)0x67,(byte)0x65,(byte)0x00};
//		byte[] SSK = {(byte)0x23,(byte)0xf3,(byte)0x74,(byte)0xae,(byte)0x1f,(byte)0x40,(byte)0x33,(byte)0xf3,(byte)0xe9,(byte)0xdb,(byte)0xdd,(byte)0xaa,(byte)0xef,(byte)0x20,(byte)0xf4,(byte)0xcf,(byte)0x0b,(byte)0x86,(byte)0xbb,(byte)0xd5,(byte)0xa1,(byte)0x38,(byte)0xa5,(byte)0xae,(byte)0x9e,(byte)0x7e,(byte)0x00,(byte)0x6b,(byte)0x34,(byte)0x48,(byte)0x9a,(byte)0x0d};
//		byte[] PVT = {(byte)0x04,(byte)0x75,(byte)0x8a,(byte)0x14,(byte)0x27,(byte)0x79,(byte)0xbe,(byte)0x89,(byte)0xe8,(byte)0x29,(byte)0xe7,(byte)0x19,(byte)0x84,(byte)0xcb,(byte)0x40,(byte)0xef,(byte)0x75,(byte)0x8c,(byte)0xc4,(byte)0xad,(byte)0x77,(byte)0x5f,(byte)0xc5,(byte)0xb9,(byte)0xa3,(byte)0xe1,(byte)0xc8,(byte)0xed,(byte)0x52,(byte)0xf6,(byte)0xfa,(byte)0x36,(byte)0xd9,(byte)0xa7,(byte)0x9d,(byte)0x24,(byte)0x76,(byte)0x92,(byte)0xf4,(byte)0xed,(byte)0xa3,(byte)0xa6,(byte)0xbd,(byte)0xab,(byte)0x77,(byte)0xd6,(byte)0xaa,(byte)0x64,(byte)0x74,(byte)0xa4,(byte)0x64,(byte)0xae,(byte)0x49,(byte)0x34,(byte)0x66,(byte)0x3c,(byte)0x52,(byte)0x65,(byte)0xba,(byte)0x70,(byte)0x18,(byte)0xba,(byte)0x09,(byte)0x1f,(byte)0x79};
//		byte[] ID = {(byte)0x32,(byte)0x30,(byte)0x31,(byte)0x31,(byte)0x2d,(byte)0x30,(byte)0x32,(byte)0x00,(byte)0x74,(byte)0x65,(byte)0x6c,(byte)0x3a,(byte)0x2b,(byte)0x34,(byte)0x34,(byte)0x37,(byte)0x37,(byte)0x30,(byte)0x30,(byte)0x39,(byte)0x30,(byte)0x30,(byte)0x31,(byte)0x32,(byte)0x33,(byte)0x00};
//		byte[] KPAK = {(byte)0x04,(byte)0x50,(byte)0xd4,(byte)0x67,(byte)0x0b,(byte)0xde,(byte)0x75,(byte)0x24,(byte)0x4f,(byte)0x28,(byte)0xd2,(byte)0x83,(byte)0x8a,(byte)0x0d,(byte)0x25,(byte)0x55,(byte)0x8a,(byte)0x7a,(byte)0x72,(byte)0x68,(byte)0x6d,(byte)0x45,(byte)0x22,(byte)0xd4,(byte)0xc8,(byte)0x27,(byte)0x3f,(byte)0xb6,(byte)0x44,(byte)0x2a,(byte)0xeb,(byte)0xfa,(byte)0x93,(byte)0xdb,(byte)0xdd,(byte)0x37,(byte)0x55,(byte)0x1a,(byte)0xfd,(byte)0x26,(byte)0x3b,(byte)0x5d,(byte)0xfd,(byte)0x61,(byte)0x7f,(byte)0x39,(byte)0x60,(byte)0xc6,(byte)0x5a,(byte)0x8c,(byte)0x29,(byte)0x88,(byte)0x50,(byte)0xff,(byte)0x99,(byte)0xf2,(byte)0x03,(byte)0x66,(byte)0xdc,(byte)0xe7,(byte)0xd4,(byte)0x36,(byte)0x72,(byte)0x17,(byte)0xf4};

		byte[] signature = eccsisign(SSK, PVT, HS, payload);

		byte[] packet = new byte[payload.length+signature.length];
		for(int i=0;i<payload.length;i++)packet[i]=payload[i];
		for(int i=0;i<signature.length;i++)packet[payload.length+i]=signature[i];
		
		return packet; 
	}
	
	private void send(BluetoothSocket sock, byte[] packet) {
		OutputStream outstream = null;
		try {
			outstream = sock.getOutputStream();
		} catch (IOException e) {debug("outstream setup");}
		try {
			outstream.write(packet);
		} catch (IOException e) {debug("outstream write");}
		try {
			sock.close();
		} catch (IOException e) {debug("socket close");}
		
		
//		try{
////			InetAddress serverAddr = InetAddress.getByName("192.168.1.2");
//			InetAddress serverAddr = InetAddress.getByName("10.0.2.2");
//			DatagramSocket dgs = new DatagramSocket();
//			DatagramPacket dgp = new DatagramPacket(packet,packet.length,serverAddr,4444);
//			dgs.send(dgp);
//		}
//		catch (Exception e){Log.e("UDP", "C:Error",e);}
	}
	
	private void debug(String in){
		Toast.makeText(this, in, Toast.LENGTH_LONG).show();
		finish();
	}
	

	public native String power(String arg);
	public native byte[] eccsisign(byte[] SSKin, byte[] PVTin, byte[] HSin, byte[] sakkepayloadin);
	public native byte[] muladdmul(String arg,String b,String Z);

	static {
		System.loadLibrary("crypto");
		System.loadLibrary("mygmp");
	}
}