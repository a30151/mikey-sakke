#!/bin/bash

ROOT_DIR=$PWD

LIB_DIR=$PWD/libraries
XCODE_ROOT=$(xcode-select -print-path)
IOS_C_COMPILER=$XCODE_ROOT/Platforms/iPhoneOS.platform/Developer/usr/bin/gcc
IOS_CXX_COMPILER=$XCODE_ROOT/Platforms/iPhoneOS.platform/Developer/usr/bin/g++
IOS_ARCHITECTURE=armv7
IOS_SDK=$XCODE_ROOT/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS6.0.sdk
IOS_LINKER=$XCODE_ROOT/Platforms/iPhoneOS.platform/Developer/usr/bin/ld

SIM_LIB_DIR=$LIB_DIR/simulator
SIM_C_COMPILER=$XCODE_ROOT/usr/bin/gcc
SIM_CXX_COMPILER=$XCODE_ROOT/usr/bin/g++
SIM_ARCHITECTURE=i386
SIM_SDK=$XCODE_ROOT/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.0.sdk
SIM_LINKER=$XCODE_ROOT/usr/bin/ld

IOS_PLATFORM=ios
SIM_PLATFORM=sim
ALL_PLATFORMS=all

if [ $# -ne 1 ]; then
   PLATFORM=$ALL_PLATFORMS
elif [ $1 = $IOS_PLATFORM -o $1 = $SIM_PLATFORM -o $1 = $ALL_PLATFORMS ]; then
   PLATFORM=$1
else
   echo Unknown platform option: $@
   exit 1
fi

function copylib {
   if [ $# -ne 2 ]; then
      echo Too many arguments to copylib: $@
      exit 1
   fi

   if [ ! -e $1 ]; then
      echo "Error expected library $1 has not been built"
      exit 1
   else
      cp $1 $2
   fi
}

function build_xcode {
   cd $1
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      xcodebuild -configuration Debug -arch $IOS_ARCHITECTURE -sdk iphoneos6.0 clean build
      copylib build/Debug-iphoneos/$2 $LIB_DIR
   fi
 
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      xcodebuild -configuration Debug -arch $SIM_ARCHITECTURE -sdk iphonesimulator6.0 clean build
      copylib build/Debug-iphonesimulator/$2 $SIM_LIB_DIR
   fi
   cd $ROOT_DIR
}

function fetch {
   uri=$1
   file=${uri##*/}
   dir=${file%%.tar.*}
   if [ ! -d $dir ]; then
      if [ ! -e $file ]; then
         curl -L0 $uri > $file
      fi
      tar xvfz $file
   fi
}

function setup_prerequisites {
   cd $ROOT_DIR
   if [ ! -d $LIB_DIR ]; then
      mkdir $LIB_DIR
   fi
   if [ ! -d $SIM_LIB_DIR ]; then
      mkdir $SIM_LIB_DIR
   fi
   if [[ $(xcode-select -print-path)  = /Developer* ]]; then
      xcode-select -switch $XCODE_ROOT
   fi
}

function build_openssl {
   cd $ROOT_DIR
   fetch http://www.openssl.org/source/openssl-1.0.1c.tar.gz
   patch -Np0 < patches/openssl.1.patch
   cd ${ROOT_DIR}/openssl-1.0.1c
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      export CC=$IOS_C_COMPILER -arch $IOS_ARCHITECTURE
      ./Configure --openssldir=iPhone-armv7 BSD-generic32 "-isysroot $IOS_SDK -arch $IOS_ARCHITECTURE"
      make
      copylib libcrypto.a $LIB_DIR
      copylib libssl.a $LIB_DIR
   fi

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      make clean
      export CC=$SIM_C_COMPILER -arch $SIM_ARCHITECTURE
      ./Configure --openssldir=iPhone-armv7 BSD-generic32 "-isysroot $SIM_SDK -arch $SIM_ARCHITECTURE"
      make
      copylib libcrypto.a $SIM_LIB_DIR
      copylib libssl.a $SIM_LIB_DIR
   fi
   unset CC
}

function build_gmp {
   cd $ROOT_DIR
   fetch http://ftp.gnu.org/gnu/gmp/gmp-5.0.4.tar.bz2
   cd ${ROOT_DIR}/gmp-5.0.4

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      ./configure CC=$IOS_C_COMPILER CXX=$IOS_CXX_COMPILER CPPFLAGS="-isysroot $IOS_SDK -arch $IOS_ARCHITECTURE -DNO_ASM" LD=$IOS_LINKER --disable-shared --enable-static --host=none-apple-darwin10 --enable-cxx
      make
      copylib .libs/libgmp.a $LIB_DIR
      copylib .libs/libgmpxx.a $LIB_DIR
   fi

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      make clean
      ./configure CC=$SIM_C_COMPILER CXX=$SIM_CXX_COMPILER CPPFLAGS="-isysroot $SIM_SDK -arch $SIM_ARCHITECTURE -DNO_ASM" LD=$SIM_LINKER --disable-shared --enable-static --host=none-apple-darwin10 --enable-cxx
      make
      copylib .libs/libgmp.a $SIM_LIB_DIR
      copylib .libs/libgmpxx.a $SIM_LIB_DIR
   fi
}

function build_boost {
   cd $ROOT_DIR
   fetch http://downloads.sourceforge.net/project/boost/boost/1.49.0/boost_1_49_0.tar.bz2
   cd ${ROOT_DIR}/boost_1_49_0
   patch -Np1 < ../patches/boost_xcode_43.patch
   cd ${ROOT_DIR}
   ./build_boost
   copylib boost_1_49_0/ios/build/armv7/libboost_date_time.a $LIB_DIR
   copylib boost_1_49_0/ios/build/armv7/libboost_filesystem.a $LIB_DIR
   copylib boost_1_49_0/ios/build/armv7/libboost_system.a $LIB_DIR
   copylib boost_1_49_0/ios/build/i386/libboost_date_time.a $SIM_LIB_DIR
   copylib boost_1_49_0/ios/build/i386/libboost_filesystem.a $SIM_LIB_DIR
   copylib boost_1_49_0/ios/build/i386/libboost_system.a $SIM_LIB_DIR
}

function build_curl {
   cd $ROOT_DIR
   fetch http://curl.haxx.se/download/curl-7.25.0.tar.bz2
   cd ${ROOT_DIR}/curl-7.25.0
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      export CC=$IOS_C_COMPILER
      export CXX=$IOS_CXX_COMPILER
      export CFLAGS="-isysroot $IOS_SDK -arch $IOS_ARCHITECTURE -I${ROOT_DIR}/openssl-1.0.1c/include"
      export LDFLAGS=-L${LIB_DIR}
      ./configure --disable-shared --host=none-apple-darwin10 --enable-static
      make
      copylib lib/.libs/libcurl.a $LIB_DIR
   fi

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      make clean
      export CC=$SIM_C_COMPILER
      export CXX=$SIM_CXX_COMPILER
      export CFLAGS="-isysroot $SIM_SDK -arch $SIM_ARCHITECTURE -I${ROOT_DIR}/openssl-1.0.1c/include"
      export LDFLAGS=-L${SIM_LIB_DIR}
      ./configure --disable-shared --host=none-apple-darwin10 --enable-static
      make
      copylib lib/.libs/libcurl.a $SIM_LIB_DIR
   fi
   unset CC CXX CFLAGS LDFLAGS
}

function build_mikey_sakke {
   cd ${ROOT_DIR}/MikeySakke
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      xcodebuild -configuration Debug -arch $IOS_ARCHITECTURE -sdk iphoneos6.0 HEADER_SEARCH_PATHS="${ROOT_DIR}/openssl-1.0.1c/include ${ROOT_DIR}/gmp-5.0.4 ${ROOT_DIR}/boost_1_49_0 ${ROOT_DIR}/curl-7.25.0/include" LIBRARY_SEARCH_PATHS=$LIB_DIR clean build
      copylib build/Debug-iphoneos/libMikeySakke.a $LIB_DIR
   fi

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      xcodebuild -configuration Debug -arch $SIM_ARCHITECTURE -sdk iphonesimulator6.0 HEADER_SEARCH_PATHS="${ROOT_DIR}/openssl-1.0.1c/include ${ROOT_DIR}/gmp-5.0.4 ${ROOT_DIR}/boost_1_49_0 ${ROOT_DIR}/curl-7.25.0/include" LIBRARY_SEARCH_PATHS=$SIM_LIB_DIR clean build
      copylib build/Debug-iphonesimulator/libMikeySakke.a $SIM_LIB_DIR
   fi
}

function build_speex {
   build_xcode ${ROOT_DIR}/minisip-iphone/speex/Xcode/SpeexCodec libSpeexCodec.a
}

function build_ltdl {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmutil/libltdl/Xcode/ltdl libltdl.a
}

function build_dlopen {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmutil/libltdl/Xcode/dlopen libdlopen.a
}

function build_mcrypto {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmcrypto/Xcode/mcrypto libmcrypto.a
}

function build_mcrypto_uuid {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmcrypto/source/uuid/Xcode/mcrypto_uuid libmcrypto_uuid.a
}

function build_mcrypto_openssl {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmcrypto/source/openssl/Xcode/mcrypto_openssl libmcrypto_openssl.a
}

function build_mikey {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmikey/Xcode/mikey libmikey.a
}

function build_minisip {
   build_xcode ${ROOT_DIR}/minisip-iphone/libminisip/Xcode/minisip libminisip.a
}

function build_mnetutil {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmnetutil/Xcode/mnetutil libmnetutil.a
}

function build_udns {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmnetutil/Xcode/udns libudns.a
}

function build_msip {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmsip/Xcode/msip libmsip.a
}

function build_mstun {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmstun/Xcode/mstun libmstun.a
}

function build_mutil {
   build_xcode ${ROOT_DIR}/minisip-iphone/libmutil/Xcode/mutil libmutil.a
}

function build_zlib {
   cd $ROOT_DIR
   fetch http://zlib.net/zlib-1.2.7.tar.bz2
   cd ${ROOT_DIR}/zlib-1.2.7
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      export CC=$IOS_C_COMPILER
      export CFLAGS="-isysroot $IOS_SDK -arch $IOS_ARCHITECTURE -I${ROOT_DIR}/openssl-1.0.1c/include"
      export LDFLAGS=-L${LIB_DIR}
      ./configure
      make
      copylib libz.a $LIB_DIR
   fi

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      make clean
      export CC=$SIM_C_COMPILER
      export CFLAGS="-isysroot $SIM_SDK -arch $SIM_ARCHITECTURE -I${ROOT_DIR}/openssl-1.0.1c/include"
      export LDFLAGS=-L${SIM_LIB_DIR}
      ./configure
      make
      copylib libz.a $SIM_LIB_DIR
   fi
   unset CC CFLAGS LDFLAGS
}

function build_ltdlc {
   cd ${ROOT_DIR}/minisip-iphone/libmutil
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      autoreconf -fvi
      ./configure CC=$IOS_C_COMPILER CXX=$IOS_CXX_COMPILER CPPFLAGS="-isysroot $IOS_SDK -arch $IOS_ARCHITECTURE -DNO_ASM" LD=$IOS_LINKER --disable-shared --enable-static --host=none-apple-darwin10 --enable-cxx CFLAGS="-isysroot $IOS_SDK -arch $IOS_ARCHITECTURE -I${ROOT_DIR}/minisip-iphone/libmutil/libltdl/Xcode/ltdl/ltdl" LDFLAGS="-L${LIB_DIR}"
      make
      copylib libltdl/.libs/libltdlc.a $LIB_DIR
   fi

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      make clean
      ./configure CC=$SIM_C_COMPILER CXX=$SIM_CXX_COMPILER CPPFLAGS="-isysroot $SIM_SDK -arch $SIM_ARCHITECTURE -DNO_ASM" LD=$SIM_LINKER --disable-shared --enable-static --host=none-apple-darwin10 --enable-cxx CFLAGS="-isysroot $SIM_SDK -arch $SIM_ARCHITECTURE -I${ROOT_DIR}/minisip-iphone/libmutil/libltdl/Xcode/ltdl/ltdl" LDFLAGS="-L${SIM_LIB_DIR}"
      make
      copylib libltdl/.libs/libltdlc.a $SIM_LIB_DIR
   fi
}

function build_opencore_amr {
   cd ${ROOT_DIR}/minisip-iphone/opencore-amr-0.1.3
   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $IOS_PLATFORM ]; then
      ./configure CC=$IOS_C_COMPILER CXX=$IOS_CXX_COMPILER CPPFLAGS="-isysroot $IOS_SDK -arch $IOS_ARCHITECTURE -DNO_ASM" LD=$IOS_LINKER --disable-shared --enable-static --host=none-apple-darwin10
      make
      copylib amrnb/.libs/libopencore-amrnb.a $LIB_DIR
   fi

   if [ $PLATFORM = $ALL_PLATFORMS -o $PLATFORM = $SIM_PLATFORM ]; then
      make clean
      ./configure CC=$SIM_C_COMPILER CXX=$SIM_CXX_COMPILER CPPFLAGS="-isysroot $SIM_SDK -arch $SIM_ARCHITECTURE -DNO_ASM" LD=$SIM_LINKER --disable-shared --enable-static --host=none-apple-darwin10
      make
      copylib amrnb/.libs/libopencore-amrnb.a $SIM_LIB_DIR
   fi
}

  
setup_prerequisites
build_openssl
build_gmp
build_boost
build_curl
build_mikey_sakke
build_speex
build_ltdl
build_dlopen
build_mcrypto
build_mcrypto_uuid
build_mcrypto_openssl
build_mikey
build_minisip
build_mnetutil
build_udns
build_msip
build_mstun
build_mutil
build_zlib
build_ltdlc
build_opencore_amr

