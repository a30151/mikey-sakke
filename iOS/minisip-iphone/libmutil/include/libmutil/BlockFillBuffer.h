#ifndef BLOCK_FILL_BUFFER_H
#define BLOCK_FILL_BUFFER_H

#include <vector>
#include "stl-pointer-from-iterator.h"

/**
 * Provides a contiguous block of memory supporting block-reserve,
 * block-fill and block-fetch operations with circular queue
 * semantics.
 *
 * XXX: Note: this only supports forward filling and iteration.  To
 * support random-access fill and fetch the implementation should be
 * enhanced with an additional vector of size maxBlockCount containing
 * a state marker for each block rather than just two iterators fetch
 * and fill into memory.
 */
template <typename T>
class BlockFillBuffer
{
   typedef typename std::vector<T>::const_iterator const_iterator;
   typedef typename std::vector<T>::iterator       iterator;

public:

   BlockFillBuffer(size_t blockSize, size_t maxBlockCount)
      : memory(blockSize * maxBlockCount)
      , blockSize(blockSize)
      , inuse(0)
      , inuse_end(memory.begin())
      , fill_begin(inuse_end)
      , fetch_begin(inuse_end)
   {
   }

public:

   /**
    * Returns the block size for this buffer.  Any non-zero pointer
    * returned from other members is a pointer to this many contiguous
    * elements of type T.
    */
   size_t getBlockSize() const { return blockSize; }

   /**
    * Reset the buffer.
    */
   void clear()
   {
      fetch_begin = fill_begin = inuse_end = memory.begin();
      inuse = 0;
   }

   /**
    * Returns a pointer to the start of a block to fill or nullptr if
    * there are no free blocks available.
    */
   T* reserveBlock()
   {
      if (inuse == memory.size())
         return 0;
      inuse += blockSize;
      return advance(inuse_end);
   }

   /**
    * Indicate that the oldest reserved block has been filled and
    * return a pointer to it.  If no blocks are reserved nullptr is
    * returned.
    */
   T* commitBlock()
   {
      if (fill_begin == inuse_end)
         return 0;
      return advance(fill_begin);
   }

   /**
    * Fetch a pointer to the first filled block or nullptr if there
    * are no filled blocks available.  If a block is returned, that
    * block is also made available for reuse.  In order to guarantee
    * integrity of the block pointed to by the return value, any
    * access to the block must be made before any subsequent operation
    * on this object.
    */
   T* fetchAndReleaseBlock()
   {
      if (fetch_begin == fill_begin)
         return 0;
      inuse -= blockSize;
      return advance(fetch_begin);
   }

private:

   /**
    * Advance the given iterator by the block size and return a
    * pointer to the block immediately preceding the new position.
    */
   T* advance(iterator& it)
   {
      if (it == memory.end())
         it = memory.begin();

      T* rc = STL_POINTER_FROM_ITERATOR(it);
      it += blockSize;
      return rc;
   }

private:

   std::vector<T> memory; ///< allocated storage
   size_t blockSize;      ///< number of elements in a block
   size_t inuse;          ///< total number of elements in use
   iterator inuse_end;    ///< end of last in-use block
   iterator fill_begin;   ///< start of first block to be filled (extends to inuse_end)
   iterator fetch_begin;  ///< start of first filled block to fetch (extends to fill_begin)
};

#endif//BLOCK_FILL_BUFFER_H

