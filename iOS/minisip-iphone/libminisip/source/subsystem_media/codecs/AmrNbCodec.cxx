#include"AmrNbCodec.h"
#include <interf_dec.h>
#include <interf_enc.h>

using namespace std;

AmrNbCodec::AmrNbCodec( MRef<Library *> lib): AudioCodec( lib ) {
}

AmrNbCodec::~AmrNbCodec(){
}

const int AmrNbCodecState::amrnb_mode_to_byte_count[16] = {13, 14, 16, 18, 20, 21, 27, 32, 0, 0, 0, 0, 0, 0, 0, 0};

AmrNbCodecState::AmrNbCodecState(){
	decoder = Decoder_Interface_init();
	encoder = Encoder_Interface_init(0); // do not allow DTX
}

AmrNbCodecState::~AmrNbCodecState(){
	Decoder_Interface_exit(decoder);
	Encoder_Interface_exit(encoder);
}

uint32_t AmrNbCodecState::encode(void *in_buf, int32_t in_buf_size, int samplerate, void *out_buf){
	massert(in_buf_size==2*160);
	
	short *in_data = (short*)in_buf;
	unsigned char *out_data = (unsigned char*)out_buf;
	int n = 0;

	if (encoder != NULL) {
		// Add RTP CMR header (no request)
		*out_data = CMR_NO_REQUEST;
		out_data++;
		n++;

		n +=  Encoder_Interface_Encode(encoder, MR122, in_data, out_data, 0);

		// Sort out F and Q bits in ToC (as per RFC 3267) which is bit position compatible with the AMR format header, but with different meanings
		*out_data &= TOC_LAST_IN_PAYLOAD_AND_PAD_MASK;
		*out_data |= TOC_QUALITY_GOOD;

		return n;
	} else {
		return 0;
	}
}

uint32_t AmrNbCodecState::decode(void *in_buf, int32_t in_buf_size, void *out_buf){
	
	unsigned char *in_data = (unsigned char*)in_buf;
	short *out_data = (short*)out_buf;
	if (decoder != NULL) {
		// skip AMR over RTP Header - CMR
		if (((in_data[0]&CMR_MASK)>>4) > CMR_MAX_NB && in_data[0] != CMR_NO_REQUEST) {
			return 0;
		}
		unsigned char *ptr = &in_data[1];
		int32_t offset = 0;
		bool last_frame_found = false;
		unsigned char header = 0;
		// check ToCs for first good quality frame, keeping track of the offset due to bad speech streams and validating ToC data
		while(!last_frame_found && 
			  !((*ptr)&TOC_PAD) && 
			  ((((*ptr)&AMR_MODE_MASK)>>3)<=AMR_MAX_NB || (((*ptr)&AMR_MODE_MASK)>>3) == AMR_NO_DATA)) {
			if ((*ptr)&TOC_QUALITY_GOOD) {
				header = *ptr;
			}
			if (header == 0) {
				offset += amrnb_mode_to_byte_count[((*ptr)&AMR_MODE_MASK) >> 3];
			}
			last_frame_found = ((*ptr)&TOC_LAST_IN_PAYLOAD) ? false : true;
			ptr++;
		}
		if (header != 0) {
			// find the speech stream
			ptr--;
			if(ptr + offset + amrnb_mode_to_byte_count[(header&AMR_MODE_MASK) >> 3] <= (((unsigned char*)in_buf) + in_buf_size)) {
				ptr += offset; // must put header info immediately before speech stream
				*ptr = header;
				// Set bits AMR decoder expects
				*ptr &= 0x7C;
				*ptr |= 0x04;
				Decoder_Interface_Decode(decoder, ptr, out_data, 0);
				return 160;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

int32_t AmrNbCodec::getSamplingSizeMs(){
	return 20;
}

int32_t AmrNbCodec::getSamplingFreq(){
	return 8000;
}


int32_t AmrNbCodec::getInputNrSamples(){
	return 160;
}

string AmrNbCodec::getCodecName(){
		return "AMR NB";
}

string AmrNbCodec::getCodecDescription(){
		return "AMR 8kHz";
}

uint8_t AmrNbCodec::getSdpMediaType(){
	// AMR is a dynamic payload type so should be in the range 96-127
	// As this codec is being added to allow interoperation with BlackBerry Linphone 1.0.1 we will use the value it offers
	return 114;
}

string AmrNbCodec::getSdpMediaAttributes(){
		return "AMR/8000/1";
}

MRef<CodecState *> AmrNbCodec::newInstance(){
	MRef<CodecState *> ret = new AmrNbCodecState( );
	ret->setCodec( this );
	return ret;
}


