//
//  iOSSoundDevice.cpp
//  minisip
//
//  Created by Mikey Project on 01/02/2013.
//  Copyright (c) 2013 IPL. All rights reserved.
//

#include "iOSSoundDevice.hpp"
#include <assert.h>
#include <config.h>


#define kOutputBus 0
#define kInputBus 1

//NOTE - access to global data must be made thread safe
int nPlaySlices = 0;
int nRecordSlices = 0;
int openedRecordStatic = 0; //static copy of class member, used in callback
int openedPlaybackStatic = 0; //static copy of class member, used in callback
// to avoid unnecessary copying do not save audio data as contiguous 'realloc'able block - just use an array of AudioBuffers
AudioBuffer *playSlices = NULL; // 1st element is earliest
AudioBuffer *recordSlices = NULL;

pthread_mutex_t inMutex;
pthread_mutex_t outMutex;
pthread_mutex_t startStopMutex;

#define checkStatus(status) if(status) {std::cerr << "checkStatus " << status << " "<< __FILE__ << " " << __LINE__ << std::endl;}

static OSStatus thePlaybackCallback(void* inRefCon,
                                    AudioUnitRenderActionFlags* ioActionFlags,
                                    const AudioTimeStamp* inTimeStamp,
                                    UInt32 inBusNumber,
                                    UInt32 inNumberFrames,
                                    AudioBufferList* ioData)
{
    assert(inBusNumber==kOutputBus);
    pthread_mutex_lock(&outMutex);
    if(ioData != NULL && openedPlaybackStatic)
    {
        // Notes: ioData contains buffers (may be more than one!)
        // Fill them up as much as you can. Remember to set the size value in each buffer to match how
        // much data is in the buffer.
        for (int i=0; i < ioData->mNumberBuffers; i++) {
            // in practice we will only ever have 1 buffer, since audio format is mono
            AudioBuffer buffer = ioData->mBuffers[i];
            if (nPlaySlices == 0 || openedPlaybackStatic != 1) {
                memset(buffer.mData, 0, buffer.mDataByteSize);
            }
            else {
                UInt32 offset = 0;
                // copy as many slices as we can
                while (offset < buffer.mDataByteSize && nPlaySlices != 0) {
                    if ((playSlices[0].mDataByteSize + offset) <= buffer.mDataByteSize) { //complete slices - remove from global data
                        memcpy(&((unsigned char*)buffer.mData)[offset], playSlices[0].mData, playSlices[0].mDataByteSize);
                        offset += playSlices[0].mDataByteSize;
                        nPlaySlices--;
                        free(playSlices[0].mData);
                        memmove(&playSlices[0], &playSlices[1], nPlaySlices*sizeof(AudioBuffer)); // move buffer info - not actual buffers
                        playSlices=(AudioBuffer*)realloc(playSlices, nPlaySlices*sizeof(AudioBuffer));
                    }  else if(offset < buffer.mDataByteSize) { // partial slice - re-size slice in global data
                        UInt32 sizeToCopy = playSlices[0].mDataByteSize;
                        if((buffer.mDataByteSize - offset) < sizeToCopy) {
                            sizeToCopy = buffer.mDataByteSize - offset;
                        }
                        memcpy(&((unsigned char*)buffer.mData)[offset], playSlices[0].mData, sizeToCopy);
                        playSlices[0].mDataByteSize -= sizeToCopy;
                        memmove(playSlices[0].mData, &((unsigned char*)playSlices[0].mData)[sizeToCopy], playSlices[0].mDataByteSize);
                        offset += sizeToCopy;
                    }
                }
                buffer.mDataByteSize = offset;
            }
        } 
    } else {
        *ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
        memset(ioData->mBuffers[0].mData, 0, ioData->mBuffers[0].mDataByteSize);
    }
    pthread_mutex_unlock(&outMutex);
    return noErr;
}

static OSStatus recordingCallback(void* inRefCon,
                                  AudioUnitRenderActionFlags* ioActionFlags,
                                  const AudioTimeStamp* inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList* ioData)
{
    assert(inBusNumber==kInputBus);
    iOSSoundDevice * THIS = (iOSSoundDevice*)inRefCon;
    pthread_mutex_lock(&inMutex); 
    if(openedRecordStatic == 1)
    {
        // Set up buffer
        AudioBuffer buffer;
        buffer.mNumberChannels = THIS->audioFormat.mChannelsPerFrame;
        buffer.mDataByteSize = inNumberFrames * THIS->audioFormat.mBytesPerFrame;
        buffer.mData = malloc(inNumberFrames * THIS->audioFormat.mBytesPerFrame);
        // Put buffer in a AudioBufferList
        AudioBufferList bufferList;
        bufferList.mNumberBuffers = THIS->audioFormat.mChannelsPerFrame;
        bufferList.mBuffers[0] = buffer;

        // Obtain recorded samples
        OSStatus status;
        status = AudioUnitRender(THIS->audioUnit,
                                 ioActionFlags,
                                 inTimeStamp,
                                 inBusNumber,
                                 inNumberFrames,
                                 &bufferList);
        checkStatus(status);
        // save samples in global data
        nRecordSlices++;
        recordSlices=(AudioBuffer *)realloc(recordSlices, nRecordSlices*sizeof(AudioBuffer));
        recordSlices[nRecordSlices-1] = buffer;
    }
    pthread_mutex_unlock(&inMutex);
    return noErr;
}

iOSSoundDevice::iOSSoundDevice( std::string const& device )
: SoundDevice(device)
{
    this->setFormat(SOUND_S16LE);
    this->samplingRate = SOUND_CARD_FREQ;
    this->nChannelsPlay = SOUND_CARD_CHANNELS;
    this->nChannelsRecord = SOUND_CARD_CHANNELS;
    started = false;
    
    pthread_mutex_init(&inMutex, NULL);
    pthread_mutex_init(&outMutex, NULL);
    pthread_mutex_init(&startStopMutex, NULL);
    
    OSStatus status;
    status = AudioSessionInitialize(NULL, NULL, NULL, NULL);
    checkStatus(status);

    // Set buffers sizes - change this to adjust latency
    Float32 preferredBufferDuration = 0.01; // 10ms
    status = AudioSessionSetProperty(kAudioSessionProperty_PreferredHardwareIOBufferDuration, sizeof(Float32), &preferredBufferDuration);
    checkStatus(status);

    // Describe audio component
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output;
    desc.componentSubType = kAudioUnitSubType_VoiceProcessingIO; //brings useful things like AGC, echo cancellation etc.
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    // Get component
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
    
    // Get audio units
    status = AudioComponentInstanceNew(inputComponent, &audioUnit);
    checkStatus(status);
    
    // Enable IO for recording
    UInt32 flag = 1;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  kInputBus,
                                  &flag,
                                  sizeof(flag));
    checkStatus(status);
    
    
    // ...and for playback
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  kOutputBus,
                                  &flag,
                                  sizeof(flag));
    checkStatus(status);
    
    // Describe format
    audioFormat.mSampleRate = this->samplingRate * 1.0;
    audioFormat.mFormatID = kAudioFormatLinearPCM;
    audioFormat.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked; // NB litle endian as big endian not set
    audioFormat.mFramesPerPacket = 1;
    audioFormat.mChannelsPerFrame = 1;
    audioFormat.mBitsPerChannel = 16;
    audioFormat.mBytesPerPacket = 2;
    audioFormat.mBytesPerFrame = 2;
    audioFormat.mReserved = 0;
    
    // Apply format
    
    UInt32 audioCategory = kAudioSessionCategory_PlayAndRecord;
    /* We want to be able to open playback and recording streams */
    status = AudioSessionSetProperty(kAudioSessionProperty_AudioCategory,
                                     sizeof(audioCategory),
                                     &audioCategory);
    checkStatus(status);
    
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    checkStatus(status);
    
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    checkStatus(status);
    
    // Set input callback
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = recordingCallback;
    callbackStruct.inputProcRefCon = this;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global,
                                  kOutputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    checkStatus(status);
    
    // Set output callback
    callbackStruct.inputProc = thePlaybackCallback;
    callbackStruct.inputProcRefCon = this;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global,
                                  kOutputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    checkStatus(status);
    
    // Disable buffer allocation for the recorder (only as we want to use our own buffers)
    flag = 0;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &flag,
                                  sizeof(flag));
    checkStatus(status);

    // Initialise
    status = AudioUnitInitialize(audioUnit);
    checkStatus(status);    
    
    // check samplerates
    Float64 freq;
    UInt32 sz = sizeof(Float64);
    status = AudioUnitGetProperty(audioUnit,
                                  kAudioUnitProperty_SampleRate,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &freq,
                                  &sz);
    checkStatus(status);
    assert(sz == sizeof(Float64) && freq == 8000.0);
    
    sz = sizeof(Float64);
    status = AudioUnitGetProperty(audioUnit,
                                  kAudioUnitProperty_SampleRate,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &freq,
                                  &sz);
    checkStatus(status);
    assert(sz == sizeof(Float64) && freq == 8000.0);

    // check buffer length
    sz = sizeof(Float32);
    status = AudioSessionGetProperty(kAudioSessionProperty_PreferredHardwareIOBufferDuration,
                                     &sz,
                                     &preferredBufferDuration);
    checkStatus(status);
    assert(preferredBufferDuration == 0.01f);
}

iOSSoundDevice::~iOSSoundDevice()
{
    shutdown();
}

int iOSSoundDevice::openPlayback( int32_t samplingRate, int nChannels, int format )
{
    pthread_mutex_lock(&startStopMutex);
    this->openedPlayback = 1;
    openedPlaybackStatic = 1;
    if(!started) {
        OSStatus status = AudioOutputUnitStart(audioUnit);
        checkStatus(status);
        started = true;
    }
    pthread_mutex_unlock(&startStopMutex);
    return 0;
}

int iOSSoundDevice::openRecord( int32_t samplingRate, int nChannels, int format )
{
    pthread_mutex_lock(&startStopMutex);  
    this->openedRecord = 1;
    openedRecordStatic=1;
    if(!started) {
        OSStatus status = AudioOutputUnitStart(audioUnit);
        checkStatus(status);
        started = true;
    }
    pthread_mutex_unlock(&startStopMutex);
    return 0;
}

void iOSSoundDevice::shutdown()
{
    closePlayback();
    closeRecord();

    AudioComponentInstanceDispose(audioUnit);
    
    // free all global data 
    if(playSlices != NULL) {
        for(int i = nPlaySlices-1; i>=0; i--) {
            free(playSlices[i].mData);
        }
        free(playSlices);
    }
    
    if(recordSlices != NULL) {
        for(int i = nRecordSlices-1; i>=0; i--) {
            free(recordSlices[i].mData);
        }
        free(recordSlices);
    }
}


int iOSSoundDevice::readFromDevice( byte_t * buffer, uint32_t nSamples )
{
    UInt32 offset = 0; // in bytes
    pthread_mutex_lock(&inMutex);

    if (openedRecord == 1 && nRecordSlices != 0) {
        // copy as many slices as we can
        while (offset < nSamples*audioFormat.mBytesPerFrame && nRecordSlices != 0) {
            if ((recordSlices[0].mDataByteSize + offset) <= nSamples*audioFormat.mBytesPerFrame) { //complete slices - remove from global data
                memcpy(&buffer[offset], recordSlices[0].mData, recordSlices[0].mDataByteSize);
                offset += recordSlices[0].mDataByteSize;
                nRecordSlices--;
                free(recordSlices[0].mData);
                memmove(&recordSlices[0], &recordSlices[1], nRecordSlices*sizeof(AudioBuffer));
                recordSlices=(AudioBuffer*)realloc(recordSlices, nRecordSlices*sizeof(AudioBuffer));
            } else if(offset < nSamples*audioFormat.mBytesPerFrame) { // partial slice - re-size in global data
                UInt32 sizeToCopy = recordSlices[0].mDataByteSize;
                if ((nSamples*audioFormat.mBytesPerFrame) - offset < sizeToCopy) {
                    sizeToCopy = (nSamples*audioFormat.mBytesPerFrame) - offset;
                }
                memcpy(&buffer[offset], recordSlices[0].mData, sizeToCopy);
                recordSlices[0].mDataByteSize -= sizeToCopy;
                memmove(recordSlices[0].mData, &((unsigned char *)recordSlices[0].mData)[sizeToCopy], recordSlices[0].mDataByteSize);
                offset += sizeToCopy;
            }
        }
    }
    pthread_mutex_unlock(&inMutex);
    return offset/audioFormat.mBytesPerFrame;
}

int iOSSoundDevice::writeToDevice( byte_t * buffer, uint32_t nSamples )
{
    int retVal = nSamples;
    pthread_mutex_lock(&outMutex);
    if(openedPlayback == 1) {
        nPlaySlices++;
        playSlices=(AudioBuffer*)realloc(playSlices, nPlaySlices*sizeof(AudioBuffer));
        playSlices[nPlaySlices-1].mData = malloc(nSamples * audioFormat.mBytesPerFrame);
        memcpy(playSlices[nPlaySlices-1].mData, buffer, nSamples * audioFormat.mBytesPerFrame);
        playSlices[nPlaySlices-1].mDataByteSize = nSamples * audioFormat.mBytesPerFrame;
        playSlices[nPlaySlices-1].mNumberChannels = audioFormat.mChannelsPerFrame;
    } else {
        retVal = -1;
    }
    
    pthread_mutex_unlock(&outMutex);
    return retVal;
}

int iOSSoundDevice::readError( int errcode, byte_t * buffer, uint32_t nSamples )
{
    // TODO
    return -1;
}

int iOSSoundDevice::writeError( int errcode, byte_t * buffer, uint32_t nSamples )
{
    // TODO
    return -1;
}

int iOSSoundDevice::closeRecord()
{
    pthread_mutex_lock(&startStopMutex);
    this->openedRecord = 0;
    openedRecordStatic = 0;
    if(started) {
        OSStatus status = AudioOutputUnitStop(audioUnit);
        checkStatus(status);
        started = false;
    }
    pthread_mutex_unlock(&startStopMutex);
    return 0;
}

int iOSSoundDevice::closePlayback()
{
    pthread_mutex_lock(&startStopMutex);    
    this->openedPlayback = 0;
    openedPlaybackStatic = 0;
    if(started) {
        OSStatus status = AudioOutputUnitStop(audioUnit);
        checkStatus(status);
        started = false;
    }
    pthread_mutex_unlock(&startStopMutex);
    return 0;
}

void iOSSoundDevice::sync()
{
    // TODO
}







