//
//  iOSSoundDevice.h
//  minisip
//
//  Created by Mikey Project on 24/01/2013.
//  Copyright (c) 2013 IPL. All rights reserved.
//

#ifndef __minisip__iOSSoundDriver__
#define __minisip__iOSSoundDriver__

#include<libminisip/libminisip_config.h>

#include<string>
#include<libmutil/MemObject.h>

#include<libminisip/media/soundcard/SoundDriver.h>


class iOSSoundDriver: public SoundDriver{
	public:
		iOSSoundDriver( MRef<Library*> lib );
		virtual ~iOSSoundDriver();
		virtual MRef<SoundDevice*> createDevice( std::string deviceId );
		virtual std::string getDescription() const { return "iOS sound driver"; };

		virtual std::vector<SoundDeviceName> getDeviceNames() const;

		virtual bool getDefaultInputDeviceName( SoundDeviceName &name ) const;
		
		virtual bool getDefaultOutputDeviceName( SoundDeviceName &name ) const;

		virtual std::string getName() const {
			return "iOS sound";
		}

		virtual std::string getMemObjectType() const { return getName(); }

		virtual uint32_t getVersion() const;
};

#endif	// __minisip__iOSSoundDriver__
