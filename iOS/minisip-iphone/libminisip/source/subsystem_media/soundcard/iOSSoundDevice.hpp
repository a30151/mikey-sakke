//
//  iOSSoundDevice.h
//  minisip
//
//  Created by Mikey Project on 24/01/2013.
//  Copyright (c) 2013 IPL. All rights reserved.
//

#ifndef __minisip__iOSSoundDevice__
#define __minisip__iOSSoundDevice__

#include <iostream>

#include<libminisip/libminisip_config.h>
#include<libminisip/media/soundcard/SoundDevice.h>

//#include <AudioUnit/AudioUnit.h>
#include <AudioToolbox/AudioConverter.h>
#include <AudioToolbox/AudioToolbox.h>
#include <CoreAudio/CoreAudioTypes.h>

class iOSSoundDevice: public SoundDevice{
public:
    iOSSoundDevice( std::string const& device );
    virtual ~iOSSoundDevice();
    
    virtual int readFromDevice( byte_t * buffer, uint32_t nSamples );
    virtual int writeToDevice( byte_t * buffer, uint32_t nSamples );
    
    virtual int readError( int errcode, byte_t * buffer, uint32_t nSamples );
    virtual int writeError( int errcode, byte_t * buffer, uint32_t nSamples );
    
    virtual int openPlayback( int32_t samplingRate, int nChannels, int format );
    virtual int openRecord( int32_t samplingRate, int nChannels, int format );
    
    virtual int closePlayback();
    virtual int closeRecord();
    
    virtual void sync();
    
    virtual std::string getMemObjectType() const { return "iOSSoundDevice";};
    
    AudioComponentInstance audioUnit;
    AudioStreamBasicDescription audioFormat;
    
private:
    
    void shutdown();
    bool started;
};


#endif /* defined(__minisip__iOSSoundDevice__) */
