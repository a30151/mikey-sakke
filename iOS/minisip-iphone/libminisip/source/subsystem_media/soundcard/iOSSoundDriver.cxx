//
//  iOSSoundDevice.h
//  minisip
//
//  Created by Mikey Project on 24/01/2013.
//  Copyright (c) 2013 IPL. All rights reserved.
//
#include "iOSSoundDriver.h"

#include "iOSSoundDevice.hpp"

#include<config.h>

#include<libminisip/media/soundcard/SoundDriver.h>
#include<libminisip/media/soundcard/SoundDriverRegistry.h>
#include<libmutil/MPlugin.h>

using namespace std;

static const char DRIVER_PREFIX[] = "ios";

iOSSoundDriver::iOSSoundDriver( MRef<Library*> lib ) : SoundDriver( DRIVER_PREFIX, lib ){
}

iOSSoundDriver::~iOSSoundDriver( ){
}

MRef<SoundDevice*> iOSSoundDriver::createDevice( string deviceId ){
	return new iOSSoundDevice( deviceId );
}

std::vector<SoundDeviceName> iOSSoundDriver::getDeviceNames() const {
	std::vector<SoundDeviceName> names;
    
	mdbg << "iOSSoundDriver::getDeviceNames not implemented" << endl;
    
	return names;
}

bool iOSSoundDriver::getDefaultInputDeviceName( SoundDeviceName &name ) const{
	name = SoundDeviceName( "recorder", "Default iOS Input Device" );
	return true;
}

bool iOSSoundDriver::getDefaultOutputDeviceName( SoundDeviceName &name ) const {
	name = SoundDeviceName( "player", "Default iOS Output Device" );
	return true;
}

uint32_t iOSSoundDriver::getVersion() const{
	return 0x00000001;
}
