LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libgnustl_shared
LOCAL_SRC_FILES := prebuilt/$(LOCAL_MODULE).so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libminisip-android
LOCAL_SRC_FILES := prebuilt/$(LOCAL_MODULE).so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libmscrypto
LOCAL_SRC_FILES := prebuilt/$(LOCAL_MODULE).so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libmskms-client
LOCAL_SRC_FILES := prebuilt/$(LOCAL_MODULE).so
include $(PREBUILT_SHARED_LIBRARY)

