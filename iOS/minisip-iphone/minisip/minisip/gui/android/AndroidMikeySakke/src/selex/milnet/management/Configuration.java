package selex.milnet.management;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import selex.milnet.calls.Caller;
import selex.milnet.management.ConfigurationFileHelper.Account;
import selex.milnet.management.ConfigurationFileHelper.Account.KeyManagementServer;
import selex.milnet.management.ConfigurationFileHelper.Account.Proxy;
import selex.milnet.management.ConfigurationFileHelper.Ports;
import selex.milnet.management.ConfigurationFileHelper.Servers;
import selex.milnet.management.ConfigurationFileHelper.SoundDevice;
import selex.milnet.management.ConfigurationFileHelper.Stun;
import selex.milnet.management.ConfigurationFileHelper.Test;
import selex.milnet.mk.R;
import selex.milnet.service.MinisipService;
import selex.milnet.util.MinisipCommand;
import selex.milnet.util.MinisipCommand.Type;
import selex.milnet.util.MinisipCommandHelper;
import selex.milnet.util.MinisipMessengerHelper;
import selex.milnet.util.MinisipOption;
import selex.milnet.util.MinisipReport;
import selex.milnet.util.MinisipReportHelper;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.CheckBoxPreference;
import android.preference.DialogPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.util.Log;

public class Configuration extends PreferenceActivity implements OnSharedPreferenceChangeListener{
	private static final String TAG =  Configuration.class.getSimpleName();
	String CONFIG_FILENAME="/etc/minisip/minisip.conf";
	boolean mExternalStorageAvailable = false;
	boolean mExternalStorageWriteable = false;
	
	// Create Generic preferences
	private CheckBoxPreference mCheckBoxPreference;
	private EditTextPreference mEditTextPreference;
	private ListPreference mListPreference;
	
	private boolean offLineMode;
	private boolean useKms;
	private boolean configupdated;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.configuration_preference);
    
        setMode();
        	
        readConfiguration();
        
        Log.w(TAG,"Created " + TAG);
    }
	
	private void setMode()
	{
		Intent intent = getIntent();
        Bundle bun = intent.getExtras(); 
        String mode = bun.getString(ConfigurationFileHelper.CONF_MODE);
        if(mode.contains(ConfigurationFileHelper.CONF_OFF_LINE))
        {
        	offLineMode = true;
        }
        else
        {
        	offLineMode = false;
        }
        
        String kms = bun.getString("kms");
        if(kms.contains("use"))
        {
        	useKms = true;
        }
        else
        {
        	useKms = false;
        }
        
        configupdated = false;
        
        if(offLineMode == false)
        {
        	// We are online so use minisip
        	connectToMinisipService();
        	
        	// We only have a small number of parameters most disabled
        	enableCheckBoxPreference(R.string.conf_pref_register_key);
        	enableCheckBoxPreference(R.string.conf_pref_secured_key);
        	enableTextPreference(R.string.conf_pref_psk_key);
        	enableCheckBoxPreference(R.string.conf_pref_psk_enabled_key);
        	enableCheckBoxPreference(R.string.conf_pref_kms_overridden_key);
        	enableTextPreference(R.string.conf_pref_kms_username_key);
        	enableTextPreference(R.string.conf_pref_kms_password_key);
        	enableTextPreference(R.string.conf_pref_kms_url_key);
        	enableCheckBoxPreference(R.string.conf_pref_kms_verify_ssl_key);
        	enableCheckBoxPreference(R.string.conf_pref_sakke_test_key);
			
        	disableTextPreference(R.string.conf_pref_version_key);
        	disableTextPreference(R.string.conf_pref_net_int_key);
        	disableTextPreference(R.string.conf_pref_phonebook_key);
        	disableTextPreference(R.string.conf_pref_account_name_key);
        	disableTextPreference(R.string.conf_pref_sip_uri_key);
        	disableTextPreference(R.string.conf_pref_proxy_address_key);
        	disableTextPreference(R.string.conf_pref_proxy_port_key);
        	disableTextPreference(R.string.conf_pref_proxy_username_key);
        	disableTextPreference(R.string.conf_pref_proxy_password_key);
        	disableCheckBoxPreference(R.string.conf_pref_pstn_account_key);
        	disableCheckBoxPreference(R.string.conf_pref_default_account_key);
        	disableListPreference(R.string.conf_pref_ka_type_key);
			disableTextPreference(R.string.conf_pref_certificate_key);
			disableTextPreference(R.string.conf_pref_private_key_key);
			disableTextPreference(R.string.conf_pref_ca_file_key);
			disableCheckBoxPreference(R.string.conf_pref_dh_enabled_key);
			disableCheckBoxPreference(R.string.conf_pref_check_cert_key);
			disableCheckBoxPreference(R.string.conf_pref_udp_server_key);
			disableCheckBoxPreference(R.string.conf_pref_tcp_server_key);
			disableCheckBoxPreference(R.string.conf_pref_tls_server_key);
			disableTextPreference(R.string.conf_pref_local_udp_port_key);
			disableTextPreference(R.string.conf_pref_local_tcp_port_key);
			disableTextPreference(R.string.conf_pref_local_tls_port_key);
			disableTextPreference(R.string.conf_pref_sound_device_key);
			disableTextPreference(R.string.conf_pref_sound_device_in_key);
			disableTextPreference(R.string.conf_pref_sound_device_out_key);
			disableListPreference(R.string.conf_pref_mixer_type_key);
			disableListPreference(R.string.conf_pref_codec_key);
			disableCheckBoxPreference(R.string.conf_pref_stun_use_key);
			disableCheckBoxPreference(R.string.conf_pref_stun_server_autodetect_key);
			disableTextPreference(R.string.conf_pref_stun_server_domain_key);
			disableTextPreference(R.string.conf_pref_stun_manual_server_key);
        }
        else
        {
        	// We show most parameters
        	enableCheckBoxPreference(R.string.conf_pref_register_key);
        	enableCheckBoxPreference(R.string.conf_pref_secured_key);
        	enableTextPreference(R.string.conf_pref_psk_key);
        	enableCheckBoxPreference(R.string.conf_pref_psk_enabled_key);
        	enableCheckBoxPreference(R.string.conf_pref_kms_overridden_key);
        	enableTextPreference(R.string.conf_pref_kms_username_key);
        	enableTextPreference(R.string.conf_pref_kms_password_key);
        	enableTextPreference(R.string.conf_pref_kms_url_key);
        	enableTextPreference(R.string.conf_pref_version_key);
        	enableTextPreference(R.string.conf_pref_net_int_key);
        	enableTextPreference(R.string.conf_pref_phonebook_key);
        	enableTextPreference(R.string.conf_pref_account_name_key);
        	enableTextPreference(R.string.conf_pref_sip_uri_key);
        	enableTextPreference(R.string.conf_pref_proxy_address_key);
        	enableTextPreference(R.string.conf_pref_proxy_port_key);
        	enableTextPreference(R.string.conf_pref_proxy_username_key);
        	enableTextPreference(R.string.conf_pref_proxy_password_key);
        	enableCheckBoxPreference(R.string.conf_pref_pstn_account_key);
        	enableCheckBoxPreference(R.string.conf_pref_default_account_key);
        	enableListPreference(R.string.conf_pref_ka_type_key);
        	enableTextPreference(R.string.conf_pref_certificate_key);
        	enableTextPreference(R.string.conf_pref_private_key_key);
        	enableTextPreference(R.string.conf_pref_ca_file_key);
        	enableCheckBoxPreference(R.string.conf_pref_dh_enabled_key);
        	enableCheckBoxPreference(R.string.conf_pref_check_cert_key);
        	enableCheckBoxPreference(R.string.conf_pref_udp_server_key);
        	enableCheckBoxPreference(R.string.conf_pref_tcp_server_key);
        	enableCheckBoxPreference(R.string.conf_pref_tls_server_key);
        	enableTextPreference(R.string.conf_pref_local_udp_port_key);
        	enableTextPreference(R.string.conf_pref_local_tcp_port_key);
        	enableTextPreference(R.string.conf_pref_local_tls_port_key);
        	enableTextPreference(R.string.conf_pref_sound_device_key);
        	enableTextPreference(R.string.conf_pref_sound_device_in_key);
        	enableTextPreference(R.string.conf_pref_sound_device_out_key);
        	enableListPreference(R.string.conf_pref_mixer_type_key);
        	enableListPreference(R.string.conf_pref_codec_key);
        	enableCheckBoxPreference(R.string.conf_pref_stun_use_key);
        	enableCheckBoxPreference(R.string.conf_pref_stun_server_autodetect_key);
        	enableTextPreference(R.string.conf_pref_stun_server_domain_key);
        	enableTextPreference(R.string.conf_pref_stun_manual_server_key);
        	enableCheckBoxPreference(R.string.conf_pref_sakke_test_key);
        	
			disableCheckBoxPreference(R.string.conf_pref_kms_verify_ssl_key);
			
			
			CheckExternalStorage();
        }
	}
	
	private void CheckExternalStorage()
	{
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {    
			// We can read and write the media    
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {    
			// We can only read the media    
			mExternalStorageAvailable = true;    
			mExternalStorageWriteable = false;
		} else {    
			// Something else is wrong. It may be one of many other states, but all we need    
			//  to know is we can neither read nor write    
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		Log.w(TAG,"CheckExternalStorage: " + mExternalStorageAvailable + " " + mExternalStorageWriteable);
	}
	
	private void readConfiguration()
	{
		// If the card is present
		if(offLineMode == false)
		{
			// We are communicating with Minisip we cant do this 
			// until the service is connected
			//readConfigurationFromMinisip();
		}
		else
		{
			// We are working with the off line configuration
			readConfigurationFromSDCard();
		}
	}
	
	private void writeConfiguration(String key)
	{
		if(offLineMode == false)
		{
			// We are communicating with Minisip
			writeConfigurationToMinisip(key);
		}
		else
		{
			writeConfigurationToSDCard();
		}
	}
	
	private void writeConfigurationToSDCard()
	{
		Log.i(TAG, "writeConfigurationToSDCard");
		
		ConfigurationFileHelper cfh = new ConfigurationFileHelper();
		
		cfh.SetVersion(getTextPreference(R.string.conf_pref_version_key));
		cfh.SetNetworkInterface(getTextPreference(R.string.conf_pref_net_int_key));
		cfh.SetPhonebook(getTextPreference(R.string.conf_pref_phonebook_key));

		Account account = cfh.GetAccount();
		account.SetName(getTextPreference(R.string.conf_pref_account_name_key));
		account.SetSipUri(getTextPreference(R.string.conf_pref_sip_uri_key));
		
		Proxy proxy = account.GetProxy();
		proxy.SetAddress(getTextPreference(R.string.conf_pref_proxy_address_key));
		proxy.SetRegister(getCheckBoxPreference(R.string.conf_pref_register_key));//
		proxy.SetPort(getTextPreference(R.string.conf_pref_proxy_port_key));
		proxy.SetUsername(getTextPreference(R.string.conf_pref_proxy_username_key));
		proxy.SetPassword(getTextPreference(R.string.conf_pref_proxy_password_key));
		
		account.SetPstnAccount(getCheckBoxPreference(R.string.conf_pref_pstn_account_key));
		account.SetDefaultAccount(getCheckBoxPreference(R.string.conf_pref_default_account_key));
		account.SetSecured(getCheckBoxPreference(R.string.conf_pref_secured_key));//
		account.SetKaType(getListBoxPreference(R.string.conf_pref_ka_type_key));
		account.SetPsk(getTextPreference(R.string.conf_pref_psk_key));//
		account.SetCertificate(getTextPreference(R.string.conf_pref_certificate_key));
		account.SetPrivateKey(getTextPreference(R.string.conf_pref_private_key_key));
		account.SetCaFile(getTextPreference(R.string.conf_pref_ca_file_key));
		account.SetDhEnabled(getCheckBoxPreference(R.string.conf_pref_dh_enabled_key));
		account.SetPskEnabled(getCheckBoxPreference(R.string.conf_pref_psk_enabled_key));//
		account.SetCheckCert(getCheckBoxPreference(R.string.conf_pref_check_cert_key));
		
		if(useKms == true)
		{
			KeyManagementServer kms = account.GetKms();
			kms.SetOverridden(getCheckBoxPreference(R.string.conf_pref_kms_overridden_key));//
			kms.SetUsername(getTextPreference(R.string.conf_pref_kms_username_key));//
			kms.SetPassword(getTextPreference(R.string.conf_pref_kms_password_key));//
			kms.SetUrl(getTextPreference(R.string.conf_pref_kms_url_key));//
		}
		
		Servers server = cfh.GetServers();
		server.SetUdpServer(getCheckBoxPreference(R.string.conf_pref_udp_server_key));
		server.SetTcpServer(getCheckBoxPreference(R.string.conf_pref_tcp_server_key));
		server.SetTlsServer(getCheckBoxPreference(R.string.conf_pref_tls_server_key));
		
		Ports ports = cfh.GetPorts();
		ports.SetUdpPort(getTextPreference(R.string.conf_pref_local_udp_port_key));
		ports.SetTcpPort(getTextPreference(R.string.conf_pref_local_tcp_port_key));
		ports.SetTlsPort(getTextPreference(R.string.conf_pref_local_tls_port_key));
		
		SoundDevice sound = cfh.GetSoundDevice();
		sound.SetDevice(getTextPreference(R.string.conf_pref_sound_device_key));
		sound.SetDeviceIn(getTextPreference(R.string.conf_pref_sound_device_in_key));
		sound.SetDeviceOut(getTextPreference(R.string.conf_pref_sound_device_out_key));
		sound.SetMixerType(getListBoxPreference(R.string.conf_pref_mixer_type_key));
		sound.SetCodec(getListBoxPreference(R.string.conf_pref_codec_key));
		
		Test test = cfh.GetTest();
		test.SetSakkeTest(getCheckBoxPreference(R.string.conf_pref_sakke_test_key));
		String configuration = cfh.Encode(useKms);
		
		// Now save the configuration
		// If the card is present
		if(mExternalStorageAvailable == false)
			return;
		
		if(mExternalStorageWriteable == false)
			return;
		
		File conf = new File(Environment.getExternalStorageDirectory(),CONFIG_FILENAME);
		try {
			FileWriter fw = new FileWriter(conf);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(configuration);
			bw.close();
			fw.close();
			
			// Now we can restart the minisip service
			//configupdated = true;
			//Log.w(TAG,"Stopping Service");
			//disconnectFromMinisipService();
			//this.stopService(new Intent(Configuration.this, MinisipService.class));
			//getBaseContext().startService(new Intent(getBaseContext(), MinisipService.class));
		} catch (IOException e) {
			Log.w(TAG,"Can't access Configuration File");
		}
	}
	
	private void writeConfigurationToMinisip(String key)
	{
		Log.i(TAG, "writeConfigurationToMinisip: " + key);
		
		ConfigurationFileHelper cfh = new ConfigurationFileHelper();
		
		String minisipOption = null;
		String minisipValue = null;
		Account account = cfh.GetAccount();
		Proxy proxy = account.GetProxy();
		KeyManagementServer kms = account.GetKms();
		
		boolean isChecked = false;
		
		// Search for a match on key
		if(key == getResources().getString(R.string.conf_pref_net_int_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_version_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_account_name_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_sip_uri_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_proxy_address_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_register_key)) {
			isChecked = getCheckBoxPreference(R.string.conf_pref_register_key);
			minisipValue = ConfigurationFileHelper.GetStringFromBoolean(isChecked);
			minisipOption = proxy.REGISTER;
		} else if(key == getResources().getString(R.string.conf_pref_proxy_port_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_proxy_username_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_proxy_password_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_pstn_account_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_default_account_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_secured_key)) {
			isChecked = getCheckBoxPreference(R.string.conf_pref_secured_key);
			minisipValue = ConfigurationFileHelper.GetStringFromBoolean(isChecked);
			minisipOption = account.SECURED;
		} else if(key == getResources().getString(R.string.conf_pref_ka_type_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_psk_key)) {
			minisipValue = getTextPreference(R.string.conf_pref_psk_key);
			minisipOption = account.PSK;
		} else if(key == getResources().getString(R.string.conf_pref_certificate_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_private_key_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_ca_file_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_dh_enabled_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_psk_enabled_key)) {
			isChecked = getCheckBoxPreference(R.string.conf_pref_psk_enabled_key);
			minisipValue = ConfigurationFileHelper.GetStringFromBoolean(isChecked);
			minisipOption = account.PSKENABLED;
		} else if(key == getResources().getString(R.string.conf_pref_check_cert_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_kms_overridden_key)) {
			isChecked = getCheckBoxPreference(R.string.conf_pref_kms_overridden_key);
			minisipValue = ConfigurationFileHelper.GetStringFromBoolean(isChecked);
			minisipOption = kms.OVERRIDDEN;
		} else if(key == getResources().getString(R.string.conf_pref_kms_username_key)) {
			minisipValue = getTextPreference(R.string.conf_pref_kms_username_key);
			minisipOption = kms.USERNAME;
		} else if(key == getResources().getString(R.string.conf_pref_kms_password_key)) {
			minisipValue = getTextPreference(R.string.conf_pref_kms_password_key);
			minisipOption = kms.PASSWORD;
		} else if(key == getResources().getString(R.string.conf_pref_kms_url_key)) {
			minisipValue = getTextPreference(R.string.conf_pref_kms_url_key);
			minisipOption = kms.URL;
		} else if(key == getResources().getString(R.string.conf_pref_kms_verify_ssl_key)) {
			minisipValue = getTextPreference(R.string.conf_pref_kms_verify_ssl_key);
			minisipOption = kms.URL;
		} else if(key == getResources().getString(R.string.conf_pref_udp_server_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_tcp_server_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_tls_server_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_local_udp_port_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_local_tcp_port_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_local_tls_port_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_sound_device_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_sound_device_in_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_sound_device_out_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_mixer_type_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_codec_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_phonebook_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_stun_use_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_stun_server_autodetect_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_stun_server_domain_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_stun_manual_server_key)) {
			// Not Implemented
		} else if(key == getResources().getString(R.string.conf_pref_sakke_test_key)) {
			isChecked = getCheckBoxPreference(R.string.conf_pref_sakke_test_key);
			minisipValue = ConfigurationFileHelper.GetStringFromBoolean(isChecked);
			minisipOption = ConfigurationFileHelper.SAKKE_TEST;
		}
		
		if(minisipOption != null)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(minisipOption);
			sb.append(",");
			sb.append(minisipValue);
			
			MinisipCommand cmd = new MinisipCommand(Type.SET_OPTION,sb.toString());
			
			Log.i(TAG,cmd.toString());
			
			// SEND TO MINISIP
			sendToMinisipService(cmd);
		}
	}
	
	private void readConfigurationFromMinisip()
	{
		// This request data from Minisip it will arrive asynchronously 
		// so update as we go
		Log.i(TAG,"readConfigurationFromMinisip");
		
		ConfigurationFileHelper cfh = new ConfigurationFileHelper();
		
		Account account = cfh.GetAccount();
		Proxy proxy = account.GetProxy();
		KeyManagementServer kms = account.GetKms();
		
		// Send multiple requests to Minisip service for data
		String target = proxy.REGISTER;
		MinisipCommand cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		
		target = account.SECURED;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = account.PSK;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = account.PSKENABLED;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = kms.OVERRIDDEN;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = kms.USERNAME;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = kms.PASSWORD;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = kms.URL;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = kms.VERIFYSSL;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
		
		target = ConfigurationFileHelper.SAKKE_TEST;
		cmd = new MinisipCommand(Type.GET_OPTION,target);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		Log.i(TAG,cmd.toString());
	}
	
	private boolean ignoreUpdates;
	private void readConfigurationFromSDCard()
	{
		if(mExternalStorageAvailable == false)
			return;
		
		File conf = new File(Environment.getExternalStorageDirectory(),CONFIG_FILENAME);
		char[] buf = null;
		int count = 0;
		try {
			buf = new char[2048];
			FileReader fr = new FileReader(conf);
			count = fr.read(buf);
			fr.close();
		} catch (FileNotFoundException e) {
			Log.w(TAG,"Can't find Configuration File");
		} catch (IOException e) {
			Log.w(TAG,"Can't access Configuration File");
		}
		
		if(count <= 0)
		{
			Log.w(TAG,"Abort Setting Preferences: Creating Default");
			// Create a default configuration
			writeConfigurationToSDCard();
			return;	// No data read
		}

		Log.w(TAG,"Setting Preferences");
		// We have data in buffer use our helper to decode it
		ConfigurationFileHelper cfh = new ConfigurationFileHelper(buf);
		if(cfh.Decode())
		{
			// We successfully parsed the file update our internal values
			setTextPreference(R.string.conf_pref_version_key,cfh.GetVersion());
			setTextPreference(R.string.conf_pref_net_int_key,cfh.GetNetworkInterface());
			setTextPreference(R.string.conf_pref_phonebook_key,cfh.GetPhonebook());
			
			Account account = cfh.GetAccount();
			setTextPreference(R.string.conf_pref_account_name_key,account.GetName());
			setTextPreference(R.string.conf_pref_sip_uri_key,account.GetSipUri());
						
			Proxy proxy = account.GetProxy();
			setTextPreference(R.string.conf_pref_proxy_address_key,proxy.GetAddress());
			setCheckBoxPreference(R.string.conf_pref_register_key,proxy.GetRegister());
			setTextPreference(R.string.conf_pref_proxy_port_key,proxy.GetPort());
			setTextPreference(R.string.conf_pref_proxy_username_key,proxy.GetUsername());
			setTextPreference(R.string.conf_pref_proxy_password_key,proxy.GetPassword());
			setCheckBoxPreference(R.string.conf_pref_pstn_account_key,account.GetPstnAccount());
			setCheckBoxPreference(R.string.conf_pref_default_account_key,account.GetDefaultAccount());
			setCheckBoxPreference(R.string.conf_pref_secured_key,account.GetSecured());
			setListBoxPreference(R.string.conf_pref_ka_type_key,account.GetKaType());
			setTextPreference(R.string.conf_pref_psk_key,account.GetPsk());
			setTextPreference(R.string.conf_pref_certificate_key,account.GetCertificate());
			setTextPreference(R.string.conf_pref_private_key_key,account.GetPrivateKey());
			setTextPreference(R.string.conf_pref_ca_file_key,account.GetCaFile());
			setCheckBoxPreference(R.string.conf_pref_dh_enabled_key,account.GetDhEnabled());
			setCheckBoxPreference(R.string.conf_pref_psk_enabled_key,account.GetPskEnabled());
			setCheckBoxPreference(R.string.conf_pref_check_cert_key,account.GetCheckCert());
			
			KeyManagementServer kms = account.GetKms();
			setCheckBoxPreference(R.string.conf_pref_kms_overridden_key,kms.GetOverridden());
			setTextPreference(R.string.conf_pref_kms_username_key,kms.GetUsername());
			setTextPreference(R.string.conf_pref_kms_password_key,kms.GetPassword());
			setTextPreference(R.string.conf_pref_kms_url_key,kms.GetUrl());
			
			Servers servers = cfh.GetServers();
			setCheckBoxPreference(R.string.conf_pref_udp_server_key,servers.GetUdpServer());
			setCheckBoxPreference(R.string.conf_pref_tcp_server_key,servers.GetTcpServer());
			setCheckBoxPreference(R.string.conf_pref_tls_server_key,servers.GetTlsServer());
			
			Ports ports = cfh.GetPorts();
			setTextPreference(R.string.conf_pref_local_udp_port_key,ports.GetUdpPort());
			setTextPreference(R.string.conf_pref_local_tcp_port_key,ports.GetTcpPort());
			setTextPreference(R.string.conf_pref_local_tls_port_key,ports.GetTlsPort());
			
			SoundDevice sound = cfh.GetSoundDevice();
			setTextPreference(R.string.conf_pref_sound_device_key,sound.GetDevice());
			setTextPreference(R.string.conf_pref_sound_device_in_key,sound.GetDeviceIn());
			setTextPreference(R.string.conf_pref_sound_device_out_key,sound.GetDeviceOut());
			setListBoxPreference(R.string.conf_pref_mixer_type_key,sound.GetMixerType());
			setListBoxPreference(R.string.conf_pref_codec_key,sound.GetCodec());

			Stun stun = cfh.GetStun();
			setCheckBoxPreference(R.string.conf_pref_stun_use_key,stun.GetUseStun());
			setCheckBoxPreference(R.string.conf_pref_stun_server_autodetect_key,stun.GetServerAutodetect());
			setTextPreference(R.string.conf_pref_stun_server_domain_key,stun.GetServerDomain());
			setTextPreference(R.string.conf_pref_stun_manual_server_key,stun.GetManualServer());
			
			Test test = cfh.GetTest();
			setCheckBoxPreference(R.string.conf_pref_sakke_test_key,test.GetSakkeTest());
		}
	}
	
	private void disableTextPreference(int key)
	{
		mEditTextPreference = getPreferenceText(key);
		mEditTextPreference.setEnabled(false);
	}
	
	private void enableTextPreference(int key)
	{
		mEditTextPreference = getPreferenceText(key);
		mEditTextPreference.setEnabled(true);
	}
	
	private void disableCheckBoxPreference(int key)
	{
		mCheckBoxPreference = getPreferenceCheckBox(key);
		mCheckBoxPreference.setEnabled(false);
	}
	
	private void enableCheckBoxPreference(int key)
	{
		mCheckBoxPreference = getPreferenceCheckBox(key);
		mCheckBoxPreference.setEnabled(true);
	}
	
	private void disableListPreference(int key)
	{
		mListPreference = getPreferenceListBox(key);
		mListPreference.setEnabled(false);
	}
	
	private void enableListPreference(int key)
	{
		mListPreference = getPreferenceListBox(key);
		mListPreference.setEnabled(true);
	}
	
	private String getTextPreference(int key)
	{
		mEditTextPreference = getPreferenceText(key);
		return mEditTextPreference.getText();
	}
	
	private boolean getCheckBoxPreference(int key)
	{
		mCheckBoxPreference = getPreferenceCheckBox(key);
		return mCheckBoxPreference.isChecked();
	}
	
	private String getListBoxPreference(int key)
	{
		mListPreference = getPreferenceListBox(key);
		return mListPreference.getValue();
	}
	
	private EditTextPreference getPreferenceText(int key)
	{
		return (EditTextPreference)getPreferenceScreen().findPreference(
                	getResources().getText(key));
	}
	
	private CheckBoxPreference getPreferenceCheckBox(int key)
	{
		return (CheckBoxPreference)getPreferenceScreen().findPreference(
                getResources().getText(key));
	}
	
	private ListPreference getPreferenceListBox(int key)
	{
		return (ListPreference)getPreferenceScreen().findPreference(
                getResources().getText(key));
	}
	
	private void setTextPreference(int key, String value)
	{
		mEditTextPreference = (EditTextPreference)getPreferenceScreen().findPreference(
            getResources().getText(key));
		mEditTextPreference.setText(value);
	}
	
	private void setCheckBoxPreference(int key, boolean value)
	{
		mCheckBoxPreference = (CheckBoxPreference)getPreferenceScreen().findPreference(
                getResources().getText(key));
		mCheckBoxPreference.setChecked(value);
	}
	
	private void setListBoxPreference(int key, String value)
	{
		mListPreference = (ListPreference)getPreferenceScreen().findPreference(
                getResources().getText(key));
		mListPreference.setValue(value);
	}
	
	
	private void saveOnlineConfiguration()
	{
		MinisipCommand cmd = new MinisipCommand(Type.SAVE_CONFIG);
		// SEND TO MINISIP
		sendToMinisipService(cmd);
		
		Log.i("saveOnlineConfiguration",cmd.toString());
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        
        Log.i(TAG,"onResume");
        
        if(offLineMode == false)
		{
        	// We are using online configuration
        	connectToMinisipService();
		}
        
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i(TAG,"onPause");
        
        if(offLineMode == false)
		{
        	// Save parameters
        	saveOnlineConfiguration();
        	
        	// We are using online configuration
        	disconnectFromMinisipService();
		}
        
        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	Log.i(TAG,"onDestroy");
    	if(offLineMode == false)
		{
    		// Save parameters
        	saveOnlineConfiguration();
    		disconnectFromMinisipService();
		}
    }
    
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		
		Log.w(TAG,"Preferences Changed");
		
		writeConfiguration(key);
    }
	
	private void processMinisipOption(MinisipOption option)
	{
		Log.i(TAG,"processMinisipOption" + option.toString());
		
		ConfigurationFileHelper cfh = new ConfigurationFileHelper();
		
		Account account = cfh.GetAccount();
		Proxy proxy = account.GetProxy();
		KeyManagementServer kms = account.GetKms();
		
		// The option will look like a set but we are ok 
		if(option.getType() == MinisipOption.Type.OPTION_SET) {
			// We should only receive Get Option
			if(option.getOption()== proxy.REGISTER) {
				// Update 
				setCheckBoxPreference(R.string.conf_pref_register_key,
						ConfigurationFileHelper.GetBooleanFromString(option.getValue()));
			} else if(option.getOption()== account.PSK) {
				// Update
				setTextPreference(R.string.conf_pref_psk_key,option.getValue());
			} else if(option.getOption()== account.SECURED) {
				// Update
				setCheckBoxPreference(R.string.conf_pref_secured_key,
						ConfigurationFileHelper.GetBooleanFromString(option.getValue()));
			} else if(option.getOption()== account.PSKENABLED) {
				// Update
				setCheckBoxPreference(R.string.conf_pref_psk_enabled_key,
						ConfigurationFileHelper.GetBooleanFromString(option.getValue()));
			} else if(option.getOption()== kms.OVERRIDDEN) {
				// Update
				setCheckBoxPreference(R.string.conf_pref_kms_overridden_key,
						ConfigurationFileHelper.GetBooleanFromString(option.getValue()));
			} else if(option.getOption()== kms.USERNAME) {
				// Update
				setTextPreference(R.string.conf_pref_kms_username_key,option.getValue());
			} else if(option.getOption()== kms.PASSWORD) {
				// Update
				setTextPreference(R.string.conf_pref_kms_password_key,option.getValue());
			} else if(option.getOption()== kms.URL) {
				// Update
				setTextPreference(R.string.conf_pref_kms_url_key,option.getValue());
			} else if(option.getOption()== kms.VERIFYSSL) {
				// Update
				setCheckBoxPreference(R.string.conf_pref_kms_verify_ssl_key,
				ConfigurationFileHelper.GetBooleanFromString(option.getValue()));
			} else if(option.getOption()== ConfigurationFileHelper.SAKKE_TEST) {
				// Update
				setCheckBoxPreference(R.string.conf_pref_sakke_test_key,
				ConfigurationFileHelper.GetBooleanFromString(option.getValue()));
			}
		}
		
	}
	
	private void processMinisipCommand(MinisipCommand cmd) {
		
	}
	
	private void sendToMinisipService(MinisipCommand cmd)
    {
    	// Create a MinisipCommand
       	Log.i(TAG, "sendToMinisipService: " + cmd.getType());
       	Message msg = Message.obtain(null, MinisipService.MSG_MINISIP_COMMAND);
        msg.setData(MinisipMessengerHelper.getCommandAsBundle(cmd));
        try {
        	mMinisipService.send(msg);
        } catch (RemoteException e) {
        	// The service is dead.  Attempt Rebind
        	disconnectFromMinisipService();
        	connectToMinisipService();
            Log.i(TAG, "RemoteException: ");
        }
    }
    
    /** Messenger for communicating with service. */
    Messenger mMinisipService = null;
    /** Flag indicating whether we have called bind on the service. */
    boolean mIsMinisipBound;
    
    /**
     * Handler of incoming messages from service.
     */
    class IncomingMinisipHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
        	Log.i("Config:IncomingMinisipHandler", "Handling Message");
            switch (msg.what) {
                case MinisipService.MSG_MINISIP_CALL:
                    //mCallbackText.setText("Received from service: " + msg.arg1);
                    break;
                case MinisipService.MSG_MINISIP_COMMAND_REPLY:
                	Bundle data = msg.getData();
                	//MinisipCommandReply = MinisipMessengerHelper
                	MinisipOption option = MinisipMessengerHelper.getBundleAsOption(data);
                	Log.i("Config:IncomingMinisipHandler", "Got Reply :" + option.toString());
                	// Now act on the information
                	processMinisipOption(option);
                	break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
    
    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingMinisipHandler());
    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mMinisipConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.
            mMinisipService = new Messenger(service);
                       
            // We want to monitor the service for as long as we are
            // connected to it.
            try {
                Message msg = Message.obtain(null,
                        MinisipService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mMinisipService.send(msg);
                
                // Give it some value as an example.
                //msg = Message.obtain(null,
                //        MinisipService.MSG_MINISIP_CALL, this.hashCode(), 0);
                //mMinisipService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }
            
            // As part of the sample, tell the user what happened.
            Log.i("mMinisipConnection","onServiceConnected");
            
            if(offLineMode == false)
            {
            	// We are on line so read minisip configuration
            	 readConfigurationFromMinisip();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mMinisipService = null;
            Log.w(TAG,"onServiceDisconnected: " + configupdated); 
            if(configupdated == true)
            {
            	// We disconnected because we want to shutdown
            	try {
            		getBaseContext().stopService(new Intent(Configuration.this, MinisipService.class));
            	} catch (SecurityException e) {
            		Log.w(TAG,"Can't stop service");
            	}
            }

            // As part of the sample, tell the user what happened.
            Log.i("mMinisipConnection","onServiceDisconnected");
        }
    };
    
    void connectToMinisipService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(Configuration.this, 
                MinisipService.class), mMinisipConnection, Context.BIND_AUTO_CREATE);
        mIsMinisipBound = true;

        // Request a Status update
        
        Log.i(TAG,"doBindService");
    }
    
    void disconnectFromMinisipService() {
        if (mIsMinisipBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mMinisipService != null) {
                try {
                    Message msg = Message.obtain(null,
                    		MinisipService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mMinisipService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }
            
            // Detach our existing connection.
            unbindService(mMinisipConnection);
            mIsMinisipBound = false;
        }
        
        Log.i(TAG,"doUnbindService");
    }
}
