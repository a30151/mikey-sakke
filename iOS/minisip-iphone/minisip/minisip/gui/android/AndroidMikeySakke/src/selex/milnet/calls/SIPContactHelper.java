package selex.milnet.calls;

import android.net.Uri;
import android.provider.ContactsContract;

public class SIPContactHelper {
	// The contacts URI
	private static Uri SIPUri = ContactsContract.Data.CONTENT_URI;
		
	/* The columns we are interested in from the database  */
	private static final String[] PROJECTION = new String[] {
		ContactsContract.Data._ID, 
		ContactsContract.Data.DISPLAY_NAME,
		ContactsContract.Data.PHOTO_ID,
		ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS,
	};
	  
	private static final String selection =  ContactsContract.Data.MIMETYPE+" ='"  
			+ ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE+"'";
	
	private static final String[] selectionArgs = 
			null;
	
	private static final String sortOrder = 
			ContactsContract.Contacts.DISPLAY_NAME+ " COLLATE LOCALIZED ASC";
	
	public static Uri GetUri()
	{
		return SIPUri;
	}
	
	public static String[] GetProjection()
	{
		return PROJECTION;
	}
	
	public static String GetSelection()
	{
		return selection;
	}
	
	public static String[] GetSelectionArgs()
	{
		return selectionArgs;
	}
	
	public static String GetSortOrder()
	{
		return sortOrder;
	}
}
