package selex.milnet.calls;

import selex.milnet.mk.R;
import android.app.ListActivity;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class SIPContacts extends ListActivity{
	private static final String TAG =  SIPContacts.class.getName();
	
    // The Contacts cursor
    Cursor mSIPCursor;
    
    /* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.sip_contact_list);
    	this.getListView().setDividerHeight(2);
    	
    	// If no data was given in the intent (because we were started
        // as a MAIN activity), then use our default content provider.
        Intent intent = getIntent();
        if (intent.getData() == null) {
            intent.setData(SIPContactHelper.GetUri());
        }
        
        // Inform the list we provide context menus for items
        getListView().setOnCreateContextMenuListener(this);
        
        //String selection =  ContactsContract.Data.MIMETYPE+" ='"  
    		//	+ ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE+"'";
    	//String[] selectionArgs = null;
    	//String sortOrder = ContactsContract.Contacts.DISPLAY_NAME+ " COLLATE LOCALIZED ASC";
    	mSIPCursor = managedQuery(SIPContactHelper.GetUri(), 
    			SIPContactHelper.GetProjection(), SIPContactHelper.GetSelection(), 
    			SIPContactHelper.GetSelectionArgs(), SIPContactHelper.GetSortOrder());
    	String[] from = new String[]{ContactsContract.Data.DISPLAY_NAME,
    			ContactsContract.Data.DATA1,
    			ContactsContract.Data.PHOTO_ID};
    	//int[] to = new int[]{android.R.id.empty};
    	int[] to = new int[]{R.id.sip_contact_name,
    			R.id.sip_contact_number,
    			R.id.sip_contact_photo_id};
    	SIPAdapter adapter = new SIPAdapter(
    			this,R.layout.sip_contact_row,mSIPCursor,from,to);
    	setListAdapter(adapter);
        
    	Log.i(TAG,"Created " + TAG);
    }
    
    @Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = null;
		try {
			info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		} catch (ClassCastException e) {
			Log.e(TAG, "bad menuInfo",e);
		}
		
		Uri profileUri = ContentUris.withAppendedId(getIntent().getData(), info.id);
		
		switch (item.getItemId()) {
		case R.id.profile_context_open:
			// Launch activity to view/edit the currently selected item
			startActivity(new Intent(Intent.ACTION_EDIT,profileUri));
			return true;
		case R.id.profile_context_delete:
			// Delete the profile that the context menu is for
			getContentResolver().delete(profileUri, null, null);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
 
	// Opens the second activity if an entry is clicked
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		String number;
		// Create an intent and pass the phone number
		Intent intent = new Intent(this,Caller.class);
		mSIPCursor.moveToPosition(position);
		int index = mSIPCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME);
		intent.putExtra("Name",mSIPCursor.getString(index));
		index = mSIPCursor.getColumnIndex(ContactsContract.Data.DATA1);
		number = mSIPCursor.getString(index);
		//intent.putExtra("Number",);
		index = mSIPCursor.getColumnIndex(ContactsContract.Data.PHOTO_ID);
		intent.putExtra("Photo",mSIPCursor.getString(index));
		
		
		// Make this a Minisip style number
		StringBuilder sb = new StringBuilder();
		//sb.append("sip:");
		sb.append(number);
		//sb.append("@localhost");
		intent.putExtra("Number",sb.toString());
		
		// Set the Mode
		intent.putExtra("Calling", true);
		
		Log.i(TAG, "callContact : " + sb.toString());
		startActivity(intent);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "SIP Cursor Closed");
		mSIPCursor.close();
	}
}
