package selex.milnet.util;

public class OptionHelper {
	private final static String TAG = OptionHelper.class.getName();
	
	private String option;
	private String value;
	
	public OptionHelper(String option, String value)
	{
		this.option = option;
		this.value = value;
	}
	
	public String getOption() {
		return option;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Option: ");
		sb.append(option);
		sb.append(" Value: ");
		sb.append(value);
		
		return sb.toString();
	}
}
