//
//  MSMinisipController.m
//  MikeyVOIPClient
//
//  Created by Mikey Project on 17/09/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import "MSMinisipController.h"

#include <libminisip/Minisip.h>
#include <libmutil/CommandString.h>
#include <libmsip/SipCommandString.h>
#include <libminisip/media/MediaCommandString.h>

#import "MSTestGui.h"

#define DEBUG_OUTPUT 1

@interface MSMinisipController ()

@end

@implementation MSMinisipController

@synthesize callingId = _callingId;
@synthesize remoteCallId = _remoteCallId;

MSTestGui* gui;

-(void)startMinisip
{
    // Get path to config file
    NSString* pathInMainBundle = [[NSBundle mainBundle] pathForResource:@"minisip" ofType:@"conf"];
    //NSArray* documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    //NSString* docsDir = [documentPaths objectAtIndex:0];
    //NSString* pathInDocuments = [docsDir stringByAppendingPathComponent:@"minisip.conf"];
    NSString* pathInDocuments = [NSString stringWithFormat:@"%@/Documents/minisip.conf", NSHomeDirectory()];
    NSLog(@"Path in documents: %@", pathInDocuments);
    BOOL exists_in_docs = [[NSFileManager defaultManager] fileExistsAtPath:pathInDocuments];
    
    BOOL exists_in_bundle = [[NSFileManager defaultManager] fileExistsAtPath:pathInMainBundle];
    if(!exists_in_docs)
    {
        NSFileManager* fmngr = [[NSFileManager alloc] init];
        NSError* error;
        if(![fmngr copyItemAtPath:pathInMainBundle toPath:pathInDocuments error:&error])
        {
            NSLog(@"Error: %@", [error description]);
        }
        [fmngr release];
    }
    
    
    //NSString* ns_args = [NSString stringWithFormat:@"-c %@", pathInDocuments];
    std::string args_str = [pathInDocuments cStringUsingEncoding:[NSString defaultCStringEncoding]];
    
    //char *buffer = new char[0];
//    std::string path = "-c\0";
    char *buffer = (char*)args_str.c_str();
    
    gui = new MSTestGui(self);
    Minisip *minisip = new Minisip(gui, 1, &buffer);
    
    minisip->startSip();
    
    //CommandString* test_invite = new CommandString("test2@192.168.1.4", "invite");
    
    
}

-(void)demoCall
{
    [self call:@"test2@192.168.1.3:5060"];
//    self.callingId = @"test2@192.168.1.3";
//    CommandString cmd = CommandString("test2@192.168.1.3:5060", SipCommandString::invite, "test2@192.168.1.3:5060");
//    std::cout << "Command string: " << cmd.getOp() << " : " << cmd.getDestinationId() << " : " << cmd.getParam() << " : " << cmd.getParam() << std::endl;
//    CommandString resp = gui->sendCommandResp("sip", cmd);
}

-(void)demoCallTest1
{
    [self call:@"test1@192.168.1.3:5060"];
//    self.callingId = @"test1@192.168.1.3";
//    CommandString cmd = CommandString("", SipCommandString::invite, "test1@192.168.1.3:5060");//test1@192.168.1.3:5060
//    std::cout << "Command string: " << cmd.getOp() << " : " << cmd.getDestinationId() << " : " << cmd.getParam() << " : " << cmd.getParam() << std::endl;
//    CommandString resp = gui->sendCommandResp("sip", cmd);
//    std::cout << "Response: " << &resp << std::endl;
}

-(void)demoCall1111111
{
    [self call:@"1111111@192.168.1.3:5060"];
//    self.callingId = @"1111111@192.168.1.3";
//    CommandString cmd = CommandString("", SipCommandString::invite, "1111111@192.168.1.3:5060");
//    std::cout << "Command string: " << cmd.getOp() << " : " << cmd.getDestinationId() << " : " << cmd.getParam() << " : " << cmd.getParam() << std::endl;
//    CommandString resp = gui->sendCommandResp("sip", cmd);
//    std::cout << "Response to call1: " << resp.getDestinationId() << std::endl;
}

-(void)demoCall2222222
{
    [self call:@"2222222@192.168.1.3:5060"];
//    self.callingId = @"2222222@192.168.1.3";
//    CommandString cmd = CommandString("", SipCommandString::invite, "2222222@192.168.1.3:5060");
//    std::cout << "Command string: " << cmd.getOp() << " : " << cmd.getDestinationId() << " : " << cmd.getParam() << " : " << cmd.getParam() << std::endl;
//    CommandString resp = gui->sendCommandResp("sip", cmd);
//    std::cout << "Response to call2: " << resp.getDestinationId() << std::endl;
}

-(void)call:(NSString*)recipient_id
{
    self.callingId = recipient_id;
    std::string c_recipient = [recipient_id cStringUsingEncoding:[NSString defaultCStringEncoding]];
    CommandString cmd = CommandString("", SipCommandString::invite, c_recipient);
    std::cout << "Command string: " << cmd.getOp() << " : " << cmd.getDestinationId() << " : " << cmd.getParam() << " : " << cmd.getParam() << std::endl;
    CommandString resp = gui->sendCommandResp("sip", cmd);
    std::cout << "Response to call: " << resp.getDestinationId() << std::endl;
    
    
    self.remoteCallId = [NSString stringWithCString:resp.getDestinationId().c_str()
                                           encoding:[NSString defaultCStringEncoding]];
    
}

-(void)registerSent
{
    NSLog(@"Register sent");
    [self performSelectorOnMainThread:@selector(sendRegisterSentNotifiation)
                           withObject:nil
                        waitUntilDone:NO];
}

-(void)sendRegisterSentNotifiation
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:3];
    //    [dict setObject:lastRSK forKey:RSK_KEY];
    //    [dict setObject:lastSSK forKey:SSK_KEY];
    //    [dict setObject:lastPVT forKey:PVT_KEY];
    //    [dict setObject:lastHS forKey:HS_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:REGISTER_SENT_EVENT
                                                        object:self
                                                      userInfo:dict];
    [dict release];
}

-(void)registerOk
{
    NSLog(@"Register ok");
    [self performSelectorOnMainThread:@selector(sendRegisterOkNotification)
                           withObject:nil
                        waitUntilDone:NO];
}

-(void)sendRegisterOkNotification
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:3];
    //    [dict setObject:lastRSK forKey:RSK_KEY];
    //    [dict setObject:lastSSK forKey:SSK_KEY];
    //    [dict setObject:lastPVT forKey:PVT_KEY];
    //    [dict setObject:lastHS forKey:HS_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:REGISTER_OK_EVENT
                                                        object:self
                                                      userInfo:dict];
    [dict release];
}

-(void)inviteOk
{
    NSLog(@"Invite ok");
    [self performSelectorOnMainThread:@selector(sendInviteOkNotification)
                           withObject:nil
                        waitUntilDone:NO];
}

-(void)sendInviteOkNotification
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:3];
    [[NSNotificationCenter defaultCenter] postNotificationName:INVITE_OK_EVENT
                                                        object:self
                                                      userInfo:dict];
    [dict release];
}

-(void)incomingAvailableFrom:(NSString*)caller withCallId:(NSString*)call_id
{
    //
    NSLog(@"Incoming available");
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:3];
    [dict setObject:call_id forKey:@"CallID"];
    [dict setObject:caller forKey:@"Caller"];

    [self performSelectorOnMainThread:@selector(sendIncomingAvailableNotification:)
                           withObject:dict
                        waitUntilDone:YES];
    [dict release];
}

-(void)sendIncomingAvailableNotification:(NSDictionary*)details
{
    [[NSNotificationCenter defaultCenter] postNotificationName:INCOMING_AVAILABLE_EVENT
                                                        object:self
                                                      userInfo:details];
}

-(void)callTerminated
{
    [self performSelectorOnMainThread:@selector(sendCallTerminatedNotification)
                           withObject:nil
                        waitUntilDone:NO];
}

-(void)sendCallTerminatedNotification
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:3];
    [[NSNotificationCenter defaultCenter] postNotificationName:CALL_TERMINATED_EVENT
                                                        object:self
                                                      userInfo:dict];
    [dict release];
}

-(void)remoteHangUp
{
    [self performSelectorOnMainThread:@selector(sendRemoteHangUpNotification)
                           withObject:nil
                        waitUntilDone:NO];
}

-(void)sendRemoteHangUpNotification
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:3];
    [[NSNotificationCenter defaultCenter] postNotificationName:REMOTE_HANG_UP_EVENT
                                                        object:self
                                                      userInfo:dict];
    [dict release];
}

-(void)acceptCallWithId:(NSString*)call_id
{
    self.remoteCallId = call_id;
    std::string c_call_id = [call_id cStringUsingEncoding:[NSString defaultCStringEncoding]];
    CommandString cmd = CommandString(c_call_id, SipCommandString::accept_invite);
    gui->sendCommand("sip", cmd);
    CommandString cmd2 = CommandString(c_call_id, MediaCommandString::set_session_sound_settings, "senders", "ON");
    gui->sendCommand("media", cmd2);
}

-(void)hangUpCallWithId:(NSString*)call_id
{
    std::string c_call_id = [call_id cStringUsingEncoding:[NSString defaultCStringEncoding]];
    CommandString cmd = CommandString(c_call_id, SipCommandString::hang_up);
    gui->sendCommand("sip", cmd);
}



@end
