//
//  MSTestGui.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 04/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSMinisipController.h"

#include <libminisip/gui/Gui.h>

class MSTestGui : public Gui
{
public:
    MSTestGui(const MSMinisipController* controller);
    
    virtual ~MSTestGui();
    
    virtual void setSipSoftPhoneConfiguration(MRef<SipSoftPhoneConfiguration *> sipphoneconfig);
    
    virtual void setContactDb(MRef<ContactDb *> contactDb);
    
    virtual void handleCommand(const CommandString &command);
    
    virtual bool configDialog( MRef<SipSoftPhoneConfiguration *> conf );
    
    virtual void run();
    
};