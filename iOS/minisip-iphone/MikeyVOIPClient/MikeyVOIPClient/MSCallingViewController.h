//
//  MSCallingViewController.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 05/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSCallingViewController : UIViewController
{
    IBOutlet UILabel* calling;
}

-(IBAction)cancelButtonPressed:(id)sender;
-(void)callAccepted;

@property (nonatomic, retain) NSString* idToCall;

@end
