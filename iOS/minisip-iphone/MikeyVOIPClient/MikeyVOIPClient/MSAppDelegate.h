//
//  MSAppDelegate.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 17/09/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MSMinisipController;

@interface MSAppDelegate : UIResponder <UIApplicationDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) MSMinisipController* controller;

@end
