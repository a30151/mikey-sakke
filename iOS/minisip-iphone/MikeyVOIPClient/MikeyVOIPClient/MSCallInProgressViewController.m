//
//  MSCallInProgressViewController.m
//  MikeyVOIPClient
//
//  Created by Mikey Project on 12/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import "MSCallInProgressViewController.h"
#import "MSAppDelegate.h"
#import "MSMinisipController.h"

@interface MSCallInProgressViewController ()

@end

@implementation MSCallInProgressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callEnded)
                                                 name:CALL_TERMINATED_EVENT
                                               object:delegate.controller];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callEnded)
                                                 name:REMOTE_HANG_UP_EVENT
                                               object:delegate.controller];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)hangUpCall:(id)sender
{
//    if(self.callDetails)
//    {
    
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    MSMinisipController* controller = delegate.controller;
    NSString* call_id = controller.remoteCallId;
    
//        NSString* caller_id = [self.callDetails objectForKey:@"CallID"];
//        if(caller_id)
//        {

            [delegate.controller hangUpCallWithId:call_id];
//        }
//    }
}

-(void)callEnded
{
    [self performSegueWithIdentifier:@"home" sender:self];
}

@end
