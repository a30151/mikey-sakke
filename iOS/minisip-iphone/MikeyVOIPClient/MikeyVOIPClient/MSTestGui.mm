//
//  MSTestGui.m
//  MikeyVOIPClient
//
//  Created by Mikey Project on 04/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import "MSTestGui.h"
#import <libmsip/SipCommandString.h>
#include <libmutil/CommandString.h>
#include <libminisip/media/MediaCommandString.h>

const MSMinisipController* theController;


MSTestGui::MSTestGui(const MSMinisipController* controller)
{
    std::cout << "MSMinisipGui" << std::endl;
    theController = controller;
}

MSTestGui::~MSTestGui()
{
    
}

void MSTestGui::setSipSoftPhoneConfiguration(MRef<SipSoftPhoneConfiguration *> sipphoneconfig)
{
    std::cout << "setSipSoftPhoneConfiguration" << std::endl;
}

void MSTestGui::setContactDb(MRef<ContactDb *> contactDb)
{
    std::cout << "setContactDb" << std::endl;
}

void MSTestGui::handleCommand(const CommandString &command)
{
    std::cout << "handleCommand: " << command.getString() << ", " << command.getOp() << ", " << command.getParam() << std::endl;
    
    if(command.getOp() == SipCommandString::incoming_available)
    {
        std::cout << "We're receiving a call!!!" << std::endl;
        std::cout << "Command: " << command.getString() << ", " << command.getOp() << ", " << command.getParam() << ", " << command.getParam2() << ", " << command.getParam3() << ", " << command.getDestinationId() << std::endl;
        NSString* caller = [NSString stringWithCString:command.getParam().c_str()
                                              encoding:NSASCIIStringEncoding];
        
        NSString* call_id = [NSString stringWithCString:command.getDestinationId().c_str()
                                               encoding:NSASCIIStringEncoding];
       
        
        [theController incomingAvailableFrom:caller
                                  withCallId:call_id];
        
    }
    else if(command.getOp() == SipCommandString::register_sent)
    {
        std::cout << "Register sent" << std::endl;
        [theController registerSent];
    }
    else if(command.getOp() == SipCommandString::register_ok)
    {
        std::cout << "Register ok" << std::endl;
        [theController registerOk];
    }
    else if(command.getOp() == SipCommandString::invite_ok)
    {
        std::cout << "Invite accepted" << std::endl;
        [theController inviteOk];
        std::string call_Id =  [theController.remoteCallId cStringUsingEncoding:[NSString defaultCStringEncoding]];
        CommandString cmdStr( call_Id, MediaCommandString::set_session_sound_settings, "senders", "ON");
        sendCommand("media", cmdStr);
    }
    else if(command.getOp() == SipCommandString::call_terminated)
    {
        std::cout << "Call terminated" << std::endl;
        [theController callTerminated];
    }
    else if(command.getOp() == SipCommandString::remote_hang_up)
    {
        std::cout << "Remote hang up" << std::endl;
        [theController remoteHangUp];
    }
    else if(command.getOp() == SipCommandString::remote_user_not_found)
    {
        std::cout << "Remote user not found" << std::endl;
    }
    else if(command.getOp() == SipCommandString::remote_cancelled_invite)
    {
        std::cout << "Remote user cancelled the call" << std::endl;
    }
    else if(command.getOp() == SipCommandString::transport_error)
    {
        std::cout << "Call not completed owing to network error" << std::endl;
    }
    else if(command.getOp() == SipCommandString::remote_reject)
    {
        std::cout << "Remote user rejected the call" << std::endl;
    }
    
}

bool MSTestGui::configDialog( MRef<SipSoftPhoneConfiguration *> conf )
{
    std::cout << "configDialog" << std::endl;
    return false;
}

void MSTestGui::run()
{
    std::cout << "run" << std::endl;
}

