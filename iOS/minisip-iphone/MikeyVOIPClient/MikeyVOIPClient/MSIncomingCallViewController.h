//
//  MSIncomingCallViewController.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 04/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MSIncomingCallViewController : UIViewController
{
    IBOutlet UILabel* caller;
    
    AVAudioPlayer* ringing;
}

-(IBAction)acceptCall:(id)sender;

@property (nonatomic, retain) NSDictionary* callDetails;

@end
