//
//  MSCallInProgressViewController.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 12/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSCallInProgressViewController : UIViewController

-(IBAction)hangUpCall:(id)sender;

@end
