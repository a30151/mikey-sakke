//
//  MSIncomingCallViewController.m
//  MikeyVOIPClient
//
//  Created by Mikey Project on 04/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import "MSIncomingCallViewController.h"

#import "MSAppDelegate.h"
#import "MSMinisipController.h"

@interface MSIncomingCallViewController ()

@end

@implementation MSIncomingCallViewController

@synthesize callDetails = _callDetails;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
//    NSString* audio_file = [[NSBundle mainBundle] pathForResource:@"Pew" ofType:@"caf"];
//    NSURL* other_file = [NSURL fileURLWithPath:audio_file];
////    NSURL* ringingFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Pew" ofType:@"caf"]];
//    AVAudioPlayer* ringing = [[AVAudioPlayer alloc] initWithContentsOfURL:other_file error:nil];
//    [ringing setNumberOfLoops:-1];
//    [ringing play];
    //[ringing release];
    
    
    NSString* audio_file = [[NSBundle mainBundle] pathForResource:@"ColdFunk" ofType:@"caf"];
    NSURL* other_file = [NSURL fileURLWithPath:audio_file];
    NSError* error;
    ringing = [[AVAudioPlayer alloc] initWithContentsOfURL:other_file error:&error];
    if(error)
    {
        NSLog(@"Error: %@", [error description]);
    }
    [ringing setNumberOfLoops:-1];
    [ringing play];
    
    if(self.callDetails)
    {
        NSString* caller_string = [self.callDetails objectForKey:@"Caller"];
        if(caller_string)
        {
            caller.text = caller_string;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)acceptCall:(id)sender
{
    [ringing stop];
    if(self.callDetails)
    {
        NSString* caller_id = [self.callDetails objectForKey:@"CallID"];
        if(caller_id)
        {
            MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            [delegate.controller acceptCallWithId:caller_id];
    //        [self dismissModalViewControllerAnimated:NO];
            [self performSegueWithIdentifier:@"inCall" sender:self];
        }
    }
}

- (IBAction)rejectCall:(id)sender {
    [ringing stop];
    if(self.callDetails)
    {
        NSString* caller_id = [self.callDetails objectForKey:@"CallID"];
        if(caller_id)
        {
            MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            [delegate.controller hangUpCallWithId:caller_id];
            //        [self dismissModalViewControllerAnimated:NO];
            [self performSegueWithIdentifier:@"home" sender:self];
        }
    }
}

@end
