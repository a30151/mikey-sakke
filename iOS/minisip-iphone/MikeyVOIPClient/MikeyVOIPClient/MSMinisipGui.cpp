//
//  MSMinisipGui.cpp
//  MikeyVOIPClient
//
//  Created by Mikey Project on 17/09/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#include "MSMinisipGui.h"
#include <libmutil/CommandString.h>
#import <libmsip/SipCommandString.h>

MSMinisipGui::MSMinisipGui()
{
    std::cout << "MSMinisipGui" << std::endl;
}

MSMinisipGui::~MSMinisipGui()
{
    
}

void MSMinisipGui::setSipSoftPhoneConfiguration(MRef<SipSoftPhoneConfiguration *> sipphoneconfig)
{
    std::cout << "setSipSoftPhoneConfiguration" << std::endl;    
}

void MSMinisipGui::setContactDb(MRef<ContactDb *> contactDb)
{
    std::cout << "setContactDb" << std::endl; 
}

void MSMinisipGui::handleCommand(const CommandString &command)
{
    std::cout << "handleCommand: " << command.getString() << ", " << command.getOp() << ", " << command.getParam() << std::endl;
    
    if(command.getOp() == SipCommandString::incoming_available)
    {
        std::cout << "We're receiving a call!!!" << std::endl;
        std::cout << "Command: " << command.getString() << ", " << command.getOp() << ", " << command.getParam() << ", " << command.getParam2() << ", " << command.getParam3() << ", " << command.getDestinationId() << std::endl;
    }
    else if(command.getOp() == SipCommandString::register_sent)
    {
        std::cout << "Register sent" << std::endl;
    }
    else if(command.getOp() == SipCommandString::register_ok)
    {
        std::cout << "Register ok" << std::endl;
    }
}

bool MSMinisipGui::configDialog( MRef<SipSoftPhoneConfiguration *> conf )
{
    std::cout << "configDialog" << std::endl; 
    return false;
}

void MSMinisipGui::run()
{
    std::cout << "run" << std::endl; 
}