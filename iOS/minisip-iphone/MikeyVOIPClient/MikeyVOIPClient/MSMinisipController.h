//
//  MSMinisipController.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 17/09/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import <Foundation/Foundation.h>

#define REGISTER_SENT_EVENT @"REGISTER_SENT"
#define REGISTER_OK_EVENT @"REGISTER_OK"
#define INVITE_OK_EVENT @"INVITE_OK"
#define INCOMING_AVAILABLE_EVENT @"INCOMING_AVAILABLE"
#define CALL_TERMINATED_EVENT @"CALL_TERMINATED"
#define REMOTE_HANG_UP_EVENT @"HANG_UP"

@interface MSMinisipController : NSObject

@property (nonatomic, retain) NSString* callingId;
@property (nonatomic, retain) NSString* remoteCallId;

-(void)startMinisip;
-(void)demoCall;
-(void)demoCallTest1;
-(void)demoCall1111111;
-(void)demoCall2222222;

-(void)registerSent;
-(void)registerOk;
-(void)inviteOk;
-(void)incomingAvailableFrom:(NSString*)caller withCallId:(NSString*)call_id;
-(void)callTerminated;
-(void)remoteHangUp;

-(void)acceptCallWithId:(NSString*)call_id;
-(void)hangUpCallWithId:(NSString*)call_id;

-(void)call:(NSString*)recipient_id;

@end
