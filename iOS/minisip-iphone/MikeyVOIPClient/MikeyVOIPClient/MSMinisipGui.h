//
//  MSMinisipGui.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 17/09/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#ifndef MikeyVOIPClient_MSMinisipGui_h
#define MikeyVOIPClient_MSMinisipGui_h

#include <libminisip/gui/Gui.h>


class MSMinisipGui : public Gui
{
public:
    MSMinisipGui();
    
    virtual ~MSMinisipGui();
    
    virtual void setSipSoftPhoneConfiguration(MRef<SipSoftPhoneConfiguration *> sipphoneconfig);
    
    virtual void setContactDb(MRef<ContactDb *> contactDb);
    
    virtual void handleCommand(const CommandString &command);
    
    virtual bool configDialog( MRef<SipSoftPhoneConfiguration *> conf );
    
    virtual void run();
    
};

#endif
