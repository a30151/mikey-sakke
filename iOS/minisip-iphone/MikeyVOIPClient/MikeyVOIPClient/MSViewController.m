//
//  MSViewController.m
//  MikeyVOIPClient
//
//  Created by Mikey Project on 17/09/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import "MSViewController.h"
#import "MSMinisipController.h"
#import "MSAppDelegate.h"
#import "MSIncomingCallViewController.h"
#import "MSCallingViewController.h"

@interface MSViewController ()

@end

@implementation MSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerSent:)
                                                 name:REGISTER_SENT_EVENT
                                               object:delegate.controller];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerOk:)
                                                 name:REGISTER_OK_EVENT
                                               object:delegate.controller];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(incomingAvailable:)
                                                 name:INCOMING_AVAILABLE_EVENT
                                               object:delegate.controller];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [sendDemoButton setHidden:YES];
    
    [sendDemoButton setHidden:NO];
}

-(IBAction)makeDemoCall
{
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    [delegate.controller demoCall];
    idToCall = @"test2";
    [self performSegueWithIdentifier:@"makingCall" sender:self];
}

-(IBAction)makeDemoCallTest1
{
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    [delegate.controller demoCallTest1];
    idToCall = @"test1";
    [self performSegueWithIdentifier:@"makingCall" sender:self];
}

-(IBAction)makeDemoCall1111111
{
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    [delegate.controller demoCall1111111];
    idToCall = @"1111111";
    [self performSegueWithIdentifier:@"makingCall" sender:self];
}

-(IBAction)makeDemoCall2222222
{
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    [delegate.controller demoCall2222222];
    idToCall = @"2222222";
    [self performSegueWithIdentifier:@"makingCall" sender:self];
}

-(IBAction)makeCall
{
    if (whoToCallField.text.length > 0)
    {
        idToCall = [NSString stringWithFormat:@"%@@192.168.1.3:5060", whoToCallField.text];
        MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
        [delegate.controller call:idToCall];
        [self performSegueWithIdentifier:@"makingCall" sender:self];
    }
}

-(void)registerSent:(NSNotification*)notification
{
    statusLabel.text = @"Register sent.";
}

-(void)registerOk:(NSNotification*)notification
{
    statusLabel.text = @"Registered successfully!";
}

-(void)incomingAvailable:(NSNotification*)notification
{
    // Get this dictionary down to the prepare for segue method below!
    lastCallInformation = notification.userInfo;
    
    [self performSegueWithIdentifier:@"incomingCall" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"incomingCall"])
    {
        MSIncomingCallViewController* view_controller = [segue destinationViewController];
        [view_controller setCallDetails:lastCallInformation];
    }
    else if([[segue identifier] isEqualToString:@"makingCall"])
    {
        MSCallingViewController* view_controller = [segue destinationViewController];
        [view_controller setIdToCall:idToCall];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

@end
