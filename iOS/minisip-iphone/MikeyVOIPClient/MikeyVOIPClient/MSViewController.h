//
//  MSViewController.h
//  MikeyVOIPClient
//
//  Created by Mikey Project on 17/09/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSViewController : UIViewController <UITextFieldDelegate>
{   
    IBOutlet UIButton* sendDemoButton;
    IBOutlet UILabel* statusLabel;
    IBOutlet UITextField* whoToCallField;
    
    NSDictionary* lastCallInformation;
    NSString* idToCall;
}

-(IBAction)makeDemoCall;
-(IBAction)makeDemoCallTest1;
-(IBAction)makeDemoCall1111111;
-(IBAction)makeDemoCall2222222;
-(IBAction)makeCall;

@end
