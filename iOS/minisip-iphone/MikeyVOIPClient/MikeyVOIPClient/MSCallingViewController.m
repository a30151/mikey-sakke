//
//  MSCallingViewController.m
//  MikeyVOIPClient
//
//  Created by Mikey Project on 05/10/2012.
//  Copyright (c) 2012 IPL. All rights reserved.
//

#import "MSCallingViewController.h"
#import "MSAppDelegate.h"
#import "MSMinisipController.h"

@interface MSCallingViewController ()

@end

@implementation MSCallingViewController

@synthesize idToCall = _idToCall;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    
    calling.text = delegate.controller.callingId;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callAccepted)
                                                 name:INVITE_OK_EVENT
                                               object:delegate.controller];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelButtonPressed:(id)sender
{
    NSLog(@"Cancel button pressed");
    [self dismissModalViewControllerAnimated:YES];
}

-(void)callAccepted
{
    [self performSegueWithIdentifier:@"inCall" sender:self];
 //   [self dismissModalViewControllerAnimated:NO];
}

@end
