//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Header File
// Item Name        : autonomous-client.h
// Item Description : Autonomous client intended as a back-end low-priority
//                    thread that would fetch and purge key material as
//                    necessary. This has not been implemented yet.
//
//******************************************************************************

#ifndef MSKMS_AUTONOMOUS_CLIENT_H
#define MSKMS_AUTONOMOUS_CLIENT_H

#include "client.h"

namespace MikeySakkeKMS
{
   typedef Client AutonomousClient; // XXX
}

#endif//MSKMS_AUTONOMOUS_CLIENT_H

