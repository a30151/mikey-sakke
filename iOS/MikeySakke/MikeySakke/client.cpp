//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Header File
// Item Name        : client.cpp
// Item Description : Class to act as a client for the KMS server.
//
//******************************************************************************

#include "client.h"
#include <curl/curl.h>
#include "json.h"
#include "key-storage.h"
#include "cptr.h"
#include "octet-string.h"
#include <iostream>

namespace MikeySakkeKMS {

Client::Client(KeyStoragePtr const& ks)
   : m_KeyStorage(ks)
{
   // TODO: HACK: use std::call_once with std::once_flag
   // TODO: ideally inside a ((constructor)) function
   static bool done = false;
   if (!done) {
      done = true;
      curl_global_init(CURL_GLOBAL_ALL);
   }
}

// Method to obtain the key material from the KMS using the supplied parameters.
// The key data is stored, and can be obtained via the KeyStoragePtr.
void Client::FetchKeyMaterial(std::string const& kmsServer,
                              SSLVerificationPolicy verifyPolicy,
                              std::string const& username,
                              std::string const& password,
                              std::string const& identifier,
                              CompletionHandler handler)
{
   using convenience::cptr;
   using namespace boost::system;
   using namespace sys::errc;

   if (kmsServer.empty() || username.empty() || password.empty() || !m_KeyStorage)
   {
      return handler(identifier, make_error_code(invalid_argument));
   }

   cptr<CURL,curl_easy_cleanup> curl = curl_easy_init(); 

   if (!curl)
   {
      return handler(identifier, make_error_code(function_not_supported));
   }

   std::string url_encoded_id;
   {
      cptr<char(),curl_free> escaped =
         curl_easy_escape(curl, identifier.c_str(), identifier.length());

      url_encoded_id.assign(escaped);
   }

   std::string url = "https://"
                   + username + ":" + password + "@"
                   + kmsServer
                   + "/secure/key?id="
                   + url_encoded_id
                   ;

   std::cerr << "FETCHING: " << url << "\n";

   struct CurlHandler
   {
      static int AppendToString(char const* data,
                                size_t size, size_t count,
                                std::string* s)
      {
         size *= count;
         s->append(data, size);
         return size;
      }
   };
   
   // Set all the required options for curl before retrieving the data from the
   // KMS
   std::string json;

   char errorBuffer[CURL_ERROR_SIZE];
   curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);  

   curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
   curl_easy_setopt(curl, CURLOPT_HEADER, 0);
   curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlHandler::AppendToString);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &json);

   if (verifyPolicy == DontVerifySSLCertificate)
      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

   // Retrieve the key data (JSON format) from the KMS
   CURLcode rc = curl_easy_perform(curl);

   if (rc != CURLE_OK)
   {
      std::cerr << errorBuffer << "\n";
      return handler(identifier, make_error_code(connection_reset));
   }

   std::cerr << "JSON: " << json << "\n";

   Json::Reader r;
   Json::Value keyData;
   if (!r.parse(json, keyData, false))
      return handler(identifier, make_error_code(bad_message));

   Json::Value privateData = keyData["private"];
   Json::Value publicData = keyData["public"];
   Json::Value communityData = keyData["community"];
   Json::Value error = keyData["error"];

   if (!error.empty())
   {
      return handler(identifier, make_error_code(operation_not_permitted));
   }

   // Store the retrieved key data
   try
   {
      if (!privateData.empty())
      {
          m_KeyStorage->StorePrivateKey(identifier, "SSK", privateData["userSecretSigningKey"].asString());
          m_KeyStorage->StorePrivateKey(identifier, "RSK", privateData["receiverSecretKey"].asString());
      }
      if (!publicData.empty())
      {
          m_KeyStorage->StorePublicKey(identifier, "PVT", publicData["userPublicValidationToken"].asString());
      }
      for (Json::ValueIterator
           it = communityData.begin(), end = communityData.end();
           it != end;
           ++it)
      {
         Json::Value const& community = *it;

         std::string communityId = community["kmsIdentifier"].asString();
   
         m_KeyStorage->AddCommunity(communityId);

         m_KeyStorage->StorePublicKey(communityId, "KPAK", community["kmsPublicAuthenticationKey"].asString());
         m_KeyStorage->StorePublicKey(communityId, "Z",    community["kmsPublicKey"].asString());
         m_KeyStorage->StorePublicParameter(communityId, "SakkeSet", community["sakkeParameterSetIndex"].asString());
      }
   }
   catch (std::invalid_argument& e)
   {
      m_KeyStorage->RevokeKeys(identifier);
      handler(identifier, make_error_code(invalid_argument));
   }

   handler(identifier, make_error_code(success));
}

} // MikeySakkeKMS

