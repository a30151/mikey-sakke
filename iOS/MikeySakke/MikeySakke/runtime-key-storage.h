//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Inline File
// Item Name        : runtime-key-storage.inl
// Item Description : Class used to store keys in memory.
//
//******************************************************************************

#ifndef MSKMS_RUNTIME_KEY_STORAGE_INL
#define MSKMS_RUNTIME_KEY_STORAGE_INL

#include "key-storage.h"
#include "octet-string.h"
#include <boost/typeof/typeof.hpp>
#include <map>
#include <set>
   
namespace MikeySakkeKMS {

/**
 * Development implementation for key storage in memory.  Key data is
 * not persisted across runs causing a fetch on each initial access
 * for each run.
 */
class RuntimeKeyStorage : public KeyStorage
{
public:

   OctetString GetPrivateKey(std::string const& identifier, 
                             std::string const& key) const
   {  
      return FetchOrDefault(privateKeys, identifier, key);
   }
   OctetString GetPublicKey(std::string const& identifier, 
                            std::string const& key) const
   {
      return FetchOrDefault(publicKeys, identifier, key);
   }
   std::string GetPublicParameter(std::string const& identifier, 
                                  std::string const& key) const
   {
      return FetchOrDefault(publicParameters, identifier, key);
   }
   void StorePrivateKey(std::string const& identifier, 
                        std::string const& key, 
                        OctetString const& value)
   {
      privateKeys[identifier][key] = value;
   }
   void StorePublicKey(std::string const& identifier, 
                       std::string const& key, 
                       OctetString const& value)
   {
      publicKeys[identifier][key] = value;
   }
   void StorePublicParameter(std::string const& identifier, 
                             std::string const& key, 
                             std::string const& value)
   {
      publicParameters[identifier][key] = value;
   }
   void AddCommunity(std::string const& community)
   {
      communities.insert(community);
   }
   std::vector<std::string> GetCommunityIdentifiers() const
   {
      return std::vector<std::string>(communities.begin(), communities.end());
   }
   void RevokeKeys(std::string const& identifier)
   {
      privateKeys.erase(identifier);
      publicKeys.erase(identifier);
      publicParameters.erase(identifier);
      communities.erase(identifier);
   }

private:


   template <class Map>
   static typename Map::mapped_type::mapped_type
   FetchOrDefault(Map const& map, 
                  std::string const& identifier, 
                  std::string const& key)
   {
      BOOST_AUTO (it, map.find(identifier));
      if (it != map.end())
      {
         BOOST_AUTO (keys, it->second);
         BOOST_AUTO (kit, keys.find(key));
         if (kit != keys.end())
            return kit->second;
      }
      return typename Map::mapped_type::mapped_type();
   }

   std::map<std::string, std::map<std::string, std::string> > publicParameters;
   std::map<std::string, std::map<std::string, OctetString> > publicKeys;
   std::map<std::string, std::map<std::string, OctetString> > privateKeys;
   std::set<std::string> communities;
};

} // MikeySakkeKMS

#endif//MSKMS_RUNTIME_KEY_STORAGE_INL

