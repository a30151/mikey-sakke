//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Class
// Item Name        : bigint-ssl.cpp
// Item Description : Class to store big integers using the open SSL library.
//
//******************************************************************************

#include "bigint-ssl.h"
#include <pthread.h>

#if TEST_BIGINT_SSL
#include <iostream>
#define TRACE(x) std::cerr << x << "\n"
#else
#define TRACE(x)
#endif

static pthread_key_t ssl_ctx_key;
static pthread_once_t ssl_ctx_key_once = PTHREAD_ONCE_INIT;
static void free_thread_specific_ctx(bigint_ssl_scratch* scratch)
{
   delete scratch;
}
static void ssl_ctx_make_key()
{
   pthread_key_create(&ssl_ctx_key, (void(*)(void*)) free_thread_specific_ctx);
}

bigint_ssl_scratch& bigint_ssl_scratch::get()
{
   pthread_once(&ssl_ctx_key_once, ssl_ctx_make_key);
   if (void* p = pthread_getspecific(ssl_ctx_key))
      return *reinterpret_cast<bigint_ssl_scratch*>(p);

   bigint_ssl_scratch* scratch = new bigint_ssl_scratch;
   pthread_setspecific(ssl_ctx_key, scratch);
   return *scratch;
}

bigint_ssl_scratch::bigint_ssl_scratch()
 : ctx(BN_CTX_new())
{
   TRACE("Create TSS context");
}
bigint_ssl_scratch::~bigint_ssl_scratch()
{
   BN_CTX_free(ctx);
   TRACE("Free TSS context");
}



bigint& to_bigint(bigint& out, BIGNUM const* in)
{
   bn_check_top(in);

   // Optimistically hope that component word size is
   // compatible between OpenSSL and GMP ...
   if ((sizeof in->d[0] * 8 == GMP_NUMB_BITS) && (BN_BITS2 == GMP_NUMB_BITS))
   {
      mpz_ptr mpz = out.get_mpz_t();

      if (!_mpz_realloc(mpz, in->top))
         throw std::runtime_error("Could not reallocate mpz to hold given BIGNUM.");

      memcpy(mpz->_mp_d, in->d, in->top * sizeof in->d[0]);
      mpz->_mp_size = in->top;
      if (in->neg)
         mpz->_mp_size = -mpz->_mp_size;

      return out;
   }

   TRACE("Warning: using slow octet conversion for BIGNUM -> mpz");

   // ... if word sizes differ go via octet representation (slower)
   //
   uint8_t octets[BN_num_bytes(in)];
   BN_bn2bin(in, octets);
   mpz_import(out.get_mpz_t(), sizeof octets, 1, 1, 1, 0, octets);
   
   // XXX: erase 'octets' securely?  Maybe pass user param.
   return out;
}


BIGNUM* to_BIGNUM(BIGNUM* out, bigint const& in)
{
   // Optimistically hope that component word size is
   // compatible between OpenSSL and GMP ...
   if ((sizeof out->d[0] * 8 == GMP_NUMB_BITS) && (BN_BITS2 == GMP_NUMB_BITS))
   {
      mpz_srcptr mpz = in.get_mpz_t();

      bool neg;
      int size = mpz->_mp_size;
      if (size < 0)
      {
         neg = true;
         size = -size;
      }
      else
         neg = false;

      BN_zero(out); // may speed up expand
      if (bn_expand2(out, size) == 0)
         throw std::runtime_error("Could not expand BIGNUM to hold given mpz.");

      out->top = size;
      memcpy(out->d, mpz->_mp_d, size * sizeof out->d[0]);
      bn_correct_top(out);
      out->neg = neg;
      return out;
   }

   TRACE("Warning: using slow octet conversion for mpz -> BIGNUM");

   // ... if word sizes differ go via octet representation (slower)
   //
   size_t octet_count = (bits(in)+7) >> 3;
   uint8_t octets[octet_count];
   mpz_export(octets, 0, 1, 1, 1, 0, in.get_mpz_t());
   BN_bin2bn(octets, octet_count, out);

   // XXX: erase 'octets' securely?  Maybe pass user param.
   return out;
}
