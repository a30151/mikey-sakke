//
//  MSDeviceCell.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSDeviceCell : UITableViewCell
{
    IBOutlet UILabel* deviceName;
}

-(void)setDevice:(NSString*)device_identifier;

@end
