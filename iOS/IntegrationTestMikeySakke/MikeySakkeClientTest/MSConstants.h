//
//  MSConstants.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#ifndef IntegrationTestMikeySakke_MSConstants_h
#define IntegrationTestMikeySakke_MSConstants_h

#define IP_ADDRESS @"192.168.1.3"
#define KMS_PORT @"7070"
#define KES_PORT @"7171"

#define CHECK_EVERY_X_SECONDS 5

#define MESSAGE_DECODED_EVENT @"messageDecoded"
#define MESSAGE_DECODED_FAILURE_EVENT @"messageDecodeFail"

#endif
