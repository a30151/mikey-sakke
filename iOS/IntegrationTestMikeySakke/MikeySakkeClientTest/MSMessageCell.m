//
//  MSMessageCell.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 12/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSMessageCell.h"

@implementation MSMessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setMessage:(MSMessage*)message
{
    if(!message.sentMessage)
    {
        title.text = @"Received message from:";
        if(message.hasDecoded && message.decodeSuccessful)
        {
            [activity setHidden:YES];
            [activity stopAnimating];
            [decodedKey setText:message.decodedMessage];
            [decodedKey setHidden:NO];
            sender.text = message.sender;
            [progress setText:@"Successfully decoded message:"];
        }
        else if(message.hasDecoded && !message.decodeSuccessful)
        {
            [activity setHidden:YES];
            [activity stopAnimating];
            [decodedKey setHidden:YES];
            sender.text = message.sender;
            [progress setText:@"Failed to decode message."];
        }
        else
        {
            [activity setHidden:NO];
            [activity startAnimating];
            [decodedKey setHidden:YES];
            sender.text = message.sender;
            [progress setText:@"Decoding key..."];
        }
    }
    else
    {
        title.text = @"Sent message to:";
        [activity setHidden:YES];
        [activity stopAnimating];
        [decodedKey setText:message.SSV];
        [decodedKey setHidden:NO];
        sender.text = message.recipient;
        [progress setText:@"Sent SSV:"];
    }
}

@end
