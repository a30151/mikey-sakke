//
//  MSDevicesTableViewController.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSDevicesTableViewController.h"
#import "MSDeviceCell.h"
#import "MSLibraryWrapper.h"
#import "MSConstants.h"
#import "ASIHTTPRequest.h"
#import "MSMessage.h"

@interface MSDevicesTableViewController ()

@end

@implementation MSDevicesTableViewController

@synthesize localDeviceList = _localDeviceList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.localDeviceList = [[NSMutableArray alloc] init];
    delegate = [[UIApplication sharedApplication] delegate];
    NSLog(@"Delegate has %d devices", [delegate.deviceList count]);
    [self.localDeviceList addObjectsFromArray:delegate.deviceList];
    NSLog(@"Local device list has %d devices", [self.localDeviceList count]);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(devicesChanged:)
                                                 name:DEVICES_CHANGED
                                               object:delegate];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.localDeviceList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"deviceCell";
    MSDeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (indexPath.row < [self.localDeviceList count])
    {
        [cell setDevice:[self.localDeviceList objectAtIndex:indexPath.row]];
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [self.localDeviceList count])
    {
        NSString* device = [self.localDeviceList objectAtIndex:indexPath.row];
        NSString* message = [NSString stringWithFormat:@"Would you like to send a message to %@", device];
        selectedDevice = device;
        
        UIAlertView* message_alert = [[UIAlertView alloc] initWithTitle:@"Send message" message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [message_alert show];
        [message_alert release];
    }
}

#pragma mark - Alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString* title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Yes"])
    {
        // Send message!
        
        // TODO put this is in an NSOperation and run it in the background!
        NSLog(@"Sending to %@",selectedDevice);
        MSLibraryWrapper* wrapper = [MSLibraryWrapper getInstance];
        NSString* sed;
        NSString* signature;
        NSString* ssv = [wrapper generateSharedSecretAndSED:&sed
                                              withRecipient:selectedDevice
                                               andSignature:&signature];
        NSLog(@"\nSSV: %@\nSED: %@", ssv, sed);

        NSLog(@"Signature: %@", signature);
        
        NSLog(@"Identifier: %@", delegate.identifier);
        
        // Send message to the server
        NSString* address = [NSString stringWithFormat:@"http://%@:%@/send?id=%@&toid=%@&sed=%@&signature=%@", IP_ADDRESS, KES_PORT, delegate.identifier, selectedDevice, sed, signature];
        NSURL* url = [NSURL URLWithString:address];
        
        ASIHTTPRequest* request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError* error = [request error];
        if(!error)
        {
            NSString* response = [request responseString];
            NSLog(@"Response: %@", response);
            // TODO do something with this?
            
            // Successful so add as a sent message
            MSMessage* new_message = [[MSMessage alloc] init];
            new_message.SED = sed;
            new_message.signature = signature;
            new_message.sender = delegate.identifier;
            new_message.sentMessage = YES;
            new_message.recipient = selectedDevice;
            new_message.SSV = ssv;
            [delegate sentMessage:new_message];
            [new_message release];
        }
        else
        {
            // Failure!
            // TODO do something with this?
        }
    }
}

-(void)devicesChanged:(NSNotification*)notification
{
    [self.localDeviceList removeAllObjects];
    [self.localDeviceList addObjectsFromArray:delegate.deviceList];
    [self.tableView reloadData];
}

@end
