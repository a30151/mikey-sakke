//
//  MSDecodeMessageOperation.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 13/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSDecodeMessageOperation.h"
#import "MSLibraryWrapper.h"
#import "MSConstants.h"

@implementation MSDecodeMessageOperation

@synthesize message = _message;

-(id)initWithMessage:(MSMessage*)decode_message
{
    if(![super init]) return nil;
    [self setMessage:decode_message];
    return self;
}

-(void)main
{
    MSLibraryWrapper* wrapper = [MSLibraryWrapper getInstance];
    
    NSString* SSV;
    
    BOOL result = [wrapper verifySignature:self.message.signature
                    andExtractSharedSecret:self.message.SED
                                    fromId:self.message.sender
                              sharedSecret:&SSV];
    
    NSLog(@"Verified: %c", result);
    
    if(result)
    {
        [self.message setDecodedMessage:SSV];
        self.message.hasDecoded = YES;
        self.message.decodeSuccessful = YES;
        [self performSelectorOnMainThread:@selector(notifySuccess) withObject:nil waitUntilDone:YES];
    }
    else
    {
        self.message.hasDecoded = YES;
        self.message.decodeSuccessful = NO;
        [self performSelectorOnMainThread:@selector(notifyError) withObject:nil waitUntilDone:YES];
    }
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(fetchedKeys:)
//                                                 name:KEY_SUCCESS_EVENT_KEY
//                                               object:wrapper];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(errorFetchingKeys:)
//                                                 name:KEY_FAILURE_EVENT_KEY
//                                               object:wrapper];
//    [wrapper connectToKMS:self.identifier];
}

-(void)notifySuccess
{
    [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_DECODED_EVENT
                                                        object:self
                                                      userInfo:nil];
}

-(void)notifyError
{
    [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_DECODED_FAILURE_EVENT
                                                        object:self
                                                      userInfo:nil];
}


@end
