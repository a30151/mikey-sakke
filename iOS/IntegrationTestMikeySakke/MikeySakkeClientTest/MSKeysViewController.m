//
//  MSKeysViewController.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSKeysViewController.h"
#import "MSAppDelegate.h"

@interface MSKeysViewController ()
-(void)configureView;
@end

@implementation MSKeysViewController

@synthesize myRSK = _myRSK;
@synthesize mySSK = _mySSK;
@synthesize myPVT = _myPVT;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self configureView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)configureView
{
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    self.myRSK.text = delegate.lastRSK;
    self.mySSK.text = delegate.lastSSK;
    self.myPVT.text = delegate.lastPVT;
}

@end
