//
//  MSDevicesTableViewController.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSAppDelegate.h"

@interface MSDevicesTableViewController : UITableViewController <UIAlertViewDelegate>
{
    MSAppDelegate* delegate;

    NSString* selectedDevice;
}

@property (nonatomic, retain) NSMutableArray* localDeviceList;

@end
