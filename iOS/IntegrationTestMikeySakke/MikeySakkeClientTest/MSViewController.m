//
//  MSViewController.m
//  MikeySakkeClientTest
//
//  Created by Mikey Project on 20/08/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSViewController.h"
#import "MSAppDelegate.h"
#import "MSLibraryWrapper.h"
#import "MSFetchKeysOperation.h"
#import "MSCheckKeyExchangeServerOperation.h"

@implementation MSViewController

@synthesize myIdentifier = _myIdentifier;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [loadingIndicator setHidden:YES];
    backgroundQueue = [[NSOperationQueue alloc] init];
}

- (void)dealloc
{
    [backgroundQueue release];
    [super dealloc];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(IBAction)connectToClient:(id)sender
{
    NSLog(@"connectToClient called");
    
    // Disable UI components
    [errorLabel setHidden:YES];
    [identifierField setEnabled:NO];
    [connectButton setEnabled:NO];
    [loadingIndicator setHidden:NO];
    [loadingIndicator startAnimating];
    
    
    MSFetchKeysOperation* fetch_op =
        [[MSFetchKeysOperation alloc] initWithIdentifier:identifierField.text];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchedKeys:)
                                                 name:KEY_SUCCESS_EVENT_KEY
                                               object:fetch_op];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorFetchingKeys:)
                                                 name:KEY_FAILURE_EVENT_KEY
                                               object:fetch_op];

    
    [backgroundQueue addOperation:fetch_op];
    [fetch_op release];
}

-(IBAction)displayKeys:(id)sender
{
    NSLog(@"displayKeys");
    /*
    std::string identifier = from_array("2011-020\0tel:+447700900123");
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *mydir = [documentsDir stringByAppendingPathComponent:@"/key-data"];
    
    KeyStoragePtr ks(new FlatFileKeyStorage([mydir cStringUsingEncoding:NSUTF8StringEncoding]));
    
    HandleFetchResult(ClientPtr(), ks, identifier, make_error_code(sys::errc::success));*/
}

-(void)fetchedKeys:(NSNotification*)notification
{
    NSLog(@"Received notification");
    
    // identifier must be valid
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    delegate.identifier = identifierField.text;
    
    // Talk to key exchange server
    MSCheckKeyExchangeServerOperation* kes_operation = [[MSCheckKeyExchangeServerOperation alloc] initWithIdentifier:identifierField.text];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(responseFromKES:)
                                                 name:KES_RESPONSE_EVENT_KEY
                                               object:kes_operation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(responseErrorFromKES:)
                                                 name:KES_RESPONSE_ERROR_EVENT_KEY
                                               object:kes_operation];
    
    [backgroundQueue addOperation:kes_operation];
    [kes_operation release];
    
    
}

-(void)responseFromKES:(NSNotification*)notification
{    
    NSDictionary* dict = notification.userInfo;
    NSString* response_string = [dict objectForKey:KES_RESPONSE_EVENT_KEY];
    NSLog(@"Got response from KES: %@", response_string);
    
    [self performSegueWithIdentifier:@"fetchedKeys" sender:nil];
}

-(void)responseErrorFromKES
{
    NSLog(@"Error from KES!");
}

-(void)errorFetchingKeys:(NSNotification*)notification
{
    [errorLabel setHidden:NO];
    [identifierField setEnabled:YES];
    [connectButton setEnabled:YES];
    [loadingIndicator setHidden:YES];
    [loadingIndicator stopAnimating];
}

#pragma mark -
#pragma mark TextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
