//
//  MSTabBarViewController.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 12/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSTabBarViewController.h"
#import "MSAppDelegate.h"

@interface MSTabBarViewController ()

@end

@implementation MSTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidAppear:(BOOL)animated
{
    // Find the tab bar item - probably not the best way but it is the quickest i could think of.
    for (UITabBarItem* tab in self.tabBar.items)
    {
        if([tab.title isEqualToString:@"Messages"])
        {
            messagesItem = tab;
            break;
        }
    }
    
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateMessageBadge:)
                                                 name:MESSAGES_CHANGED
                                               object:delegate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    // TODO cancel the badge when the messages tab is viewed
    if(item == messagesItem)
    {
        // reset badge
        [messagesItem setBadgeValue:nil];
    }
}

-(void)updateMessageBadge:(NSNotification*)notification
{
    NSDictionary* user_info = notification.userInfo;
    if(user_info)
    {
        NSArray* new_msgs = [user_info objectForKey:NEW_MESSAGES];
        if(new_msgs && messagesItem)
        {
            int current_value = 0;
            if(messagesItem.badgeValue.length > 0 )
            {
                current_value = [messagesItem.badgeValue intValue];
            }

            NSString* badge_value =
                [NSString stringWithFormat:@"%d", current_value + [new_msgs count]];
            [messagesItem setBadgeValue:badge_value];
        }
    }
}

@end
