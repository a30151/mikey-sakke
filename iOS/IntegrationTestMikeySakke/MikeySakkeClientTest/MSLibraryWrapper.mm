//
//  MSLibraryWrapper.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 10/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSLibraryWrapper.h"

#import "MSConstants.h"

#include "client.h"
#include "flat-file-key-storage.h"
#include "sakke.h"
#include "eccsi.h"
#include "random.h"
#include <ctime>
#include <openssl/rand.h>

#import "parameter-set.h"

#define NTP_EPOCH_OFFSET 2208988800UL

using namespace MikeySakkeKMS;

template<int N> inline std::string from_array(char const (&array) [N])
{
    return std::string(array, N);
}

@interface MSLibraryWrapper()
-(void)setRSK:(NSString*)rsk_string;
-(void)setSSK:(NSString*)ssk_string;
-(void)setPVT:(NSString*)pvt_string;
-(void)setHS:(NSString*)hs_string;
-(void)finishedSuccessfully;
-(void)finishedWithErrors;
@end

@implementation MSLibraryWrapper

static MSLibraryWrapper* singleton;

KeyStoragePtr cachedKeyStore;

-(id)init
{
    self = [super init];
    if(self)
    {

    }
    return self;
}

-(void)dealloc
{
    [singleton release];
    [super dealloc];
}

+(MSLibraryWrapper*)getInstance
{
    if(!singleton)
    {
        singleton = [[MSLibraryWrapper alloc] init];
    }
    
    return singleton;
}


-(void)connectToKMS:(NSString*)user_identifier
{
    NSString *kmsServer = [NSString stringWithFormat:@"%@:%@", IP_ADDRESS, KMS_PORT];
    NSString *userName = @"jim";
    NSString *password = @"jimpass";

    std::string name = [user_identifier cStringUsingEncoding:[NSString defaultCStringEncoding]];

    std::string date = "2012-09\0";
    
    int length = 8 + name.length() + 1;
    
    char name_array[length];
    for (int i = 0; i < length; i++) {
        if(i < date.length())
        {
            name_array[i] = date[i];
        }
        else if(i == date.length())
        {
            name_array[i] = '\0';
        }
        else if(i < date.length() + name.length() + 1)
        {
            name_array[i] = name[i - date.length() - 1];
        }
        else
        {
            name_array[i] = '\0';
        }
        std::cout << name_array[i];
    }
    
    std::string identifier (name_array, length);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *mydir = [documentsDir stringByAppendingPathComponent:@"/key-data"];

    KeyStoragePtr ks(new FlatFileKeyStorage([mydir cStringUsingEncoding:NSUTF8StringEncoding]));
    
    ClientPtr kms(new Client(ks));

    std::string server = [kmsServer cStringUsingEncoding:[NSString defaultCStringEncoding]];
    std::string user = [userName cStringUsingEncoding:[NSString defaultCStringEncoding]];
    std::string pword = [password cStringUsingEncoding:[NSString defaultCStringEncoding]];


    kms->FetchKeyMaterial(server, Client::DontVerifySSLCertificate, user, pword, identifier,
                          std::bind(HandleFetchResult, kms, ks, std::placeholders::_1, std::placeholders::_2));
}

std::string lastPeerCommunity;
std::string lastIdentifier;

void HandleFetchResult(ClientPtr kms, KeyStoragePtr keys, std::string const& identifier,
                       sys::error_code error)
{
    NSLog(@"HandleFetchResult called");
    
    std::cout<< "Identifier: " << identifier << std::endl;
    
    cachedKeyStore = keys;
    lastIdentifier = identifier;
    
    bool validate_keys_success = false;
    bool validate_secret_success = false;
    NSString* ns_rsk_string;
    NSString* ns_ssk_string;
    NSString* ns_pvt_string;
    NSString* ns_hs_string;
    
    if (error)
    {
        std::cerr << "Failed to fetch keys for identifier '" << identifier << "': "
        << error.message() << "\n";
    }
    else
    {
        OctetString rsk_string = keys->GetPrivateKey(identifier, "RSK");
        
        ns_rsk_string =
            [NSString stringWithCString:rsk_string.translate().c_str()
                               encoding:NSASCIIStringEncoding];
        
        OctetString ssk_string = keys->GetPrivateKey(identifier, "SSK");
        
        ns_ssk_string =
            [NSString stringWithCString:ssk_string.translate().c_str()
                               encoding:NSASCIIStringEncoding];
        
        OctetString pvt_string = keys->GetPublicKey(identifier, "PVT");
        
        ns_pvt_string =
            [NSString stringWithCString:pvt_string.translate().c_str()
                               encoding:NSASCIIStringEncoding];
        
        std::cout << "RSK: " << rsk_string << "\n";
        std::cout << "SSK: " << ssk_string << "\n";
        
        std::vector<std::string> communities = keys->GetCommunityIdentifiers();
        
        for (std::vector<std::string>::const_iterator it = communities.begin(), end = communities.end();
             it != end; ++it)
        {
            std::cout << "\nCommunity: '" << *it << "'\n'"
            << "\n";
            // TODO: Add cout stuff
        }
        
        lastPeerCommunity = communities[0];
        
        try
        {
            validate_keys_success =
                MikeySakkeCrypto::ValidateSigningKeysAndCacheHS(identifier, communities[0], keys);
            OctetString hs_octet = keys->GetPublicKey(identifier, "HS");
            ns_hs_string = [NSString stringWithCString:hs_octet.translate().c_str()
                                              encoding:NSASCIIStringEncoding];
        } catch (std::exception e) {
            NSLog(@"Failed ValidateSigningKeysAndCacheHS");
        }

        if (!validate_keys_success)
        {
            NSLog(@"This did not work");
        }
        else
        {
            NSLog(@"This did work");
        }
        
        try {
            validate_secret_success =
                MikeySakkeCrypto::ValidateReceiverSecretKey(identifier, communities[0], keys);
        } catch (std::exception e) {
            NSLog(@"Failed ValidateReceiverSecretKey");
        }
        
        if (!validate_secret_success)
        {
            NSLog(@"This did not work");
        }
        else
        {
            NSLog(@"This did work");
        }
        
        std::cout << "End of HandleFetchResult called" << std::endl;
        NSLog(@"The End");
        
        
    }
    
    MSLibraryWrapper* wrapper = [MSLibraryWrapper getInstance];
    
    if(validate_keys_success && validate_secret_success)
    {
        [wrapper setRSK:ns_rsk_string];
        [wrapper setSSK:ns_ssk_string];
        [wrapper setPVT:ns_pvt_string];
        [wrapper setHS:ns_hs_string];
        [wrapper finishedSuccessfully];
    }
    else
    {
        [wrapper finishedWithErrors];
    }
}

#pragma mark -
#pragma mark Mixed C++/ObjC methods

-(void)setRSK:(NSString*)rsk_string
{
    lastRSK = rsk_string;
}

-(void)setSSK:(NSString*)ssk_string
{
    lastSSK = ssk_string;
}

-(void)setPVT:(NSString*)pvt_string
{
    lastPVT = pvt_string;
}

-(void)setHS:(NSString*)hs_string
{
    lastHS = [hs_string retain];
}

-(void)finishedSuccessfully
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:3];
    [dict setObject:lastRSK forKey:RSK_KEY];
    [dict setObject:lastSSK forKey:SSK_KEY];
    [dict setObject:lastPVT forKey:PVT_KEY];
    [dict setObject:lastHS forKey:HS_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:KEY_SUCCESS_EVENT_KEY
                                                        object:self
                                                      userInfo:dict];
    [dict release];
}

-(void)finishedWithErrors
{
    NSLog(@"Finished with errors");

    [[NSNotificationCenter defaultCenter] postNotificationName:KEY_FAILURE_EVENT_KEY
                                                        object:self
                                                      userInfo:nil];
}

-(NSString*)generateSharedSecretAndSED:(NSString**)SED
                         withRecipient:(NSString*)identifier
                          andSignature:(NSString**)signature
{    
    std::string uri =
        [identifier cStringUsingEncoding:[NSString defaultCStringEncoding]];
    
    OctetString::Translation const raw = OctetString::Untranslated;
    OctetString senderId("2012-09", raw);
    senderId.concat(0).concat(uri, raw).concat(0);
    
    std::cout << "Recipient identifier: " << senderId << std::endl;
    
    OctetString sed_octet;
    
    OctetString ssv = MikeySakkeCrypto::GenerateSharedSecretAndSED(sed_octet,
                                           senderId,
                                           lastPeerCommunity,
                                           (bool (*) (void*, size_t)) randomize,
                                           cachedKeyStore);
    
    *SED = [NSString stringWithCString:sed_octet.translate().c_str()
                              encoding:NSASCIIStringEncoding];
    
    OctetString sendId(lastIdentifier, raw);
    
    OctetString octet_signature = MikeySakkeCrypto::Sign(sed_octet.raw(),
                                                   sed_octet.size(),
                                                   sendId,
                                                   lastPeerCommunity,
                                                   (bool (*) (void*, size_t)) randomize,
                                                   cachedKeyStore);

    *signature = [NSString stringWithCString:octet_signature.translate().c_str()
                                    encoding:NSASCIIStringEncoding];
    
    return [NSString stringWithCString:ssv.translate().c_str()
                              encoding:NSASCIIStringEncoding];
}

bool randomize(void* buffer, size_t length)
{
    return RAND_bytes((unsigned char*)buffer, (int)length) != 0;
}

OctetString idDateStamp(uint64_t time_s32_f32)
{
    time_t seconds = (time_s32_f32 >> 32) - NTP_EPOCH_OFFSET;
    struct tm local;
    localtime_r(&seconds, &local);
    
    // YYYY-DD\0
    OctetString rc(4+1+2+1);
    snprintf((char*) rc.raw(), rc.size(), "%04d-%02d", 1900+local.tm_year, 1+local.tm_mon);
    
    return rc;
}

-(BOOL)verifySignature:(NSString*)signature
andExtractSharedSecret:(NSString*)message
                fromId:(NSString*)sender
          sharedSecret:(NSString**)SSV
{
    BOOL result = NO; 
    
    OctetString::Translation const raw = OctetString::Untranslated;
        
    std::string uri =
        [sender cStringUsingEncoding:[NSString defaultCStringEncoding]];
    OctetString senderId("2012-09", raw);
    senderId.concat(0).concat(uri, raw).concat(0);
    
    OctetString localId(lastIdentifier, raw);
    
    OctetString signature_octet;
    signature_octet.concat([signature cStringUsingEncoding:[NSString defaultCStringEncoding]]);
    //signature_octet.concat(0);
    
    OctetString message_octet;
    message_octet.concat([message cStringUsingEncoding:
                          [NSString defaultCStringEncoding]]);
//    message_octet.concat("0");
    
    bool verified = MikeySakkeCrypto::Verify(message_octet.raw(),
                             message_octet.size(),
                             signature_octet.raw(),
                             signature_octet.size(),
                             senderId,
                             lastPeerCommunity,
                             cachedKeyStore);
    
    result = verified;
    
    OctetString shared_secret = MikeySakkeCrypto::ExtractSharedSecret(message_octet,
                                                                    localId,
                                                                    lastPeerCommunity,
                                                                    cachedKeyStore);
    
    *SSV = [NSString stringWithCString:shared_secret.translate().c_str()
                              encoding:NSASCIIStringEncoding];
    
    return result;
}
    
@end
