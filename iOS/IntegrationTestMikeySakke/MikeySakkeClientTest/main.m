//
//  main.m
//  MikeySakkeClientTest
//
//  Created by Mikey Project on 20/08/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MSAppDelegate class]));
    }
}
