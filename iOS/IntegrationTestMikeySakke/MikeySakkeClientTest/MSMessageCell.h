//
//  MSMessageCell.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 12/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSMessage.h"

@interface MSMessageCell : UITableViewCell
{
    IBOutlet UILabel* title;
    IBOutlet UILabel* sender;
    IBOutlet UILabel* progress;
    IBOutlet UILabel* decodedKey;
    IBOutlet UIActivityIndicatorView* activity;
}

-(void)setMessage:(MSMessage*)message;

@end
