//
//  MSCheckKeyExchangeServerOperation.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSCheckKeyExchangeServerOperation.h"
#import "MSConstants.h"
#import "MSAppDelegate.h"
#import "ASIHTTPRequest.h"
#import "SBJsonParser.h"
#import "MSMessage.h"

@interface MSCheckKeyExchangeServerOperation()
-(NSArray*)processMessageObjects:(NSArray*)json_messages;
@end

@implementation MSCheckKeyExchangeServerOperation

@synthesize identifier = _identifier;

-(id)initWithIdentifier:(NSString*)identifier
{
    if(![super init]) return nil;
    [self setIdentifier:identifier];
    return self;
}

-(void)main
{
    NSString* address = [NSString stringWithFormat:@"http://%@:%@/check?id=%@", IP_ADDRESS, KES_PORT, self.identifier];
    NSURL* url = [NSURL URLWithString:address];
    
    ASIHTTPRequest* request = [ASIHTTPRequest requestWithURL:url];
    [request startSynchronous];
    NSError* error = [request error];
    if(!error)
    {
        NSString* response = [request responseString];
        NSLog(@"Response: %@", response);
        // Success!
        [self processResponse:response];
        
        [self performSelectorOnMainThread:@selector(notifyCaller:) withObject:response waitUntilDone:YES];
    }
    else
    {
        // Failure!
        [self performSelectorOnMainThread:@selector(notifyCallerOfError) withObject:nil waitUntilDone:YES];
    }
}

-(void)processResponse:(NSString*)response
{
    SBJsonParser* json_parser = [[SBJsonParser alloc] init];
    NSError* error = nil;
    NSDictionary* json_objects = [json_parser objectWithString:response];
    [json_parser release];
     MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    
    if(!error)
    {
        // check device list
        NSArray* devices = [json_objects objectForKey:@"devices"];
        NSLog(@"Found %d devices", [devices count]);
        
        [delegate performSelectorOnMainThread:@selector(setDevices:)
                                   withObject:devices
                                waitUntilDone:YES];
        
        // check messages
        NSArray* messages = [json_objects objectForKey:@"messages"];
        NSLog(@"Found %d messages", [messages count]);

        NSArray* message_objects = [self processMessageObjects:messages];
        
        [delegate performSelectorOnMainThread:@selector(setMessages:)
                                   withObject:message_objects
                                waitUntilDone:YES];
        
        // Finished
        [delegate performSelectorOnMainThread:@selector(finishedCheckingKES)
                                   withObject:nil
                                waitUntilDone:YES];
    }
    else
    {
        // Finished - keep checking even if there were errors!
        [delegate performSelectorOnMainThread:@selector(finishedCheckingKES)
                                   withObject:nil
                                waitUntilDone:YES];
    }
}

-(void)notifyCaller:(NSString*)response
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:1];
    [dict setObject:response forKey:KES_RESPONSE_EVENT_KEY];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KES_RESPONSE_EVENT_KEY
                                                        object:self
                                                      userInfo:dict];
}

-(void)notifyCallerOfError
{
    [[NSNotificationCenter defaultCenter] postNotificationName:KES_RESPONSE_ERROR_EVENT_KEY
                                                        object:self
                                                      userInfo:nil];
}

-(NSArray*)processMessageObjects:(NSArray*)json_messages
{
    NSMutableArray* message_objects = [[NSMutableArray alloc] initWithCapacity:[json_messages count]];
    
    for (NSDictionary* message_dict in json_messages)
    {
        NSString* sed = [message_dict objectForKey:@"sed"];
        NSString* sig = [message_dict objectForKey:@"signature"];
        NSString* from = [message_dict objectForKey:@"from"];
        
        if(sed && sig && from)
        {
            MSMessage* new_message = [[MSMessage alloc] init];
            new_message.SED = sed;
            new_message.signature = sig;
            new_message.sender = from;
            new_message.sentMessage = NO;
            [message_objects addObject:new_message];
            [new_message release];
        }
    }
    
    
//    "signature":"3a632e5342f1cabda01e13c0842a27caed6fe21fd56798f70c8eda4477527a98ce4a4694b58a993ba724d8939d49f894f5e2ecf8ea3b6fcfb79cf8cfa79c52ad04758a142779be89e829e71984cb40ef758cc4ad775fc5b9a3e1c8ed52f6fa36d9a79d247692f4eda3a6bdab77d6aa6474a464ae4934663c5265ba7018ba091f79",
//    "sed":"040c408cdbcbd111a7e8a48fd3a7dec0d3486e2ffc4167e07e715d6dc69425756b7369ed30114dad91908285eed97d3be3fde7857f27bb010a6b2eff7c45c660f66e25257692f2de48c4fd5f863cccba9a8fd3cd95a9ebf930fdf3fc89db54e3a03cd7fdfb4675488e4b29659d0d4854ec772f6a8ad21e8feaf35930f484dbd62e2932f7e9b28e6727e95002bd516ddb4700c9a0b48a2d966cbcea8a427a939354bed653b2737fa84ea9a54d0a0f7b13a3bb68fdc57b279e38b91266a81d8054d5e68a21425ae30b865214cf62731234a1c2661db529898f7627c7149ab8453f392c862f268f1acd23f7a759e587b607dba468e49b07b2578cd4abf582c70d0d5d02337eb8aa05c3d05103a34ac9e9ba4c",
//        "from":"pctestapp"
    
    return message_objects;
}

@end
