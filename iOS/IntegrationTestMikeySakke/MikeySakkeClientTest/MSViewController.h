//
//  MSViewController.h
//  MikeySakkeClientTest
//
//  Created by Mikey Project on 20/08/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSViewController : UIViewController <UITextFieldDelegate>
{
    NSOperationQueue* backgroundQueue;
    
    IBOutlet UITextField* identifierField;
    IBOutlet UIButton* connectButton;
    IBOutlet UIActivityIndicatorView* loadingIndicator;
    IBOutlet UILabel* errorLabel;
}

@property (nonatomic, retain) IBOutlet UITextField *myIdentifier;

-(IBAction)connectToClient:(id)sender;
-(IBAction)displayKeys:(id)sender;

@end
