//
//  MSFetchKeysOperation.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSFetchKeysOperation.h"
#import "MSLibraryWrapper.h"
#import "MSAppDelegate.h"

@implementation MSFetchKeysOperation

@synthesize identifier = _identifier;

-(id)initWithIdentifier:(NSString*)identifier
{
    if(![super init]) return nil;
    [self setIdentifier:identifier];
    return self;
}

-(void)main
{
    MSLibraryWrapper* wrapper = [MSLibraryWrapper getInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchedKeys:)
                                                 name:KEY_SUCCESS_EVENT_KEY
                                               object:wrapper];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorFetchingKeys:)
                                                 name:KEY_FAILURE_EVENT_KEY
                                               object:wrapper];
    [wrapper connectToKMS:self.identifier];
}

-(void)fetchedKeys:(NSNotification*)notification
{
    // Success!
    [self performSelectorOnMainThread:@selector(notifyCaller:) withObject:notification waitUntilDone:YES];
}

-(void)errorFetchingKeys:(NSNotification*)notification
{
    // Failure!
    [self performSelectorOnMainThread:@selector(notifyCallerOfError:) withObject:notification waitUntilDone:YES];
}

-(void)notifyCaller:(NSNotification*)notification
{
    NSDictionary* event_info = [notification userInfo];
    
    MSAppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    delegate.lastRSK = [event_info valueForKey:RSK_KEY];
    delegate.lastSSK = [event_info valueForKey:SSK_KEY];
    delegate.lastPVT = [event_info valueForKey:PVT_KEY];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KEY_SUCCESS_EVENT_KEY
                                                        object:self
                                                      userInfo:notification.userInfo];
}

-(void)notifyCallerOfError:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:KEY_FAILURE_EVENT_KEY
                                                        object:self
                                                      userInfo:notification.userInfo];
}

@end
