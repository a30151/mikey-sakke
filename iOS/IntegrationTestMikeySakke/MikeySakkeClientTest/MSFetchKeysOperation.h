//
//  MSFetchKeysOperation.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSFetchKeysOperation : NSOperation

@property (nonatomic, retain) NSString* identifier;

-(id)initWithIdentifier:(NSString*)identifier;

@end
