//
//  MSMessage.m
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 12/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSMessage.h"

@implementation MSMessage

@synthesize SED = _SED;
@synthesize SSV = _SSV;
@synthesize signature = _signature;
@synthesize sender = _sender;
@synthesize decodedMessage = _decodedMessage;
@synthesize hasDecoded = _hasDecoded;
@synthesize decodeSuccessful = _decodeSuccessful;
@synthesize sentMessage = _sentMessage;
@synthesize recipient = _recipient;

@end
