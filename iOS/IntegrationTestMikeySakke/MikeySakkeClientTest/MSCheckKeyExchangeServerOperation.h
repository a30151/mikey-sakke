//
//  MSCheckKeyExchangeServerOperation.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KES_RESPONSE_EVENT_KEY @"KESResponse"
#define KES_RESPONSE_ERROR_EVENT_KEY @"KESResponseError"

@interface MSCheckKeyExchangeServerOperation : NSOperation

@property (nonatomic, retain) NSString* identifier;

-(id)initWithIdentifier:(NSString*)identifier;

@end
