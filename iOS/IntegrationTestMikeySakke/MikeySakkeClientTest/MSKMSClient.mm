//
//  MSKMSClient.m
//  MikeySakkeClientTest
//
//  Created by Mikey Project on 20/08/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSKMSClient.h"

#include "client.h"
#include "flat-file-key-storage.h"

#include <dirent.h> // Temporary code

using namespace MikeySakkeKMS;

template<int N> inline std::string from_array(char const (&array) [N])
{
    return std::string(array, N);
}

@implementation MSKMSClient

/*
void HandleFetchResult(ClientPtr kms, KeyAccessPtr keys, std::string const& identifier,
                       sys::error_code error)
{
    NSLog(@"HandleFetchResult called");
    
    if (error)
    {
        std::cerr << "Failed to fetch keys for identifier '" << identifier << "': "
            << error.message() << "\n";
    }
    
    std::cout << "Retrieved keys for :" << identifier << "\n";
    std::cout << "RSK: " << keys->GetPrivateKey(identifier, "RSK") << "\n";
    std::cout << "SSK: " << keys->GetPrivateKey(identifier, "SSK") << "\n";
    
    std::vector<std::string> communities = keys->GetCommunityIdentifiers();
    
    for (std::vector<std::string>::const_iterator it = communities.begin(), end = communities.end();
         it != end; ++it)
    {
        std::cout << "\nCommunity: '" << *it << "'\n'"
                  << "\n";
        // TODO: Add cout stuff
    }
    
    std::cout << "End of HandleFetchResult called" << std::endl;
    NSLog(@"The End");
}
*/



@end
