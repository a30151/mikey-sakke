//
//  MSMessage.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 12/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSMessage : NSObject

@property (nonatomic, retain) NSString* SED;
@property (nonatomic, retain) NSString* SSV;
@property (nonatomic, retain) NSString* signature;
@property (nonatomic, retain) NSString* sender;
@property (nonatomic, retain) NSString* recipient;
@property (nonatomic, retain) NSString* decodedMessage;
@property (nonatomic, assign) BOOL hasDecoded;
@property (nonatomic, assign) BOOL decodeSuccessful;
@property (nonatomic, assign) BOOL sentMessage;

@end
