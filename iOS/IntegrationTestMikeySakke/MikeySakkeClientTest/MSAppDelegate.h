//
//  MSAppDelegate.h
//  MikeySakkeClientTest
//
//  Created by Mikey Project on 20/08/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DEVICES_CHANGED @"devicesChanged"
#define MESSAGES_CHANGED @"messagesChanged"
#define NEW_MESSAGES @"newMessages"

@class MSMessage;

@interface MSAppDelegate : UIResponder <UIApplicationDelegate>
{
    NSOperationQueue* backgroundQueue;
    NSOperationQueue* decodingQueue;
    BOOL restartQueue;
}

@property (strong, nonatomic) UIWindow *window;

// Horrible shortcut to let every viewcontroller have access to the last keys fetched
@property (nonatomic, retain) NSString* lastRSK;
@property (nonatomic, retain) NSString* lastSSK;
@property (nonatomic, retain) NSString* lastPVT;
@property (nonatomic, retain) NSString* identifier;

@property (nonatomic, retain) NSMutableArray* deviceList;
@property (nonatomic, retain) NSMutableArray* messageList;

-(void)setDevices:(NSArray*)new_devices;
-(void)setMessages:(NSArray*)new_messages;
-(void)sentMessage:(MSMessage*)new_sent_message;
-(void)finishedCheckingKES;

@end
