//
//  MSAppDelegate.m
//  MikeySakkeClientTest
//
//  Created by Mikey Project on 20/08/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import "MSAppDelegate.h"
#import "MSConstants.h"
#import "MSCheckKeyExchangeServerOperation.h"
#import "MSMessage.h"
#import "MSDecodeMessageOperation.h"

@implementation MSAppDelegate

@synthesize lastRSK = _lastRSK;
@synthesize lastSSK = _lastSSK;
@synthesize lastPVT = _lastPVT;
@synthesize deviceList = _deviceList;
@synthesize messageList = _messageList;

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    restartQueue = NO;
    self.deviceList = [[NSMutableArray alloc] init];
    self.messageList = [[NSMutableArray alloc] init];
    backgroundQueue = [[NSOperationQueue alloc] init];
    decodingQueue = [[NSOperationQueue alloc] init];
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    restartQueue = YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if(restartQueue)
    {
        restartQueue = NO;
        [backgroundQueue cancelAllOperations];
        [self checkKES];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if(restartQueue)
    {
        restartQueue = NO;
        [backgroundQueue cancelAllOperations];
        [self checkKES];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)setDevices:(NSArray*)new_devices
{
    BOOL equivalent = YES;
    if ([new_devices count] == [self.deviceList count])
    {

        // check whether we need to update (and therefor notify) or not
        for (NSString* device_string in new_devices)
        {
            if ([self.deviceList containsObject:device_string])
            {
                // continue
            }
            else
            {
                equivalent = NO;
                break;
            }
        }
    }
    else
    {
        equivalent = NO;
    }
    
    if(equivalent == NO)
    {
        [self.deviceList removeAllObjects];
        [self.deviceList addObjectsFromArray:new_devices];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:DEVICES_CHANGED
                                                            object:self
                                                          userInfo:nil];
    }
}

-(void)setMessages:(NSArray*)new_messages
{
    if([new_messages count] > 0)
    {
        [self.messageList addObjectsFromArray:new_messages];
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithCapacity:1];
        [dict setObject:new_messages forKey:NEW_MESSAGES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGES_CHANGED
                                                            object:self
                                                          userInfo:dict];
        [dict release];
        
        for (MSMessage* message in new_messages)
        {
            MSDecodeMessageOperation* decode_message =
                [[MSDecodeMessageOperation alloc] initWithMessage:message];
            [decodingQueue addOperation:decode_message];
            [decode_message release];
        }
    }
}

-(void)sentMessage:(MSMessage*)new_sent_message
{
    [self.messageList addObject:new_sent_message];
    [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGES_CHANGED
                                                        object:self
                                                      userInfo:nil];
}

-(void)finishedCheckingKES
{
    [self performSelector:@selector(checkKES)
               withObject:nil
               afterDelay:CHECK_EVERY_X_SECONDS];
}

-(void)checkKES
{
    MSCheckKeyExchangeServerOperation* kes_operation = [[MSCheckKeyExchangeServerOperation alloc] initWithIdentifier:self.identifier];
    [backgroundQueue addOperation:kes_operation];
    [kes_operation release];
}

@end
