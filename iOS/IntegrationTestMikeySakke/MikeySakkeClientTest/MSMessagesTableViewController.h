//
//  MSMessagesTableViewController.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 13/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSAppDelegate.h"

@interface MSMessagesTableViewController : UITableViewController
{
    MSAppDelegate* delegate;
}

@property (nonatomic, retain) NSMutableArray* localMessagesArray;

@end
