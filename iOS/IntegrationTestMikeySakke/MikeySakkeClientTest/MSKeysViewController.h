//
//  MSKeysViewController.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 11/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSKeysViewController : UIViewController

@property (nonatomic, retain) IBOutlet UITextView *myRSK;
@property (nonatomic, retain) IBOutlet UITextView *mySSK;
@property (nonatomic, retain) IBOutlet UITextView *myPVT;

@end
