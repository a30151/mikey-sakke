//
//  MSLibraryWrapper.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 10/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RSK_KEY @"RSK"
#define SSK_KEY @"SSK"
#define PVT_KEY @"PVT"
#define HS_KEY @"HS"
#define KEY_SUCCESS_EVENT_KEY @"MikeySakkeKeySuccess"
#define KEY_FAILURE_EVENT_KEY @"MikeySakkeKeyFail"

@interface MSLibraryWrapper : NSObject
{
    NSString* lastRSK;
    NSString* lastSSK;
    NSString* lastPVT;
    NSString* lastHS;
}

+(MSLibraryWrapper*)getInstance;
-(void)connectToKMS:(NSString*)user_identifier;
-(NSString*)generateSharedSecretAndSED:(NSString**)SED
                         withRecipient:(NSString*)identifier
                          andSignature:(NSString**)signature;
-(BOOL)verifySignature:(NSString*)signature
andExtractSharedSecret:(NSString*)message
                fromId:(NSString*)sender
          sharedSecret:(NSString**)SSV;


@end
