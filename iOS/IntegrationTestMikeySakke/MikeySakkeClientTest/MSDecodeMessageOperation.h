//
//  MSDecodeMessageOperation.h
//  IntegrationTestMikeySakke
//
//  Created by Mikey Project on 13/09/2012.
//  Copyright (c) 2012 Mikey Project. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSMessage.h"

@interface MSDecodeMessageOperation : NSOperation

@property (nonatomic, retain) MSMessage* message;

-(id)initWithMessage:(MSMessage*)decode_message;

@end
