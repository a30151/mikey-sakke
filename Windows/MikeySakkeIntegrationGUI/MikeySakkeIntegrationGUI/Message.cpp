#include "stdafx.h"
#include "Message.h"

Message::Message(std::string fromDeviceId, 
				 std::string toDeviceId, 
				 std::string sakkeEncrpytedData, 
				 std::string signature)
{
	m_FromDeviceId = fromDeviceId;
	m_ToDeviceId = toDeviceId;
	m_SakkeEncryptedData = sakkeEncrpytedData;
	m_Signature = signature;
}


Message::~Message(void)
{
}

std::string Message::getFromDeviceId()
{
	return m_FromDeviceId;
}

std::string Message::getToDeviceId()
{
	return m_ToDeviceId;
}

std::string Message::getSakkeEncryptedData()
{
	return m_SakkeEncryptedData;
}

std::string Message::getSignature()
{
	return m_Signature;
}

void Message::setFromDeviceId(std::string fromDeviceId)
{
	m_FromDeviceId = fromDeviceId;
}

void Message::setToDeviceId(std::string toDeviceId)
{
	m_ToDeviceId = toDeviceId;
}

void Message::setSakkeEncryptedData(std::string sakkeEncrpytedData)
{
	m_SakkeEncryptedData = sakkeEncrpytedData;
}

void Message::setSignature(std::string signature)
{
	m_Signature = signature;
}
