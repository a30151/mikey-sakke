// MikeySakkeIntegrationGUI.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MikeySakkeIntegrationGUI.h"
#include <cstring>
#include <cstdlib>

#include <mskms\client.h>
#include <mskms\client-fwd.h>
#include <mskms\key-storage.h>
#include <mskms\flat-file-key-storage.inl>
#include <convenience\cptr.h>
#include <mscrypto\sakke.h>
#include <mscrypto\eccsi.h>
#include <mscrypto\random.h>
#include <curl\curl.h>
#include <json\json.h>
#include <openssl\rand.h>

#include <boost\system\error_code.hpp>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Labels
HWND l_status;									// Displaying current status
HWND l_uid;										// User ID
HWND l_rsk_label;								// RSK Label
HWND l_rsk;										// RSK
HWND l_ssk_label;								// SSK Label
HWND l_ssk;										// SSK
HWND l_pvt_label;								// PVT Label
HWND l_pvt;										// PVT
HWND l_rMessages;								// Received messages
HWND l_sMessages;								// Sent messages

// Textboxes
HWND t_uid;										// User ID
HWND t_sMessages;								// Sent messages
HWND t_rMessages;								// Received messages

// Buttons
HWND b_kms_connect;								// Connect to KMS
HWND b_kms_disconnect;							// Disconnect from KMS
HWND b_view_keys;								// View keys (RSK, SSK, PVT)
HWND b_check_kes;								// Check KES
HWND b_view_messages;							// View messages
HWND b_send_message;							// Send message
HWND b_back;									// Back

// Combobox
HWND cb_devices;								// Displays list of available devices recv from KES

bool devices;									// Is set to true if devices have been recv from KES
												// Used to enable/disable send message button

// Global vars
MikeySakkeKMS::KeyStoragePtr cachedKeyStore;
std::string lastPeerCommunity;
std::string lastIdentifier;

std::wstring lastRSK;
std::wstring lastSSK;
std::wstring lastPVT;
std::wstring lastHS;

std::string userID;

std::list<std::string> deviceList;				// Used to store devices recv from KES - populates device combobox

// Multimap which maps (from/to)device to message
std::multimap<std::string, std::string> sentMessages;		// Map of sent messages
std::multimap<std::string, std::string> decodedMessages;	// Map of recv messages

// Function definitions
void ConnectToKMS(std::string uid);
void HandleFetchResult(MikeySakkeKMS::ClientPtr, MikeySakkeKMS::KeyStoragePtr, std::string const&, boost::system::error_code);
void checkKES();
void decodeMessages(std::list<Message>);
void updateStatus(std::string);
void displayMessages();
bool sendMessage(std::string);
bool randomize(void*, size_t);

void setRSK(std::string);
void setSSK(std::string);
void setPVT(std::string);
void setHS(std::string);
void setUserID(std::string);
std::string ws2s(std::wstring);
std::wstring s2ws(std::string);

// Function definitions (GUI)
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Send(HWND, UINT, WPARAM, LPARAM);

void CreateObjects(HWND);
void SwitchScreen(INT);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MIKEYSAKKEINTEGRATIONGUI, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MIKEYSAKKEINTEGRATIONGUI));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}


// Function to connect to the KMS
// uid is the device identifier (generally windowstest in this instance)
void ConnectToKMS(std::string uid)
{
	std::stringstream server;
	server << IP_ADDRESS;
	server << ":";
	server << KMS_PORT;

	std::string kmsServer = server.str();
	std::string userName = KMS_USER;
	std::string password = KMS_PASS;

	std::string date = DATE_STRING;

	// Use hard-coded for now
	//std::string uid = "windowstest";
	//std::string date = "2012-09\0";

	// Construct the identifier string ("2012-09\0windowstest\0")
	int length = 8 + uid.length() + 1;
	char *name_array;
	name_array = new char[length];

    for (int i = 0; i < length; i++) {
        if(i < date.length())
        {
            name_array[i] = date[i];
        }
        else if(i == date.length())
        {
            name_array[i] = '\0';
        }
        else if(i < date.length() + uid.length() + 1)
        {
            name_array[i] = uid[i - date.length() - 1];
        }
        else
        {
            name_array[i] = '\0';
        }
    }  
    std::string identifier (name_array, length);

	// Free memory
	delete [] name_array;

	// Construct KeyStorage and Client
	MikeySakkeKMS::KeyStoragePtr ks(new MikeySakkeKMS::FlatFileKeyStorage);
	MikeySakkeKMS::ClientPtr kms(new MikeySakkeKMS::Client(ks));

	// Fetch Keys from KMS
	kms->FetchKeyMaterial(kmsServer, MikeySakkeKMS::Client::DontVerifySSLCertificate, userName, password, identifier,
							std::bind(HandleFetchResult, kms, ks, std::placeholders::_1, std::placeholders::_2));
}

// Function to handle the returned result from FetchKeyMaterial
void HandleFetchResult(MikeySakkeKMS::ClientPtr kms, MikeySakkeKMS::KeyStoragePtr keys, std::string const& identifier, boost::system::error_code error)
{
	// Store keys and identifier globally
	cachedKeyStore = keys;
    lastIdentifier = identifier;
    
    bool validate_keys_success = false;
    bool validate_secret_success = false;

	std::string rsk_string;
	std::string ssk_string;
	std::string pvt_string;
	std::string hs_string;

	if(error)
	{
			// Re-enable text field and button
			EnableWindow(b_kms_connect, true);
			EnableWindow(t_uid, true);
			// Update status
			updateStatus("Failed to fetch keys");
	}
	else
	{
		// Extract keys
		OctetString rsk_oct_string = keys->GetPrivateKey(identifier, "RSK");
		OctetString ssk_oct_string = keys->GetPrivateKey(identifier, "SSK");
		OctetString pvt_oct_string = keys->GetPublicKey(identifier, "PVT");

		// Convert to std::string
		rsk_string = rsk_oct_string.translate();
		ssk_string = ssk_oct_string.translate();
		pvt_string = pvt_oct_string.translate();

		std::vector<std::string> communities = keys->GetCommunityIdentifiers();	
		//for (std::vector<std::string>::const_iterator it = communities.begin(), end = communities.end();
        //     it != end; ++it)
        //{
            //std::cout << "\nCommunity: '" << *it << "'\n'"
            //<< "\n";
            // TODO: Add cout stuff
        //}
        
        lastPeerCommunity = communities[0];

		// Attempt to validate received keys
		try
		{
			validate_keys_success = MikeySakkeCrypto::ValidateSigningKeysAndCacheHS(identifier, communities[0], keys);
			OctetString hs_oct_string = keys->GetPublicKey(identifier, "HS");
			hs_string = hs_oct_string.translate();
		}
		catch (std::exception e)
		{
			// Re-enable text field and button
			EnableWindow(b_kms_connect, true);
			EnableWindow(t_uid, true);
			// Update status
			updateStatus("ValidateSigningKeysAndCacheHS failed");
		}

		try
		{
			validate_secret_success = MikeySakkeCrypto::ValidateReceiverSecretKey(identifier, communities[0], keys);
		}
		catch (std::exception e)
		{
			// Re-enable text field and button
			EnableWindow(b_kms_connect, true);
			EnableWindow(t_uid, true);
			// Update status
			updateStatus("ValidateReceiverSecretKey failed");
		}
	}

	// If keys validated, store keys globally
	if(validate_keys_success && validate_secret_success)
	{
		setRSK(rsk_string);
		setSSK(ssk_string);
		setPVT(pvt_string);
		setHS(hs_string);

		// If connected switch screen
		SwitchScreen(SCR_CONNECTED);
		updateStatus("Connected");
	}
}

// Function to send a request to the KES to check for registered devices and any new messages
void checkKES()
{
	using convenience::cptr;

	struct CurlHandler
	{
		static int AppendToString(char const* data, size_t size, size_t count, std::string* s)
		{
			size *= count;
			s->append(data, size);
			return size;
		}
	};

	try
	{
		// Create the URL request
		std::stringstream url_stream;
		url_stream << "http://";
		url_stream << IP_ADDRESS;
		url_stream << ":";
		url_stream << KES_PORT;
		url_stream << "/check?id=";
		url_stream << userID;

		std::string url = url_stream.str();

		cptr<CURL,curl_easy_cleanup> curl = curl_easy_init();
		// Set all the required options for curl before retrieving the data from the KES
		std::string json;

		char errorBuffer[CURL_ERROR_SIZE];
		curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);  

		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_HEADER, 0);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlHandler::AppendToString);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &json);

		//if (verifyPolicy == DontVerifySSLCertificate)
		//   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

		// Retrieve the key data (JSON format) from the KMS
		CURLcode rc = curl_easy_perform(curl);
		   
		if (rc != CURLE_OK)
		{
			// ERROR
			updateStatus("Failed to retrieve data from KES");
			return;
		}

		// Convert to wstring and display
		//std::wstring json_wstring = s2ws(json);
		//SetWindowText(t_json, json_wstring.c_str());

		Json::Reader r;
		Json::Value kesData;
		if (!r.parse(json, kesData, false))
		{
			// ERROR
			updateStatus("Failed to parse data from KES");
			return;
		}

		// Split kesData into devices and messages
		Json::Value devices = kesData["devices"];
		Json::Value jMessages = kesData["messages"];

		// Clear the current deviceList, and populate with received devices
		deviceList.clear();
		for (int i=0; i < devices.size(); i++)
		{
			deviceList.push_back(devices[i].asString());
		}
		updateStatus("Retrieved devices");

		// Iterate through received messages (if any) and store in a list
		std::list<Message> messages;
		for (Json::ValueIterator it = jMessages.begin(), end = jMessages.end();
			 it != end;
			 ++it)
		{
			Json::Value const& jMessage = *it;

			std::string signature = jMessage["signature"].asString();
			std::string sed		  = jMessage["sed"].asString();
			std::string from	  = jMessage["from"].asString();

			messages.push_back(Message(from, userID, sed, signature));
		}

		// Decode received messages
		if (messages.size() > 0)
			decodeMessages(messages);
		
	}
	catch(std::exception e)
	{
		updateStatus("Check KES operation failed");
	}
}

// Function to decode received message from the KES
// Takes a list of messages as input
void decodeMessages(std::list<Message> messages)
{
	try
	{
		updateStatus("Decoding messages...");
		//OctetString identifier(lastIdentifier.c_str());

		OctetString::Translation const raw = OctetString::Untranslated;
		OctetString identifier;
		identifier.concat(lastIdentifier,raw);

		OctetString SSV;

		for (std::list<Message>::iterator list_it = messages.begin(); list_it != messages.end(); ++list_it)
		{
			// Create senderID octet
			OctetString senderID(DATE_STRING, raw);
			senderID.concat(0).concat((*list_it).getFromDeviceId(), raw).concat(0);

			// Create encrypted data octet
			OctetString SED;
			SED.concat((*list_it).getSakkeEncryptedData());

			// Create signature octet
			OctetString signature;
			signature.concat((*list_it).getSignature());

			// Verify message signature
			bool signatureVerified = MikeySakkeCrypto::Verify(SED.raw(),
															  SED.size(),
															  signature.raw(),
															  signature.size(),
															  senderID,
															  lastPeerCommunity,
															  cachedKeyStore);
			

			if(signatureVerified)
			{

				// Extract shared secret value
				SSV = MikeySakkeCrypto::ExtractSharedSecret(SED,
															lastIdentifier,
															lastPeerCommunity,
															cachedKeyStore);
				if (SSV == OctetString(""))
				{
					updateStatus("Failed to extract key");
				}
				else
				{
					// If successful, store from device and message
					updateStatus("Successfully decoded messages");
					decodedMessages.insert(std::pair<std::string,std::string>((*list_it).getFromDeviceId(), SSV.translate()));
				}
			}
			else
			{
				// if signature fails to verify, update status and continue loop.
				updateStatus("Signature failed to verify");
			}
		}
	}
	catch (std::exception e)
	{
		updateStatus("Message decoding failed");
	}
}

// Function to update the status label
// Takes a std::string as input
void updateStatus(std::string status)
{
	SetWindowText(l_status, s2ws(status).c_str());
}

// Function to iterate through both message maps and display them
void displayMessages()
{
	std::stringstream d_stream, s_stream;

	for (std::multimap<std::string, std::string>::iterator d_Iter = decodedMessages.begin();
		 d_Iter != decodedMessages.end();
		 ++d_Iter)
	{
		d_stream << "From: ";
		d_stream << (*d_Iter).first;
		d_stream << "\r\n";
		d_stream << "SSV: ";
		d_stream << (*d_Iter).second;
		d_stream << "\r\n";
		d_stream << "\r\n";
	}

	for (std::multimap<std::string, std::string>::iterator s_Iter = sentMessages.begin();
		 s_Iter != sentMessages.end();
		 ++s_Iter)
	{
		s_stream << "To: ";
		s_stream << (*s_Iter).first;
		s_stream << "\r\n";
		s_stream << "SSV: ";
		s_stream << (*s_Iter).second;
		s_stream << "\r\n";
		s_stream << "\r\n";
	}

	SetWindowText(t_rMessages, s2ws(d_stream.str()).c_str());
	SetWindowText(t_sMessages, s2ws(s_stream.str()).c_str());
}

// Function that sends an encrypted message to the recipientID
bool sendMessage(std::string recipientID)
{
	try
	{
		OctetString::Translation const raw = OctetString::Untranslated;

		// Create recipientID octet
		OctetString recipID(DATE_STRING, raw);
		recipID.concat(0).concat(recipientID, raw).concat(0);

		OctetString SED;

		// Function to produce a cryptographically safe random number
		struct SSVRandom
		{
			static void generate(uint8_t* p, size_t len)
			{
				randomize(p, len);
			}
		};

		// Generate shared secret value and sakke encrypted data
		OctetString SSV = MikeySakkeCrypto::GenerateSharedSecretAndSED(SED,
																	   recipID,
																	   lastPeerCommunity,
																	   SSVRandom::generate,
																	   cachedKeyStore);

		// Generate message signature
		OctetString signature = MikeySakkeCrypto::Sign(SED.raw(),
													   SED.size(),
													   lastIdentifier,
													   lastPeerCommunity,
													   SSVRandom::generate,
													   cachedKeyStore);

		using convenience::cptr;

		// Create the URL request
		std::stringstream url_stream;
		url_stream << "http://";
		url_stream << IP_ADDRESS;
		url_stream << ":";
		url_stream << KES_PORT;
		url_stream << "/send?id=";
		url_stream << userID;					// windowstest
		url_stream << "&toid=";
		url_stream << recipientID;				// iphone (example)
		url_stream << "&sed=";
		url_stream << SED.translate();
		url_stream << "&signature=";
		url_stream << signature.translate();

		std::string url = url_stream.str();

		cptr<CURL,curl_easy_cleanup> curl = curl_easy_init();
		// Set all the required options for curl before sending message to the KES

		char errorBuffer[CURL_ERROR_SIZE];
		curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);  

		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_HEADER, 0);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);

		//if (verifyPolicy == DontVerifySSLCertificate)
		//   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

		// Send the message
		CURLcode rc = curl_easy_perform(curl);

		// Store the recipient along with the sent message
		sentMessages.insert(std::pair<std::string,std::string>(recipientID, SSV.translate()));
		return true;
	}
	catch (std::exception e)
	{
		return false;
	}
}

// Function used by random number generator
bool randomize(void* buffer, size_t length)
{
	return RAND_bytes((unsigned char*)buffer, (int)length) != 0;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MIKEYSAKKEINTEGRATIONGUI));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MIKEYSAKKEINTEGRATIONGUI);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable
   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 450, 600, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	int len = 0;
	std::wstring w_uid;
	std::string uid, recipientID;
	TCHAR *szText;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDB_KMS_CONNECT:
			// Get uid from textbox
			len = 255;
			szText = new TCHAR[len];
			GetDlgItemText(hWnd, IDT_UID, szText, len);
			w_uid = szText;
			delete[] szText;

			EnableWindow(b_kms_connect, false);
			EnableWindow(t_uid, false);

			updateStatus("Connecting...");

			uid = ws2s(w_uid);

			// Set global var
			setUserID(uid);
			// Connect to KMS using uid
			ConnectToKMS(uid);
			break;
		case IDB_KMS_DISCONNECT:
			// Disconnect really means deleting all keys, messages and devices
			cachedKeyStore.reset();
			lastPeerCommunity = "";
			lastIdentifier = "";
			// Delete global RSK,SSK,PVT,HS,userID
			setRSK("");
			setSSK("");
			setPVT("");
			setHS("");
			setUserID("");
			// Delete stored devices
			deviceList.clear();
			SendDlgItemMessage(hWnd, IDB_COMBOBOX, CB_RESETCONTENT, 0, 0);
			// Delete stored messages
			sentMessages.clear();
			decodedMessages.clear();
			// Switch to connect screen
			SwitchScreen(SCR_CONNECT);
			EnableWindow(b_kms_connect, true);
			EnableWindow(t_uid, true);
			updateStatus("Connect to KMS");
			break;
		case IDB_VIEW_KEYS:
			SwitchScreen(SCR_VIEW_KEYS);
			updateStatus("View Keys");

			// Display keys
			SetWindowText(l_rsk, lastRSK.c_str());
			SetWindowText(l_ssk, lastSSK.c_str());
			SetWindowText(l_pvt, lastPVT.c_str());

			break;
		case IDB_VIEW_MESSAGES:
			SwitchScreen(SCR_VIEW_MESSAGES);
			updateStatus("View Messages");
			displayMessages();
			break;
		case IDB_CHECK_KES:
			//SetWindowText(l_status, TEXT("Checking KES..."));
			updateStatus("Checking KES...");
			checkKES();
			devices = false;
			// When JSON message is received check if it contains only new devices or all devices.
			// If it contains all devices will need to remove all items from ComboBox and re-add

			// Remove all values
			SendDlgItemMessage(hWnd, IDB_COMBOBOX, CB_RESETCONTENT, 0, 0);

			for (std::list<std::string>::iterator it=deviceList.begin(); it != deviceList.end(); ++it)
			{
				devices = true;
				SendMessage(cb_devices, CB_ADDSTRING, 0, (LPARAM)s2ws(*it).c_str());
			}

			// If devices received - enable send message button
			if (devices)
			{
				SendMessage(cb_devices, CB_SETCURSEL, 0, 0); //highlight/select the first option
				EnableWindow(b_send_message, true);
			}
			else
			{
				EnableWindow(b_send_message, false);
			}
			break;
		case IDB_SEND_MESSAGE:
			// Get selected device from combobox
			len = 255;
			TCHAR buf[255];
			szText = new TCHAR[len];
			GetWindowText(cb_devices,(LPWSTR)szText, len);

			_tcscpy_s(buf, 255, TEXT("Message sent to "));
			_tcscat_s(buf, 255, szText);

			// Dialog box will pop-up to confirm sending a message
			if(DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SAVEBOX), hWnd, Send, (LPARAM)szText)==TRUE)
			{
				recipientID = ws2s(szText);

				if (sendMessage(recipientID))
					updateStatus(ws2s(buf));
				else
					updateStatus("Send Message Failed.");
			}
			delete[] szText;
			break;
		case IDB_BACK:
			SwitchScreen(SCR_CONNECTED);
			SetWindowText(l_status, TEXT("Connected"));
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About); 
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_CREATE:
		CreateObjects(hWnd);
		SwitchScreen(SCR_CONNECT);
		break;
	case WM_PAINT:
		// TODO: Add any drawing code here...	

		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Function to create all GUI objects
void CreateObjects(HWND hWnd)
{
		// Connection objects
		l_status = CreateWindow(TEXT("Static"),TEXT("Connect to KMS"),WS_CHILD ,20,5,200,20,hWnd,0,hInst,0);
		l_uid = CreateWindow(TEXT("Static"),TEXT("ID"),WS_CHILD ,20,35,20,20,hWnd,0,hInst,0);	
		t_uid = CreateWindow(TEXT("Edit"), NULL, WS_BORDER | NULL | WS_CHILD | NULL | NULL ,40,35,200,20,hWnd,(HMENU)IDT_UID,hInst,0);
		b_kms_connect = CreateWindow(TEXT("Button"),TEXT("Connect"), BS_PUSHBUTTON | WS_CHILD ,20,60,100,25,hWnd,(HMENU)IDB_KMS_CONNECT,hInst,0);

		// Connected objects
		b_kms_disconnect = CreateWindow(TEXT("Button"),TEXT("Disconnect"), BS_PUSHBUTTON | WS_CHILD ,20,35,100,25,hWnd,(HMENU)IDB_KMS_DISCONNECT,hInst,0);
		b_view_keys = CreateWindow(TEXT("Button"),TEXT("View Keys"), BS_PUSHBUTTON | WS_CHILD ,20,65,100,25,hWnd,(HMENU)IDB_VIEW_KEYS,hInst,0);
		b_check_kes = CreateWindow(TEXT("Button"),TEXT("Check KES"), BS_PUSHBUTTON | WS_CHILD ,20,95,100,25,hWnd,(HMENU)IDB_CHECK_KES,hInst,0);
		b_view_messages = CreateWindow(TEXT("Button"),TEXT("Messages"), BS_PUSHBUTTON | WS_CHILD ,20,125,100,25,hWnd,(HMENU)IDB_VIEW_MESSAGES,hInst,0);
		cb_devices = CreateWindow(TEXT("ComboBox"), NULL, WS_CHILD  | WS_TABSTOP, 20, 155, 100, 150, hWnd, (HMENU)IDB_COMBOBOX, NULL, NULL);
		b_send_message = CreateWindow(TEXT("Button"),TEXT("Send"), BS_PUSHBUTTON | WS_CHILD ,20,295,100,25,hWnd,(HMENU)IDB_SEND_MESSAGE,hInst,0);

		// View messages objects
		l_sMessages = CreateWindow(TEXT("Static"),TEXT("Sent:"),WS_CHILD ,20,65,200,20,hWnd,0,hInst,0);
		t_sMessages = CreateWindow(TEXT("Edit"), NULL, WS_BORDER | NULL | WS_CHILD | ES_MULTILINE | WS_VSCROLL ,20,90,400,200,hWnd,(HMENU)IDT_SENT,hInst,0);
		l_rMessages = CreateWindow(TEXT("Static"),TEXT("Received:"),WS_CHILD ,20,295,200,20,hWnd,0,hInst,0);
		t_rMessages = CreateWindow(TEXT("Edit"), NULL, WS_BORDER | NULL | WS_CHILD | ES_MULTILINE | WS_VSCROLL ,20,320,400,200,hWnd,(HMENU)IDT_REC,hInst,0);

		b_back = CreateWindow(TEXT("Button"),TEXT("Back"), BS_PUSHBUTTON | WS_CHILD ,20,35,100,25,hWnd,(HMENU)IDB_BACK,hInst,0);

		// View keys objects
		l_rsk_label = CreateWindow(TEXT("Static"),TEXT("RSK:"),WS_CHILD ,20,65,200,20,hWnd,0,hInst,0);
		l_rsk		= CreateWindow(TEXT("Edit"),NULL,WS_CHILD | ES_MULTILINE | WS_VSCROLL ,20,95,400,100,hWnd,0,hInst,0);
		l_ssk_label = CreateWindow(TEXT("Static"),TEXT("SSK:"),WS_CHILD ,20,205,200,20,hWnd,0,hInst,0);
		l_ssk		= CreateWindow(TEXT("Edit"),NULL,WS_CHILD | ES_MULTILINE | WS_VSCROLL ,20,235,400,100,hWnd,0,hInst,0);
		l_pvt_label = CreateWindow(TEXT("Static"),TEXT("PVT:"),WS_CHILD ,20,345,200,20,hWnd,0,hInst,0);
		l_pvt		= CreateWindow(TEXT("Edit"),NULL,WS_CHILD | ES_MULTILINE | WS_VSCROLL ,20,375,400,100,hWnd,0,hInst,0);
}

// Function to control the display of elements when the screen is changed due to a button press
void SwitchScreen(int screen)
{
	// Hide all items
	ShowWindow(l_status, SW_HIDE);
	ShowWindow(l_uid, SW_HIDE);
	ShowWindow(t_uid, SW_HIDE);
	ShowWindow(b_kms_connect, SW_HIDE);

	ShowWindow(b_kms_disconnect, SW_HIDE);
	ShowWindow(b_view_keys, SW_HIDE);
	ShowWindow(b_check_kes, SW_HIDE);
	ShowWindow(b_view_messages, SW_HIDE);
	ShowWindow(cb_devices, SW_HIDE);
	ShowWindow(b_send_message, SW_HIDE);
	EnableWindow(b_send_message, false);

	ShowWindow(b_back, SW_HIDE);

	ShowWindow(l_rsk_label, SW_HIDE);
	ShowWindow(l_rsk, SW_HIDE);
	ShowWindow(l_ssk_label, SW_HIDE);
	ShowWindow(l_ssk, SW_HIDE);
	ShowWindow(l_pvt_label, SW_HIDE);
	ShowWindow(l_pvt, SW_HIDE);

	ShowWindow(l_sMessages, SW_HIDE);
	ShowWindow(t_sMessages, SW_HIDE);
	ShowWindow(l_rMessages, SW_HIDE);
	ShowWindow(t_rMessages, SW_HIDE);

	// show only items relevant to screen
	switch(screen)
	{
	case SCR_CONNECT:
		ShowWindow(l_status, SW_SHOW);
		ShowWindow(l_uid, SW_SHOW);
		ShowWindow(t_uid, SW_SHOW);
		ShowWindow(b_kms_connect, SW_SHOW);
		break;
	case SCR_CONNECTED:
		ShowWindow(l_status, SW_SHOW);
		ShowWindow(b_kms_disconnect, SW_SHOW);
		ShowWindow(b_view_keys, SW_SHOW);
		ShowWindow(b_check_kes, SW_SHOW);
		ShowWindow(b_view_messages, SW_SHOW);
		ShowWindow(cb_devices, SW_SHOW);
		ShowWindow(b_send_message, SW_SHOW);
		if (devices)
			EnableWindow(b_send_message, true);
		break;
	case SCR_VIEW_KEYS:
		ShowWindow(l_status, SW_SHOW);
		ShowWindow(b_back, SW_SHOW);
		ShowWindow(l_rsk_label, SW_SHOW);
		ShowWindow(l_rsk, SW_SHOW);
		ShowWindow(l_ssk_label, SW_SHOW);
		ShowWindow(l_ssk, SW_SHOW);
		ShowWindow(l_pvt_label, SW_SHOW);
		ShowWindow(l_pvt, SW_SHOW);
		break;
	case SCR_VIEW_MESSAGES:
		ShowWindow(l_status, SW_SHOW);
		ShowWindow(b_back, SW_SHOW);
		ShowWindow(l_sMessages, SW_SHOW);
		ShowWindow(t_sMessages, SW_SHOW);
		ShowWindow(l_rMessages, SW_SHOW);
		ShowWindow(t_rMessages, SW_SHOW);
		break;
	default:
		break;
	}
}

// Message handler for send box.
INT_PTR CALLBACK Send(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		TCHAR output[255];

		_tcscpy_s(output, 255, TEXT("Do you want to send a message to "));
		_tcscat_s(output, 255, (TCHAR*)lParam);
		_tcscat_s(output, 255, TEXT("?"));

		SetWindowText(GetDlgItem(hDlg, IDC_STATIC), output);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)FALSE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void setRSK(std::string rsk_string)
{
	lastRSK = s2ws(rsk_string);
}

void setSSK(std::string ssk_string)
{
	lastSSK = s2ws(ssk_string);
}

void setPVT(std::string pvt_string)
{
	lastPVT = s2ws(pvt_string);
}

void setHS(std::string hs_string)
{
	lastHS = s2ws(hs_string);
}

void setUserID(std::string uid)
{
	userID = uid;
}

// Convert wide char string to a basic_string
std::string ws2s(std::wstring w_str)
{
	std::string str;
	str.assign(w_str.begin(), w_str.end());
	return str;
}

// Convert a basic_string to a wide char string
std::wstring s2ws(std::string str)
{
	std::wstring w_str;
	w_str.assign(str.begin(), str.end());
	return w_str;
}
