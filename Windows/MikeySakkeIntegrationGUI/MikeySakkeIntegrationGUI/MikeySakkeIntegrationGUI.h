#pragma once

#include "resource.h"
#include "Message.h"

#define IP_ADDRESS "192.168.1.3"
#define KMS_PORT "7070"
#define KES_PORT "7171"

#define DATE_STRING "2012-09"

#define KMS_USER "jim"
#define KMS_PASS "jimpass"
