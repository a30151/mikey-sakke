#pragma once
#include <string>

class Message
{
private:
	std::string m_FromDeviceId;
	std::string m_ToDeviceId;
	std::string m_SakkeEncryptedData;
	std::string m_Signature;

public:
	Message(std::string, std::string, std::string, std::string);
	~Message(void);

	std::string getFromDeviceId();
	std::string getToDeviceId();
	std::string getSakkeEncryptedData();
	std::string getSignature();

	void setFromDeviceId(std::string);
	void setToDeviceId(std::string);
	void setSakkeEncryptedData(std::string);
	void setSignature(std::string);
};

