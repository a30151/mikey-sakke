//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MikeySakkeIntegrationGUI.rc
//
#define IDCANCEL                        0
#define IDC_MYICON                      2
#define IDD_MIKEYSAKKEINTEGRATIONGUI_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MIKEYSAKKEINTEGRATIONGUI    107
#define IDI_SMALL                       108
#define IDC_MIKEYSAKKEINTEGRATIONGUI    109
#define IDD_SAVEBOX                     110
#define IDR_MAINFRAME                   128
#define IDB_KMS_CONNECT                 150
#define IDB_KMS_DISCONNECT              151
#define IDB_VIEW_KEYS                   152
#define IDB_CHECK_KES                   153
#define IDB_VIEW_MESSAGES               154
#define IDB_SEND_MESSAGE                155
#define IDB_BACK                        156
#define IDB_COMBOBOX					157
#define IDT_UID							201
#define IDT_JSON						202
#define IDT_SENT						203
#define IDT_REC							204
#define SCR_CONNECT                     300
#define SCR_CONNECTED                   301
#define SCR_VIEW_KEYS                   302
#define SCR_VIEW_MESSAGES               303
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
