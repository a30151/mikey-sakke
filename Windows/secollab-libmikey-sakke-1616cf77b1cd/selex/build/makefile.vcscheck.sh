#!/usr/bin/env bash

# runs vcs status on the files passed via stdin

if [[ $OS = *Windows* ]]
then
   NOWHERE=NUL
else
   NOWHERE=/dev/null
fi

# run appropriate vcs status command

if git branch >$NOWHERE 2>$NOWHERE
then
   git_toplevel=$(readlink -f $(git rev-parse --show-toplevel))
   while IFS= read -r line
   do
      if [[ $line = $git_toplevel* ]]
      then echo $line;
      else echo >&2 "note: '$line' is outside repository"
      fi
   done |
   xargs git status -u
   exit $?
fi

if svn info >$NOWHERE 2>$NOWHERE
then
   xargs svn status
   exit $?
fi

