//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Header File
// Item Name        : client.h
// Item Description : Class to act as a client for the KMS server.
//
//******************************************************************************

#ifndef MSKMS_CLIENT_H
#define MSKMS_CLIENT_H

#include <mskms/client-fwd.h>
#include <boost/system/error_code.hpp>


namespace MikeySakkeKMS {


namespace sys = boost::system;


class Client : public std::enable_shared_from_this<Client>
{
public:

   // Signature of completion handler to be called from FetchKeyMaterial once
   // processing is complete.
   typedef std::function
           <void (std::string const& identifier, sys::error_code)>
           CompletionHandler;

   enum SSLVerificationPolicy
   {
      DontVerifySSLCertificate,
      VerifySSLCertificate,
   };

   Client(KeyStoragePtr const&);

   void FetchKeyMaterial(std::string const& kmsServer,
                         SSLVerificationPolicy,
                         std::string const& username,
                         std::string const& password,
                         std::string const& identifier,
                         CompletionHandler);

private:

   KeyStoragePtr m_KeyStorage;

   // Prevent an instance of the class being created without key storage
   Client();

   // Prevent copy construction and assignment
   Client(const Client&);
   Client& operator=(const Client&);

};


} // MikeySakkeKMS

#endif//MSKMS_CLIENT_H

