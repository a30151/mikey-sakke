//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_random_brute_force.cpp
// Item Description : Brute force test
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#include <util/bigint-ssl.h>
#include <util/octet-string.h>
#include <mscrypto/parameter-set.h>
#include <mscrypto/eccsi.h>
#include <mscrypto/sakke.h>
#include <mscrypto/hash/sha256.h>
#include <mskms/runtime-key-storage.inl>

#include <openssl/ec.h>

#include <cstdlib>

using namespace MikeySakkeCrypto;

namespace
{
	gmp_randstate_t randomState;

	struct EphemeralFrom6507 // define a fixed 'random' function
	{
	   static void fixed(uint8_t* p, size_t len)
	   {
	      OctetString("34567").deposit_bigendian(p, len);
	   }
	};

	struct SSVFrom6508 // define a fixed 'random' function
	{
	   static void fixed(uint8_t* p, size_t len)
	   {
	      OctetString(OctetString::skipws
	         ("12345678 9ABCDEF0 12345678 9ABCDEF0")).deposit_bigendian(p,len);
	   }
	};

	static std::string community = "aliceandbob.co.uk";

	// Generate a random number between 1 and the specified parameter.
	bigint_ssl generateRandomSSLNumber(const bigint& lessThan)
	{
		bigint randomInt;
		do
		{
			mpz_urandomm(randomInt.get_mpz_t(), randomState, lessThan.get_mpz_t());
		} while ((randomInt == 0) || (randomInt == lessThan));

		return as_bigint_ssl(randomInt);
	}

	// Generate a random identifier of random length between 1 and 255.
	OctetString generateRandomIdentifier()
	{
		OctetString randomId;
		bigint randomInt;
		mp_bitcnt_t bitCount;
		do
		{
			bitCount = (rand() % 2040) + 1;	// Up to 255 * 8 bits
			mpz_urandomb(randomInt.get_mpz_t(), randomState, bitCount);
			to<OctetString>(randomId, randomInt);
		} while ((randomInt == 0) ||
				 (randomId.size() == 0) ||
				 (randomId.size() > 255));

		return randomId;
	}

	// Multiple a point on a curve by a big int, and ensure that the result is
	// the expected length.
	OctetString multiply(const ECC::PrimeCurveJacobianPtr& E,
						 const OctetString& point,
						 const bigint_ssl& multiplyBy,
						 const int expectedLength)
	{
		MikeySakkeCrypto::ECC::Point<bigint_ssl> bigintPoint(E, point);

		EC_GROUP const* ecg_E = E->read_internal<EC_GROUP>();
		EC_POINT const* ecp_G = E->read_internal<EC_POINT>();

		BN_CTX* scratch = bigint_ssl_scratch::get();

		EC_POINT_mul(ecg_E,
				   	 bigintPoint.readwrite_internal<EC_POINT>(),
				   	 0,
				   	 ecp_G,
				   	 multiplyBy,
				   	 scratch);

		OctetString result;
		result.concat("04");
		result.concat(as_octet_string(bigintPoint.x(), expectedLength));
		result.concat(as_octet_string(bigintPoint.y(), expectedLength));

		return result;
	}

	void runMSCryptoTest()
	{
		// Generate Master Secret (z) and KMS Public Key (Z) - RFC 6509
		SakkeParameterSet sakkeParams = sakke_param_set_1();
		bigint const& q = sakkeParams.E_a->point_order();
		bigint_ssl z = generateRandomSSLNumber(q);
		OctetString Z = multiply(sakkeParams.E_j,
								 sakkeParams.E_j->base_point_octets(), //(Px,Py)
								 z,
								 sakkeParams.n);

		// Generate KSAK and KPAK - RFC 6507
		ECC::PrimeCurveJacobianPtr E = eccsi_6509_param_set().curve;
		bigint qbigint = as_bigint(E->point_order());
		bigint_ssl KSAK = generateRandomSSLNumber(qbigint);
		OctetString KPAK = multiply(E,
									E->base_point_octets(), // G
									KSAK,
									32);	// N

		// Generate random identifiers for Alice and Bob
		OctetString aliceId = generateRandomIdentifier();
		OctetString bobId = generateRandomIdentifier();

		// Generate Public Validation Token (PVT)
		bigint_ssl v = generateRandomSSLNumber(qbigint);
		OctetString PVT = multiply(E, E->base_point_octets(), v, 32);

		// Generate HS = hash (G || KPAK || ID || PVT)
		SHA256Digest aliceHS;
		aliceHS.digest(E->base_point_octets());
		aliceHS.digest(KPAK);
		aliceHS.digest(aliceId);
		aliceHS.digest(PVT);
		aliceHS.complete();
		bigint_ssl intHS = as_bigint_ssl(aliceHS);

		// Generate SSK = (v*HS + KSAK) mod q
		BN_CTX* scratch = bigint_ssl_scratch::get();
		bigint_ssl intSSK;
		BN_mod_mul(intSSK, v, intHS, as_bigint_ssl(qbigint), scratch);
		BN_mod_add(intSSK, intSSK, KSAK, as_bigint_ssl(qbigint), scratch);

		OctetString aliceSSK;
		to<OctetString>(aliceSSK, as_bigint(intSSK));

		MikeySakkeKMS::KeyStoragePtr aliceKeys(new MikeySakkeKMS::RuntimeKeyStorage);
		aliceKeys->StorePrivateKey(aliceId, "SSK", aliceSSK);
		aliceKeys->StorePublicKey(aliceId, "PVT", PVT);
		aliceKeys->StorePublicKey(community, "KPAK", KPAK);
		aliceKeys->StorePublicKey(community, "Z", Z);
		aliceKeys->StorePublicParameter(community, "SakkeSet", "1");

		BOOST_TEST_MESSAGE("Validating the Signing Keys...");
		BOOST_CHECK( ValidateSigningKeysAndCacheHS(
			            	aliceId, community, aliceKeys) );

		BOOST_TEST_MESSAGE("Validating the Hash Key...");
		BOOST_CHECK( OctetString(aliceHS) ==
							aliceKeys->GetPublicKey(aliceId, "HS") );

		// Generate RSK
		bigint_ssl inv;
		BN_add(inv, as_bigint_ssl(bobId), z);
		BN_mod_inverse(inv, inv, as_bigint_ssl(q), scratch);

		OctetString bobRSK = multiply(sakkeParams.E_j,
									  sakkeParams.E_j->base_point_octets(),
									  inv,
									  sakkeParams.n);

		MikeySakkeKMS::KeyStoragePtr bobKeys(new MikeySakkeKMS::RuntimeKeyStorage);
		bobKeys->StorePrivateKey(bobId, "RSK", bobRSK);
		bobKeys->StorePublicKey(community, "Z", Z);
		bobKeys->StorePublicKey(community, "KPAK", KPAK);
		bobKeys->StorePublicParameter(community, "SakkeSet", "1");

		BOOST_TEST_MESSAGE("Validating the RSK...");
		BOOST_CHECK( ValidateReceiverSecretKey( bobId, community, bobKeys) );

		OctetString message = generateRandomIdentifier();

		BOOST_TEST_MESSAGE("Signing the message...");
		OctetString signature = Sign(message.raw(),
				                     message.size(),
			                         aliceId,
			                         community,
			                         EphemeralFrom6507::fixed,
			                         aliceKeys);

		BOOST_TEST_MESSAGE("Checking that the signature isn't empty...");
		BOOST_CHECK( signature != OctetString() );

		BOOST_TEST_MESSAGE("Verify the signature...");
		BOOST_CHECK( Verify(message.raw(),
				            message.size(),
	                        signature.raw(),
	                        signature.size(),
	                        aliceId,
	                        community,
	                        bobKeys) );

		BOOST_TEST_MESSAGE("Generating the SSV & SED...");
		OctetString encrypted;
		OctetString alice_ssv = GenerateSharedSecretAndSED(encrypted,
				                                           bobId,
				                                           community,
				                                           SSVFrom6508::fixed,
				                                           aliceKeys);

		BOOST_TEST_MESSAGE("Checking the length of SED...");
		size_t expectedLength = 273;
		BOOST_CHECK( expectedLength == encrypted.size() );

		BOOST_TEST_MESSAGE("Extracting the SSV...");
		OctetString bob_ssv = ExtractSharedSecret(encrypted,
				                                  bobId,
				                                  community,
				                                  bobKeys);

		BOOST_TEST_MESSAGE("Checking the generated and extracted SSVs...");
		BOOST_CHECK( alice_ssv == bob_ssv );
	}
}


BOOST_AUTO_TEST_CASE( BruteForceTest )
{
	gmp_randinit_mt(randomState);

	int maxTestRuns = 10000; // This takes a long time to run, reduce to single figures if a quick run is needed
	for (int i = 0; i < maxTestRuns; ++i)
	{
		std::stringstream testStream;
		testStream << "Running Test: " << i;
		BOOST_TEST_MESSAGE(testStream.str());
		runMSCryptoTest();
	}
}


