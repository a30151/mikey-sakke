//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_curve_openssl.cpp
// Item Description : Unit Tests for the curve-openssl.cpp module.
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#include <mscrypto/parameter-set.h>
#include <util/octet-string.h>
#include <util/bigint-ssl.h>
#include <openssl/ec.h>

using namespace MikeySakkeCrypto;

namespace
{
	static OctetString p = OctetString::skipws(
			  "ffffffff000000010000000000000000"
			  "00000000ffffffffffffffffffffffff");

	static OctetString q = OctetString::skipws(
			"FFFFFFFF 00000000 FFFFFFFF FFFFFFFF"
			"BCE6FAAD A7179E84 F3B9CAC2 FC632551");

	static OctetString Gx = OctetString::skipws(
			"6B17D1F2 E12C4247 F8BCE6E5 63A440F2"
			"77037D81 2DEB33A0 F4A13945 D898C296");

	static OctetString Gy = OctetString::skipws(
			"4FE342E2 FE1A7F9B 8EE7EB4A 7C0F9E16"
	        "2BCE3357 6B315ECE CBB64068 37BF51F5");
}

BOOST_AUTO_TEST_CASE( testECCCurve )
{
	ECC::PrimeCurveJacobianPtr p256(new ECC::PrimeCurveJacobian("P-256"));

	BOOST_CHECK( as_octet_string(p256->field_order()) == p );
	BOOST_CHECK( as_octet_string(p256->point_order()) == q );
	BOOST_CHECK( as_octet_string(p256->base_x()) == Gx );
	BOOST_CHECK( as_octet_string(p256->base_y()) == Gy );

	ECC::Point<bigint_ssl> point(p256, OctetString::skipws
	        ("04                                 "
	         "758A1427 79BE89E8 29E71984 CB40EF75"
	         "8CC4AD77 5FC5B9A3 E1C8ED52 F6FA36D9"
	         "A79D2476 92F4EDA3 A6BDAB77 D6AA6474"
	         "A464AE49 34663C52 65BA7018 BA091F79"));

	EC_POINT const* ecp = point.read_internal<EC_POINT>();
	bigint_ssl x2, y2;
	EC_POINT_get_affine_coordinates_GFp(p256->read_internal<EC_GROUP>(),
	         	 	 	 	 	 	  	ecp,
	         	 	 	 	 	 	  	x2,
	         	 	 	 	 	 	  	y2,
	         	 	 	 	 	 	  	bigint_ssl_scratch::get());


	BOOST_CHECK( as_octet_string(point.x()) == as_octet_string(x2) );
	BOOST_CHECK( as_octet_string(point.y()) == as_octet_string(y2) );
}
