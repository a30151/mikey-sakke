//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_verify_msg_against_signature.cpp
// Item Description : Unit Tests for the Verify method.
//
//******************************************************************************


#include <boost/test/unit_test.hpp>

#include <mscrypto/eccsi.h>
#include <mscrypto/parameter-set.h>
#include <mskms/runtime-key-storage.inl>
#include <util/octet-string.h>

#include "key_data.h"

using namespace mikey_sakke_unit_test;
using namespace MikeySakkeCrypto;

namespace
{
	static std::string community = "testverifymsgagainstsignature.test";

	struct SSVFrom6508 // define a fixed 'random' function
	{
	   static void fixed(uint8_t* p, size_t len)
	   {
		  OctetString(OctetString::skipws
			 ("12345678 9ABCDEF0 12345678 9ABCDEF0")).deposit_bigendian(p,len);
	   }
	};

	static OctetString message("6d65737361676500");

	static OctetString signature = OctetString::skipws(
				"269D4C8FDEB66A74E4EF8C0D5DCC597D"
                "DFE6029C2AFFC4936008CD2CC1045D81"
                "E09B528D0EF8D6DF1AA3ECBF80110CFC"
                "EC9FC68252CEBB679F4134846940CCFD"
                "04                              "
                "758A142779BE89E829E71984CB40EF75"
                "8CC4AD775FC5B9A3E1C8ED52F6FA36D9"
                "A79D247692F4EDA3A6BDAB77D6AA6474"
                "A464AE4934663C5265BA7018BA091F79");
}

BOOST_AUTO_TEST_CASE( testVerifyOK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( Verify(message.raw(), message.size(),
                        signature.raw(), signature.size(),
                        id, community, keys) );
}

BOOST_AUTO_TEST_CASE( testVerifyIncorrectSignature )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString badSignature = OctetString::skipws(
					"269D4C8FDEB66A74E4EF8C0D5DCC597D"
	                "DFE6029C2AFFC4936008CD2CC1045D81"
	                "E09B528D0EF8D6DF1AA3ECBF80110CFC"
	                "EC9FC68252CEBB679F4134846940CCFD"
	                "04                              "
	                "758A142779BE89E829E71984CB40EF75"
	                "8CC4AD775FC5B9A3E1C8ED52F6FA36D9"
	                "A79D247692F4EDA3A6BDAB77D6AA6474"
	                "A464AE4934663C5265BA7018BA091F12");

	BOOST_CHECK( Verify(message.raw(), message.size(),
                        badSignature.raw(), badSignature.size(),
                        id, community, keys) == false );
}

BOOST_AUTO_TEST_CASE( testVerifyIncorrectMessage )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString badMessage("6d65137261676500");

	BOOST_CHECK( Verify(badMessage.raw(), badMessage.size(),
                        signature.raw(), signature.size(),
                        id, community, keys) == false );
}

BOOST_AUTO_TEST_CASE( testVerifyIncorrectKPAK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "KPAK", getInvalidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( Verify(message.raw(), message.size(),
                        signature.raw(), signature.size(),
                        id, community, keys) == false );
}

BOOST_AUTO_TEST_CASE( testVerifyIncorrectID )
{
	OctetString id = constructId("tel:+447700900125");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( Verify(message.raw(), message.size(),
                        signature.raw(), signature.size(),
                        id, community, keys) == false );
}

