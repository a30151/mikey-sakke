//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : key_data.h
// Item Description : Data used by the unit tests.
//
//******************************************************************************

#ifndef KEY_DATA_H
#define KEY_DATA_H

#include <util/octet-string.h>

namespace mikey_sakke_unit_test
{

OctetString constructId(const std::string& uri);
OctetString getValidKPAK();
OctetString getInvalidKPAK();

OctetString getValidZ();
OctetString getInvalidZ();

OctetString getValidSSK();
OctetString getInvalidSSK();

OctetString getValidPVT();
OctetString getInvalidPVT();

OctetString getValidRSK();
OctetString getInvalidRSK();

OctetString getValidSED();
OctetString getInvalidSED();

OctetString getValidSignature();

}

#endif /* KEY_DATA_H_ */
