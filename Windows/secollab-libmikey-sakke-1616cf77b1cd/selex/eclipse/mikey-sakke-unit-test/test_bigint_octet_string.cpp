//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_bigint_octet_string.cpp
// Item Description : Unit Tests for bigint and OctetString.
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#if _MSC_VER == 1700
#include <mpirxx.h>
#else
#include <gmpxx.h>
#endif
#include <util/bigint.h>
#include <iostream>

BOOST_AUTO_TEST_CASE( testBigIntOctetString )
{
    size_t n = 0;
    mpz_class x;
    bigint y(x);
    y += 15;
    mpz_class a("0f30 4020", 16);

    BOOST_CHECK( bits(a) == 28 );
    BOOST_CHECK( as<OctetString>(a) == OctetString("0f304020") );
    BOOST_CHECK( as<OctetString>(a).size() == 4 );

    a += bigint("70000000", 16);
    BOOST_CHECK( bits(a) == 31 );
    BOOST_CHECK( as<OctetString>(a) == OctetString("7f304020") );
    BOOST_CHECK( as<OctetString>(a).size() == 4 );

    a *= 7;
    BOOST_CHECK( bits(a) == 34 );
    BOOST_CHECK( as<OctetString>(a) == OctetString("37A51C0E0") );
    BOOST_CHECK( as<OctetString>(a).size() == 5 );

    BOOST_CHECK_THROW(as<OctetString>(a, 4), std::range_error);

    OctetString s = as<OctetString>(a, 6);
    BOOST_CHECK( s.size() == 6);
    BOOST_CHECK( s == OctetString("00037A51C0E0") );

    s.concat(s);
    BOOST_CHECK( s.size() == 12);
    BOOST_CHECK( s == OctetString("00037A51C0E000037A51C0E0") );

    bigint i = as_bigint(s);
    std::stringstream istream;
    istream << i.get_str(); // Here be dragons - looks like use of c++ GMP commands is problematic http://gmplib.org/list-archives/gmp-discuss/2007-July/002796.html
    BOOST_CHECK( istream.str() == "4204414254132419716301024" );
    BOOST_CHECK( as<OctetString>(i) == OctetString("37A51C0E000037A51C0E0") );
    BOOST_CHECK( as<OctetString>(i,12) == OctetString("00037A51C0E000037A51C0E0") );

    n = bits(y);
    BOOST_CHECK( x == 0 );
    BOOST_CHECK( y == 15 );
    BOOST_CHECK( n == 4 );
}
