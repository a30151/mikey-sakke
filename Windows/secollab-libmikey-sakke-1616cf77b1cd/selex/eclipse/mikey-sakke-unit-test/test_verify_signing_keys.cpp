//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_verify_signing.cpp
// Item Description : Unit Tests for the ValidateReceiverSecretKey method.
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#include <mscrypto/eccsi.h>
#include <mscrypto/parameter-set.h>
#include <mskms/runtime-key-storage.inl>
#include <util/octet-string.h>

#include "key_data.h"

using namespace mikey_sakke_unit_test;
using namespace MikeySakkeCrypto;

namespace
{
	static std::string community = "testverifysigningkeys.test";
}

BOOST_AUTO_TEST_CASE( testVerifySigningKeysOK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateSigningKeysAndCacheHS(id, community, keys) );
}

BOOST_AUTO_TEST_CASE( testVerifySigningKeysIncorrectKPAK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicKey(community, "KPAK", getInvalidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateSigningKeysAndCacheHS(id, community, keys) == false );
}

BOOST_AUTO_TEST_CASE( testVerifySigningKeysIncorrectID )
{
	OctetString id = constructId("tel:+447700900125");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateSigningKeysAndCacheHS(id, community, keys) == false );
}

BOOST_AUTO_TEST_CASE( verifySigningKeysIncorrectSSKTest )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "SSK", getInvalidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateSigningKeysAndCacheHS(id, community, keys) == false );
}

BOOST_AUTO_TEST_CASE( testVerifySigningKeysIncorrectPVT )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getInvalidPVT());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateSigningKeysAndCacheHS(id, community, keys) == false );
}

