//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_verify_rsk.cpp
// Item Description : Unit Tests for the ValidateReceiverSecretKey method.
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#include <mscrypto/sakke.h>
#include <mskms/runtime-key-storage.inl>
#include <util/octet-string.h>

#include "key_data.h"

using namespace mikey_sakke_unit_test;
using namespace MikeySakkeCrypto;

namespace
{
	static std::string community = "testverifyrsk.test";
}

BOOST_AUTO_TEST_CASE( testVerifyRSKOK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "RSK", getValidRSK());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateReceiverSecretKey(id, community, keys) == true );
}

BOOST_AUTO_TEST_CASE( testVerifyRSKIncorrectZ )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "RSK", getValidRSK());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicKey(community, "Z", getInvalidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	// An exception will be thrown as the EC point will be invalid or not on the curve
	BOOST_CHECK_THROW( ValidateReceiverSecretKey(id, community, keys),
						std::exception );
}

BOOST_AUTO_TEST_CASE( testVerifyRSKIncorrectID )
{
	OctetString id = constructId("tel:+447700900125");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "RSK", getValidRSK());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateReceiverSecretKey(id, community, keys) == false );
}

BOOST_AUTO_TEST_CASE( testVerifyRSKIncorrectRSK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePrivateKey(id, "RSK", getInvalidRSK());
	keys->StorePublicKey(community, "KPAK", getValidKPAK());
	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateReceiverSecretKey(id, community, keys) == false );
}

