//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_mscrypto_success.cpp
// Item Description : Unit Test running through sakke/eccsi methods.
//
//******************************************************************************

#define BOOST_TEST_MAIN
#define BOOST_TEST_MODULE MikeySakkeTest

#include <boost/test/unit_test.hpp>

#include <mscrypto/eccsi.h>
#include <mscrypto/parameter-set.h>
#include <mscrypto/sakke.h>
#include <mskms/runtime-key-storage.inl>
#include <util/octet-string.h>

#include "key_data.h"

using namespace mikey_sakke_unit_test;
using namespace MikeySakkeCrypto;

namespace
{
struct SSVFrom6508 // define a fixed 'random' function
{
   static void fixed(uint8_t* p, size_t len)
   {
      OctetString(OctetString::skipws
         ("12345678 9ABCDEF0 12345678 9ABCDEF0")).deposit_bigendian(p,len);
   }
};
}

BOOST_AUTO_TEST_CASE( testMscryptoSuccess )
{
	(void) eccsi_6509_param_set();
	(void) sakke_param_set_1();

	std::string community = "aliceandbob.co.uk";

	OctetString communityPublicAuthenticationKey = getValidKPAK();
	OctetString communityPublicKey = getValidZ();

	OctetString::Translation const raw = OctetString::Untranslated;

	std::string alice_uri = "tel:+447700900123";
	OctetString alice_id;
	alice_id.concat("2011-02",raw).concat(0).concat(alice_uri,raw).concat(0);

	MikeySakkeKMS::KeyStoragePtr alice_keys(new MikeySakkeKMS::RuntimeKeyStorage);

	alice_keys->StorePrivateKey(alice_id, "SSK", getValidSSK());
	alice_keys->StorePublicKey(alice_id, "PVT", getValidPVT());
	alice_keys->StorePublicKey(community, "KPAK", communityPublicAuthenticationKey);
	alice_keys->StorePublicKey(community, "Z", communityPublicKey);
	alice_keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateSigningKeysAndCacheHS(
	            	alice_id, community, alice_keys) );

	std::string bob_uri = "tel:+447700900123";
	OctetString bob_id;
	bob_id.concat("2011-02",raw).concat(0).concat(bob_uri,raw).concat(0);

	MikeySakkeKMS::KeyStoragePtr bob_keys(new MikeySakkeKMS::RuntimeKeyStorage);

	bob_keys->StorePrivateKey(bob_id, "RSK", getValidRSK());
	bob_keys->StorePublicKey(community, "KPAK", communityPublicAuthenticationKey);
	bob_keys->StorePublicKey(community, "Z", communityPublicKey);
	bob_keys->StorePublicParameter(community, "SakkeSet", "1");

	BOOST_CHECK( ValidateReceiverSecretKey(
	             bob_id, community, bob_keys) );

	OctetString message("message", raw);
	message.concat(0);

	struct EphemeralFrom6507 // define a fixed 'random' function
	{
	   static void fixed(uint8_t* p, size_t len)
	   {
	      OctetString("34567").deposit_bigendian(p, len);
	   }
	};

	OctetString signature = Sign(message.raw(), message.size(),
	         alice_id, community, EphemeralFrom6507::fixed, alice_keys);

	BOOST_CHECK( Verify(message.raw(), message.size(),
                               signature.raw(), signature.size(),
                               alice_id, community, bob_keys) );

	OctetString encrypted;
	OctetString alice_ssv = GenerateSharedSecretAndSED(
	            encrypted, bob_id, community, SSVFrom6508::fixed, alice_keys);

	OctetString bob_ssv = ExtractSharedSecret(encrypted, bob_id, community, bob_keys);

	BOOST_CHECK( alice_ssv == bob_ssv );
}

