LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

MINISIP_ROOT := $(LOCAL_PATH)/../../../../..

LOCAL_CPP_EXTENSION := .cxx
LOCAL_MODULE        := minisip-android
LOCAL_SRC_FILES     := JavaSupport.cxx
LOCAL_CFLAGS        := -fexceptions -frtti -g -O0 -fno-inline -rdynamic 

LOCAL_CPPFLAGS      := -I$(MINISIP_ROOT)/libminisip/include \
							  -I$(MINISIP_ROOT)/libmutil/include \
							  -I$(MINISIP_ROOT)/libmikey/include \
							  -I$(MINISIP_ROOT)/libmstun/include \
							  -I$(MINISIP_ROOT)/libmsip/include \
							  -I$(MINISIP_ROOT)/libmnetutil/include \
							  -I$(MINISIP_ROOT)/libmcrypto/include \
							  -I$(LIBMIKEYSAKKE_ROOT)/mskms/client/include \
							  -I$(LIBMIKEYSAKKE_ROOT)/util/include \

LOCAL_LDFLAGS       := -L$(MINISIP_ROOT)/openssl-android/libs/armeabi \
							  -L$(MINISIP_ROOT)/libminisip/$(TARGET_MACH)/.libs \
							  -L$(MINISIP_ROOT)/libmutil/$(TARGET_MACH)/.libs \
							  -L$(MINISIP_ROOT)/libmikey/$(TARGET_MACH)/.libs \
							  -L$(MINISIP_ROOT)/libmstun/$(TARGET_MACH)/.libs \
							  -L$(MINISIP_ROOT)/libmsip/$(TARGET_MACH)/.libs \
							  -L$(MINISIP_ROOT)/libmnetutil/$(TARGET_MACH)/.libs \
							  -L$(MINISIP_ROOT)/libmcrypto/$(TARGET_MACH)/.libs \
							  -L$(LIBMIKEYSAKKE_ROOT)/mskms/client/$(OUTPUT_SPEC)/lib \
							  -L$(LIBMIKEYSAKKE_ROOT)/mscrypto/$(OUTPUT_SPEC)/lib \
							  -L$(BOOST)/$(CROSS_PREFIX)stage/lib \
							  -export-dynamic

LOCAL_LDLIBS        := -lminisip -lmsip -lmikey -lmcrypto -lmstun -lmnetutil -lmutil \
							  -lmsip -lmnetutil -lmcrypto -lboost_filesystem \
							  -lmskms-client -lmscrypto \
							  -lcrypto -lssl -llog -lOpenSLES \

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libmscrypto
LOCAL_SRC_FILES := prebuilt/$(LOCAL_MODULE).so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libmskms-client
LOCAL_SRC_FILES := prebuilt/$(LOCAL_MODULE).so
include $(PREBUILT_SHARED_LIBRARY)

# Support relink if dependent libraries updated; place in vpath (for
# .a and .so) the paths to system libraries and any paths added to
# LDFLAGS
#
nul:=
space:=$(nul) $(nul)
LIBVPATH:=$(patsubst :%,%,$(patsubst %:,%,$(subst $(space),:,\
	$(subst --sysroot=,,$(filter --sysroot%,$(LDFLAGS)))/usr/lib \
	$(subst -L,,$(filter -L%,$(LDFLAGS) $(TARGET_LDFLAGS) $(LOCAL_LDFLAGS))) \
)))
vpath %.a $(LIBVPATH)
vpath %.so $(LIBVPATH)

# Make the final module build-dependent on the necessary libs
$(LOCAL_BUILT_MODULE): $(LOCAL_LDLIBS)

