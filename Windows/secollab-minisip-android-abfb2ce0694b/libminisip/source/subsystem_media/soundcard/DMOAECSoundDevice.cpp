#include "DMOAECSoundDevice.h"
#include <wmcodecdsp.h>
#include <dshow.h>
#include <propsys.h>
#include <config.h>
#include <comdef.h>
#include<libmutil/merror.h>

#define SAFE_RELEASE(pointer) if(pointer){pointer->Release(); pointer = NULL;}

#define TraceCOMError(hr) _com_error error(hr); LPCTSTR errorText = error.ErrorMessage(); std::wstring w=errorText;merr << std::string("") +  std::string(__FILE__) + std::string(" ") + itoa(__LINE__) + std::string(" ") + std::string(w.begin(), w.end()) + std::string("\n")

#define SAMPLE_SIZE 2

DMOAECSoundDevice::DMOAECSoundDevice(std::string deviceName): SoundDevice(deviceName), pDMO(NULL), pbOutputBuffer(NULL), stopThreads(false), recordThread(0) {
	this->sampleSize = SAMPLE_SIZE;
	this->samplingRate = SOUND_CARD_FREQ;
	recordMutex = CreateMutex(NULL, FALSE, NULL);
	if (recordMutex == NULL) {
#ifdef DEBUG_OUTPUT
		merr << "Failed to create recordMutex";
#endif
	}
	HRESULT hr;
	hr = CoInitialize(NULL);
	if (SUCCEEDED(hr)) {
		hr = CoCreateInstance(CLSID_CWMAudioAEC, NULL, CLSCTX_INPROC_SERVER, IID_IMediaObject, (LPVOID*)&pDMO);
		if(SUCCEEDED(hr)) {
			IPropertyStore *propStore = NULL;
			hr = pDMO->QueryInterface(IID_IPropertyStore, (void**)&propStore);
			if(SUCCEEDED(hr)) {
				PROPVARIANT pvSysMode;
				PropVariantInit(&pvSysMode);
				pvSysMode.vt = VT_I4;
				pvSysMode.lVal = (LONG)(AEC_SYSTEM_MODE::SINGLE_CHANNEL_AEC);
				propStore->SetValue(MFPKEY_WMAAECMA_SYSTEM_MODE, pvSysMode);
				PropVariantClear(&pvSysMode);
				SAFE_RELEASE(propStore);

				DMO_MEDIA_TYPE mt;  // Media type.
				mt.majortype = MEDIATYPE_Audio;
				mt.subtype = MEDIASUBTYPE_PCM;
				mt.lSampleSize = 0;
				mt.bFixedSizeSamples = TRUE;
				mt.bTemporalCompression = FALSE;
				mt.formattype = FORMAT_WaveFormatEx;

				// Allocate the format block to hold the WAVEFORMATEX structure.
				hr = MoInitMediaType(&mt, sizeof(WAVEFORMATEX));
				if (SUCCEEDED(hr))
				{
					WAVEFORMATEX *pwav = (WAVEFORMATEX*)mt.pbFormat;
					pwav->wFormatTag = WAVE_FORMAT_PCM;
					pwav->nChannels = 1;
					pwav->nSamplesPerSec = SOUND_CARD_FREQ;
					pwav->nAvgBytesPerSec = SOUND_CARD_FREQ * SAMPLE_SIZE;
					pwav->nBlockAlign = SAMPLE_SIZE;
					pwav->wBitsPerSample = 8*SAMPLE_SIZE;
					pwav->cbSize = 0;

					// Set the output type.
					hr = pDMO->SetOutputType(0, &mt, 0);
					if(!SUCCEEDED(hr)) {
#ifdef DEBUG_OUTPUT
						merr << "Failed to set output type";
						TraceCOMError(hr);
#endif
					}

					// allocate output buffer
					cOutputBufLen = pwav->nSamplesPerSec * pwav->nBlockAlign / 100; // max out at 10ms of data
					pbOutputBuffer = new BYTE[cOutputBufLen]; // 1 seconds worth

					// Free the format block.
					MoFreeMediaType(&mt);

					// Create a DMO output buffer object
					// main loop to get microphone output from the DMO
					OutputBufferStruct.dwStatus = 0;
					OutputBufferStruct.rtTimelength = 0;
					OutputBufferStruct.rtTimestamp = 0;
					OutputBufferStruct.pBuffer = &outputBuffer;

					pDMO->AllocateStreamingResources();
				} else {
#ifdef DEBUG_OUTPUT
					merr << "Failed to MoInitMediaType";
					TraceCOMError(hr);
#endif
				}
			} else {
#ifdef DEBUG_OUTPUT
				merr << "Failed to get PropertyStore";
				TraceCOMError(hr);
#endif
			}
		} else {
#ifdef DEBUG_OUTPUT
			merr << "Failed to create CLSID_CWMAudioAEC instance";
			TraceCOMError(hr);
#endif
		}
	} else {
#ifdef DEBUG_OUTPUT
		merr << "Failed to CoInitialize";
		TraceCOMError(hr);
#endif
	}
};
DMOAECSoundDevice::~DMOAECSoundDevice() {
	SAFE_RELEASE(pDMO); 
	delete [] pbOutputBuffer;
	CloseHandle(recordMutex);
	CoUninitialize();
	emptyBuffers();
}

DWORD WINAPI DMOAECSoundDevice::doRecord(LPVOID lpParam)
{
	// main loop to get microphone output from the DMO
	ULONG cbProduced = 0;
	DWORD dwStatus;
	HRESULT hr;
	DMOAECSoundDevice *dmo = (DMOAECSoundDevice *)lpParam;

	while (dmo != NULL && !dmo->stopThreads)
	{
		Sleep(5); //sleep 5ms
		do{
			dmo->outputBuffer.Init(dmo->pbOutputBuffer, dmo->cOutputBufLen, 0);
			dmo->OutputBufferStruct.dwStatus = 0;
			hr = dmo->pDMO->ProcessOutput(0, 1, &(dmo->OutputBufferStruct), &dwStatus);

			if (hr == S_FALSE) {
				cbProduced = 0;
			} else {
				hr = dmo->outputBuffer.GetBufferAndLength(NULL, &cbProduced);
				if (!SUCCEEDED(hr)) {
					std::cerr << "GetBufferAndLength failed" << std::endl;
					TraceCOMError(hr);
				}
			}
			// write microphone output data from pbOutputBuffer (length in bytes is cbProduced)
			if(cbProduced != 0) {
				DWORD waitResult = WaitForSingleObject(dmo->recordMutex, INFINITE);
				if(waitResult == WAIT_OBJECT_0) {
					struct DMOAECSoundDevice::buffer *buf = new struct DMOAECSoundDevice::buffer;
					buf->length = cbProduced;
					buf->offset = 0;
					//swap buffers rather than copy
					buf->data = dmo->pbOutputBuffer;
					dmo->pbOutputBuffer = new unsigned char[dmo->cOutputBufLen];
					dmo->buffers.push_back(buf);
					ReleaseMutex(dmo->recordMutex);
				}
			}
		} while (dmo->OutputBufferStruct.dwStatus & DMO_OUTPUT_DATA_BUFFERF_INCOMPLETE && !dmo->stopThreads);
	}
	return 0;
}

int DMOAECSoundDevice::openRecord( int samplingRate, int nChannels, int format )
{
	this->nChannelsRecord = 1;
	this->openedRecord = true;
	// do not allow multiple threads or creation when stopping
	if (!stopThreads && !recordThread) {
		DWORD ThreadID;
		recordThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) DMOAECSoundDevice::doRecord, this, 0, &ThreadID);
	}
	return 0;
}

int DMOAECSoundDevice::closeRecord()
{
	openedRecord = false;
	stopThreads = true;
	WaitForSingleObject(recordThread, INFINITE);
	recordThread = 0;
	stopThreads = false; // allow re-starting of record
	emptyBuffers();
	return 0;
}

int DMOAECSoundDevice::readFromDevice( byte_t * buffer, uint32_t nSamples )
{
	unsigned int i = 0; // sample index
	if(buffers.size() > 0) {
		DWORD waitResult = WaitForSingleObject(recordMutex, INFINITE);
		if (waitResult == WAIT_OBJECT_0) {
			while ( i < nSamples && buffers.size() > 0) {
				struct buffer *buf = buffers.front();
				if(buf->length > (nSamples - i)*SAMPLE_SIZE) {
					memcpy(&buffer[i], &buf->data[buf->offset], (nSamples - i)*SAMPLE_SIZE);
					buf->length -= (nSamples - i)*SAMPLE_SIZE;
					buf->offset += (nSamples - i)*SAMPLE_SIZE;
					i = nSamples;
				} else {
					memcpy(&buffer[i], &buf->data[buf->offset], buf->length);
					i += buf->length/SAMPLE_SIZE;
					buffers.pop_front();
					delete [] buf->data;
					delete buf;
				}
			}
			ReleaseMutex(recordMutex);
		}
	}
	return i;
}

int DMOAECSoundDevice::readError( int errcode, byte_t * buffer, uint32_t nSamples )
{
	return -1;
}

void DMOAECSoundDevice::sync()
{
}

int DMOAECSoundDevice::openPlayback( int samplingRate, int nChannels, int format )
{
	return 0;
}
		
int DMOAECSoundDevice::closePlayback()
{
	return 0;
}

int DMOAECSoundDevice::writeToDevice( byte_t * buffer, uint32_t nSamples )
{
	return 0;
}

int DMOAECSoundDevice::writeError( int errcode, byte_t * buffer, uint32_t nSamples )
{
	return -1;
}

void DMOAECSoundDevice::emptyBuffers()
{
	// free up buffers
	while(buffers.size() != 0) {
		struct DMOAECSoundDevice::buffer *buf = buffers.front();
		buffers.pop_front();
		delete [] buf->data;
		delete buf;
	}
}