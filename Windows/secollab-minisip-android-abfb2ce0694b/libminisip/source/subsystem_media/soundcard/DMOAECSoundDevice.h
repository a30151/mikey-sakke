
#ifndef DMO_AEC_SOUND_DEVICE_H
#define DMO_AEC_SOUND_DEVICE_H

#include<libminisip/libminisip_config.h>

#include<libminisip/media/soundcard/SoundDevice.h>

#include <list>
#include <dmo.h>
#include <Windows.h>

class CBaseMediaBuffer : public IMediaBuffer {
public:
   CBaseMediaBuffer() {}
   CBaseMediaBuffer(BYTE *pData, ULONG ulSize, ULONG ulData) :
      m_pData(pData), m_ulSize(ulSize), m_ulData(ulData), m_cRef(1) {}
   STDMETHODIMP_(ULONG) AddRef() {
      return InterlockedIncrement((long*)&m_cRef);
   }
   STDMETHODIMP_(ULONG) Release() {
      long l = InterlockedDecrement((long*)&m_cRef);
      if (l == 0)
         delete this;
      return l;
   }
   STDMETHODIMP QueryInterface(REFIID riid, void **ppv) {
      if (riid == IID_IUnknown) {
         AddRef();
         *ppv = (IUnknown*)this;
         return NOERROR;
      }
      else if (riid == IID_IMediaBuffer) {
         AddRef();
         *ppv = (IMediaBuffer*)this;
         return NOERROR;
      }
      else
         return E_NOINTERFACE;
   }
   STDMETHODIMP SetLength(DWORD ulLength) {m_ulData = ulLength; return NOERROR;}
   STDMETHODIMP GetMaxLength(DWORD *pcbMaxLength) {*pcbMaxLength = m_ulSize; return NOERROR;}
   STDMETHODIMP GetBufferAndLength(BYTE **ppBuffer, DWORD *pcbLength) {
      if (ppBuffer) *ppBuffer = m_pData;
      if (pcbLength) *pcbLength = m_ulData;
      return NOERROR;
   }
protected:
   BYTE *m_pData;
   ULONG m_ulSize;
   ULONG m_ulData;
   ULONG m_cRef;
};

class CStaticMediaBuffer : public CBaseMediaBuffer {
public:
    STDMETHODIMP_(ULONG) AddRef() {return 2;}
    STDMETHODIMP_(ULONG) Release() {return 1;}
    void Init(BYTE *pData, ULONG ulSize, ULONG ulData) {
        m_pData = pData;
        m_ulSize = ulSize;
        m_ulData = ulData;
    }
};

class DMOAECSoundDevice : public SoundDevice{
	public:
		DMOAECSoundDevice( std::string deviceName );

		virtual ~DMOAECSoundDevice();

		virtual int openRecord( int samplingRate, int nChannels, int format );
		virtual int openPlayback( int samplingRate, int nChannels, int format );
		
		virtual int closeRecord();
		virtual int closePlayback();

		virtual int readFromDevice( byte_t * buffer, uint32_t nSamples );
		virtual int writeToDevice( byte_t * buffer, uint32_t nSamples );

		virtual int readError( int errcode, byte_t * buffer, uint32_t nSamples );
		virtual int writeError( int errcode, byte_t * buffer, uint32_t nSamples );
		
		virtual void sync();

		struct buffer {
			unsigned int length;
			unsigned int offset;
			unsigned char *data;
		};

	private:
		static DWORD WINAPI doRecord(LPVOID lpParam);
		void emptyBuffers();

	IMediaObject *pDMO;
	int cOutputBufLen;
	BYTE *pbOutputBuffer;
	CStaticMediaBuffer outputBuffer;
	DMO_OUTPUT_DATA_BUFFER OutputBufferStruct;
	HANDLE recordMutex;
	HANDLE recordThread;
	bool stopThreads;
	std::list<struct buffer*> buffers;
};

#endif //DMO_AEC_SOUND_DEVICE_H
