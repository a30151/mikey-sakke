

#include<config.h>

#include<libminisip/media/soundcard/SoundDriver.h>
#include<libminisip/media/soundcard/SoundDriverRegistry.h>
#include<libmutil/MPlugin.h>

#include <Windows.h>
#include <dshow.h>
#include"DMOAECSoundDriver.h"
#include"DMOAECSoundDevice.h"

using namespace std;

static const char DRIVER_PREFIX[] = "dmo_aec";
/*static std::list<std::string> pluginList;
static int initialized;

extern "C" LIBMINISIP_API
std::list<std::string> *mwasapi_LTX_listPlugins( MRef<Library*> lib ){
	if( !initialized ){
		pluginList.push_back("getWASAPIPlugin");
		initialized = true;
	}

	return &pluginList;
}

extern "C" LIBMINISIP_API
MPlugin * mwasapi_LTX_getWASAPIPlugin( MRef<Library*> lib ){
	return new DMOAECSoundDriver( lib );
}*/

DMOAECSoundDriver::DMOAECSoundDriver( MRef<Library*> lib ) : SoundDriver( DRIVER_PREFIX, lib ){
}

DMOAECSoundDriver::~DMOAECSoundDriver( ){
}

	//Take "0" to mean default input and output devices
	// TODO non-default devices
MRef<SoundDevice*> DMOAECSoundDriver::createDevice( string deviceId ){
	return new DMOAECSoundDevice( deviceId );
}

std::vector<SoundDeviceName> DMOAECSoundDriver::getDeviceNames() const {
	std::vector<SoundDeviceName> names;
	names.push_back(SoundDeviceName("DMO capture only", "Windows Direct Media Object driver for capturing voice with acoustic echo cancellation", 1, 1));

	return names;
}

bool DMOAECSoundDriver::getDefaultInputDeviceName( SoundDeviceName &name ) const{
	mdbg << "DMOAECSoundDriver::getDefaultInputDeviceName not implemented" << endl;

	return false;
}

bool DMOAECSoundDriver::getDefaultOutputDeviceName( SoundDeviceName &name ) const {
	mdbg << "DMOAECSoundDriver::getDefaultOutputDeviceName not implemented" << endl;

	return false;
}

uint32_t DMOAECSoundDriver::getVersion() const{
	return 0x00000001;
}
