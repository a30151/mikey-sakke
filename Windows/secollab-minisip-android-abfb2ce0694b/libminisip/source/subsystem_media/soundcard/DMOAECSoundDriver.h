#ifndef DMOAECSOUNDDRIVER_H
#define DMOAECSOUNDDRIVER_H

#include<libminisip/libminisip_config.h>

#include<string>
#include<libmutil/MemObject.h>

#include<libminisip/media/soundcard/SoundDriver.h>


class DMOAECSoundDriver: public SoundDriver{
	public:
		DMOAECSoundDriver( MRef<Library*> lib );
		virtual ~DMOAECSoundDriver();
		virtual MRef<SoundDevice*> createDevice( std::string deviceId );
		virtual std::string getDescription() const { return "DMO AEC sound capture driver"; };

		virtual std::vector<SoundDeviceName> getDeviceNames() const;

		virtual bool getDefaultInputDeviceName( SoundDeviceName &name ) const;
		
		virtual bool getDefaultOutputDeviceName( SoundDeviceName &name ) const;

		virtual std::string getName() const {
			return "DMO";
		}

		virtual std::string getMemObjectType() const { return getName(); }

		virtual uint32_t getVersion() const;
};

#endif	// DMOAECSOUNDDRIVER_H
