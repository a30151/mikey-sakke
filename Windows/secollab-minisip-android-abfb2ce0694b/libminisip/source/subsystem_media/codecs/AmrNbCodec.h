#ifndef AMRNBCODEC_H
#define AMRNBCODEC_H

#include<libminisip/libminisip_config.h>

#include<libminisip/media/codecs/Codec.h>
class AmrNbCodecState : public CodecState{
	public:
		AmrNbCodecState();
		~AmrNbCodecState();

		/**
		* Will only encode in fixed bit rate of 12.2Kbs, mono, octet-aligned
		* @returns Number of bytes in output buffer
		*/
		virtual uint32_t encode(void *in_buf, int32_t in_buf_size, int samplerate, void *out_buf);

		/**
		*
		* @returns Number of samples in output buffer, from octet-aligned AMR stream compliant with RFC3267 (only uses first good quality frame - subsequent ones ignored)
		*/
		virtual uint32_t decode(void *in_buf, int32_t in_buf_size, void *out_buf);

private:
	void *decoder;
	void *encoder;

	// See RFC 3267 for details of AMR over RTP format
	const static unsigned char CMR_NO_REQUEST = 0xF0;
	const static unsigned char CMR_MASK = 0xF0;
	const static unsigned char CMR_MAX_NB = 7;
	const static unsigned char TOC_LAST_IN_PAYLOAD_AND_PAD_MASK = 0x7C;
	const static unsigned char TOC_PAD = 0x03;
	const static unsigned char TOC_LAST_IN_PAYLOAD = 0x80;
	const static unsigned char TOC_QUALITY_GOOD = 0x04;
	const static unsigned char AMR_MODE_MASK = 0x78;
	const static unsigned char AMR_MAX_NB = 8;
	const static unsigned char AMR_NO_DATA = 15;
	const static int amrnb_mode_to_byte_count[16];
};

// Interface to the AMR Narrow Band codec from Opencore
class AmrNbCodec : public AudioCodec{
	public:
		virtual MRef<CodecState *> newInstance();
		
		AmrNbCodec( MRef<Library *> lib );
		virtual ~AmrNbCodec();
	
		/**
		 * @return Requested sampling freq for the CODEC
		 */
		virtual int32_t getSamplingFreq();

		/**
		 * Time in milliseconds to put in each frame/packet. This is 20ms for the AMR NB codec.
		 */
		virtual int32_t getSamplingSizeMs();

		/**
		 * size of the output of the codec in bytes. This is 160.
		 */
//		virtual int32_t getEncodedNrBytes();
		
		virtual int32_t getInputNrSamples();
		
		virtual std::string getCodecName();
		
		virtual std::string getCodecDescription();

		virtual uint8_t getSdpMediaType();

		virtual std::string getSdpMediaAttributes();
		
		virtual std::string getMemObjectType() const {return "AmrNbCodec";}		
		
		virtual uint32_t getVersion()const {return 1;}

};
#endif //AMRNBCODEC_H