#include"AmrNbCodec.h"
#include <interf_dec.h>
#include <interf_enc.h>

using namespace std;

AmrNbCodec::AmrNbCodec( MRef<Library *> lib): AudioCodec( lib ) {
#ifdef _DEBUG
	remove("amr_encoded_audio_tx.amr");
	remove("encode_input.raw");
	remove("amr_decoded.raw");
	remove("decode_input.amr");
#endif
}

AmrNbCodec::~AmrNbCodec(){
}

const int AmrNbCodecState::amrnb_mode_to_byte_count[16] = {13, 14, 16, 18, 20, 21, 27, 32, 0, 0, 0, 0, 0, 0, 0, 0};

AmrNbCodecState::AmrNbCodecState(){
	decoder = Decoder_Interface_init();
	encoder = Encoder_Interface_init(0); // do not allow DTX
}

AmrNbCodecState::~AmrNbCodecState(){
	Decoder_Interface_exit(decoder);
	Encoder_Interface_exit(encoder);
}

uint32_t AmrNbCodecState::encode(void *in_buf, int32_t in_buf_size, int samplerate, void *out_buf){
	massert(in_buf_size==2*160);
	
	short *in_data = (short*)in_buf;
	unsigned char *out_data = (unsigned char*)out_buf;
	int n = 0;

	if (encoder != NULL) {
		// Add RTP CMR header (no request)
		*out_data = CMR_NO_REQUEST;
		out_data++;
		n++;
#ifdef _DEBUG
		FILE *famr = fopen("amr_encoded_audio_tx.amr","a+b");
		fseek(famr, 0, SEEK_SET);
		unsigned char hdr[16];
		if (fread(hdr, 1, 6, famr) != 6 || memcmp(hdr , "#!AMR\n", 6) != 0) {
			fseek(famr, 0, SEEK_SET);
			fwrite("#!AMR\n", 1, 6, famr);
		}
		fseek(famr, 0, SEEK_END);
#endif _DEBUG

		n +=  Encoder_Interface_Encode(encoder, MR122, in_data, out_data, 0);

#ifdef _DEBUG
		fwrite(out_data, 1 ,n-1, famr); // use AMR format output (not AMR over RTP format)
		fclose(famr);

		famr = fopen("encode_input.raw", "a+b");
		fwrite(in_data, sizeof(short), in_buf_size/sizeof(short), famr);
		fclose(famr);
#endif
		// Sort out F and Q bits in ToC (as per RFC 3267) which is bit position compatible with the AMR format header, but with different meanings
		*out_data &= TOC_LAST_IN_PAYLOAD_AND_PAD_MASK;
		*out_data |= TOC_QUALITY_GOOD;

		return n;
	} else {
		return 0;
	}
}

uint32_t AmrNbCodecState::decode(void *in_buf, int32_t in_buf_size, void *out_buf){
	
	unsigned char *in_data = (unsigned char*)in_buf;
	short *out_data = (short*)out_buf;
	if (decoder != NULL) {
		// skip AMR over RTP Header - CMR
		if (((in_data[0]&CMR_MASK)>>4) > CMR_MAX_NB && in_data[0] != CMR_NO_REQUEST) {
			return 0;
		}
		unsigned char *ptr = &in_data[1];
		int32_t offset = 0;
		bool last_frame_found = false;
		unsigned char header = 0;
		// check ToCs for first good quality frame, keeping track of the offset due to bad speech streams and validating ToC data
		while(!last_frame_found && 
			  !((*ptr)&TOC_PAD) && 
			  ((((*ptr)&AMR_MODE_MASK)>>3)<=AMR_MAX_NB || (((*ptr)&AMR_MODE_MASK)>>3) == AMR_NO_DATA)) {
			if ((*ptr)&TOC_QUALITY_GOOD) {
				header = *ptr;
			}
			if (header == 0) {
				offset += amrnb_mode_to_byte_count[((*ptr)&AMR_MODE_MASK) >> 3];
			}
			last_frame_found = ((*ptr)&TOC_LAST_IN_PAYLOAD) ? false : true;
			ptr++;
		}
		if (header != 0) {
			// find the speech stream
			ptr--;
			if(ptr + offset + amrnb_mode_to_byte_count[(header&AMR_MODE_MASK) >> 3] <= (((unsigned char*)in_buf) + in_buf_size)) {
				ptr += offset; // must put header info immediately before speech stream
				*ptr = header;
				// Set bits AMR decoder expects
				*ptr &= 0x7C;
				*ptr |= 0x04;
				Decoder_Interface_Decode(decoder, ptr, out_data, 0);
#ifdef _DEBUG
				FILE * ofp;
				ofp = fopen("amr_decoded.raw", "a+b");
				fwrite(out_data, sizeof(short), 160, ofp);// NB needs to be little endian for WAV compatibility
				fclose(ofp);

				ofp = fopen("decode_input.amr", "a+b"); // Needs "!#AMR\n" header adding to be playable
				fwrite(ptr, 1, amrnb_mode_to_byte_count[((*ptr)&AMR_MODE_MASK) >> 3], ofp);
				fclose(ofp);
#endif
				return 160;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

int32_t AmrNbCodec::getSamplingSizeMs(){
	return 20;
}

int32_t AmrNbCodec::getSamplingFreq(){
	return 8000;
}


int32_t AmrNbCodec::getInputNrSamples(){
	return 160;
}

string AmrNbCodec::getCodecName(){
		return "AMR NB";
}

string AmrNbCodec::getCodecDescription(){
		return "AMR 8kHz";
}

uint8_t AmrNbCodec::getSdpMediaType(){
	// AMR is a dynamic payload type so should be in the range 96-127
	// As this codec is being added to allow interoperation with BlackBerry Linphone 1.0.1 we will use the value it offers
	return 114;
}

string AmrNbCodec::getSdpMediaAttributes(){
		return "AMR/8000/1";
}

MRef<CodecState *> AmrNbCodec::newInstance(){
	MRef<CodecState *> ret = new AmrNbCodecState( );
	ret->setCodec( this );
	return ret;
}


