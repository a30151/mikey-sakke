package se.kth.minisip;

public class Minisip
{
   /**
    * Temporary hack.  FIXME: ditch this.
    * XXX: Currently need to set this up before construction; urk.
    */
   public native static void setHomeDir(String s);

   /**
    * Construct the native minisip stack.  Call startSip() to actually
    * get things going.
    */ 
   public Minisip( NativeGuiAdaptor gui, String[] args )
   {
      initialize(gui, args);
   }

   /**
    * Trigger stack shutdown.
    */
   public native int stop();

   /**
    * Wait for worker threads to exit.
    */
   public native int join();

   /**
    * Start stack.
    */
   public native void startSip();

   public int exit()
   {
      return stop() + join();
   }

   public native void startDebugger();
   public native void stopDebugger();

   /** Native counterpart **/

   private long counterpart;
   private native void initialize(NativeGuiAdaptor gui, String[] args);
   private native void free();
   protected void finalize() throws Throwable { free(); super.finalize(); }
}

