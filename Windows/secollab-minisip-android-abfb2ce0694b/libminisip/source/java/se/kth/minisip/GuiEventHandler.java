package se.kth.minisip;

import se.kth.mutil.CommandString;

public interface GuiEventHandler
{
   /**
    * Handle a command from the back-end.
    */
   void handleCommand(CommandString cmd);

   /**
    * Handle a response from the back-end.
    */
   CommandString handleCommandResp(String subsystem, CommandString cmd);
}

