#ifndef PHONE_BOOK_PERSON_INTERFACE_H
#define PHONE_BOOK_PERSON_INTERFACE_H
#include<libmutil/MemObject.h>

class LIBMINISIP_API PhoneBookPersonInterface2 : public MObject{
};

class LIBMINISIP_API ContactEntryInterface : public MObject {
public:
	virtual void setPersonIndex(uint32_t index) = 0;
	virtual std::string getUri() = 0;
	virtual uint32_t getId() = 0;
};

class LIBMINISIP_API PhoneBookInterface : public MObject{
public:
	virtual void delPerson( MRef< PhoneBookPersonInterface2 * > person ) = 0;
};
//based on PhoneBookPerson in PhoneBook.h - minimal interface to avoid forward referencing problems
class LIBMINISIP_API PhoneBookPersonInterface : public PhoneBookPersonInterface2{
	public:
		PhoneBookPersonInterface( std::string name );
		PhoneBookPersonInterface(){;}
		~PhoneBookPersonInterface(){;}
		virtual std::string getName() = 0;
		virtual void setPhoneBook( MRef<PhoneBookInterface *> phoneBook ) = 0;
		virtual void addEntry( MRef<ContactEntryInterface *> ) = 0;
		
		virtual std::string getMemObjectType() const {return "PhoneBookPersonInterface";}

};

#endif