/* ------------------------------------------------------------------
 * Copyright (C) 2009 Martin Storsjo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 * -------------------------------------------------------------------
 */
#include "stdafx.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <interf_enc.h>
#include <math.h>

#include <stdlib.h>
#include "wavreader.h"
#include "wavwriter.h"

// getopt stuff from http://note.sonots.com/Comp/CompLang/cpp/getopt.html
#define NULL	0
#define EOF	(-1)
#define ERR(s, c)	if(opterr){\
	char errbuf[2];\
	errbuf[0] = c; errbuf[1] = '\n';\
	fputs(argv[0], stderr);\
	fputs(s, stderr);\
	fputc(c, stderr);}

int	opterr = 1;
int	optind = 1;
int	optopt;
char	*optarg;

int getopt(int argc, char **argv, char *opts)
{
	static int sp = 1;
	register int c;
	register char *cp;

	if(sp == 1)
		if(optind >= argc ||
		   argv[optind][0] != '-' || argv[optind][1] == '\0')
			return(EOF);
		else if(strcmp(argv[optind], "--") == NULL) {
			optind++;
			return(EOF);
		}
	optopt = c = argv[optind][sp];
	if(c == ':' || (cp=strchr(opts, c)) == NULL) {
		ERR(": illegal option -- ", c);
		if(argv[optind][++sp] == '\0') {
			optind++;
			sp = 1;
		}
		return('?');
	}
	if(*++cp == ':') {
		if(argv[optind][sp+1] != '\0')
			optarg = &argv[optind++][sp+1];
		else if(++optind >= argc) {
			ERR(": option requires an argument -- ", c);
			sp = 1;
			return('?');
		} else
			optarg = argv[optind++];
		sp = 1;
	} else {
		if(argv[optind][++sp] == '\0') {
			sp = 1;
			optind++;
		}
		optarg = NULL;
	}
	return(c);
}


void usage(const char* name) {
	fprintf(stderr, "%s [-r bitrate] [-d] [-s | in.wav] out.amr\n-d enables DTX\n-s generate sine", name);
}

enum Mode findMode(const char* str) {
	struct {
		enum Mode mode;
		int rate;
	} modes[] = {
		{ MR475,  4750 },
		{ MR515,  5150 },
		{ MR59,   5900 },
		{ MR67,   6700 },
		{ MR74,   7400 },
		{ MR795,  7950 },
		{ MR102, 10200 },
		{ MR122, 12200 }
	};
	int rate = atoi(str);
	int closest = -1;
	int closestdiff = 0;
	unsigned int i;
	for (i = 0; i < sizeof(modes)/sizeof(modes[0]); i++) {
		if (modes[i].rate == rate)
			return modes[i].mode;
		if (closest < 0 || closestdiff > abs(modes[i].rate - rate)) {
			closest = i;
			closestdiff = abs(modes[i].rate - rate);
		}
	}
	fprintf(stderr, "Using bitrate %d\n", modes[closest].rate);
	return modes[closest].mode;
}

int main(int argc, char *argv[]) {
	enum Mode mode = MR122;
	int ch, j, dtx = 0, sine = 0;
	const char *infile, *outfile;
	FILE *out;
	void *wav, *amr, *wavout;
	int format, sampleRate, channels = 1, bitsPerSample;
	int inputSize;
	uint8_t* inputBuf = NULL;
	uint8_t littleendian[320], *ptr;
	while ((ch = getopt(argc, argv, "r:d:s")) != -1) {
		switch (ch) {
		case 'r':
			mode = findMode(optarg);
			break;
		case 'd':
			dtx = 1;
			break;
		case 's':
			sine = 1;
			break;
		case '?':
		default:
			usage(argv[0]);
			return 1;
		}
	}
	if ((!sine && (argc - optind < 2)) || (sine && (argc - optind < 1))) {
		usage(argv[0]);
		return 1;
	}
	if (!sine) {
		infile = argv[optind];
		outfile = argv[optind + 1];

		wav = wav_read_open(infile);
		if (!wav) {
			fprintf(stderr, "Unable to open wav file %s\n", infile);
			return 1;
		}
		if (!wav_get_header(wav, &format, &channels, &sampleRate, &bitsPerSample, NULL)) {
			fprintf(stderr, "Bad wav file %s\n", infile);
			return 1;
		}
		if (format != 1) {
			fprintf(stderr, "Unsupported WAV format %d\n", format);
			return 1;
		}
		if (bitsPerSample != 16) {
			fprintf(stderr, "Unsupported WAV sample depth %d\n", bitsPerSample);
			return 1;
		}
		if (channels != 1)
			fprintf(stderr, "Warning, only compressing one audio channel\n");
		if (sampleRate != 8000)
			fprintf(stderr, "Warning, AMR-NB uses 8000 Hz sample rate (WAV file has %d Hz)\n", sampleRate);
		inputSize = channels*2*160;
		inputBuf = (uint8_t*) malloc(inputSize);
	} else {
		outfile = argv[optind];
		inputSize = 2*160;
		inputBuf = (uint8_t*) malloc(inputSize);
		for (j = 0; j < 160; j++) {
			((short*)inputBuf)[j] = 32767*sin(400*2*3.141592654*j/8000);
		}
		/* Convert to little endian and write to wav */
		ptr = littleendian;
		for (j = 0; j < 160; j++) {
			*ptr++ = (((short*)inputBuf)[j] >> 0) & 0xff;
			*ptr++ = (((short*)inputBuf)[j] >> 8) & 0xff;
		}
		wavout = wav_write_open("sine.wav", 8000, 16, 1);
		if (!wavout) {
			fprintf(stderr, "Unable to open sine.wav\n");
			return 1;
		}
	}

	amr = Encoder_Interface_init(dtx);
	out = fopen(outfile, "wb");
	if (!out) {
		perror(outfile);
		return 1;
	}

	fwrite("#!AMR\n", 1, 6, out);
	j = 0;
	while (1) {
		short buf[160];
		uint8_t outbuf[500];
		int read, i, n;
		if(!sine) {
			read = wav_read_data(wav, inputBuf, inputSize);
			read /= channels;
			read /= 2;
			if (read < 160)
				break;
		} else {
			wav_write_data(wavout, littleendian, 320);
			j++;
			if (j > 500) // only write out 10 seconds of sine
				break;
		}
		for (i = 0; i < 160; i++) {
			const uint8_t* in = &inputBuf[2*channels*i];
			buf[i] = in[0] | (in[1] << 8);
		}
		n = Encoder_Interface_Encode(amr, mode, buf, outbuf, 0);
		fwrite(outbuf, 1, n, out);
	}
	if(sine) {
		wav_write_close(wavout);
	} else {
		wav_read_close(wav);
	}
	free(inputBuf);
	fclose(out);
	Encoder_Interface_exit(amr);

	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	main(argc, argv);
}
