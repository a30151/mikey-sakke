package selex.milnet.profiles;

import android.app.ListActivity;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import selex.milnet.mk.R;
import selex.milnet.profiles.Profile.ProfileColumns;

public class ProfileList extends ListActivity{
	private static final String TAG =  ProfileList.class.getName();
	
	/* The columns we are interested in from the database  */
    private static final String[] PROJECTION = new String[] {
        ProfileColumns._ID, // 0
        ProfileColumns.PROFILE_COLUMN_NAME, // 1
        ProfileColumns.DOMAIN_COLUMN_NAME, // 2
        ProfileColumns.USER_COLUMN_NAME, // 3
        ProfileColumns.PASSWORD_COLUMN_NAME, // 4
    };
    
    private static final int COLUMN_INDEX_TITLE = 1; 

	/* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.profile_list);
    	this.getListView().setDividerHeight(2);
    	
    	// If no data was given in the intent (because we were started
        // as a MAIN activity), then use our default content provider.
        Intent intent = getIntent();
        if (intent.getData() == null) {
            intent.setData(ProfileColumns.CONTENT_URI);
        }
        
        // Inform the list we provide context menus for items
        getListView().setOnCreateContextMenuListener(this);
        
        // Perform a managed query. The Activity will handle closing and requerying the cursor
        // when needed.
        Cursor cursor = managedQuery(getIntent().getData(), PROJECTION, null, null,
                                        ProfileColumns.DEF_SORT_ORDER_NAME);

        // Used to map profiles entries from the database to views
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.profile_list, cursor,
                new String[] { ProfileColumns.PROFILE_COLUMN_NAME }, new int[] { android.R.id.empty });
        setListAdapter(adapter);
        
        ComponentName name = getCallingActivity();
        if(name != null)
        {
        	Log.w(TAG,"Called by : " + name.toString());
        }
        else
        {
        	Log.w(TAG,"Called by : NOTHING");
        }

    }
    
    /* Create the menu based on the XML defintion */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.profile_options_menu, menu);
		
		// Generate any additional actions that can be performed on the
        // overall list.  In a normal install, there are no additional
        // actions found here, but this allows other applications to extend
        // our menu with their own actions.
        Intent intent = new Intent(null, getIntent().getData());
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        menu.addIntentOptions(Menu.CATEGORY_ALTERNATIVE, 0, 0,
                new ComponentName(this, ProfileList.class), null, intent, 0, null);

        return super.onCreateOptionsMenu(menu);
	}

	/* Reaction to the menu selection */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			/* A request to add has been received */
			Intent i = new Intent(Intent.ACTION_INSERT,getIntent().getData());
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info;
        try {
             info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
            return;
        }

        Cursor cursor = (Cursor) getListAdapter().getItem(info.position);
        if (cursor == null) {
            // For some reason the requested item isn't available, do nothing
            return;
        }

        // Inflate menu from XML resource
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_context_menu, menu);
        
        // Set the context menu header
        menu.setHeaderTitle(cursor.getString(COLUMN_INDEX_TITLE));
        
        // Append to the
        // menu items for any other activities that can do stuff with it
        // as well.  This does a query on the system for any activities that
        // implement the ALTERNATIVE_ACTION for our data, adding a menu item
        // for each one that is found.
        Intent intent = new Intent(null, Uri.withAppendedPath(getIntent().getData(), 
                                        Integer.toString((int) info.id) ));
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        menu.addIntentOptions(Menu.CATEGORY_ALTERNATIVE, 0, 0,
                new ComponentName(this, ProfileList.class), null, intent, 0, null);
    }
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = null;
		try {
			info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		} catch (ClassCastException e) {
			Log.e(TAG, "bad menuInfo",e);
		}
		
		Uri profileUri = ContentUris.withAppendedId(getIntent().getData(), info.id);
		
		switch (item.getItemId()) {
		case R.id.profile_context_open:
			// Launch activity to view/edit the currently selected item
			startActivity(new Intent(Intent.ACTION_EDIT,profileUri));
			return true;
		case R.id.profile_context_delete:
			// Delete the profile that the context menu is for
			getContentResolver().delete(profileUri, null, null);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	// Opens the second activity if an entry is clicked
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Uri profileUri = ContentUris.withAppendedId(getIntent().getData(), id);
		//super.onListItemClick(l, v, position, id);
		String action = getIntent().getAction();
		if(Intent.ACTION_PICK.equals(action) ||
				Intent.ACTION_GET_CONTENT.equals(action)) {
			// Caller is waiting for us to return a profile selected by the 
			// user. One has been clicked so return it.
			setResult(RESULT_OK,new Intent().setData(profileUri));
			Log.w(TAG,"RESULT_OK" + profileUri.toString());
			finish();
		} else {
			// Otherwise launch the activity to view/edit
			startActivity(new Intent(Intent.ACTION_EDIT,profileUri));
			Log.w(TAG,"Launch Editor" + profileUri.toString());
		}
	}
}
