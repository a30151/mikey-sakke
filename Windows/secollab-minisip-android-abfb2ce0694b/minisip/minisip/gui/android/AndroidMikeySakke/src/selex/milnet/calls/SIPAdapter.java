package selex.milnet.calls;

import selex.milnet.mk.R;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class SIPAdapter extends SimpleCursorAdapter{
	private static final String TAG =  SIPAdapter.class.getName();
	
	private Context mContext;
	private int mLayout;
	
	public SIPAdapter(Context context, int layout, Cursor c, String[] from,	int[] to) {
		super(context, layout, c, from, to);
		this.mContext = context;
		this.mLayout = layout;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent)
	{
		Cursor c = getCursor();
		final LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(mLayout,parent,false);
		
		UpdateViews(v,c);

		return v;
	}
	
	@Override
    public void bindView(View v, Context context, Cursor c) {

		UpdateViews(v,c);
    }
	
	private void UpdateViews(View view, Cursor cursor)
	{
		// Get the data
		int nameIndex = cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME);
		int numberIndex = cursor.getColumnIndex(ContactsContract.Data.DATA1);
		int photoIndex = cursor.getColumnIndex(ContactsContract.Data.PHOTO_ID);
		
		// Update the views
		TextView name_text = (TextView) view.findViewById(R.id.sip_contact_name);
        if (name_text != null) {
            name_text.setText(cursor.getString(nameIndex));
        }
        TextView number_text = (TextView) view.findViewById(R.id.sip_contact_number);
        if (number_text != null) {
        	number_text.setText(cursor.getString(numberIndex));
        }
        ImageView photo_text = (ImageView) view.findViewById(R.id.sip_contact_photo_id);
        String photoId = cursor.getString(photoIndex);
        if (photo_text != null) {
        	if((photoId == null) || (photoId.isEmpty()))
        	{
        		// No Photo
        		photo_text.setImageResource(R.drawable.ic_contact_picture);
        	}
        	else
        	{
        		// We have a photo
        		photo_text.setImageResource(R.drawable.ic_contact_picture_2);
        	}
        }
	}

}
