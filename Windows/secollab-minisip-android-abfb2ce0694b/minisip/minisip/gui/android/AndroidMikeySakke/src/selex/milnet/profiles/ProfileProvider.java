package selex.milnet.profiles;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import selex.milnet.profiles.Profile.ProfileColumns;

public class ProfileProvider extends ContentProvider{
private static final String TAG = ProfileProvider.class.getName();
	
	// Authority for sharing
	//public static final String AUTHORITY = "selex.milnet.mk.provider.Profiles";
	
	// Database
	private static final String DATABASE_NAME = "securiCall.db";
    private static final int DATABASE_VERSION = 2;
    //public static final String REG_TABLE_NAME = "Registration";
    
    private static final int REGISTRATIONS = 1;
    private static final int REG_ID = 2;
    //private static final int LIVE_FOLDER_REGS = 3;
    
    //private static final String LIVE_FOLDER_NAME = "live_folders";
    
    //private static final String DEF_SORT_ORDER_NAME = ProfileTable.PROFILE_COLUMN_NAME;
	
    private static final UriMatcher sUriMatcher;
    
    /* The content:// style URL for this table */
    //public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + ProfileTable.REG_TABLE_NAME);
    /* The MIME type of {@link #CONTENT_URI} providing a directory of registration profiles. */
    //public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + ProfileTable.REG_TABLE_NAME;
    /* The MIME type of a {@link #CONTENT_URI} sub-directory of a registration profile. */
    //public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + ProfileTable.REG_TABLE_NAME;
    
    // Projections for the database
    private static HashMap<String, String> sProfilesProjectionMap;
    //private static HashMap<String, String> sLiveFolderProjectionMap;

    
	/**
     * This class helps open, create, and upgrade the database file.
     */
    private static class RegistrationDBHelper extends SQLiteOpenHelper {

    	RegistrationDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        	ProfileColumns.onCreate(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        	ProfileColumns.onUpgrade(db, oldVersion, newVersion);
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            //db.execSQL("DROP TABLE IF EXISTS " + REG_TABLE_NAME);
            //onCreate(db);
        }
    }
    
    // Declare our helper
    private RegistrationDBHelper mOpenHelper;
    
	@Override
    public boolean onCreate() {
        mOpenHelper = new RegistrationDBHelper(getContext());
        return true;
    }
	
	@Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(ProfileColumns.REG_TABLE_NAME);
        
        // Check columns exist
        checkColumns(projection);


        switch (sUriMatcher.match(uri)) {
        case REGISTRATIONS:
            qb.setProjectionMap(sProfilesProjectionMap);
            break;

        case REG_ID:
            qb.setProjectionMap(sProfilesProjectionMap);
            qb.appendWhere(ProfileColumns._ID + "=" + uri.getPathSegments().get(1));
            break;

        //case LIVE_FOLDER_NOTES:
        //    qb.setProjectionMap(sLiveFolderProjectionMap);
        //    break;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        
        // If no sort order is specified use the default
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = ProfileColumns.DEF_SORT_ORDER_NAME;
        } else {
            orderBy = sortOrder;
        }

        // Get the database and run the query
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);

        // Tell the cursor what uri to watch, so it knows when its source data changes
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }
	
	@Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
        case REGISTRATIONS:
        //case LIVE_FOLDER_NOTES:
            return ProfileColumns.CONTENT_TYPE;

        case REG_ID:
            return ProfileColumns.CONTENT_ITEM_TYPE;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }
	
	@Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        // Validate the requested uri
        if (sUriMatcher.match(uri) != REGISTRATIONS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

        // Make sure that the fields are all set
        if (values.containsKey(ProfileColumns.PROFILE_COLUMN_NAME) == false) {
            values.put(ProfileColumns.PROFILE_COLUMN_NAME, ""/*ProfileColumns.PROFILE_COLUMN_NAME*/);
        }

        if (values.containsKey(ProfileColumns.DOMAIN_COLUMN_NAME) == false) {
            values.put(ProfileColumns.DOMAIN_COLUMN_NAME, ""/*ProfileColumns.PROFILE_COLUMN_NAME*/);
        }

        if (values.containsKey(ProfileColumns.USER_COLUMN_NAME) == false) {
            values.put(ProfileColumns.USER_COLUMN_NAME, ""/*ProfileColumns.PROFILE_COLUMN_NAME*/);
        }

        if (values.containsKey(ProfileColumns.PASSWORD_COLUMN_NAME) == false) {
            values.put(ProfileColumns.PASSWORD_COLUMN_NAME, ""/*ProfileColumns.PROFILE_COLUMN_NAME*/);
        }

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = db.insert(ProfileColumns.REG_TABLE_NAME, ProfileColumns.PROFILE_COLUMN_NAME, values);
        if (rowId > 0) {
            Uri noteUri = ContentUris.withAppendedId(ProfileColumns.CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }

        throw new SQLException("Failed to insert row into " + uri);
    }
	
	@Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
        case REGISTRATIONS:
            count = db.delete(ProfileColumns.REG_TABLE_NAME, where, whereArgs);
            break;

        case REG_ID:
            String noteId = uri.getPathSegments().get(1);
            count = db.delete(ProfileColumns.REG_TABLE_NAME, ProfileColumns._ID + "=" + noteId
                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
            break;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
	
	@Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
        case REGISTRATIONS:
            count = db.update(ProfileColumns.REG_TABLE_NAME, values, where, whereArgs);
            break;

        case REG_ID:
            String noteId = uri.getPathSegments().get(1);
            count = db.update(ProfileColumns.REG_TABLE_NAME, values, ProfileColumns._ID + "=" + noteId
                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
            break;

        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
	
	private void checkColumns(String[] projection) {
		String[] available = { ProfileColumns._ID,
				ProfileColumns.DOMAIN_COLUMN_NAME, ProfileColumns.PROFILE_COLUMN_NAME,
				ProfileColumns.USER_COLUMN_NAME, ProfileColumns.PASSWORD_COLUMN_NAME };
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(available));
			// Check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException(
						"Unknown columns in projection");
			}
		}
	}

	static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(Profile.AUTHORITY, ProfileColumns.REG_TABLE_NAME, REGISTRATIONS);
        sUriMatcher.addURI(Profile.AUTHORITY, ProfileColumns.REG_TABLE_NAME + "/#", REG_ID);
        //sUriMatcher.addURI(AUTHORITY, LIVE_FOLDER_NAME + "/" + REG_TABLE_NAME, LIVE_FOLDER_REGS);

        sProfilesProjectionMap = new HashMap<String, String>();
        sProfilesProjectionMap.put(ProfileColumns._ID, ProfileColumns._ID);
        sProfilesProjectionMap.put(ProfileColumns.PROFILE_COLUMN_NAME, ProfileColumns.PROFILE_COLUMN_NAME);
        sProfilesProjectionMap.put(ProfileColumns.DOMAIN_COLUMN_NAME, ProfileColumns.DOMAIN_COLUMN_NAME);
        sProfilesProjectionMap.put(ProfileColumns.USER_COLUMN_NAME, ProfileColumns.USER_COLUMN_NAME);
        sProfilesProjectionMap.put(ProfileColumns.PASSWORD_COLUMN_NAME, ProfileColumns.PASSWORD_COLUMN_NAME);

        // Support for Live Folders. Deprecated use AppWidget Collection
        //sLiveFolderProjectionMap = new HashMap<String, String>();
        //sLiveFolderProjectionMap.put(LiveFolders._ID, REG_ID_NAME + " AS " +
        //        LiveFolders._ID);
        //sLiveFolderProjectionMap.put(LiveFolders.NAME, PROFILE_COLUMN_NAME + " AS " +
        //        LiveFolders.NAME);
        // Add more columns here for more robust Live Folders.
    }
}
