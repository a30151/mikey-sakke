/*
 *  Copyright (C) 2006  Mikael Magnusson
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Authors: Mikael Magnusson <mikma@users.sourceforge.net>
 */

#include<config.h>

#include<fstream>

#include"MinisipWin32UI.h"
#include<libminisip/Minisip.h>
#include<libminisip/gui/ConsoleDebugger.h>

using namespace std;

int main( int argc, char *argv[] )
{
#if defined(DEBUG_OUTPUT) || !defined(WIN32)
	setThreadName("main");
#endif
	merr.setPrintStreamName(true);
	mout.setPrintStreamName(true);
	mdbg.setPrintStreamName(true);

	cerr << endl << "Starting MiniSIP in32UI ... welcome!" << endl << endl;
	setupDefaultSignalHandling(); //Signal handlers are created for all 
				      //threads created with libmutil/Thread.h
				      //For the main thread we have to
				      //install them
	
#ifndef DEBUG_OUTPUT
	redirectOutput( UserConfig::getFileName( "minisip.log" ).c_str() );
#endif

	cerr << "Creating TextUI"<< endl;
	MRef<MinisipWin32UI *> gui = new MinisipWin32UI();
	cerr << "Minisip: 1" << endl;
 	LogEntry::handler = (MinisipWin32UI *)*gui;
	cerr << "Minisip: 2" << endl;

	Minisip minisip( *gui, argc, argv );
	if( minisip.startSip() > 0 ) {
#ifdef DEBUG_OUTPUT
		minisip.startDebugger();
#else
		//in non-debug mode, send merr to the gui
		merr.setExternalHandler( dynamic_cast<DbgHandler *>( *gui ) );
		mout.setExternalHandler( dynamic_cast<DbgHandler *>( *gui ) );
		mdbg.setExternalHandler( dynamic_cast<DbgHandler *>( *gui ) );
#endif	// DEBUG_OUTPUT

		minisip.runGui();
#ifndef DEBUG_OUTPUT
		merr.setExternalHandler( NULL );
		mout.setExternalHandler( NULL );
		mdbg.setExternalHandler( NULL );
#endif
	} else {
		cerr << endl << "ERROR while starting SIP!" << endl << endl;
	}
	minisip.exit();
	return 0;
}

