
// vs12_proj_guiDlg.cpp : implementation file
//

#include "stdafx.h"
#include "vs12_proj_gui.h"
#include "vs12_proj_guiDlg.h"
#include "afxdialogex.h"
#include <iostream>
#include <fstream>
#include <sstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAX_EDIT_SIZE 0xFFFF


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cvs12_proj_guiDlg dialog



Cvs12_proj_guiDlg::Cvs12_proj_guiDlg(WindowsInterfaceFromGui *wifg, CWnd* pParent /*=NULL*/)
	: wifg(wifg), errQ(), lineCount(0), CDialogEx(Cvs12_proj_guiDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	createdLogFile = false;
}

void Cvs12_proj_guiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Cvs12_proj_guiDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, &Cvs12_proj_guiDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &Cvs12_proj_guiDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &Cvs12_proj_guiDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &Cvs12_proj_guiDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_CHECK1,  &Cvs12_proj_guiDlg::OnCheckboxClicked)
END_MESSAGE_MAP()


// Cvs12_proj_guiDlg message handlers

BOOL Cvs12_proj_guiDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON2)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON4)->EnableWindow(TRUE);
	//set multi-line edit to maximum size
	GetDlgItem(IDC_EDIT2)->SendMessage(EM_LIMITTEXT, MAX_EDIT_SIZE, 0);

	timer = SetTimer(1,250, NULL); // start timer for writing out text

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Cvs12_proj_guiDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Cvs12_proj_guiDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Cvs12_proj_guiDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Cvs12_proj_guiDlg::OnBnClickedButton1()
{
    CString str;
	GetDlgItem(IDC_EDIT1)->GetWindowText(str);
	CT2CA pszAnsi(str);
	std::string str1(pszAnsi);
	wifg->call(str1);
}

void Cvs12_proj_guiDlg::OnBnClickedButton2()
{
	wifg->end();
}

void Cvs12_proj_guiDlg::OnBnClickedButton3()
{
	wifg->answer();
}

void Cvs12_proj_guiDlg::OnBnClickedButton4()
{
	wifg->openConfig();
}

void Cvs12_proj_guiDlg::OnCheckboxClicked()
{
	CButton* pButton1 = (CButton*) GetDlgItem(IDC_CHECK1);
	int checked = pButton1->GetCheck();
	if (checked == 0)
	{
		wifg->setAutoanswer(false);
	}
	else if (checked == 1)
	{
		wifg->setAutoanswer(true);
	}
}



void Cvs12_proj_guiDlg::notifyIncomingCall()
{
	GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON2)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON3)->EnableWindow(TRUE);
}

void Cvs12_proj_guiDlg::notifyCallInProgress()
{
	GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON2)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);
}

void Cvs12_proj_guiDlg::notifyOutgoingCall()
{
	GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON2)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);
}

void Cvs12_proj_guiDlg::notifyCallEnded()
{
	GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON2)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);
}

void Cvs12_proj_guiDlg::OnClose()
{
	CDialogEx::OnClose();
}

void Cvs12_proj_guiDlg::OnCancel()
{
	KillTimer(timer);
	wifg->stop();
	DestroyWindow();
}

void Cvs12_proj_guiDlg::OnOK()
{
	KillTimer(timer);
	wifg->stop();
	DestroyWindow();
}

void Cvs12_proj_guiDlg::OnTimer(UINT nIDEvent)
{
	if(!createdLogFile) {
		// Create empty logFile
		std::string filePath;
		wifg->getConfigFilepath(filePath);
		std::string conf = "minisip.conf";

		unsigned found = filePath.rfind(conf);
		if (found != std::string::npos)
			filePath.replace(found, conf.length(), "log.txt");
		else
			filePath = "log.txt";

		logFilePath = filePath;
		std::ofstream logFile;
		logFile.open(logFilePath, std::ios::trunc);
		logFile.close();

		createdLogFile = true;
	}
	displayErrors();
}

void appendStringToControl(HWND hwnd, LPSTR str)
{
	int ndx = GetWindowTextLength(hwnd);
	CStringW wstr(str);
	SendMessage(hwnd, EM_SETSEL, (WPARAM)ndx, (LPARAM) ndx);
	SendMessage(hwnd, EM_REPLACESEL, 0, (LPARAM) wstr.GetString());
}

std::wstring Cvs12_proj_guiDlg::s2ws(std::string str)
{
	std::wstring w_str;
	w_str.assign(str.begin(), str.end());
	return w_str;
}

void Cvs12_proj_guiDlg::setStatus(std::string str)
{
	GetDlgItem(IDC_STATIC2)->SetWindowTextW(s2ws(str).c_str());
	//GetDlgItem(IDC_STATIC2)->SendMessage(WM_SETTEXT, 0, (LPARAM)str)
}

void Cvs12_proj_guiDlg::displayError(std::string str)
{
	errQ.push(str);
}
void Cvs12_proj_guiDlg::displayErrors()
{
	// move to end of edit control and append string, removing oldest lines
	// Info on limits from http://support.microsoft.com/kb/74225
#define MAX_LINE_LENGTH 1024
#define MAX_NUM_LINES 16000
#define CONTROL_OVERHEAD 200

	//HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	//HANDLE hFile = CreateFile(L"LOG.TXT", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	//WriteFile(hFile, "Created File", sizeof "Created File", NULL, NULL);
	//CloseHandle(hFile);

	std::ofstream logFile;
	logFile.open(logFilePath, std::ios::app);

	std::string str;
	while(errQ.pop(&str,0))
	{
		logFile << str;

		int ndx = GetDlgItem(IDC_EDIT2)->GetWindowTextLength();
		CString oldStr;
		GetDlgItem(IDC_EDIT2)->GetWindowText(oldStr);

		//calculate characters and lines to be added
		str.erase(str.find_last_not_of(" \n\r\t")+1);
		int charsToAdd = ((str.length() % MAX_LINE_LENGTH) + 2) + ((str.length() / MAX_LINE_LENGTH)*(MAX_LINE_LENGTH + 2));
		int linesToAdd = (str.length() / MAX_LINE_LENGTH) + 1;
		std::string empty = "";
		while ((charsToAdd + ndx) > (MAX_EDIT_SIZE - CONTROL_OVERHEAD) || (lineCount + linesToAdd) > MAX_NUM_LINES)
		{
			// remove lines from start of edit box
			int i = oldStr.Find(CString("\r\n"),0);
			GetDlgItem(IDC_EDIT2)->SendMessage(EM_SETSEL, (WPARAM)0, (LPARAM) (i + 2));
			GetDlgItem(IDC_EDIT2)->SendMessage(EM_REPLACESEL, 0, (LPARAM) ((LPSTR)empty.c_str()));
			ndx -= (i + 2);
			lineCount--;
		}

		// chop into lines that will fit and append
		unsigned int count = 0;
		while (count < str.length())
		{
			std::string sstr = str.substr(count, MAX_LINE_LENGTH-2);
			sstr += "\r\n";
			appendStringToControl(GetDlgItem(IDC_EDIT2)->GetSafeHwnd(), ((LPSTR)sstr.c_str()));
			count += sstr.length();
			lineCount++;
		}
	}

	logFile.close();
}

