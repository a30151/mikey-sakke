#ifndef WINDOWS_INTERFACE_H
#define WINDOWS_INTERFACE_H

#include <string>

class WindowsInterfaceToGui{
public:
	virtual void notifyIncomingCall() = 0;
	virtual void notifyCallInProgress() = 0;
	virtual void notifyOutgoingCall() = 0;
	virtual void notifyCallEnded() = 0;
	virtual void displayError(std::string str) = 0;
	virtual void setStatus(std::string str) = 0;
};

class WindowsInterfaceFromGui{
public:
	virtual void answer() = 0;
	virtual void setAutoanswer(bool option) = 0;
	virtual void end() = 0;
	virtual void call(std::string str) = 0;
	virtual void start() = 0;
	virtual void stop() = 0;
	virtual void openConfig() = 0;
	virtual void getConfigFilepath(std::string &str) = 0;
};

#endif