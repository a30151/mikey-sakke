
// vs12_proj_gui.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// Cvs12_proj_guiApp:
// See vs12_proj_gui.cpp for the implementation of this class
//

class Cvs12_proj_guiApp : public CWinApp
{
public:
	Cvs12_proj_guiApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Cvs12_proj_guiApp theApp;