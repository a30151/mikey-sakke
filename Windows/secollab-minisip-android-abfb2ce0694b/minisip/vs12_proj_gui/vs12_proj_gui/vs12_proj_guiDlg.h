
// vs12_proj_guiDlg.h : header file
//

#pragma once

#include "WindowsInterface.h"
#include <deque>

template <typename T> class PCSqueue{
    CRITICAL_SECTION access;
    std::deque<T> *objectQueue;
    HANDLE queueSema;
public:
    PCSqueue(){
        objectQueue=new std::deque<T>;
        InitializeCriticalSection(&access);
        queueSema=CreateSemaphore(NULL,0,MAXINT,NULL);
    };
    void push(T ref){
        EnterCriticalSection(&access);
        objectQueue->push_front(ref);
        LeaveCriticalSection(&access);
        ReleaseSemaphore(queueSema,1,NULL);
    };
    bool pop(T *ref,DWORD timeout){
        if (WAIT_OBJECT_0==WaitForSingleObject(queueSema,timeout)) {
            EnterCriticalSection(&access);
            *ref=objectQueue->back();
            objectQueue->pop_back();
            LeaveCriticalSection(&access);
            return(true);
        }
        else
            return(false);
    };
};

// Cvs12_proj_guiDlg dialog
class Cvs12_proj_guiDlg : public CDialogEx, public WindowsInterfaceToGui
{
// Construction
public:
	Cvs12_proj_guiDlg(WindowsInterfaceFromGui *wifg, CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_VS12_PROJ_GUI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnCheckboxClicked();
	void OnClose();
	void OnCancel();
	void OnOK();
	void OnTimer(UINT nIDEvent);
	void notifyIncomingCall();
	void notifyCallInProgress();
	void notifyOutgoingCall();
	void notifyCallEnded();
	void displayError(std::string str);//thread safe as text not written to UI immediately
	void setStatus(std::string str);
private:
	WindowsInterfaceFromGui *wifg;
	void displayErrors();// write text to UI
	PCSqueue<std::string> errQ;//Thread safe queue to hold strings
	UINT_PTR timer;
	int lineCount;
	std::string logFilePath;
	bool createdLogFile;
	std::wstring s2ws(std::string str);
};
