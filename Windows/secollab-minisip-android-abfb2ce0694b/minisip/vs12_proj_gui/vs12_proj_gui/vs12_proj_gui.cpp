
// vs12_proj_gui.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "vs12_proj_gui.h"
#include "vs12_proj_guiDlg.h"

#include<config.h>

#include<fstream>

#include"MinisipWin32UI.h"
#include<libminisip/Minisip.h>
#include<libminisip/gui/ConsoleDebugger.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cvs12_proj_guiApp

BEGIN_MESSAGE_MAP(Cvs12_proj_guiApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// Cvs12_proj_guiApp construction

Cvs12_proj_guiApp::Cvs12_proj_guiApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only Cvs12_proj_guiApp object

Cvs12_proj_guiApp theApp;


// Cvs12_proj_guiApp initialization

BOOL Cvs12_proj_guiApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	#ifdef DEBUG_OUTPUT
	setThreadName("main");
#endif
	merr.setPrintStreamName(true);
	mout.setPrintStreamName(true);
	mdbg.setPrintStreamName(true);

	//cerr << endl << "Starting MiniSIP TextUI ... welcome!" << endl << endl;
	setupDefaultSignalHandling(); //Signal handlers are created for all 
				      //threads created with libmutil/Thread.h
				      //For the main thread we have to
				      //install them
	
	MRef<MinisipWin32UI *> gui = new MinisipWin32UI();

	Cvs12_proj_guiDlg dlg(*gui);
	dlg.Create(IDD_VS12_PROJ_GUI_DIALOG);
	dlg.ShowWindow(SW_SHOW);
	m_pMainWnd = &dlg;
	gui->attachWindowsInterface(&dlg);

	merr.setExternalHandler( dynamic_cast<DbgHandler *>( *gui ) );
	mout.setExternalHandler( dynamic_cast<DbgHandler *>( *gui ) );
	mdbg.setExternalHandler( dynamic_cast<DbgHandler *>( *gui ) );

	gui->start();

	// run dialog message loop
	MSG Msg;
    while(gui->isLive())
	{
		if(PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&Msg);
			DispatchMessage(&Msg);
		}
	}

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

