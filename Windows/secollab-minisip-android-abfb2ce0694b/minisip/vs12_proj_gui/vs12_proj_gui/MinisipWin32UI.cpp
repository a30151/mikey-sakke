/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Copyright (C) 2004 
 *
 * Authors: Erik Eliasson <eliasson@it.kth.se>
 *          Johan Bilien <jobi@via.ecp.fr>
*/
#include "stdafx.h"

#include<config.h>

#include<stdio.h>
#include<stdlib.h>
#include"MinisipWin32UI.h"
#include<libminisip/Minisip.h>
#include<libminisip/gui/Bell.h>
#include<libminisip/media/MediaCommandString.h>
#include<libmutil/MemObject.h>
#include<libmsip/SipCommandString.h>

using namespace std;

MinisipWin32UI::MinisipWin32UI(): wInt(NULL), m_isLive(true), autoanswer(false){
	inCall=false;
	state="IDLE";
	semSipReady = new Semaphore();
	filePath = "unknown";
}

void MinisipWin32UI::run(){
	semSipReady->dec();
	semSipReady = NULL;
}

void MinisipWin32UI::displayMessage(string msg, int style=-1){
	wInt->displayError(msg);
}

void MinisipWin32UI::handleCommand(const CommandString &cmd){
#ifdef DEBUG_OUTPUT
	mdbg << FG_MAGENTA << "MinisipWin32UI::handleCommand: Got "<<cmd.getString() << PLAIN <<endl;
#endif
	bool handled = false;

	if (cmd.getOp()==SipCommandString::ask_password){
		string user =(const_cast<CommandString &>(cmd))["identityUri"];
		string realm=(const_cast<CommandString &>(cmd))["realm"];
		displayMessage( "WARNING: Could not authenticate user "+user+ " to realm "+realm+". Wrong password?" );
	}

	if (cmd.getOp()=="register_ok"){
		handled=true;
		displayMessage("Register to proxy "+cmd.getParam()+" OK");
	}

	if (cmd.getOp()=="call_terminated"){
		handled=true;
		wInt->notifyCallEnded();
		stopRinging();
	}

	if (cmd.getOp()=="register_sent"){
		handled=true; // we don't need to inform the user
	}
	
	if (cmd.getOp()=="invite_ok"){
		handled=true;
		state="INCALL";
		inCall = true;
		wInt->notifyCallInProgress();
		wInt->setStatus("In call with "+cmd.getParam());
		displayMessage("PROGRESS: remote participant accepted the call...");
		
		displayMessage("PROGRESS: Unmuting sending of sound.");
		CommandString cmdstr( callId,
				MediaCommandString::set_session_sound_settings,
				"senders", "ON");
		sendCommand("media", cmdstr);
		stopRinging();
	}
	else if (cmd.getOp()=="security_failed"){
		handled=true;
		state="IDLE";
		displayMessage("Security is not handled by the receiver");
		inCall=false;
	}
	
	if (cmd.getOp()=="remote_ringing"){
		handled=true;
		state="REMOTE RINGING";
		wInt->notifyOutgoingCall();
		
		displayMessage("PROGRESS: the remote UA is ringing...");
	}
	
	if (cmd.getOp()==SipCommandString::remote_user_not_found){
		handled=true;
		state="IDLE";
		wInt->setStatus("User not found.");
		displayMessage("User "+cmd.getParam()+" not found.");
		callId=""; //FIXME: should check the callId of cmd.
	}
	
	if (cmd.getOp()==SipCommandString::remote_hang_up){
		handled=true;
		state="IDLE";
		wInt->notifyCallEnded();
		wInt->setStatus("Remote user ended the call.");
		displayMessage("Remote user ended the call.");
		callId=""; //FIXME: should check the callId of cmd.
		inCall=false;
		stopRinging();
	}
	else if (cmd.getOp()==SipCommandString::remote_cancelled_invite){
		handled=true;
		state="IDLE";
		wInt->notifyCallEnded();
		wInt->setStatus("Remote user cancelled the call.");
		displayMessage("Remote user cancelled the call.");
		callId=""; //FIXME: should check the callId of cmd.
		inCall=false;
		stopRinging();
	}
	
	if (cmd.getOp()==SipCommandString::transport_error){
		handled=true;
		state="IDLE";
		displayMessage("The call could not be completed because of a network error.");
		callId=""; //FIXME: should check the callId of cmd.
	}
	
	if (cmd.getOp()=="error_message"){
		handled=true;
		displayMessage("ERROR: "+cmd.getParam());
	}
	
	if (cmd.getOp()=="remote_reject"){
		handled=true;
		state="IDLE";
		wInt->notifyCallEnded();
		callId="";
		wInt->setStatus("Remote user rejected the call.");
		displayMessage("The remote user rejected the call.");
		stopRinging();
	}
	
	if (cmd.getOp()==SipCommandString::incoming_available){
		handled=true;
		if(state=="IDLE"){
			state="ANSWER?";
			wInt->notifyIncomingCall();
			callId=cmd.getDestinationId();
			wInt->setStatus("Incoming call from "+cmd.getParam());
			displayMessage("Incoming call from "+cmd.getParam());
			startRinging();
		}
		else{
			wInt->setStatus("Missed call from "+cmd.getParam());
			displayMessage("You missed a call from "+cmd.getParam());
			CommandString hup(cmd.getDestinationId(), SipCommandString::reject_invite);
			sendCommand("sip",hup);
			stopRinging();
		}
	}

	if (autoanswer && state=="ANSWER?"){
		handled=true;
		CommandString command(callId, SipCommandString::accept_invite);
		sendCommand("sip",command);
		state="INCALL";
		wInt->notifyCallInProgress();
		wInt->setStatus("In call with "+cmd.getParam());
		displayMessage("Autoanswered call from "+ cmd.getParam());
		displayMessage("Unmuting sending of sound.");
		CommandString cmdstr( callId,
				MediaCommandString::set_session_sound_settings,
				"senders", "ON");
		sendCommand("media",cmdstr);
		stopRinging();
	}

	if (!handled){
		displayMessage("WARNING: Gui did not handle command: "+ cmd.getString() );
	}
}

bool MinisipWin32UI::configDialog( MRef<SipSoftPhoneConfiguration *> /*conf*/ ){
	cout << "ERROR: MinisipWin32UI::configDialog is not implemented"<< endl;
	return false;
}

void MinisipWin32UI::showMem(){
	string all;
	minilist<string> names = getMemObjectNames();
	for (int i=0; i<names.size();i++){
		all = all+names[i]+"\n";
	}
	displayMessage(all+itoa(getMemObjectCount())+" objects");
}

void MinisipWin32UI::guiExecute(string cmd){
	string command = trim(cmd);
	string regproxy;
	bool handled = false;

	if (command == "answer"){
		CommandString command(callId, SipCommandString::accept_invite);
		sendCommand("sip", command);
		displayMessage("A call with the most recent callId will be accepted");
		handled=true;
        inCall = true;
		displayMessage("Unmuting sending of sound.");
		CommandString cmdstr( callId,
				MediaCommandString::set_session_sound_settings,
				"senders", "ON");
		sendCommand("media",cmdstr);
	}

	if (trim(command) == "hangup"){
		CommandString hup(callId, SipCommandString::hang_up);
		sendCommand("sip", hup);
		
		state="IDLE";
		displayMessage("hangup");
		handled=true;
		inCall=false;
	}

	if ((command.size()>=4) && (command.substr(0,4) == "call")){
		if (command.size()>=6){
			if (state!="IDLE"){
				displayMessage("UNIMPLEMENTED - only one call at the time with this UI.");
			}else{
				string uri = trim(command.substr(5));
				displayMessage("Uri: "+uri);
				
				//CONTINUEHERE!!
				//callId = callback->guicb_doInvite(uri);
				CommandString invite("",SipCommandString::invite,uri);
				CommandString resp = callback->handleCommandResp("sip",invite);
				callId = resp.getDestinationId();
				
				if (callId=="malformed"){
					state="IDLE";
					displayMessage("The URI is not a valid SIP URI");
					callId="";

				}else{
					state="TRYING";
					displayMessage(string("Created call with id=")+callId);    
				}
			}
		}else{
			displayMessage("Usage: call <userid>");
		}
		handled=true;
	}
	if ((command == "conf")){
		if (state!="IDLE"){
			displayMessage("UNIMPLEMENTED - only one call/conference at the time with this UI.");
		}else{
			currentconfname = itoa(rand());
			string mysipuri = config->defaultIdentity->getSipUri().getUserIpString();
			currentconf=new ConferenceControl(mysipuri,currentconfname, true);
			//conf->setGui(this);
			confCallback->setConferenceController(currentconf);
			//currentconf->setCallback(callback);
			//state="CONF";
			displayMessage("Conf. Name: "+currentconfname);
			confCallback->guicb_handleConfCommand(currentconfname);
			state="CONF";
		}
		handled=true;
	}

	if (!handled && command.size()>0){
		displayMessage("Unknown command: "+command);
	}
}

/* handle interaction of gui with state */
void MinisipWin32UI::answer(){
	CommandString command(callId, SipCommandString::accept_invite);
	sendCommand("sip", command);
	displayMessage("A call with the most recent callId will be accepted");
    inCall = true;
	displayMessage("Unmuting sending of sound.");
	CommandString cmdstr( callId,
			MediaCommandString::set_session_sound_settings,
			"senders", "ON");
	sendCommand("media",cmdstr);
}
void MinisipWin32UI::end(){
	CommandString hup(callId, SipCommandString::hang_up);
	sendCommand("sip", hup);
	state="IDLE";
	wInt->setStatus("Call ended.");
	displayMessage("hangup");
	inCall=false;
	wInt->notifyCallEnded();
	stopRinging();
}
void MinisipWin32UI::call(std::string str){
	if (state!="IDLE"){
		displayMessage("UNIMPLEMENTED - only one call at the time with this UI.");
	}else{
		string uri = trim(str);
		displayMessage("Uri: "+uri);

		CommandString invite("",SipCommandString::invite,uri);
		CommandString resp = callback->handleCommandResp("sip",invite);
		callId = resp.getDestinationId();
				
		if (callId=="malformed"){
			state="IDLE";
			displayMessage("The URI is not a valid SIP URI");
			callId="";
		}else{
			state="TRYING";
			wInt->setStatus("Calling "+uri);
			displayMessage(string("Created call with id=")+callId);    
		}
	}
}

void MinisipWin32UI::startRinging()
{
	if(!bell)
	{
		bell = new Bell();
	}
	bell->start();
	CommandString cmdstr = CommandString( "", MediaCommandString::start_ringing );
	//mainWindow->getCallback()->guicb_handleMediaCommand( cmdstr );
	//mainWindow->getCallback()->handleCommand("media", cmdstr );
	sendCommand("media", cmdstr);
	displayMessage("Start ringing");
}

void MinisipWin32UI::stopRinging()
{
	if (bell)
	{
		bell->stop();
		bell=NULL;
	}
	CommandString cmdstr = CommandString( "", MediaCommandString::stop_ringing );
	//mainWindow->getCallback()->handleCommand("media", cmdstr );
	sendCommand("media", cmdstr);
	displayMessage("Stop ringing");
}

void MinisipWin32UI::setAutoanswer(bool option){
	autoanswer = option;
	if (option)
		displayMessage("Autoanswer enabled");
	else
		displayMessage("Autoanswer disabled");
}

std::wstring s2ws(std::string str){
	std::wstring w_str;
	w_str.assign(str.begin(), str.end());
	return w_str;
}

void MinisipWin32UI::openConfig() {
	ShellExecuteW(NULL, NULL, L"notepad.exe", s2ws(filePath).c_str(), NULL, SW_SHOWNORMAL);
}

void MinisipWin32UI::getConfigFilepath(std::string &str) {
	str = filePath;
}

void MinisipWin32UI::setSipSoftPhoneConfiguration(MRef<SipSoftPhoneConfiguration *>sipphoneconfig){
	config = sipphoneconfig;       
}

void MinisipWin32UI::setCallback(MRef<CommandReceiver*> callback){
	Gui::setCallback(callback);
	MRef<Semaphore *> localSem = semSipReady;
	if( localSem ){
		localSem->inc();
	}
}

void MinisipWin32UI::attachWindowsInterface(WindowsInterfaceToGui *wi)
{
	wInt = wi;
}

void MinisipWin32UI::handle( MRef<LogEntry *> logEntry) {
	string logStr;
	struct tm * startTm = new struct tm;
	localtime_s( startTm, &logEntry->start );
	logStr = itoa( startTm->tm_hour ) + ":" + 
				((startTm->tm_min < 10) ? "0" : "") + 
		                 itoa( startTm->tm_min );

	ContactEntry * contactEntry;
	if( contactDb && 
		( contactEntry = contactDb->lookUp( logEntry->peerSipUri ) )){
		logStr += contactEntry->getName();
	}
	else{
		logStr += logEntry->peerSipUri;
	}
	MRef<LogEntryFailure *> failureEntry;
	failureEntry = dynamic_cast<LogEntryFailure *>( *logEntry );
	if( *failureEntry != NULL ){
		logStr += failureEntry->error;
	}
	wInt->displayError(logStr);
}

void MinisipWin32UI::setContactDb( MRef<ContactDb *> contactDb ){
	this->contactDb = contactDb;
}

void MinisipWin32UI::start()
{
	minisip = new Minisip( this, 0, NULL );

	if( minisip->startSip() > 0 ) {

		minisip->runGui();
	} else {
		wInt->displayError("ERROR while starting SIP!");
	}

	// Get config file
	filePath = minisip->getConfigFilename();

}

void MinisipWin32UI::stop()
{
	minisip->exit();
	m_isLive = false;
}

bool MinisipWin32UI::isLive()
{
	return m_isLive;
}