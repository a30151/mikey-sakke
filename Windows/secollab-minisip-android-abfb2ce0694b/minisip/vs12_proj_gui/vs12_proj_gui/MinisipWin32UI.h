/* based on MinisipTextUI.h */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Copyright (C) 2004 
 *
 * Authors: Erik Eliasson <eliasson@it.kth.se>
 *          Johan Bilien <jobi@via.ecp.fr>
*/

#ifndef _MINISIPWIN32UI_H
#define _MINISIPWIN32UI_H

#include<libminisip/Minisip.h>
#include<libminisip/gui/ConsoleDebugger.h>
#include<libmutil/CommandString.h>
#include<libminisip/signaling/sip/SipSoftPhoneConfiguration.h>
#include<libminisip/gui/Gui.h>
#include<libminisip/gui/Bell.h>
#include<libmutil/minilist.h>
#include<libmutil/Semaphore.h>

#include "WindowsInterface.h"

#include<string>

/**
 * A basic Windows user interface. 
 * The interface interacts with the user via the WindowsInterface, and with the <code>Sip</code> class
 * with <code>SipSMCommands</code> via the <code>MessageRouter</code>.
 */
class MinisipWin32UI: public Gui, public LogEntryHandler, public DbgHandler, public WindowsInterfaceFromGui{
	public:
		MinisipWin32UI();
	
		std::string getMemObjectType() const {return "MinisipWin32UI";}
		void handle( MRef<LogEntry *> logEntry);
		
		virtual void handleCommand(const CommandString&);
		virtual void setSipSoftPhoneConfiguration(MRef<SipSoftPhoneConfiguration *>sipphoneconfig);
		virtual void setContactDb(MRef<ContactDb *>);
		virtual bool configDialog( MRef<SipSoftPhoneConfiguration *> conf );
	
		virtual void displayMessage(std::string msg, int style);
	
		virtual void run();
	
		virtual void setCallback(MRef<CommandReceiver*> callback);
	
		virtual void guiExecute(std::string cmd);
		virtual void attachWindowsInterface(WindowsInterfaceToGui *wi);

		virtual void answer();
		virtual void setAutoanswer(bool option);
		virtual void end();
		virtual void call(std::string str);
		virtual void start();
		virtual void stop();
		virtual void openConfig();
		virtual void getConfigFilepath(std::string &str);

		bool isLive();

	protected:
		
		MRef<Bell *> bell;

		void startRinging();
		void stopRinging();
	
	private:
		
		void showMem();

		ConferenceControl *currentconf;
		std::string currentconfname;
		std::string currentcaller;
		std::string input;
		std::string callId;
		std::string state;
		std::string filePath;
		MRef<SipSoftPhoneConfiguration *> config;
		bool autoanswer;
		MRef<Semaphore *> semSipReady;
		WindowsInterfaceToGui *wInt;
		MRef<ContactDb *> contactDb;
		Minisip *minisip;
		
		
		///indicates that the user is in a call and cannot answer any other incoming calls
		bool inCall;

		bool m_isLive;

};

#endif