package dja.bob;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class BobBlue extends Activity {
	/** Called when the activity is first created. */
	private MyThread thread;
	private boolean go = true;

	private void debug(String in){
		Toast.makeText(this, in, Toast.LENGTH_LONG).show();
		finish();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		//check for support
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		if(adapter==null) {debug("Bluetooth is not available");return;}

		//check enabled
		if(!adapter.isEnabled()){
			Intent btenable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(btenable,3);
		}

		//get serversocket
		final UUID myuuid = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
		BluetoothServerSocket srvsock = null;

		try {srvsock = adapter.listenUsingRfcommWithServiceRecord("dja", myuuid);}
		catch (IOException e) {debug("Server Socket setup");return;}

		BluetoothSocket sock =null;
		while(true){
			try{
				sock = srvsock.accept();
			}
			catch(IOException ioe){debug("Socket accept\n"+ioe.getMessage());break;}

			if(sock!=null){
				try {
					byte[] bytes = new byte[402];
					sock.getInputStream().read(bytes);
					publishProgress(bytes);

				} catch (IOException e) {debug(e.getMessage());break;}

				try {srvsock.close();
				} catch (IOException e) {}
				break;
			}
		}
	}
	
	protected void publishProgress(byte[] progress) {
		long stime = System.nanoTime();
		long time = stime;
		long time1 = time;

		byte[] payload = new byte[273];
		for(int i=0;i<273;i++) payload[i]=progress[i];

		byte[] sig = new byte[129];
		for(int i=273;i<402;i++) sig[i-273]=progress[i];

		// ECCSI verification input variables
		byte[] ID = {(byte)0x32,(byte)0x30,(byte)0x31,(byte)0x31,(byte)0x2d,(byte)0x30,(byte)0x32,(byte)0x00,(byte)0x74,(byte)0x65,(byte)0x6c,(byte)0x3a,(byte)0x2b,(byte)0x34,(byte)0x34,(byte)0x37,(byte)0x37,(byte)0x30,(byte)0x30,(byte)0x39,(byte)0x30,(byte)0x30,(byte)0x31,(byte)0x32,(byte)0x33,(byte)0x00};
		byte[] KPAK = {(byte)0x04,(byte)0x50,(byte)0xd4,(byte)0x67,(byte)0x0b,(byte)0xde,(byte)0x75,(byte)0x24,(byte)0x4f,(byte)0x28,(byte)0xd2,(byte)0x83,(byte)0x8a,(byte)0x0d,(byte)0x25,(byte)0x55,(byte)0x8a,(byte)0x7a,(byte)0x72,(byte)0x68,(byte)0x6d,(byte)0x45,(byte)0x22,(byte)0xd4,(byte)0xc8,(byte)0x27,(byte)0x3f,(byte)0xb6,(byte)0x44,(byte)0x2a,(byte)0xeb,(byte)0xfa,(byte)0x93,(byte)0xdb,(byte)0xdd,(byte)0x37,(byte)0x55,(byte)0x1a,(byte)0xfd,(byte)0x26,(byte)0x3b,(byte)0x5d,(byte)0xfd,(byte)0x61,(byte)0x7f,(byte)0x39,(byte)0x60,(byte)0xc6,(byte)0x5a,(byte)0x8c,(byte)0x29,(byte)0x88,(byte)0x50,(byte)0xff,(byte)0x99,(byte)0xf2,(byte)0x03,(byte)0x66,(byte)0xdc,(byte)0xe7,(byte)0xd4,(byte)0x36,(byte)0x72,(byte)0x17,(byte)0xf4};
		// SAKKE processing input variables
		String b = "323031312d30320074656c3a2b34343737303039303031323300";
		String Z = "045958EF1B1679BF099B3A030DF255AA6A23C1D8F143D4D23F753E69BD27A832F38CB4AD53DDEF4260B0FE8BB45C4C1FF510EFFE300367A37B61F701D914AEF09724825FA0707D61A6DFF4FBD7273566CDDE352A0B04B7C16A78309BE640697DE747613A5FC195E8B9F328852A579DB8F99B1D0034479EA9C5595F47C4B2F54FF21508D37514DCF7A8E143A6058C09A6BF2C9858CA37C258065AE6BF7532BC8B5B63383866E0753C5AC0E72709F8445F2E6178E065857E0EDA10F68206B63505ED87E534FB2831FF957FB7DC619DAE61301EEACC2FDA3680EA4999258A833CEA8FC67C6D19487FB449059F26CC8AAB655AB58B7CC796E24E9A394095754F5F8BAE";
		String K = "0493AF67E5007BA6E6A80DA793DA300FA4B52D0A74E25E6E7B2B3D6EE9D18A9B5C5023597BD82D8062D34019563BA1D25C0DC56B7B979D74AA50F29FBF11CC2C93F5DFCA615E609279F6175CEADB00B58C6BEE1E7A2A47C4F0C456F05259A6FA94A634A40DAE1DF593D4FECF688D5FC678BE7EFC6DF3D6835325B83B2C6E69036B155F0A27241094B04BFB0BDFAC6C670A65C325D39A069F03659D44CA27D3BE8DF311172B554160181CBE94A2A783320CED590BC42644702CF371271E496BF20F588B78A1BC01ECBB6559934BDD2FB65D2884318A33D1A42ADF5E33CC5800280B28356497F87135BAB9612A17260424409AC15FEE996B744C332151235DECB0F5";



		time = System.nanoTime();
		boolean verification = eccsiverify(KPAK, ID, sig, payload);
		time1 = System.nanoTime();
		setSomeText("Verified: " + verification);
		setSomeText(" in "+(time1-time)/1000000000.0+"s");


		// process the SAKKE data if the signature is verified
		long ptime = System.nanoTime();
		if(verification) {
			time=System.nanoTime();			

			byte[] R = new byte[257];
			for(int i=0;i<257;i++) R[i]=progress[i];

			byte[] H = new byte[16];
			for(int i=257;i<273;i++) H[i-257]=progress[i];

			String w = pair(R,K);
			setSomeText("\nPairing: "+(System.nanoTime()-time)/1000000000.0+"s");

			int n = 128;		
			String hx = conv(H);
			BigInteger BH = new BigInteger(hx,16);
			BigInteger SSV = BH.xor(HTIR.htir(w, BigInteger.ONE.add(BigInteger.ONE).pow(n)));

			time = System.nanoTime();

			BigInteger q = new BigInteger("265EAEC7C2958FF69971846636B4195E905B0338672D20986FA6B8D62CF8068BBD02AAC9F8BF03C6C8A1CC354C69672C39E46CE7FDF222864D5B49FD2999A9B4389B1921CC9AD335144AB173595A07386DABFD2A0C614AA0A9F3CF14870F026AA7E535ABD5A5C7C7FF38FA08E2615F6C203177C42B1EB3A1D99B601EBFAA17FB",16);
			BigInteger r = HTIR.htir(SSV.toString(16)+b, q);
			byte[] Rbs = muladdmul(r.toString(16),b,Z);
			boolean test = true;
			for(int i=0;i<257;i++) {
				test &=(Rbs[i]==R[i]);
			}

			setSomeText("\nTested: " + test);
			setSomeText(" in "+(System.nanoTime()-time)/1000000000.0+"s");
			time=System.nanoTime();

			setSomeText("\nSSV: "+SSV.toString(16));
		}
		else {
			setSomeText("\nSAKKE data not processed as signature not verified.");
		}
		setSomeText("\nPackage processing: "+((System.nanoTime()-ptime)/1000000000.0+"s"));

		setSomeText("\nTotal: "+((System.nanoTime()-stime)/1000000000.0+"s" + "\n"));
	}

	//	public void onDestroy(){
	//		//		go=false;
	//		//		try {thread.sock.close();
	//		//		} catch (IOException e) {}
	//				super.onDestroy();
	//	}

	public void function(){
		//		int n = 128;
		//		BigInteger tn = new BigInteger("340282366920938463463374607431768211456",10);
		//		String b = "323031302d30370074656c3a2b34343132333435363738393000";
		//		BigInteger q = new BigInteger("265EAEC7C2958FF69971846636B4195E905B0338672D20986FA6B8D62CF8068BBD02AAC9F8BF03C6C8A1CC354C69672C39E46CE7FDF222864D5B49FD2999A9B4389B1921CC9AD335144AB173595A07386DABFD2A0C614AA0A9F3CF14870F026AA7E535ABD5A5C7C7FF38FA08E2615F6C203177C42B1EB3A1D99B601EBFAA17FB",16);
		//		
		//		String ra = "157B9F356C8A3138A9532EC262B0460483EA33A826247411D852136CE543020C52BDF196E5955121FF83A18321E90A5A7EC1D0E1B433FEFBFD082C968674682AA935EFDAE984F5572B677D5131E8C90CCC77519D7C88B20C5C829287B2204A3EE7DBEE5DF797537524D7215B2F3D969886720EF45CB61745CB69DA22C87EE985";
		//		String rb = "5D9C7EC1A67942D3BF0F82F29CC1C1D55E0FDBA6F51B01790DA75F060BE7E9B0DCC06CE7A200E8EBA0F778756DF2C587DE65DE8467A522EEDA10774CC7043F52D7B61B652109DE22209C1B80D0744FCB2A35C51F335962FADBFF52C94A60AF826795356C16F0DB7F995CF68B7EF7D367B5F96B76FC8E477809406FC97DF810B3";
		//		BigInteger H = new BigInteger("7D1699071DDEAAA15413C703346B36D8",16);
		//		String w = pair(ra,rb);
		//		BigInteger SSV = H.xor(HTIR.htir(w, tn));
		//		setSomeText("SSV = "+SSV.toString(16)+"\n");
		//		
		//		BigInteger r = HTIR.htir(SSV.toString(16)+b, q);
		//		
		//		setSomeText("TEST: " + ((("04"+ra+rb).contentEquals(muladdmul(r.toString(16))))? "PASSED":"FAILED") );
	}

	public void setSomeText(String in){
		TextView tv = ((TextView)findViewById(R.id.tv));
		tv.append(in);
	}

	public native String pair(byte[] R, String K);
	public native String conv(byte[] hash);
	public native String conv2(byte[] hash);
	public native byte[] muladdmul(String arg,String b,String Z);
	public native boolean eccsiverify(byte[] KPAKin, byte[] IDin, byte[] signaturein, byte[] sakkepayloadin);


	static {
		System.loadLibrary("crypto");
		System.loadLibrary("mygmp");
	}

	class MyThread extends AsyncTask<Void,byte[],Void>{
		private BluetoothSocket sock;
		private InputStream instream;
		private BobBlue self;

		public MyThread(BluetoothSocket socket,BobBlue inself) throws IOException{
			super();

			self=inself;
			sock=socket;
			instream = sock.getInputStream();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			byte[] bytes = new byte[402];

			//			while(go){
			try{
				instream.read(bytes);
				publishProgress(bytes);
			}
			catch(IOException ioe){}//break;}
			//			}

			return null;
		}

		protected void onProgressUpdate(byte[]... progress) {
			super.onProgressUpdate(progress);
			long stime = System.nanoTime();
			long time = stime;
			long time1 = time;

			byte[] payload = new byte[273];
			for(int i=0;i<273;i++) payload[i]=progress[0][i];

			byte[] sig = new byte[129];
			for(int i=273;i<402;i++) sig[i-273]=progress[0][i];

			// ECCSI verification input variables
			byte[] ID = {(byte)0x32,(byte)0x30,(byte)0x31,(byte)0x31,(byte)0x2d,(byte)0x30,(byte)0x32,(byte)0x00,(byte)0x74,(byte)0x65,(byte)0x6c,(byte)0x3a,(byte)0x2b,(byte)0x34,(byte)0x34,(byte)0x37,(byte)0x37,(byte)0x30,(byte)0x30,(byte)0x39,(byte)0x30,(byte)0x30,(byte)0x31,(byte)0x32,(byte)0x33,(byte)0x00};
			byte[] KPAK = {(byte)0x04,(byte)0x50,(byte)0xd4,(byte)0x67,(byte)0x0b,(byte)0xde,(byte)0x75,(byte)0x24,(byte)0x4f,(byte)0x28,(byte)0xd2,(byte)0x83,(byte)0x8a,(byte)0x0d,(byte)0x25,(byte)0x55,(byte)0x8a,(byte)0x7a,(byte)0x72,(byte)0x68,(byte)0x6d,(byte)0x45,(byte)0x22,(byte)0xd4,(byte)0xc8,(byte)0x27,(byte)0x3f,(byte)0xb6,(byte)0x44,(byte)0x2a,(byte)0xeb,(byte)0xfa,(byte)0x93,(byte)0xdb,(byte)0xdd,(byte)0x37,(byte)0x55,(byte)0x1a,(byte)0xfd,(byte)0x26,(byte)0x3b,(byte)0x5d,(byte)0xfd,(byte)0x61,(byte)0x7f,(byte)0x39,(byte)0x60,(byte)0xc6,(byte)0x5a,(byte)0x8c,(byte)0x29,(byte)0x88,(byte)0x50,(byte)0xff,(byte)0x99,(byte)0xf2,(byte)0x03,(byte)0x66,(byte)0xdc,(byte)0xe7,(byte)0xd4,(byte)0x36,(byte)0x72,(byte)0x17,(byte)0xf4};
			// SAKKE processing input variables
			String b = "323031312d30320074656c3a2b34343737303039303031323300";
			String Z = "045958EF1B1679BF099B3A030DF255AA6A23C1D8F143D4D23F753E69BD27A832F38CB4AD53DDEF4260B0FE8BB45C4C1FF510EFFE300367A37B61F701D914AEF09724825FA0707D61A6DFF4FBD7273566CDDE352A0B04B7C16A78309BE640697DE747613A5FC195E8B9F328852A579DB8F99B1D0034479EA9C5595F47C4B2F54FF21508D37514DCF7A8E143A6058C09A6BF2C9858CA37C258065AE6BF7532BC8B5B63383866E0753C5AC0E72709F8445F2E6178E065857E0EDA10F68206B63505ED87E534FB2831FF957FB7DC619DAE61301EEACC2FDA3680EA4999258A833CEA8FC67C6D19487FB449059F26CC8AAB655AB58B7CC796E24E9A394095754F5F8BAE";
			String K = "0493AF67E5007BA6E6A80DA793DA300FA4B52D0A74E25E6E7B2B3D6EE9D18A9B5C5023597BD82D8062D34019563BA1D25C0DC56B7B979D74AA50F29FBF11CC2C93F5DFCA615E609279F6175CEADB00B58C6BEE1E7A2A47C4F0C456F05259A6FA94A634A40DAE1DF593D4FECF688D5FC678BE7EFC6DF3D6835325B83B2C6E69036B155F0A27241094B04BFB0BDFAC6C670A65C325D39A069F03659D44CA27D3BE8DF311172B554160181CBE94A2A783320CED590BC42644702CF371271E496BF20F588B78A1BC01ECBB6559934BDD2FB65D2884318A33D1A42ADF5E33CC5800280B28356497F87135BAB9612A17260424409AC15FEE996B744C332151235DECB0F5";



			time = System.nanoTime();
			boolean verification = eccsiverify(KPAK, ID, sig, payload);
			time1 = System.nanoTime();
			setSomeText("Verified: " + verification);
			setSomeText(" in "+(time1-time)/1000000000.0+"s");


			// process the SAKKE data if the signature is verified
			long ptime = System.nanoTime();
			if(verification) {
				time=System.nanoTime();			

				byte[] R = new byte[257];
				for(int i=0;i<257;i++) R[i]=progress[0][i];

				byte[] H = new byte[16];
				for(int i=257;i<273;i++) H[i-257]=progress[0][i];

				String w = pair(R,K);
				setSomeText("\nPairing: "+(System.nanoTime()-time)/1000000000.0+"s");

				int n = 128;		
				String hx = conv(H);
				BigInteger BH = new BigInteger(hx,16);
				BigInteger SSV = BH.xor(HTIR.htir(w, BigInteger.ONE.add(BigInteger.ONE).pow(n)));

				time = System.nanoTime();

				BigInteger q = new BigInteger("265EAEC7C2958FF69971846636B4195E905B0338672D20986FA6B8D62CF8068BBD02AAC9F8BF03C6C8A1CC354C69672C39E46CE7FDF222864D5B49FD2999A9B4389B1921CC9AD335144AB173595A07386DABFD2A0C614AA0A9F3CF14870F026AA7E535ABD5A5C7C7FF38FA08E2615F6C203177C42B1EB3A1D99B601EBFAA17FB",16);
				BigInteger r = HTIR.htir(SSV.toString(16)+b, q);
				byte[] Rbs = muladdmul(r.toString(16),b,Z);
				boolean test = true;
				for(int i=0;i<257;i++) {
					test &=(Rbs[i]==R[i]);
				}

				setSomeText("\nTested: " + test);
				setSomeText(" in "+(System.nanoTime()-time)/1000000000.0+"s");
				time=System.nanoTime();

				setSomeText("\nSSV: "+SSV.toString(16));
				Toast.makeText(self, SSV.toString(16), Toast.LENGTH_LONG);
			}
			else {
				setSomeText("\nSAKKE data not processed because signature not verified.");
			}
			setSomeText("\nPackage processing: "+((System.nanoTime()-ptime)/1000000000.0+"s"));

			setSomeText("\nTotal: "+((System.nanoTime()-stime)/1000000000.0+"s" + "\n"));
		}
	}
}