//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_sign.cpp
// Item Description : Unit Tests for the Sign method.
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#include <mscrypto/eccsi.h>
#include <mscrypto/parameter-set.h>
#include <mscrypto/sakke.h>
#include <mskms/runtime-key-storage.inl>
#include <util/octet-string.h>
#include <util/bigint-ssl.h>

#include "key_data.h"

using namespace mikey_sakke_unit_test;
using namespace MikeySakkeCrypto;

namespace
{
	static std::string community = "testsign.test";

	static OctetString message("6d65737361676500");

	static OctetString badHS = OctetString::skipws
			("490F3FEBBC1C902F6289723D7F8CBF79"
             "DB88930849D19F38F0295B5C276C14D8");

	struct EphemeralFrom6507 // define a fixed 'random' function
	{
		static void fixed(uint8_t* p, size_t len)
		{
		    OctetString("34567").deposit_bigendian(p, len);
		}
	};

	struct ZeroFirstRandomNumberGenerator
	{
		static bool firstTry;
		static bool secondTry;

		static void generator(uint8_t* p, size_t len)
		{
			if (firstTry)
			{
				firstTry = false;
				secondTry = true;
				(as<OctetString>(bigint(0))).deposit_bigendian(p, len);
			}
			else if (secondTry)
			{
				secondTry = false;
				ECC::PrimeCurveJacobianPtr E = eccsi_6509_param_set().curve;
				(as<OctetString>(E->point_order())).deposit_bigendian(p, len);
			}
			else
			{
				OctetString("34567").deposit_bigendian(p, len);
			}
		}
	};

	bool ZeroFirstRandomNumberGenerator::firstTry = true;
	bool ZeroFirstRandomNumberGenerator::secondTry = false;

	static OctetString hS = OctetString::skipws
			("490F3FEBBC1C902F6289723D7F8CBF79"
             "DB88930849D19F38F0295B5C276C14D1");
}

BOOST_AUTO_TEST_CASE( testSignOK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicParameter(community, "SakkeSet", "1");
	keys->StorePublicKey(id, "HS", hS);

	OctetString signature = Sign(message.raw(), message.size(),
	         id, community, EphemeralFrom6507::fixed, keys);

	BOOST_CHECK( signature == getValidSignature() );
}

BOOST_AUTO_TEST_CASE( testSignIncorrectSSV )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "SSK", getInvalidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicParameter(community, "SakkeSet", "1");
	keys->StorePublicKey(id, "HS", hS);

	OctetString signature = Sign(message.raw(), message.size(),
	         id, community, EphemeralFrom6507::fixed, keys);

	BOOST_CHECK( ( signature == getValidSignature() ) == false );
}

BOOST_AUTO_TEST_CASE( testSignIncorrectPVT )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getInvalidPVT());
	keys->StorePublicParameter(community, "SakkeSet", "1");
	keys->StorePublicKey(id, "HS", hS);

	OctetString signature = Sign(message.raw(), message.size(),
	         id, community, EphemeralFrom6507::fixed, keys);

	BOOST_CHECK( ( signature == getValidSignature() ) == false );
}

BOOST_AUTO_TEST_CASE( testSignIncorrectSignatureLength )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getInvalidPVT());
	keys->StorePublicParameter(community, "SakkeSet", "1");
	keys->StorePublicKey(id, "HS", hS);

	OctetString signature = Sign(message.raw(), message.size(),
	         id, community, EphemeralFrom6507::fixed, keys);

	BOOST_CHECK( ( signature == getValidSignature() ) == false );
}

BOOST_AUTO_TEST_CASE( testignIncorrectHS )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicParameter(community, "SakkeSet", "1");
	keys->StorePublicKey(id, "HS", badHS);

	OctetString signature = Sign(message.raw(), message.size(),
	         id, community, EphemeralFrom6507::fixed, keys);

	BOOST_CHECK( ( signature == getValidSignature() ) == false );
}

BOOST_AUTO_TEST_CASE( testSignEmptyHS )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString signature = Sign(message.raw(), message.size(),
	         id, community, EphemeralFrom6507::fixed, keys);

	BOOST_CHECK( ( signature == getValidSignature() ) == false );

	OctetString emptyHS("");

	keys->StorePublicKey(id, "HS", emptyHS);

	BOOST_CHECK( ( signature == getValidSignature() ) == false );
}

BOOST_AUTO_TEST_CASE( testSignRandomNumberZero )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);
	keys->StorePrivateKey(id, "SSK", getValidSSK());
	keys->StorePublicKey(id, "PVT", getValidPVT());
	keys->StorePublicParameter(community, "SakkeSet", "1");
	keys->StorePublicKey(id, "HS", hS);

	OctetString signature = Sign(message.raw(), message.size(),
	         id, community, ZeroFirstRandomNumberGenerator::generator, keys);

	BOOST_CHECK( signature == getValidSignature() );
}

