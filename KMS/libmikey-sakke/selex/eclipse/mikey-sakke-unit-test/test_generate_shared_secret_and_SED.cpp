//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_generate_shared_secret_and_SED.cpp
// Item Description : Unit Tests for the GenerateSharedSecretAndSED method.
//
//******************************************************************************

#include <boost/test/unit_test.hpp>

#include <mscrypto/parameter-set.h>
#include <mscrypto/sakke.h>
#include <mskms/runtime-key-storage.inl>
#include <util/octet-string.h>

#include "key_data.h"

using namespace mikey_sakke_unit_test;
using namespace MikeySakkeCrypto;

namespace
{
	static std::string community = "testgeneratesharedsecretandSED.test";

	struct SSVFrom6508 // define a fixed 'random' function
	{
	   static void fixed(uint8_t* p, size_t len)
	   {
		  OctetString(OctetString::skipws
			 ("12345678 9ABCDEF0 12345678 9ABCDEF0")).deposit_bigendian(p,len);
	   }
	};

	static OctetString expectedSSV = OctetString::skipws(
				"12345678 9ABCDEF0 12345678 9ABCDEF0");
}

BOOST_AUTO_TEST_CASE( testGenerateSharedSecretAndSEDOK )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString encrypted;
	OctetString ssv = GenerateSharedSecretAndSED(
	            encrypted, id, community, SSVFrom6508::fixed, keys);

	BOOST_CHECK( expectedSSV == ssv );
	BOOST_CHECK( encrypted == getValidSED() );
}

BOOST_AUTO_TEST_CASE( testGenerateSharedSecretAndSEDIncorrectID )
{
	OctetString id = constructId("tel:+447700900125");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "Z", getValidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString encrypted;
	OctetString ssv = GenerateSharedSecretAndSED(
	            encrypted, id, community, SSVFrom6508::fixed, keys);

	BOOST_CHECK( ( expectedSSV == ssv ) );
	BOOST_CHECK( ( encrypted == getValidSED() ) == false );
}

BOOST_AUTO_TEST_CASE( testGenerateSharedSecretAndSEDIncorrectZ )
{
	OctetString id = constructId("tel:+447700900123");

	MikeySakkeKMS::KeyStoragePtr keys(new MikeySakkeKMS::RuntimeKeyStorage);

	keys->StorePublicKey(community, "Z", getInvalidZ());
	keys->StorePublicParameter(community, "SakkeSet", "1");

	OctetString encrypted;
	BOOST_CHECK_THROW( GenerateSharedSecretAndSED(
	            encrypted, id, community, SSVFrom6508::fixed, keys), std::exception );
}
