//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Unit Test
// Item Name        : test_bigint_ssl.cpp
// Item Description : Unit Tests for bigint_ssl.
//
//******************************************************************************


#include <boost/test/unit_test.hpp>

#define TEST_BIGINT_SSL

#include <util/bigint-ssl.h>
#include <gmp-impl.h>
#include <iostream>

namespace
{
    void* test_thread(void*)
	{
		BN_CTX* ctx = bigint_ssl_scratch::get();
		ctx = bigint_ssl_scratch::get();
		(void) ctx;
		return 0;
	}

    void checkBigInt(const bigint_ssl& value, const std::string& expectedValue)
    {
    	std::stringstream bnstream;
    	bnstream << value;
    	BOOST_CHECK( bnstream.str() == expectedValue );
    	BOOST_CHECK( as<OctetString>(value) == OctetString(expectedValue) );
    }
}

BOOST_AUTO_TEST_CASE( testBigIntSSL)
{
    mpz_t xxx;
    mpz_init(xxx);

    BN_CTX* ctx = bigint_ssl_scratch::get();
    ctx = bigint_ssl_scratch::get();
    (void) ctx;

    std::stringstream bytesPerLimbStream;
    bytesPerLimbStream << "BYTES_PER_MP_LIMB defined to be " << BYTES_PER_MP_LIMB;
    BOOST_TEST_MESSAGE( bytesPerLimbStream.str() );

    pthread_t t1; pthread_create(&t1, 0, test_thread, 0);
    pthread_t t2; pthread_create(&t2, 0, test_thread, 0);

    pthread_join(t1, 0);
    pthread_join(t2, 0);

    OctetString xs("1234567890abcdef123456");
    OctetString ys("f0123456789abcdef01234");
    OctetString zs("102468ACF09468ACE02468A");

    bigint x = as_bigint(xs);
    bigint y = as_bigint(ys);
    bigint z = x + y;

    std::stringstream zstream;
    zstream << z;
    BOOST_CHECK( zstream.str() == "312235987683590176774440586" );

    BOOST_CHECK( as<OctetString>(x) == xs);
    BOOST_CHECK( as<OctetString>(y) == ys);
    BOOST_CHECK( as<OctetString>(z) == zs);

    bigint_ssl xbn_froms = as_bigint_ssl(xs);
    checkBigInt(xbn_froms, "1234567890ABCDEF123456");

    bigint_ssl ybn_froms = as_bigint_ssl(ys);
    checkBigInt(ybn_froms, "F0123456789ABCDEF01234");

    bigint_ssl xbn_frombigint = as_bigint_ssl(x);
    checkBigInt(xbn_frombigint, "1234567890ABCDEF123456");

    bigint_ssl ybn_frombigint = as_bigint_ssl(y);
    checkBigInt(ybn_frombigint, "F0123456789ABCDEF01234");

    bigint_ssl zbn;
    BN_add(zbn, xbn_frombigint, ybn_frombigint);
    checkBigInt( zbn, "0102468ACF09468ACE02468A" ) ;

    bigint x2 = as_bigint(xbn_froms);
    BOOST_CHECK( as<OctetString>(x2) == xs );

    bigint y2 = as_bigint(ybn_froms);
    BOOST_CHECK( as<OctetString>(y2) == ys );

    bigint z2 = as_bigint(zbn);
    BOOST_CHECK( as<OctetString>(z2) == zs );
}
