//******************************************************************************
//
// System           : MikeySakke
// Item Type        : Header File
// Item Name        : std.h
// Item Description : Standard header file for the project
//
//******************************************************************************

#ifndef MSKMS_CONFIG_H
#define MSKMS_CONFIG_H

#include <string>

#if __ANDROID__
typedef unsigned long long uint64_t;
#endif

#if __cplusplus >= 201103L

#include <memory>
#include <functional>
#include <cstdint>

#else // XXX: workaround for non C++11 compilers that provide TR1

#include <tr1/memory>
#include <tr1/functional>
#include <stdint.h>
namespace std { using namespace tr1;  }

#endif

#endif//MSKMS_CLIENT_CONFIG_H

