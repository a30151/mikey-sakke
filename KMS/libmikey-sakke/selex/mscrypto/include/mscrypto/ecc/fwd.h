//******************************************************************************
//
// System           : MikeySakke 
// Item Type        : Header File
// Item Name        : fwd.h
// Item Description : Header file used to define prime curve types for the
//                    library.
//
//******************************************************************************

#ifndef MSCRYPTO_ECC_FWD_H
#define MSCRYPTO_ECC_FWD_H

#include <util/std.h>
#include <util/bigint.h>

class bigint_ssl;

namespace MikeySakkeCrypto {
namespace ECC {

template <typename BigInt> class PrimeCurve;
template <typename BigInt> class Point;

typedef PrimeCurve<bigint>     PrimeCurveAffine;
typedef PrimeCurve<bigint_ssl> PrimeCurveJacobian;

typedef std::shared_ptr<const PrimeCurveJacobian> PrimeCurveJacobianPtr;
typedef std::shared_ptr<const PrimeCurveAffine>   PrimeCurveAffinePtr;

}} // MikeySakkeCrypto::ECC

#endif//MSCRYPTO_ECC_FWD_H

