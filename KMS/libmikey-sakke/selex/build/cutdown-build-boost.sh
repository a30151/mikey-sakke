
cd "$BOOST"

bjam -v 2>/dev/null || (cd tools/build/v2/engine/ && ./build.sh && cp $(ls -t bin.*/bjam | head -1) ~/bin) || exit $?

echo "using gcc : : ${CROSS_PREFIX}g++ : <cxxflags>\"$CPPFLAGS $CXXFLAGS\" <linkflags>\"$LDFLAGS\" ;" > ${CROSS_PREFIX}boost-mskms.config

# XXX: current disable filesystem3 as it requires std::wstring which
# XXX: is not provided in android native libstdc++.  (ideally want
# XXX: --disable-filesystem2 here.)

# fix-up config on android
#
patch -f -p0 2>/dev/null <<'EOP'
--- boost/detail/endian.hpp
+++ boost/detail/endian.hpp
@@ -66,7 +66,8 @@
    || defined(_M_ALPHA) || defined(__amd64) \
    || defined(__amd64__) || defined(_M_AMD64) \
    || defined(__x86_64) || defined(__x86_64__) \
-   || defined(_M_X64) || defined(__bfin__)
+   || defined(_M_X64) || defined(__bfin__) \
+   || defined(__ANDROID__)
 
 # define BOOST_LITTLE_ENDIAN
 # define BOOST_BYTE_ORDER 1234
--- libs/filesystem/v2/src/v2_operations.cpp
+++ libs/filesystem/v2/src/v2_operations.cpp
@@ -58,13 +58,15 @@
 
 # else // BOOST_POSIX_API
 #   include <sys/types.h>
-#   if !defined(__APPLE__) && !defined(__OpenBSD__)
+#   if !defined(__APPLE__) && !defined(__OpenBSD__) && !defined(__ANDROID__) && !defined(ANDROID)
 #     include <sys/statvfs.h>
 #     define BOOST_STATVFS statvfs
 #     define BOOST_STATVFS_F_FRSIZE vfs.f_frsize
 #   else
 #ifdef __OpenBSD__
 #     include <sys/param.h>
+#elif defined(__ANDROID__) || defined(ANDROID)
+#     include <sys/vfs.h>
 #endif
 #     include <sys/mount.h>
 #     define BOOST_STATVFS statfs
EOP

bjam toolset=gcc link=static \
      --disable-filesystem3 \
      --with-filesystem \
      define=BOOST_FILESYSTEM_VERSION=2 \
      --with-system \
      --with-date_time \
      --build-dir=./${CROSS_PREFIX}build \
      --stagedir=./${CROSS_PREFIX}stage \
      --user-config=./${CROSS_PREFIX}boost-mskms.config \
      "$@"

