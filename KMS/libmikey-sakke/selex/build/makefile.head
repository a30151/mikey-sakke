##############################################################################
#
#	Multi-component parallel build system.
#
##############################################################################
#
#	INDIVIDUAL MAKEFILE OVERVIEW:
#	-------------------------------------
#
#	A simple client makefiles might look something like:
#		
#		include $(maketools)/makefile.head
#
#		program := program-name
#		sources := src/file1.s src/file2.c src/file3.cpp
#
#		include $(maketools)/makefile.tail
#
#	The above will produce a single program output composed of the
#	given files.  All output and dependency information is
#	auto-generated by the framework.  When only one target is specified
#	(as above) the plain 'sources' variable can be used to specify the
#	source files. A more complex makefile might look something like:
#
#		include $(maketools)/makefile.head
#
#		program := program-name
#		archive := archive-name
#		shared-object := shared-object-name
#
#		program.sources := src/file1.s
#		archive.sources := src/file2.c
#		shared-object.sources := src/file3.cpp
#
#		ASFLAGS += ...
#		CFLAGS += ...
#		CXXFLAGS += ...
#		LDLIBS += -lshared-object-name ...
#		shared-object.LDLIBS += -larchive-name ...
#		...
#
#		include $(maketools)/makefile.tail
#
#		$(call findobj,file2.c): OPTFLAGS = -O4
#
#	Here the 'target.sources' syntax is used to specify source files
#	since the makefile generates three outputs, a program, an archive
#	and a shared-object.  Also some build tool options are specified.
#	Note the use of findobj to override the optimization option used
#	for a building a specific output file.  For all built-in tool
#	options see the OPTIONS list below and their defaults.
#
#	When -llibrary-name is used as a linker option, the framework
#	determines whether that library (either archive or shared-object)
#	is a target of the makefile (or any other included makefile) and
#	generates the appropriate dependencies automatically.
#
#	Note: $(maketools) in the above example is simply a variable
#	expanding to the explicit path of the build system (i.e. it expands
#	to the directory containing makefile.head and makefile.tail)
#
#
#	MULTIPLE MAKEFILES: THE REAL BENEFIT!
#	-------------------------------------
#
#	Each makefile written using this framework is well-formed in its
#	own right.	However the real benefit of the framework is that many
#	makefiles can be combined into one makefile via basic make
#	inclusion.	In this usage mode, make is aware of all inputs,
#	dependencies and outputs for a number of components.	With this
#	information it can maximize use of the available processors in
#	order to build independent units in parallel.  For instance, a long
#	compile or heavy final link in one component could be occurring on
#	one processor whilst other non-dependent compilations are
#	distributed across other processors.
#
#	Achieving this massive concurrency in the build is easy from the
#	client's point-of-view.  The client can either write or
#	auto-generate a top-level makefile which simply includes each
#	component's makefile with a basic include line.  For example:
#
#		include proj1.mk
#		include proj2.mk
#		include proj3.mk
#		include proj4.mk
#		include another/makefile
#
#	Order is not important as the framework will allow make to
#	determine the build order at the individual target level.
#
#	No more waiting for a huge program to link or heavy meta-program to
#	compile before going on to build subsequent components.	Each
#	independent target will be paralleled up as much as mathematically
#	possible limited only by the number of processors available for
#	use.
#
#
#	INSTALLATION: USING THE FRAMEWORK
#	-------------------------------------
#	
#	Simply copy makefile.head and makefile.tail to somewhere in your
#	tree that you can reference via an absolute path from you build
#	environment.  That's it.  They are the only two files required.
#	Well except that you'll need a recent version of GNU make.
#	GNU Make 3.82 or above should do.
#
#
#	ADDITIONAL CONFIGURATION:
#	-------------------------------------
#
#	Specify the following as arguments to make to affect the output.
#
#	 VERBOSE=1			 Echo each command rather than just a
#							 short banner for each target.
#
#	 ALLOW_REDEF=1		 Show make diagnostics when an output is
#							 redefined (possibly by multiple inclusion of the
#							 same [sub-]makefile).
#
#	 SINGLE_JOB=1		 Format output as if running in single job mode.
#							 I.e. don't include component and process id on
#							 each output line.
#
#	 outdir=some/path  Set the output directory to be 'some/path'.
#							 The output directory will contain executables in
#							 bin, libraries (archives and shared-objects) in
#							 lib and temporaries (intermediate objects and
#							 dependency files) in tmp.  Each sub-path is
#							 configurable via the bindir, libdir and tmpdir
#							 variables.
#								 If outdir is not specified and a variable
#							 OUTPUT exists, outdir will be set to
#							 $OUTPUT/release.
#								 If outdir is not specified and no variable
#							 OUTPUT exists, outdir will be set to a path of
#							 the form:
#								 output-gcc-<GCCVER>-<GCCTARGET>
#							 where <GCCVER> is the compiler version and
#									 <GCCTARGET> is the compiler's target system
#							 an example could be:
#								 output-gcc-4.6.0-i686-pc-linux-gnu
#							 or
#								 output-gcc-4.3.4-arm-xscale-linux-gnueabi
#
#	 HARD_DEPS=1		 Mostly for debugging the framework rather than
#							 an end-user option.  Requires that all
#							 dependency files are loadable.	Will break
#							 initial builds where dependency files have not
#							 yet been created.
#
#	 TRACE=1				 Trace selected important expansions and
#							 dependencies.  Used for debugging makefiles.
#
##############################################################################
#
# TODO: Document client utilities... For now just skip down to the
# utilities section.
#
##############################################################################

# FIXME: TODO: Support nested inclusion.
# Currently do sanity check to make sure this has not occurred as
# behaviour is undefined.
nest := $(nest)x
ifneq "$(findstring xx,$(nest))" ""
$(error \
	This system currently does not support nested inclusion, \
	include multiple files in series of head-tail pairs)
endif

# directory of the including makefile
#
makedir := $(dir $(lastword $(filter-out %~~,$(MAKEFILE_LIST)~~)))
makedir := $(patsubst %/,%,$(makedir))

ifndef MULTI_MAKE_PROLOGUE
MULTI_MAKE_PROLOGUE := 1

.SECONDEXPANSION:
.ONESHELL:
##############################################################################

# build tools
#   c-preprocessor
#   c-compiler
#   c++-compiler
#   assembler
#   archiver
#   linker
#   symbol stripper
CPP=$(CROSS_PREFIX)cpp
CC=$(CROSS_PREFIX)gcc
CXX=$(CROSS_PREFIX)g++
AS=$(CROSS_PREFIX)as
AR=$(RM) $@ && $(CROSS_PREFIX)ar
LD=$(CROSS_PREFIX)ld
STRIP=$(CROSS_PREFIX)strip

##############################################################################

# build tool options
#   c-preprocessor flags
#   c-compiler flags
#   c++-compiler flags
#   assembler flags
#   linker flags
OPTIONS := CPPFLAGS CFLAGS CXXFLAGS ARFLAGS ASFLAGS LDFLAGS LDLIBS

# record input environment (or command-line overrides) in ENV.<OPT>
$(foreach o,$(OPTIONS),$(eval ENV.$o := $$($o)))

CPPFLAGS=$(ENV.CPPFLAGS) $(THREADFLAGS)
CFLAGS=$(ENV.CFLAGS) $(STD.c) $(WARN) $(WARN.c) $(DEBUGFLAGS) $(OPTFLAGS) $(PIPE) $(POST_CFLAGS)
CXXFLAGS=$(ENV.CXXFLAGS) $(STD.cpp) $(WARN) $(WARN.cpp) $(DEBUGFLAGS) $(OPTFLAGS) $(PIPE) $(POST_CXXFLAGS)
ARFLAGS=scr
ASFLAGS=$(ENV.ASFLAGS)
LDFLAGS=$(ENV.LDFLAGS)
LDLIBS=$(ENV.LDLIBS)

# Build optimized without debug assertion code
# but including symbol information for gdb.
#
# Optimization can be configured via OPTFLAGS
# Debug information and runtime assertion code
# can be configured via DEBUGFLAGS.
#
THREADFLAGS=$(if $(msw),-mthreads,-pthread)
STD.c=-std=c99
STD.cpp=$(CXX_CPLUSPLUS0X)
WARN=-W -Wall -Wextra $(PEDANTIC)
WARN.c=
WARN.cpp=-Wno-long-long
PEDANTIC=
OPTFLAGS=-O3
DEBUGFLAGS=-DNDEBUG -ggdb
SOLDFLAGS=-Bshareable -Bsymbolic
PIPE=-pipe

##############################################################################

ifneq "$(TARGET_SYSTEM)" ""
CROSS := $(TARGET_SYSTEM)
OPTFLAGS=-O2
endif

# architecture and machine options
#    target options for high-level languages (C,C++,...)
#    target options for assembly
TARGET_ARCH=
TARGET_MACH=$(TARGET_ARCH)

# tool prefix if cross-compiling
CROSS_PREFIX:=$(if $(CROSS),$(CROSS)-,$(CROSS_PREFIX))

# portable redirection to nowhere - use $(>null) or $(2>null)
NIL:=$(if $(findstring Windows,$(OS)),NUL,/dev/null)
-:=>null
$-:=2>$(NIL) 1>$(NIL)
-:=2>null
$-:=2>$(NIL)
undefine -

##############################################################################

# imaginary targets
.PHONY: clean all 
all:
clean:
	rm -rf $(clean)
clean:=


##############################################################################
# Multi-process tool execution and reporting
#

# Tool: Execute the given command with suitable reporting.
#
# In normal mode the tool-name 'Tool' will be output followed by the
# message 'Message'.  The conventional specification of Message is $@,
# i.e. the current rule's target.  If Message is passed as empty then
# the command string will be used as the message in a similar way
# to verbose mode.
#
# In verbose mode, both 'Tool' and 'Message' are ignored and the
# command will be output as executed.
#
# If 'Command' contains commas or is multi-line consider wrapping the
# whole thing in parentheses -- this is acceptable to the shell and
# will prevent make considering embedded commas as argument
# terminators or considering new-lines as command terminators.
#
define Tool # (Tool, Message, Command)
   $(Tool.@)( \
	 $(call BANNER,$1,$(if $2,$2,$$($(call DumpLiterally,$3)))) \
	 $(call SHOWCMD,$3) $3 \
	) $(LINE_FILTER); \
	$(if $(LINE_FILTER),exit $${PIPESTATUS[0]}$${pipestatus[1]};)
endef

# set to blank on the command line to print the details of Tool
Tool.@:=@

# expansion within a shell will cause its argument
# to dumped literally to stdout.
DumpLiterally = cat <<'EndOfDumpLiterally';$(\n)$1$(\n)EndOfDumpLiterally$(\n)

ifeq "$(VERBOSE)" "1"
BANNER := echo "----------------------------------------------------------------------";
SHOWCMD = $(DumpLiterally)
else
BANNER = printf "  %-6s  %s\n" "$1" "$(if $2,$2,$@)";
SHOWCMD :=
endif

ifeq "$(SINGLE_JOB)" "1"
LINE_FILTER :=
else
LINE_FILTER := 2>&1 | (while IFS= read line; do \
	printf "[%5s] %s\n" "$$$$" "$$line"; done)
endif


##############################################################################

gcc.version = $(shell $(CROSS_PREFIX)gcc -dumpversion)
gcc.target = $(shell $(CROSS_PREFIX)gcc -v 2>&1 | grep Target: | awk '{print $$2}')

override msw := $(if $(findstring Windows,$(OS)),1)
override msw.cygwin := $(if $(findstring cygwin,$(OSTYPE)),1)
override msw.native := $(if $(findstring Win$(msw.cygwin)dows,$(OS)),1)

# output directories
ifdef OUTPUT
outdir := $(OUTPUT)/release
endif
ifdef outdir
outdir.spec := fixed
else
outdir.spec := per-component
endif
ifeq ($(outdir.spec),fixed)
docdir := $(outdir)/doc
incdir := $(outdir)/include
libdir := $(outdir)/lib
bindir := $(outdir)/bin
logdir := $(outdir)/log
tmpdir := $(outdir)/tmp
endif

##############################################################################
# utility functions

mkbin    = $(addprefix $(bindir)/,$1)
mklib    = $(addprefix $(libdir)/,$1)
mklib.a  = $(call mklib,$(addprefix lib,$(addsuffix .a,$1)))
ifneq ($(msw),1)
mkprog   = $(call mkbin,$1)
mklib.so = $(call mklib,$(addprefix lib,$(addsuffix .so,$1)))
undefine exe_suf
else
mkprog   = $(call mkbin,$(addsuffix .exe,$1))
mklib.so = $(call mklib,$(addsuffix .dll,$1))
exe_suf := .exe
endif

# resolve the given base-relative path to an absolute one if it ends
# up not being under basedir (e.g. if it is already absolute or
# contains sufficient '..' directory specifiers)
#
base.resolve = $(foreach x,$1,$(if $(filter /%,$x),$x,$(subst $(basedir.abs)/,,$(abspath $(basedir)/$x))))

# wildcard and abspath variants that support $(basedir) relative arguments
#
base.wildcard    = $(call base.resolve,$(wildcard $(addprefix $(basedir.abs)/,$1)))
base.abspath     = $(foreach x,$1,$(addprefix $(abspath $(basedir))/,$1))
base.abswildcard = $(wildcard $(addprefix $(abspath $(basedir))/,$1))

# expands to 1 on first invocation with a particular name and empty
# on subsequent invocations with the same name.
# the intention is to be used in an ifeq, e.g.
#
#   ifeq ($(call define-once,something),1)
#     do something once
#   else
#     do something on subsequent inclusions
#   endif
#   do something always
#
define-once = $(if $($1),,1$(eval $1:=1))

or = $(if $1,$1,$2)#   returns first arg if not empty, otherwise second arg.
ifx = $(if $1,$1$2)#   returns first arg followed by second arg if first arg not empty.

# allow for selected statements/expansions to be traced
ifeq "$(TRACE)" "1"
trace.eval = $(warning $1)$(eval $1)
trace.expand = $(warning $1)$1
trace = $(warning $1)
else
trace.eval = $(eval $1)
trace.expand = $1
trace :=
endif
debug.eval = $(warning $1)$(eval $1)
debug.expand = $(warning $1)$1

##############################################################################
# literal expansions
#
# the following contorted weirdness allows for inserting literals in
# embedded scripts, passing macro parameters or in single-line
# expansions.  Use $(\t) for tab, $(\n) for newline, $(\nt) for
# newline-tab, $(\0) for an empty string, $( ) for a space and $(,)
# for a comma.
\0:=#
\t:=$(\0)	#
define \n :=


endef
\nt:=$(\n)$(\t)
-:=$(\0) #
$-:=$-
undefine -
,:=,

#$(warning test the above hieroglyphics: t$( )e$(\n)s$(\0)t$(\t)t$(,)h$(\nt)is$(\t)tab)
#$(warning test the above hieroglyphics: t$ e$(\n)s$(\0)t$(\t)t$(,)h$(\nt)is$(\t)tab)


##############################################################################
# search paths for source files

delim := $(if $(msw.native),;,:)
toolname.so := $(if $(msw),LD.DLL,LD.SO)

# add library search paths for link dependencies
ifdef CROSS
vpath %.a $(libdir)$(delim)$(CROSS_LIBRARY_PATH)$(delim)$(CROSS_SYS_LIBDIR)
vpath %.so $(libdir)$(delim)$(CROSS_LIBRARY_PATH)$(delim)$(CROSS_SYS_LIBDIR)
else
vpath %.a $(libdir)$(delim)$(LIBRARY_PATH)
vpath %.so $(libdir)$(delim)$(LIBRARY_PATH)
endif
ifeq ($(msw),1)
vpath %.dll $(libdir)$(delim)$(LIBRARY_PATH)$(delim)$(PATH)
.LIBPATTERNS := lib%.a %.dll
endif

##############################################################################
# tool options initialization

# record initial (make-global) values for the above options flags to
# allow for them to be reset on each inclusion.
$(foreach OPT,$(OPTIONS), \
$(call trace.eval,$(OPT).global = $(value $(OPT))) \
)

# set options for a specific target $1 and its dependencies
# any variable named in the 'local' set is expanded immediately
#
SetTargetOptions = $(foreach OPT,$(OPTIONS),\
$(call trace.eval,$($1) : $(OPT) = $(call ExpandLocal,$($1.$(OPT)))))

# expand only the given variable $1 in the unexpanded string $2
# for example: given
#    x = a$(a)b c$(b)d e$(c)f
#    a = 1
#    b = 2
#    c = 3
# the expression
#    $(eval y = $(call ExpandOnly,b,$(value x)))
# yields
#    y = a$(a)b c2d e$(c)f
#
ExpandOnly = $(subst $$($1),$($1),$2)
ExpandLocal = $(eval override - = $(value 1))$(foreach x,$(local),$(eval override - = $(call ExpandOnly,$x,$(value -))))$(value -)


##############################################################################
# build rules

# prerequisite for creating target directory - specify as order-only
# prerequisite as in the .config target immediately below
mkdir := $$(@D)/.

# generate build config
ifeq ($(outdir.spec),fixed)
include $(dir $(lastword $(MAKEFILE_LIST)))/makefile.config
endif

#--------------------------
# directory creation
#--------------------------
.PRECIOUS: %/.
%/.:
	$(call Tool,MKDIR,$@,mkdir -p $@)

#--------------------------------------
# c objects and dependencies
#--------------------------------------
define CompileCC
	$(call Tool,CC,$@,\
	$(COMPILE.c) $< $(OUTPUT_OPTION) -MMD)
endef

#--------------------------------------
# c++ objects and dependencies
#--------------------------------------
define CompileCXX
	$(call Tool,CXX,$@,\
	$(COMPILE.cpp) $< $(OUTPUT_OPTION) -MMD)
endef

#--------------------------------------
# assembly objects and dependencies
#--------------------------------------
define CompileAS
	$(call Tool,AS,$@,(\
	depfile="$(@:.o=.d)"
	$(COMPILE.s) $< $(OUTPUT_OPTION) --MD $$depfile ||
  		(rc=$$?; rm $$depfile; exit $$rc;)
	sed -i "s|$@[ :]*|$@ $$depfile : |g" $$depfile;))
endef

# last-resort rule to workaround missing header issues
%.h %.hpp %.hxx %.hh %.H %.inl %.impl::
	@echo "Warning: Assuming stale dependency '$@'; file does not exist."

# include code to check whether all source is
# added to version control 
include $(dir $(lastword $(MAKEFILE_LIST)))/makefile.vcscheck

endif # MULTI_MAKE_PROLOGUE
##############################################################################

# base directory for source files
basedir := $(makedir)
basedir.abs = $(abspath $(basedir))

# any variables that should be force-expanded in SetTargetOptions so
# that references to them are not resolved late (i.e. when multiple
# inclusion has overwritten the value expected by earlier makefiles)
local := basedir

# in the case of a project-specific outdir generate the directory
# variables here and add them to the local list
ifeq ($(outdir.spec),per-component)
outdir := $(makedir)/output-gcc-$(gcc.version)-$(gcc.target)
outdir := $(patsubst ./%,%,$(outdir))
docdir := $(outdir)/doc
incdir := $(makedir)/include
libdir := $(outdir)/lib
bindir := $(outdir)/bin
logdir := $(outdir)/log
tmpdir := $(outdir)/tmp
local := $(local) docdir incdir libdir bindir logdir tmpdir
# generate build config
include $(dir $(lastword $(MAKEFILE_LIST)))/makefile.config
endif

# reset top-level entities on each inclusion
undefine component
undefine program
undefine archive
undefine shared-object
undefine prereq
undefine sources
undefine objects

# reset all options
$(foreach OPT,$(OPTIONS), \
$(eval override undefine $(OPT)) \
$(eval $(OPT) = $$($(OPT).global)) \
$(eval program.$(OPT) = $$(value $(OPT))) \
$(eval archive.$(OPT) = $$(value $(OPT))) \
$(eval shared-object.$(OPT) = $$(value $(OPT))) \
)

# add default pathopts
CPPFLAGS += -I$(incdir)
LDFLAGS += -L$(outdir)/lib
ifeq "$(CROSS)" "" # build lib path into image for development
program.LDFLAGS += -Wl$(,)-rpath$(,)$(abspath $(libdir))
else # if cross-building the specify a link-time path to locate transitive shared libraries
program.LDFLAGS += -Wl$(,)-rpath-link$(,)$(libdir)
endif

# reset prereq and sources sets
$(foreach target,program archive shared-object, \
$(foreach list,prereq sources, \
$(eval override undefine $(target).$(list)) \
))

# disable facilities that won't work until after
# tail has been included for this component
$(foreach fac,findobj mktmp, \
$(eval override $(fac) = $$(error Facility '$(fac)' may only be called after including makefile.tail)) \
)

# support tail auto-inclusion from user files
#
undefine tail-includes
tail-include = $(if $(at-tail),1,$(eval tail-includes += $1))
tail-include-self = $(call tail-include,$(lastword $(MAKEFILE_LIST)))

# validation functions 
# --------------------
# TODO: make this queue up requirements to a check target which is
# TODO: made a dependency of all targets.
requires-env = $(foreach x,$1,$(if $(findstring undefined,$(origin $x)),$(error Makefile requires variable '$x' to be defined)))

