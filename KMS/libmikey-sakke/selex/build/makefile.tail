
component ?= $(subst $( ),-,$(sort $(program) $(archive) $(shared-object)))
component := $(component)

basedir := $(basedir)

$(call trace,component = $(component))
$(call trace,basedir = $(basedir))

# strip user-provided paths, allows for cases where users have
# symlinked root paths or just want to simplify output directories
#
ifdef STRIP_PATHS
strip-paths = $(patsubst $(addsuffix %,$(STRIP_PATHS)),%,$1)
else
strip-paths = $1
endif

ifeq ($(msw.native),1)
override mktmp = $(addprefix $(tmpdir)/$(component)/,$(subst :,,$(call strip-paths,$(call base.resolve,$1))))
else
override mktmp = $(addprefix $(tmpdir)/$(component)/,$(call strip-paths,$(call base.resolve,$1)))
endif
override findobj = $(filter %$(addsuffix .o,$1),$(objects))

$(foreach target,program archive shared-object, \
$(foreach list,prereq sources, \
$(eval $(target).$(list) ?= $(value $(list))) \
))

################################################################
ifdef tail-includes
at-tail := 1
$(foreach f,$(tail-includes),$(call trace.eval,include $f))
undefine at-tail
undefine tail-includes
endif

################################################################
ifneq "$(archive)" ""
archive := $(call mklib.a,$(archive))
ifeq "$(call define-once$(ALLOW_REDEF),'$(archive)')" "1"
$(call SetTargetOptions,archive)
archive.objects := $(call mktmp,$(addsuffix .o,$(archive.sources)))
objects += $(archive.objects)
$(archive) : $(archive.objects) $(archive.prereq) | $(mkdir)
	$(call Tool,AR,$@,\
	$(AR) $(ARFLAGS) $@ $^)
all: $(archive)
clean += $(archive)
endif # wasn't already defined
endif # was specified as target by user

################################################################
ifneq "$(shared-object)" ""
shared-object := $(call mklib.so,$(shared-object))
ifeq "$(call define-once$(ALLOW_REDEF),'$(shared-object)')" "1"
shared-object.LINK ?= $(LINK.cpp)
$(shared-object).LINK := $(shared-object.LINK)
$(call SetTargetOptions,shared-object)
shared-object.objects := $(call mktmp,$(addsuffix .o,$(shared-object.sources)))
$(eval objects += $(shared-object.objects))
$(shared-object) : $(filter -l%,$(LDFLAGS) $(LDLIBS)) $($(shared-object).prereq) $(archive) | $(mkdir)
$(shared-object) : $(shared-object.objects)
	$(call Tool,$(toolname.so),$@,\
	$($@.LINK) -shared $(filter-out %.a %.so %.dll,$^) $(LOADLIBES) $(SOLDFLAGS) $(CROSS_LD_PATH_OPTS) $(LDFLAGS) $(LDLIBS) -o $@)
all: $(shared-object)
clean += $(shared-object)
endif # wasn't already defined
endif # was specified as target by user

################################################################
ifneq "$(program)" ""
program := $(call mkprog,$(program))
ifeq "$(call define-once$(ALLOW_REDEF),'$(program)')" "1"
program.LINK ?= $(LINK.cpp)
$(program).LINK := $(program.LINK)
$(call SetTargetOptions,program)
program.objects := $(call mktmp,$(addsuffix .o,$(program.sources)))
$(eval objects += $(program.objects))
$(program) : $(filter -l%,$(LDFLAGS) $(LDLIBS)) $($(program).prereq) $(archive) $(shared-object) | $(mkdir)
$(program) : $(program.objects)
	$(call Tool,LD,$@,\
	$($@.LINK) $(filter-out %.a %.so %.dll,$^) $(LOADLIBES) $(CROSS_LD_PATH_OPTS) $(LDFLAGS) $(LDLIBS) -o $@)
all: $(program)
clean += $(program)
endif # wasn't already defined
endif # was specified as target by user

################################################################
clean += $(objects) $(objects:.o=.d)

ifeq ($(HARD_DEPS),1)
include $(objects:.o=.d)
else
-include $(objects:.o=.d)
endif

# Here the component variable needs to be expanded immediately in the
# recipe but everything else needs to be delayed as normal; hence it
# is nastily wrapped in an eval.
#
$(call trace.eval,$(call mktmp,%.c.o):	$(basedir.abs)/%.c 	| $$$(mkdir); $$(CompileCC))
$(call trace.eval,$(call mktmp,%.cpp.o):	$(basedir.abs)/%.cpp	| $$$(mkdir); $$(CompileCXX))
$(call trace.eval,$(call mktmp,%.s.o):	$(basedir.abs)/%.s 	| $$$(mkdir); $$(CompileAS))

# Since the basedir is specified explicitly above, absolute path specs
# no longer match -- this solves that issue whilst remaining in the
# basedir directory when building.
#
# Unfortunately, special handling is required for native absolute
# paths in windows as drive-specs cannot appear as directory names --
# this is partly solved by removing ':' in mktmp but the resulting
# path needs to be correctly matched here.
#
define GenAbsoluteRules
$(call trace.eval,$(call mktmp,$1%.c.o):	$2%.c 	| $$$(mkdir); $$(CompileCC))
$(call trace.eval,$(call mktmp,$1%.cpp.o):	$2%.cpp	| $$$(mkdir); $$(CompileCXX))
$(call trace.eval,$(call mktmp,$1%.s.o):	$2%.s 	| $$$(mkdir); $$(CompileAS))
endef

# for non-windows or non-native-mode windows, just do it naturally
ifneq ($(msw.native),1)
$(eval $(GenAbsoluteRules))
else
# produce a list of drives used by absolute source files
# and expand GenAbsoluteRules using D/ and D:/ as values
# for 1 and 2 for each drive D in $(drives).
drives := $(subst :,,$(sort $(filter %:,$(subst :,: ,$(sources) $(program.sources) $(archive.sources) $(shared-object.sources)))))
$(foreach d,$(drives),$(eval 1=$d/)$(eval 2=$d:/)$(eval $(GenAbsoluteRules)))
$(foreach v,drives 1 2,$(eval undefine $v))
endif

ifdef STRIP_PATHS
$(foreach p,$(STRIP_PATHS),$(eval 2=$p/)$(eval $(GenAbsoluteRules)))
undefine 2
endif

nest := $(patsubst %x,%,$(nest))

all-sources := $(all-sources) $(call base.abspath,$(sources) $(program.sources) $(archive.sources) $(shared-object.sources))
all-deps := $(all-deps) $(objects:.o=.d)

